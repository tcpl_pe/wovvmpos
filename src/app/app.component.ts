import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, MenuController, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SyncProvider } from '../providers/sync/sync';
import { UserData } from '../providers/user-data';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { DynamicmenuPage } from '../pages/dynamicmenu/dynamicmenu';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  public username: any;
  @ViewChild(Nav) nav: Nav;
  rootPage: any = '';

  constructor(public platform: Platform, public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public sync: SyncProvider, public events: Events, 
    public userData: UserData, public http: Http,
    public menu: MenuController, public storage: Storage,
    public toastCtrl: ToastController) {
    this.initializeApp();
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser!=null && currentUser!=undefined)
    {
      this.username = currentUser.party_Name;
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.GetConfig();
      this.sync.GetLocalData();
      this.Appload();
    });
  }

  Appload() {
    this.storage.get('IsActivated').then(
      (IsActivated) => {
        if (IsActivated) {
          this.rootPage = 'LoginPage';
        }
        else {
          this.rootPage = 'DemoActivationPage';
        }
      }
    );
  }

  GetConfig() {
    this.http.get('assets/config.json').map(res => res.json()).subscribe(data => {
      var config = data[0];
      this.sync.demoURL = config.demoURL;
      this.sync.demoDomain = config.demoDomain;
      this.sync.demoRegMacId = config.demoRegMacId;
      this.sync.demoVertical = config.demoVertical;
      this.sync.URL = config.URL;
      this.sync.Domain = config.Domain;
      this.sync.OrderId = config.OrderId;
    });
  }

  logout() {
    localStorage.setItem('isLoggedIn', 'no');
    localStorage.removeItem('UserID');
    localStorage.removeItem('onlineToken');
    //this.viewCtrl.dismiss();
    let toast = this.toastCtrl.create({
      message: 'Logout Sucessfully!',
      duration: 3000,
      position:'top'
    });
    toast.present();
    this.nav.setRoot('LoginPage');
  }
  openPage(pageName)
  {
    if (pageName == 'dashboard') this.nav.setRoot(DynamicmenuPage);
  }
  // GetConfig() {
  //   var config = {
  //     "demoURL": "https://10.10.10.227:8443",
  //     "demoDomain": "jawalaundry",
  //     "demoRegMacId": "aa-bb-4B-C8-E9-dd",
  //     "demoVertical": "Laundry1",
  //     "URL": "https://10.10.10.227:8443",
  //     "Domain": "",
  //     "OrderId": ""
  //   };
  //   this.sync.demoURL = config.demoURL;
  //   this.sync.demoDomain = config.demoDomain;
  //   this.sync.demoRegMacId = config.demoRegMacId;
  //   this.sync.demoVertical = config.demoVertical;
  //   this.sync.URL = config.URL;
  //   this.sync.Domain = config.Domain;
  //   this.sync.OrderId = config.OrderId;
  // }
}
