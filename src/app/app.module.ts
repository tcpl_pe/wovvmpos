import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicSwipeAllModule } from 'ionic-swipe-all';
import { MyApp } from './app.component';
import { SalesPage } from '../pages/home/home';
import { PaymentPage } from '../pages/payment/payment';
import { CartPage } from '../pages/cart/cart';
import { LongPressModule } from 'ionic-long-press';
import { EditPage } from '../pages/edit/edit';
import { BarcodePage } from '../pages/barcode/barcode';
import { CustomerPage } from '../pages/customer/customer';
import { SyncProvider } from '../providers/sync/sync';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { UserData } from '../providers/user-data';
import { IonicStorageModule } from '@ionic/storage';
import { DbProvider } from '../providers/db/db';
import { Device } from '@ionic-native/device';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { LoadingProvider } from '../providers/loading/loading';
import { LoginserviceProvider } from '../providers/loginservice/loginservice';
import { MenuserviceProvider } from '../providers/menuservice/menuservice';
import { MycartProvider } from '../providers/mycart/mycart';
import { AutoCompleteModule } from 'ionic2-auto-complete';
import { Printer } from '@ionic-native/printer';
import { OrderdetailProvider } from '../providers/orderdetail/orderdetail';
import 'hammerjs';
import { mSales } from '../pages/salesinvoice/salesinvoice';
@NgModule({
  declarations: [
    MyApp,SalesPage,PaymentPage,CartPage,EditPage,BarcodePage,CustomerPage,mSales
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    AutoCompleteModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['sqlite', 'websql', 'indexeddb']
    }),
    LongPressModule,
    IonicSwipeAllModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,SalesPage,PaymentPage,CartPage,EditPage,BarcodePage,CustomerPage,mSales
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SyncProvider,
    UserData,
    MenuserviceProvider,
    File,
    LoginserviceProvider,
    FileTransfer,
    InAppBrowser,
    DbProvider,
    Device,
    MycartProvider,
    LoadingProvider,
    Printer,
    OrderdetailProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
