var DemoApp = angular.module('DemoApp', ['dx']);

DemoApp.controller('DemoController', function DemoController($scope) {
    var applyFilterTypes = [{
        key: "auto",
        name: "Immediately"
    }, 
	{
        key: "onClick",
        name: "On Button Click"
    }];
    
   
    $scope.gridOptions = {
        dataSource: orders,
        bindingOptions: {
            filterRow: "filterRow",
            headerFilter: "headerFilter"
        },
		paging: {
            enabled: false
        },
		editing: {
            mode: "row",
            allowUpdating: true,
            allowDeleting: true
			
        }, 
        
        columns: [{
            caption: "Additional Charge",
            dataField: "CustomerStoreState"
        },{
            dataField: "SaleAmount",
            alignment: "right",
            format: "currency",
            headerFilter: {
                dataSource: [ {
                    text: "Less than $3000",
                    value: ["SaleAmount", "<", 3000]
                }, {
                    
                    text: "$3000 - $5000",
                    value: [["SaleAmount", ">=", 3000], ["SaleAmount", "<", 5000]]
                }, {
                    
                    text: "$5000 - $10000",
                    value: [["SaleAmount", ">=", 5000], ["SaleAmount", "<", 10000]]
                }, {
                    
                    text: "$10000 - $20000",
                    value: [["SaleAmount", ">=", 10000], ["SaleAmount", "<", 20000]]
                }, {
                    
                    text: "Greater than $20000",
                    value: ["SaleAmount", ">=", 20000]
                }],
								
				
            }
			
			
        }],
		  onContentReady: function(e){
            moveEditColumnToLeft(e.component);
        },
		 onCellPrepared: function(e) {
            if(e.rowType === "data" && e.column.command === "edit") {
                var isEditing = e.row.isEditing,
                    $links = e.cellElement.find(".dx-link");
    
                $links.text("");
    
                if(isEditing){
                    $links.filter(".dx-link-save").addClass("dx-icon-save");
                    $links.filter(".dx-link-cancel").addClass("dx-icon-revert");
                } else {
                    $links.filter(".dx-link-edit").addClass("dx-icon-edit");
                    $links.filter(".dx-link-delete").addClass("dx-icon-trash");
                }
            }
        },
		
		summary: {
            totalItems: [ {
                column: "SaleAmount",
                summaryType: "sum",
                valueFormat: "currency"
            }]
        }
    };
    
    
    
});


