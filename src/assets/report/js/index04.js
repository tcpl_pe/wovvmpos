var DemoApp = angular.module('DemoApp', ['dx']);

DemoApp.controller('DemoController', function DemoController($scope) {
    var applyFilterTypes = [{
        key: "auto",
        name: "Immediately"
    }, 
	{
        key: "onClick",
        name: "On Button Click"
    }];
	
	/////////
	$scope.gridOptions02 = {
        dataSource: orders,
		
        bindingOptions: {
            filterRow: "filterRow",
            headerFilter: "headerFilter"
        },
		paging: {
            enabled: false
        },
		  selection: {
            mode: "multiple"
        },
		
	
		 
		
        
        columns: [{
            	dataField: "OrderNumber",
				caption: "Doc.No"
        },
		{
		   		dataField: "OrderDate",
				caption: "Date",
                dataType: "date",
        },

		{
		   caption: "Party",
            dataField: "Employee"
        },
		
		{
		   caption: "Status",
		   alignment: "center",
           dataField: "Status"
        },
		
		{
            dataField: "SaleAmount",
            alignment: "right",
            format: "currency",
            headerFilter: {
                dataSource: [ {
                    text: "Less than $3000",
                    value: ["SaleAmount", "<", 3000]
                }, {
                    
                    text: "$3000 - $5000",
                    value: [["SaleAmount", ">=", 3000], ["SaleAmount", "<", 5000]]
                }, {
                    
                    text: "$5000 - $10000",
                    value: [["SaleAmount", ">=", 5000], ["SaleAmount", "<", 10000]]
                }, {
                    
                    text: "$10000 - $20000",
                    value: [["SaleAmount", ">=", 10000], ["SaleAmount", "<", 20000]]
                }, {
                    
                    text: "Greater than $20000",
                    value: ["SaleAmount", ">=", 20000]
                }],
								
				
            }
			
			
        }],
		  onContentReady: function(e){
            moveEditColumnToLeft(e.component);
        },
		 onCellPrepared: function(e) {
            if(e.rowType === "data" && e.column.command === "edit") {
                var isEditing = e.row.isEditing,
                    $links = e.cellElement.find(".dx-link");
    
                $links.text("");
    
                if(isEditing){
                    $links.filter(".dx-link-save").addClass("dx-icon-save");
                    $links.filter(".dx-link-cancel").addClass("dx-icon-revert");
                } else {
                    $links.filter(".dx-link-edit").addClass("dx-icon-edit");
                    $links.filter(".dx-link-delete").addClass("dx-icon-trash");
                }
            }
        },
		
		summary: {
            totalItems: [ {
                column: "SaleAmount",
                summaryType: "sum",
                valueFormat: "currency"
            }]
        }
		
		
		
    };
	
	////////
	
$scope.gridOptions03 = {
        dataSource: orders,
		
        bindingOptions: {
            filterRow: "filterRow",
            headerFilter: "headerFilter"
        },
		paging: {
            enabled: false
        },
		  selection: {
            mode: "multiple"
        },
		 
		
        
        columns: [{
            	dataField: "OrderNumber",
                
				caption: "Doc.No"
        },
		{
		   		dataField: "OrderDate",
               
				caption: "Date",
                dataType: "date",
        },

		{
		   caption: "Party",
		    
            dataField: "Employee"
        },
		
		{
		   caption: "Status",
		  
		   alignment: "center",
           dataField: "Status"
        },
		
		{
            dataField: "SaleAmount",
            alignment: "right",
            format: "currency",
            headerFilter: {
                dataSource: [ {
                    text: "Less than $3000",
                    value: ["SaleAmount", "<", 3000]
                }, {
                    
                    text: "$3000 - $5000",
                    value: [["SaleAmount", ">=", 3000], ["SaleAmount", "<", 5000]]
                }, {
                    
                    text: "$5000 - $10000",
                    value: [["SaleAmount", ">=", 5000], ["SaleAmount", "<", 10000]]
                }, {
                    
                    text: "$10000 - $20000",
                    value: [["SaleAmount", ">=", 10000], ["SaleAmount", "<", 20000]]
                }, {
                    
                    text: "Greater than $20000",
                    value: ["SaleAmount", ">=", 20000]
                }],
								
				
            }
			
			
        }],
		  onContentReady: function(e){
            moveEditColumnToLeft(e.component);
        },
		 onCellPrepared: function(e) {
            if(e.rowType === "data" && e.column.command === "edit") {
                var isEditing = e.row.isEditing,
                    $links = e.cellElement.find(".dx-link");
    
                $links.text("");
    
                if(isEditing){
                    $links.filter(".dx-link-save").addClass("dx-icon-save");
                    $links.filter(".dx-link-cancel").addClass("dx-icon-revert");
                } else {
                    $links.filter(".dx-link-edit").addClass("dx-icon-edit");
                    $links.filter(".dx-link-delete").addClass("dx-icon-trash");
                }
            }
        },
		
		summary: {
            totalItems: [ {
                column: "SaleAmount",
                summaryType: "sum",
                valueFormat: "currency"
            }]
        }
		
		
		
    };
	
	
	////////
	
	
	$scope.gridOptions04 = {
        dataSource: orders,
		
        bindingOptions: {
            filterRow: "filterRow",
            headerFilter: "headerFilter"
        },
		paging: {
            enabled: false
        },
		  
        
        columns: [{
            	dataField: "OrderNumber",
                alignment: "left",
				caption: "Counter"
        },
		{
		   		dataField: "Employee",
               
				caption: "User",
                
        },

		{
		   caption: "Op.Float Cash",
		    
            dataField: "OrderNumber"
        },
		
		{
		   caption: "Till Limit",
           dataField: "OrderNumber"
        },
		
		{
		   caption: "Start Time",
		   alignment: "center",
           dataField: "Status"
        },
		
		{
		   caption: "End Time",
		  
		   alignment: "center",
           dataField: "Status"
        }
		
		]
		
		
    };
	
	
	////////
	
	$scope.gridOptions05 = {
        dataSource: orders,
		
        bindingOptions: {
            filterRow: "filterRow",
            headerFilter: "headerFilter"
        },
		paging: {
            enabled: false
        },
		  
        
        columns: [
		{
		   		dataField: "OrderDate",
               
				caption: "Start",
                dataType: "date",
        },
		{
		   		dataField: "OrderDate",
               
				caption: "End",
                dataType: "date",
        },
		{
		   		dataField: "Employee",
               
				caption: "Started By"
                
        },
		
		{
		   		dataField: "Employee",
               
				caption: "Stopped By"
                
        }

		
		
		]
		  
		
    };
	
	///////////
    
	$scope.gridOptions06 = {
        dataSource: orders,
        bindingOptions: {
            filterRow: "filterRow",
            headerFilter: "headerFilter"
        },
		paging: {
            enabled: false
        },
		
        
        columns: [{
            caption: "",
            dataField: "CustomerStoreState"
        },{
            dataField: "SaleAmount",
			caption: "Actual",
            alignment: "right",
            format: "currency",
            headerFilter: {
                dataSource: [ {
                    text: "Less than $3000",
                    value: ["SaleAmount", "<", 3000]
                }, {
                    
                    text: "$3000 - $5000",
                    value: [["SaleAmount", ">=", 3000], ["SaleAmount", "<", 5000]]
                }, {
                    
                    text: "$5000 - $10000",
                    value: [["SaleAmount", ">=", 5000], ["SaleAmount", "<", 10000]]
                }, {
                    
                    text: "$10000 - $20000",
                    value: [["SaleAmount", ">=", 10000], ["SaleAmount", "<", 20000]]
                }, {
                    
                    text: "Greater than $20000",
                    value: ["SaleAmount", ">=", 20000]
                }],
								
				
            }
			
			
        }],
		  onContentReady: function(e){
            moveEditColumnToLeft(e.component);
        },
		 onCellPrepared: function(e) {
            if(e.rowType === "data" && e.column.command === "edit") {
                var isEditing = e.row.isEditing,
                    $links = e.cellElement.find(".dx-link");
    
                $links.text("");
    
                if(isEditing){
                    $links.filter(".dx-link-save").addClass("dx-icon-save");
                    $links.filter(".dx-link-cancel").addClass("dx-icon-revert");
                } else {
                    $links.filter(".dx-link-edit").addClass("dx-icon-edit");
                    $links.filter(".dx-link-delete").addClass("dx-icon-trash");
                }
            }
        },
		
		summary: {
            totalItems: [ {
                column: "SaleAmount",
                summaryType: "sum",
                valueFormat: "currency"
            }]
        }
    };
	
    /////////////
	
	///////////
    
	$scope.gridOptions07 = {
        dataSource: orders,
        bindingOptions: {
            filterRow: "filterRow",
            headerFilter: "headerFilter"
        },
		paging: {
            enabled: false
        },
		
        
        columns: [{
            caption: "",
            dataField: "CustomerStoreState"
        },{
            dataField: "SaleAmount",
			caption: "Actual",
            alignment: "right",
            format: "currency",
            headerFilter: {
                dataSource: [ {
                    text: "Less than $3000",
                    value: ["SaleAmount", "<", 3000]
                }, {
                    
                    text: "$3000 - $5000",
                    value: [["SaleAmount", ">=", 3000], ["SaleAmount", "<", 5000]]
                }, {
                    
                    text: "$5000 - $10000",
                    value: [["SaleAmount", ">=", 5000], ["SaleAmount", "<", 10000]]
                }, {
                    
                    text: "$10000 - $20000",
                    value: [["SaleAmount", ">=", 10000], ["SaleAmount", "<", 20000]]
                }, {
                    
                    text: "Greater than $20000",
                    value: ["SaleAmount", ">=", 20000]
                }],
								
				
            }
			
			
        },
		
		
		{
            dataField: "SaleAmount",
			caption: "Expected",
            alignment: "right",
            format: "currency",
            headerFilter: {
                dataSource: [ {
                    text: "Less than $3000",
                    value: ["SaleAmount", "<", 3000]
                }, {
                    
                    text: "$3000 - $5000",
                    value: [["SaleAmount", ">=", 3000], ["SaleAmount", "<", 5000]]
                }, {
                    
                    text: "$5000 - $10000",
                    value: [["SaleAmount", ">=", 5000], ["SaleAmount", "<", 10000]]
                }, {
                    
                    text: "$10000 - $20000",
                    value: [["SaleAmount", ">=", 10000], ["SaleAmount", "<", 20000]]
                }, {
                    
                    text: "Greater than $20000",
                    value: ["SaleAmount", ">=", 20000]
                }],
								
				
            }
			
			
        }
		],
		  onContentReady: function(e){
            moveEditColumnToLeft(e.component);
        },
		 onCellPrepared: function(e) {
            if(e.rowType === "data" && e.column.command === "edit") {
                var isEditing = e.row.isEditing,
                    $links = e.cellElement.find(".dx-link");
    
                $links.text("");
    
                if(isEditing){
                    $links.filter(".dx-link-save").addClass("dx-icon-save");
                    $links.filter(".dx-link-cancel").addClass("dx-icon-revert");
                } else {
                    $links.filter(".dx-link-edit").addClass("dx-icon-edit");
                    $links.filter(".dx-link-delete").addClass("dx-icon-trash");
                }
            }
        },
		
		summary: {
            totalItems: [ {
                column: "SaleAmount",
                summaryType: "sum",
                valueFormat: "currency"
            }]
        }
    };
	
	//////////
	
	$scope.gridOptions08 = {
        dataSource: orders,
        bindingOptions: {
            filterRow: "filterRow",
            headerFilter: "headerFilter"
        },
		paging: {
            enabled: false
        },
		
		
        
        columns: [{
            caption: "Branch Name",
			width: 240,
			 fixed: true,
            dataField: "CustomerStoreCity",
			
        
        },
		{
            caption: "Register Name",
			width: 240,
            dataField: "CustomerStoreState"
        },
		{
            caption: "Register Name",
			width: 240,
            dataField: "CustomerStoreState"
        },
		{
            caption: "Register Name",
			width: 240,
            dataField: "CustomerStoreState"
        }
		],
		
		
    };
	
	
	////////////////
	
	
	$scope.gridOptions09 = {
        dataSource: orders,
		
        bindingOptions: {
            filterRow: "filterRow",
            headerFilter: "headerFilter"
        },
		paging: {
            enabled: false
        },
		  selection: {
            mode: "multiple"
        },
		 
		
        
        columns: [{
            	dataField: "OrderNumber",
                
				caption: "Doc.No"
        },
		{
		   		dataField: "OrderDate",
               
				caption: "Date",
                dataType: "date",
        },

		{
		   caption: "Party",
		    
            dataField: "Employee"
        },
		
		{
		   caption: "Status",
		  
		   alignment: "center",
           dataField: "Status"
        },
		
		{
            dataField: "SaleAmount",
            alignment: "right",
            format: "currency",
            headerFilter: {
                dataSource: [ {
                    text: "Less than $3000",
                    value: ["SaleAmount", "<", 3000]
                }, {
                    
                    text: "$3000 - $5000",
                    value: [["SaleAmount", ">=", 3000], ["SaleAmount", "<", 5000]]
                }, {
                    
                    text: "$5000 - $10000",
                    value: [["SaleAmount", ">=", 5000], ["SaleAmount", "<", 10000]]
                }, {
                    
                    text: "$10000 - $20000",
                    value: [["SaleAmount", ">=", 10000], ["SaleAmount", "<", 20000]]
                }, {
                    
                    text: "Greater than $20000",
                    value: ["SaleAmount", ">=", 20000]
                }],
								
				
            }
			
			
        }],
		  onContentReady: function(e){
            moveEditColumnToLeft(e.component);
        }
		
		
		
		
    };
	
	
	////////
    
});

 


