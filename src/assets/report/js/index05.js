var DemoApp = angular.module('DemoApp', ['dx']);

DemoApp.controller('DemoController', function DemoController($scope) {
	
	  var applyFilterTypes = [{
        key: "auto",
        name: "Immediately"
    }, 
	{
        key: "onClick",
        name: "On Button Click"
    }];
	
    $scope.gridOptions = {
        dataSource: {
            store: {
                type: "array",
                key: "ID",
                data: employees
            }
        },
		
        columns: [{
            	dataField: "OrderNumber",
				caption: "Doc.No"
        },
		{
		   		dataField: "OrderDate",
				caption: "Date",
                dataType: "date",
        },{
		   caption: "Party",
            dataField: "Employee"
        },
		
		{
		   caption: "Status",
		   alignment: "center",
           dataField: "Status"
        },
		{
            dataField: "SaleAmount",
            alignment: "right",
            format: "currency",
            headerFilter: {
                dataSource: [ {
                    text: "Less than $3000",
                    value: ["SaleAmount", "<", 3000]
                }, {
                    
                    text: "$3000 - $5000",
                    value: [["SaleAmount", ">=", 3000], ["SaleAmount", "<", 5000]]
                }, {
                    
                    text: "$5000 - $10000",
                    value: [["SaleAmount", ">=", 5000], ["SaleAmount", "<", 10000]]
                }, {
                    
                    text: "$10000 - $20000",
                    value: [["SaleAmount", ">=", 10000], ["SaleAmount", "<", 20000]]
                }, {
                    
                    text: "Greater than $20000",
                    value: ["SaleAmount", ">=", 20000]
                }],
								
				
            }
			
			
        }
        ],
        masterDetail: {
            enabled: true,
            template: "detail"
        }
    };
    
    $scope.completedValue = function(rowData) {
        return rowData.Status == "Completed";
    };
    
});