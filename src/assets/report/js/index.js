
/*----wovv ipos version 1.5.1-----*/

var DemoApp = angular.module('DemoApp', ['dx', 'ngSanitize']);

DemoApp.controller('DemoController', function DemoController($scope) {
    var applyFilterTypes = [{
        key: "auto",
        name: "Immediately"
    }, 
	{
        key: "onClick",
        name: "On Button Click"
    }];
    
    function getOrderDay(rowData) {
        return (new Date(rowData.OrderDate)).getDay();
    }
	
    
    $scope.filterRow = {
        visible: true,
        applyFilter: "auto"
    };
	
	
	
    
    $scope.headerFilter = {
        visible: true
    };
    
    $scope.gridOptions = {
        dataSource: orders,
        bindingOptions: {
            filterRow: "filterRow",
            headerFilter: "headerFilter"
        },
        searchPanel: {
            visible: true,
            width: 240,
            placeholder: "Search..."
        },  
		
		export: {
            enabled: true,
            fileName: "Employees",
            allowExportSelectedData: true
        },
		
		
		allowColumnReordering: true,
        allowColumnResizing: true,
        columnAutoWidth: true,
		columnChooser: {
            enabled: true
        }, 
		
		 
        columns: [{
            dataField: "OrderNumber",
            width: 130,
            caption: "Invoice Number",
            headerFilter: {
                groupInterval: 10000
            }
        }, {
            dataField: "OrderDate",
            alignment: "right",
            dataType: "date",
            headerFilter: {
                dataSource: function(data) {
                    data.dataSource.postProcess = function(results) {
                        results.push({
                            text: "Weekends",
                            value: [[getOrderDay, "=", 0],
                              "or",
                            [getOrderDay, "=", 6]]
                        });
                        
                        return results;
                    };
                }
            }
        }, {
            dataField: "SaleAmount",
            alignment: "right",
            format: "currency",
            headerFilter: {
                dataSource: [ {
                    text: "Less than $3000",
                    value: ["SaleAmount", "<", 3000]
                }, {
                    
                    text: "$3000 - $5000",
                    value: [["SaleAmount", ">=", 3000], ["SaleAmount", "<", 5000]]
                }, {
                    
                    text: "$5000 - $10000",
                    value: [["SaleAmount", ">=", 5000], ["SaleAmount", "<", 10000]]
                }, {
                    
                    text: "$10000 - $20000",
                    value: [["SaleAmount", ">=", 10000], ["SaleAmount", "<", 20000]]
                }, {
                    
                    text: "Greater than $20000",
                    value: ["SaleAmount", ">=", 20000]
                }],
				
				
            }
        }, "Employee", {
            caption: "City",
            dataField: "CustomerStoreCity"
        }, {
            caption: "State",
            dataField: "CustomerStoreState"
        }],
		
		masterDetail: {
            enabled: true,
            template: "detail"
        }
    };
    
    $scope.filterTypesOptions = {
        items: applyFilterTypes,
        value: applyFilterTypes[0].key,
        valueExpr: "key",
        displayExpr: "name",
        bindingOptions: {
            value: "filterRow.applyFilter" 
        }
    };
    
    $scope.filterVisibleOptions = {
        text: "Filter Row",
        bindingOptions: {
            value: "filterRow.visible"
        },
        onValueChanged: function(data) {
            $("#gridContainer").dxDataGrid("instance").clearFilter();
            $(".apply-filter-option").css("display", data.value ? "block" : "none");
        }
    };
    
    $scope.headerFilterOptions = {
        text: "Header Filter",
        bindingOptions: {
            value: "headerFilter.visible"
        },
        onValueChanged: function() {
            $("#gridContainer").dxDataGrid("instance").clearFilter();
        }
    };
	
	
	
	
    
    
});