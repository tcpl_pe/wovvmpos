var DemoApp = angular.module('DemoApp', ['dx']);

DemoApp.controller('DemoController', function DemoController($scope) {
    var formInstance;

    $scope.formOptions = {
        formData: formData,
		colCount: 2,
        readOnly: false,
        showColonAfterLabel: true,
        showValidationSummary: true,
        validationGroup: "customerData",
        onInitialized: function(e) {
            formInstance = e.component;
        },
        items: [{
            itemType: "group",
            caption: "Personal Data",
            items: [{
                dataField: "Name of Store",
                validationRules: [{
                    type: "required",
                    message: "Name of Store is required"
                }, {
                    type: "pattern",
                    pattern: "^[^0-9]+$",
                    message: "Do not use digits in the Name"
                }]

            },
			
			{
                dataField: "Email ID",
                validationRules: [{
                    type: "required",
                    message: "Email ID is required"
                }]

            },
			
			
			 {
                dataField: "Financial Year begin date & Month",
                editorType: "dxDateBox",
                label: {
                    text: "Financial Year begin date & Month"
                },
                editorOptions: {
                    invalidDateMessage: "The date must have the following format: MM/dd/yyyy"
                },
                validationRules: [{
                    type: "required",
                    message: "Financial Year begin date & Month is required"
                }]
            },
			{
                dataField: "User Name",
                validationRules: [{
                    type: "required",
                    message: "User Name is required"
                }]

            },
			
			{
                dataField: "Password",
                validationRules: [{
                    type: "required",
                    message: "Password is required"
                }]

            },
			
			{
                dataField: "Currency",
                validationRules: [{
                    type: "required",
                    message: "Currency is required"
                }]

            }
			
			]
        }, {
            itemType: "group",
            caption: "Billing Address",
            items: [{
                dataField: "Country",
                editorType: "dxSelectBox",
                editorOptions: {
                    dataSource: countries
                },
                validationRules: [{
                    type: "required",
                    message: "Country is required"
                }]
            }, {
                dataField: "City",
                validationRules: [{
                    type: "pattern",
                    pattern: "^[^0-9]+$",
                    message: "Do not use digits in the City name"
                }, {
                    type: "stringLength",
                    min: 2,
                    message: "City must have at least 2 symbols"
                }, {
                    type: "required",
                    message: "City is required"
                }]
            }, {
                dataField: "Address",
                validationRules: [{
                    type: "required",
                    message: "Address is required"
                }]
            }, {
                dataField: "Phone",
                helpText: "",
                editorOptions: {
                    mask: "+1 (X00) 000-0000",
                    maskRules: {
                        "X": /[02-9]/
                    },
                    maskInvalidMessage: "The phone must have a correct USA phone format",
                    useMaskedValue: true
                },
                 validationRules: [{
                    type: "pattern",
                    pattern: /^\+\s*1\s*\(\s*[02-9]\d{2}\)\s*\d{3}\s*-\s*\d{4}$/,
                    message: "The phone must have a correct USA phone format"
                }]
            },
			
			{
                dataField: "Language",
                validationRules: [{
                    type: "required",
                    message: "Language is required"
                }, {
                    type: "pattern",
                    pattern: "^[^0-9]+$",
                    message: "Do not use digits in the Name"
                }]

            }
			
			]
        }, {
            dataField: "Accepted",
            label: {
                visible: false
            },
            editorOptions: {
                text: "I agree to the Terms and Conditions"
            },
            validationRules: [{
                type: "compare",
                comparisonTarget: function() { return true; },
                message: "You must agree to the Terms and Conditions"
            }]
        }]
    };

    $scope.buttonOptions = {
        text: "Create My Store",
        type: "success",
        useSubmitBehavior: true,
        validationGroup: "customerData"
    }

    $scope.onFormSubmit = function(e) {
        DevExpress.ui.notify({
            message: "You have submitted the form",
            position: {
                my: "center top",
                at: "center top"
            }
        }, "success", 3000);
        
        e.preventDefault();
    }

});