/**
 * Created by Omkar on 5/25/2018.
 */
/**
 * client send handle api { formList }
 * @param {*Response} response
 * return updated "OWT_Hdr"
 *
 * @author Bhumi
 * Rule Version 1.2.2 Date 04-01-2017
 * Rule Version 2 Date 09-01-2017
 */
function checkDrltoAngular(response) {
    var objRes = [];
    var setTax = [];

    //----------------------------------Owt_Hdr_Veriables_Start--------------------------------------
    var oh_Coupon_Share_Per = 0.0;
    var oh_Goods_Val = 0.0;
    var oh_Total_Disc_Val = 0.0;
    var oh_Prod_Disc_Val = 0.0;
    var oh_Promo_Val = 0.0;
    var oh_Coupon_Val = 0.0;
    var oh_Doc_Disc_Val = 0.0;
    var oh_Other_Disc_Val = 0.0;
    var oh_Doc_Disc_Per = 0.0;
    var oh_Tax_total = 0.0;
    var oh_Doc_fright_Val = 0.0;
    var oh_one_Val = 0.0;
    var oh_Two_Val = 0.0;
    var oh_Three_Val = 0.0;
    var oh_Four_Val = 0.0;
    var oh_Five_Val = 0.0;
    var oh_Six_Val = 0.0;
    var oh_Seven_Val = 0.0;
    var oh_Eight_Val = 0.0;
    var oh_Nine_Val = 0.0;
    var oh_Ten_Val = 0.0;

    var oh_Doc_Net_Amt = 0.0;
    var oh_Doc_Item_Qty = 0.0;
    var oh_Taxable_Goods_Amt = 0.0;
    var oh_Prod_QtyXPrice_Amt = 0.0;
    var oh_Doc_Item_Count = 0;
    var oh_ProdXPrice_Amt = 0.0;

    //----------------------------------Owt_Hdr_Veriables_End--------------------------------------
    //----------------------------------Owt_Dtl_Veriables_Start--------------------------------------
    var od_Bfr_Tax_Price = 0.0;
    var od_Calc_Price = 0.0;
    var od_Goods_Amt =0.0;

    var od_Prod_QtyXPrice_Amt = 0.0;
    var total_rate = 0.0;
    var running_total = 0.0;
    var prerate = 0.0;
    var total = 0.0;
    var new_rate = 0.0;

    var total_od_Prod_Disc_Per = 0.0;
    var od_Prod_Disc_Amt = 0.0;
    var total_od_Prod_Promo_Per = 0.0;
    var od_Prod_Promo_Amt = 0.0;
    var od_Prod_Promo_Per = 0.0;
    var total_od_Prod_Sp_Disc_Per = 0.0;
    var od_Prod_Sp_Disc_Per = 0.0;
    var od_Prod_Sp_Disc_Amt = 0.0 ;
    var total_od_Coup_Per = 0.0;
    var od_Coup_Amt = 0.0;
    var od_Prod_Disc_Per = 0.0;
    var od_Inv_Disc_Amt = 0.0;
    var od_Discount = 0.0;
    var od_Inv_Disc_Per = 0.0;
    var od_Rate = 0.0;
    var od_Prod_Tax1_Amt = 0.0;
    var od_Prod_Tax2_Amt = 0.0;
    var od_Prod_Tax3_Amt = 0.0;
    var od_Prod_Tax4_Amt = 0.0;
    var od_Prod_Tax5_Amt = 0.0;
    var od_Prod_Tax_Amt = 0.0;
    var od_Tax_Rate = 0.0;
    var od_Taxable_Goods_Amt = 0.0;
    var od_Prod_NetAmt = 0.0;
    var od_Tax_Inclusive;
    var od_Free_Tag = 0;
    var prodName = "";
    var prodId;
    var count = 0;

    //----------------------------------Owt_Dtl_Veriables_End--------------------------------------
    //----------------------------------Owt_Tax_Veriables_Start--------------------------------------
    var otax_1_Amt = 0.0;
    var otax_2_Amt = 0.0;
    var otax_3_Amt = 0.0;
    var otax_4_Amt = 0.0;
    var otax_5_Amt = 0.0;
    var otaxble_Goods_Amt = 0.0;

    var otax_1_Rate_owt = 0.0;
    var otax_2_Rate_owt = 0.0;
    var oTax_3_Rate_owt = 0.0;
    var otax_4_Rate_owt = 0.0;
    var otax_5_Rate_owt = 0.0;
    var od_Tax_Code_owt = 0;
    var otax_1_Code = 0;
    var sum = 0.0;
    //----------------------------------Owt_Tax_Veriables_End--------------------------------------

    try{
        response.forEach(function (o, k) {

            // condition for checking Document Discount as Value
            if (o.oh_Doc_Disc_Val != 0 && o.oh_Doc_Disc_Per == 0) {

                o.owt_Dtl_Code.forEach(function (d, k) {

                    count++;
                    od_Free_Tag = d.od_Free_Tag;

                    if(od_Free_Tag == 1) {
                        //oh_Doc_Item_Qty = oh_Doc_Item_Qty + d.getod_Qty().doubleValue();
                    } else {
                        prodName = d.od_Prod_Name;
                        // prod_id=d.od_Prod_Code.id;
                        //help(drools, "in for loop..." + prod_id);
                        // Condition for Tax Inclusive/Exclusive
                        //@author: Tanveer
                        if (d.od_Tax_Inclusive == null) {
                            //  throw new GlobalException(ErrorCodes.MANDATORY_PARAMETERS_NOT_SET, "Mandatory Parameter od_Tax_Inclusive can not be null");
                            objRes={
                                message:{
                                    'code':1001,
                                    'message' : 'Mandatory Parameter od_Tax_Inclusive can not be null'
                                }

                            };
                            throw "Mandatory Parameter od_Tax_Inclusive can not be null";
                        }
                        else {
                            od_Tax_Inclusive = d.od_Tax_Inclusive;
                            if (od_Tax_Inclusive == 1) {
                                // Begin : Inclusive_Tax_Calculation
                                if (d.od_Calc_Price == null) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_Tax_Inclusive can not be null'
                                        }

                                    };
                                    throw "Mandatory Parameter od_Calc_Price can not be null for Product name: ";
                                } else if (d.od_Rate == null) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_Rate can not be null'
                                        }

                                    };
                                    throw "Mandatory Parameter od_Rate can not be null for Product name";
                                } else if (d.od_Qty == null || d.od_Qty == 0) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_Qty can not be null'
                                        }

                                    };
                                    throw "Mandatory Parameter od_Qty can not be null for Product name: ";
                                } else if (d.od_MRP == null || d.od_MRP == 0) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_MRP can not be null'
                                        }

                                    };
                                    throw "Mandatory Parameter od_MRP can not be null for Product name: ";
                                } else {
                                    if(d.od_Calc_Price == 0) {
                                        od_Calc_Price = d.od_Rate;		// Calculated Price
                                    }
                                    else {
                                        od_Calc_Price = d.od_Calc_Price;
                                    }
                                }

                                od_Prod_QtyXPrice_Amt = od_Calc_Price * d.od_Qty;		//Price * quantity
                                if (d.od_Tax_Rate == null) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_Tax_Rate can not be null'
                                        }

                                    };
                                    throw "Mandatory Parameter od_Tax_Rate can not be null for Product name"
                                }

                                od_Tax_Rate = d.od_Tax_Rate;
                                od_Bfr_Tax_Price = (od_Calc_Price * 100) / (od_Tax_Rate + 100);
                                od_Prod_QtyXPrice_Amt = od_Bfr_Tax_Price * d.od_Qty;

                                //help(drools, "rate : " + d.od_Rate + "  before : " + od_Bfr_Tax_Price + " tax : " + od_Tax_Rate + " b4value : " + od_Prod_QtyXPrice_Amt);

                            }
                            // End : Inclusive_Tax_Calculation
                            else {
                                // Begin : Exclusive_Tax_Calculation
                                //help(drools, "--------------------prod disc------------------------* qty" + d.od_Qty());
                                //if(d.od_Calc_Price == d.od_Rate) {

                                //@author: Tanveer
                                if (d.od_Calc_Price == null) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_Calc_Price can not be null'
                                        }

                                    };
                                    throw "Mandatory Parameter od_Calc_Price can not be null for Product name: ";
                                } else if (d.od_Rate == null) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_Rate can not be null'
                                        }

                                    };
                                    throw "Mandatory Parameter od_Rate can not be null for Product name: ";
                                } else if (d.od_Qty == null || d.od_Qty == 0) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_Qty can not be null'
                                        }

                                    };
                                    throw "Mandatory Parameter od_Qty can not be null for Product name: " ;
                                } else if (d.od_MRP == null || d.od_MRP == 0) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_MRP can not be null'
                                        }

                                    };
                                    throw "Mandatory Parameter od_MRP can not be null for Product name: ";
                                } else {
                                    if(d.od_Calc_Price == 0) {
                                        od_Calc_Price = d.od_Rate;		// Calculated Price
                                    }
                                    else {
                                        od_Calc_Price = d.od_Calc_Price;
                                    }
                                }

                                od_Prod_QtyXPrice_Amt = od_Calc_Price * d.od_Qty;		//Price * quantity
                                od_Tax_Rate = d.od_Tax_Rate;
                                od_Bfr_Tax_Price = (od_Calc_Price);
                                od_Prod_QtyXPrice_Amt = od_Bfr_Tax_Price * d.od_Qty;

                            }
                        }

                        // End : Exclusive_Tax_Calculation
                        // Begin : Special_Discount_Calculation

                        if (d.od_Prod_Sp_Disc_Per != 0 && d.od_Prod_Sp_Disc_Amt != 0) {
                            od_Prod_Sp_Disc_Amt = (d.od_Prod_Sp_Disc_Per / 100) * od_Prod_QtyXPrice_Amt;
                        }
                        else if (d.od_Prod_Sp_Disc_Amt != 0) {
                            od_Prod_Sp_Disc_Amt = d.od_Prod_Promo_Amt;
                        }
                        else if (d.od_Prod_Sp_Disc_Per != 0) {
                            od_Prod_Sp_Disc_Amt = (d.od_Prod_Promo_Per / 100) * od_Prod_QtyXPrice_Amt;
                        }
                        else {
                            od_Prod_Sp_Disc_Amt = d.od_Prod_Sp_Disc_Amt;
                            od_Prod_Sp_Disc_Per = d.od_Prod_Sp_Disc_Per;
                        }
                        od_Prod_QtyXPrice_Amt = (od_Prod_QtyXPrice_Amt - od_Prod_Sp_Disc_Amt);		//=  Price*QTY - Special_Discount

                        // End : Special_Discount_Calculation

                        // Begin : Product_Discount_Calculation


                        if(d.od_Prod_Disc_Per != 0 && d.od_Prod_Disc_Amt != 0 ) {
                            od_Prod_Disc_Amt = (d.od_Prod_Disc_Per / 100) * od_Prod_QtyXPrice_Amt;
                            od_Prod_Disc_Per = d.od_Prod_Disc_Per;
                        }
                        else if (d.od_Prod_Disc_Amt != 0) {
                            od_Prod_Disc_Amt = d.od_Prod_Disc_Amt;
                            od_Prod_Disc_Per = d.od_Prod_Disc_Per;
                        }
                        else if (d.od_Prod_Disc_Per != 0) {
                            od_Prod_Disc_Amt = (d.od_Prod_Disc_Per / 100) * od_Prod_QtyXPrice_Amt;
                            od_Prod_Disc_Per = d.od_Prod_Disc_Per;
                        }
                        else {
                            od_Prod_Disc_Amt = d.od_Prod_Disc_Amt;
                            od_Prod_Disc_Per = d.od_Prod_Disc_Per;
                        }
                        od_Prod_QtyXPrice_Amt = (od_Prod_QtyXPrice_Amt - od_Prod_Disc_Amt);		//=  Price*QTY - Product_Discount

                        // End : Product_Discount_Calculation

                        // Begin : Promo_Discount_Calculation

                        if (d.od_Prod_Promo_Per != 0 && d.od_Prod_Promo_Amt != 0) {
                            od_Prod_Promo_Amt = (d.od_Prod_Promo_Per / 100) * od_Prod_QtyXPrice_Amt;
                        }
                        else if (d.od_Prod_Promo_Amt != 0) {
                            od_Prod_Promo_Amt = d.od_Prod_Promo_Amt;
                        }
                        else if (d.od_Prod_Promo_Per != 0) {
                            od_Prod_Promo_Amt = (d.od_Prod_Promo_Per / 100) * od_Prod_QtyXPrice_Amt;
                        }
                        else {
                            od_Prod_Promo_Amt = d.od_Prod_Promo_Amt;
                            od_Prod_Promo_Per = d.od_Prod_Promo_Per;
                        }
                        od_Prod_QtyXPrice_Amt = (od_Prod_QtyXPrice_Amt - od_Prod_Promo_Amt);		//=  Price*QTY - Promo_Discount

                        // End : Promo_Discount_Calculation

                        oh_Total_Disc_Val = (oh_Total_Disc_Val + od_Prod_Disc_Amt + od_Prod_Sp_Disc_Amt + od_Prod_Promo_Amt);
                        od_Taxable_Goods_Amt = (od_Taxable_Goods_Amt + od_Prod_QtyXPrice_Amt);
                        total = od_Taxable_Goods_Amt;

                        d.od_Bfr_Tax_Price = od_Bfr_Tax_Price;
                        d.od_Prod_QtyXPrice_Amt = od_Prod_QtyXPrice_Amt;
                        d.od_Prod_Disc_Per = od_Prod_Disc_Per;
                        d.od_Prod_Disc_Amt = od_Prod_Disc_Amt;
                        d.od_Prod_Promo_Per = od_Prod_Promo_Per;
                        d.od_Prod_Promo_Amt = od_Prod_Promo_Amt;
                        d.od_Prod_Sp_Disc_Per = total_od_Prod_Sp_Disc_Per;
                        d.od_Prod_Sp_Disc_Amt = od_Prod_Sp_Disc_Amt;
                        d.od_Taxable_Goods_Amt = od_Taxable_Goods_Amt;
                        d.od_Calc_Price = od_Calc_Price;

                    }
                });

                // Begin : Document_Discount_Calculation(as Value)
                o.owt_Dtl_Code.forEach(function (d, k) {

                    od_Free_Tag = d.od_Free_Tag;
                    if(od_Free_Tag == 1) {
                        //System.out.println("free tag is applied on the product : " + d.getod_Prod_Name() + " ( id ) : " + d.getod_Prod_Code().getId().longValue());
                        oh_Doc_Item_Qty = oh_Doc_Item_Qty + d.od_Qty;
                        // set.add(d);
                    } else {
                        //System.out.println("--------------------document disc------------------------");
                        //help(drools, "new rate before doc disc in doc loop:------------------" + od_Prod_QtyXPrice_Amt);
                        //help(drools, "before doc disc: in doc loop------------------" + od_Taxable_Goods_Amt);
                        if (o.oh_Doc_Disc_Val != 0) {
                            running_total = d.od_Prod_QtyXPrice_Amt;
                            oh_Doc_Disc_Val = o.oh_Doc_Disc_Val;
                            total_rate = running_total;
                            running_total = (running_total + (running_total / total) * oh_Doc_Disc_Val);
                            od_Inv_Disc_Amt = running_total - total_rate;
                        }

                        od_Prod_QtyXPrice_Amt = (d.od_Prod_QtyXPrice_Amt - od_Inv_Disc_Amt);
                        od_Taxable_Goods_Amt = od_Prod_QtyXPrice_Amt;
                        oh_Total_Disc_Val = (oh_Total_Disc_Val + od_Inv_Disc_Amt);
                        od_Discount = od_Prod_Disc_Amt + od_Prod_Promo_Amt + od_Prod_Sp_Disc_Amt + od_Coup_Amt;
                        // End : Document_Discount_Calculation

                        // Begin : Owt_Hdr_Calculation

                        oh_Promo_Val = (oh_Promo_Val + od_Prod_Promo_Amt);

                        oh_Coupon_Val = (oh_Coupon_Val + d.od_Coup_Amt);

                        oh_Other_Disc_Val = (oh_Other_Disc_Val + d.od_Prod_Sp_Disc_Amt);

                        oh_Doc_fright_Val = (oh_Doc_fright_Val + d.od_Fright_Amt);

                        oh_one_Val = (oh_one_Val + d.od_one_Val);

                        oh_Two_Val = (oh_Two_Val + d.od_Two_Val);

                        oh_Three_Val = (oh_Three_Val + d.od_Three_Val);

                        oh_Four_Val = (oh_Four_Val + d.od_Four_Val);

                        oh_Five_Val = (oh_Five_Val + d.od_Five_Val);

                        oh_Six_Val = (oh_Six_Val + d.od_Six_Val);

                        oh_Seven_Val = (oh_Seven_Val + d.od_Seven_Val);

                        oh_Eight_Val = (oh_Eight_Val + d.od_Eight_Val);

                        oh_Nine_Val = (oh_Nine_Val + d.od_Nine_Val);

                        oh_Ten_Val = (oh_Ten_Val + d.od_Ten_Val);

                        oh_Doc_Item_Qty = (oh_Doc_Item_Qty + d.od_Qty);

                        oh_Prod_QtyXPrice_Amt = (oh_Prod_QtyXPrice_Amt + od_Prod_QtyXPrice_Amt);

                        oh_ProdXPrice_Amt = (oh_ProdXPrice_Amt + od_Prod_QtyXPrice_Amt);

                        // End : Owt_Hdr_Calculation

                        // Begin : Product_Tax_Calculation
                        od_Prod_Tax1_Amt = (d.od_Tax1_Rate / 100) * od_Prod_QtyXPrice_Amt;
                        od_Prod_Tax2_Amt = (d.od_Tax2_Rate / 100) * od_Prod_QtyXPrice_Amt;
                        od_Prod_Tax3_Amt = (d.od_Tax3_Rate / 100) * od_Prod_QtyXPrice_Amt;
                        od_Prod_Tax4_Amt = (d.od_Tax4_Rate / 100) * od_Prod_QtyXPrice_Amt;
                        od_Prod_Tax5_Amt = (d.od_Tax5_Rate / 100) * od_Prod_QtyXPrice_Amt;

                        od_Prod_Tax_Amt = (od_Prod_Tax1_Amt + od_Prod_Tax2_Amt + od_Prod_Tax3_Amt + od_Prod_Tax4_Amt + od_Prod_Tax5_Amt);
                        od_Prod_NetAmt = (od_Prod_QtyXPrice_Amt + od_Prod_Tax_Amt);
                        od_Goods_Amt = (od_Prod_QtyXPrice_Amt + od_Prod_Tax_Amt);
                        oh_Goods_Val = (oh_Goods_Val + od_Goods_Amt);
                        oh_Taxable_Goods_Amt = (oh_Taxable_Goods_Amt + od_Taxable_Goods_Amt);
                        oh_Tax_total = (oh_Tax_total + od_Prod_Tax_Amt);
                        oh_Doc_fright_Val = (oh_Doc_fright_Val + d.od_Fright_Amt);

                        // End : Product_Tax_Calculation

                        // Begin : Owt_Dtl_Set

                        d.od_Goods_Amt = od_Goods_Amt;
                        d.od_Coup_Amt = od_Coup_Amt;
                        d.od_Prod_Tax1_Amt = od_Prod_Tax1_Amt;
                        d.od_Prod_Tax2_Amt = od_Prod_Tax2_Amt;
                        d.od_Prod_Tax3_Amt = od_Prod_Tax3_Amt;
                        d.od_Prod_Tax4_Amt = od_Prod_Tax4_Amt;
                        d.od_Prod_Tax5_Amt = od_Prod_Tax5_Amt;
                        d.od_Inv_Disc_Amt = od_Inv_Disc_Amt;
                        d.od_Prod_Tax_Amt = od_Prod_Tax_Amt;
                        d.od_Taxable_Goods_Amt = od_Taxable_Goods_Amt;
                        d.od_Prod_NetAmt = od_Prod_NetAmt;

                        // End : Owt_Dtl_Set
                        // set.add(d);
                    }
                });

            } else {
                o.owt_Dtl_Code.forEach(function (d, k) {

                    count++;
                    od_Free_Tag = d.od_Free_Tag;
                    if(od_Free_Tag == 1) {
                        //System.out.println("free tag is applied on the product : " + d.getod_Prod_Name() + " ( id ) : " + d.getod_Prod_Code().getId().longValue());
                        oh_Doc_Item_Qty = oh_Doc_Item_Qty + d.od_Qty;
                        //set.add(d);
                    } else {
                        //@author: Tanveer
                        prodName = d.od_Prod_Name;
                        //prod_id=d.od_Prod_Code.id;
                        //help(drools, "in for loop..." + prod_id);
                        // Condition for Tax Inclusive/Exclusive

                        if (d.od_Tax_Inclusive == null) {

                            // throw new GlobalException(ErrorCodes.MANDATORY_PARAMETERS_NOT_SET, "Mandatory Parameter od_Tax_Inclusive can not be null for product id: " + prod_id);
                            objRes={
                                message:{
                                    'code':1001,
                                    'message' : 'Mandatory Parameter od_Tax_Inclusive can not be null'
                                }

                            };
                            throw "Mandatory Parameter od_Tax_Inclusive can not be null";
                        }
                        else {
                            od_Tax_Inclusive = d.od_Tax_Inclusive;
                            if (od_Tax_Inclusive == 1) {
                                //help(drools, "tax inclusive...");
                                // Begin : Inclusive_Tax_Calculation

                                //@author: Tanveer
                                if (d.od_Calc_Price == null) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_Calc_Price can not be null'
                                        }

                                    };
                                    throw "Mandatory Parameter od_Calc_Price can not be null for Product name";
                                } else if (d.od_Rate == null) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_Rate can not be null'
                                        }

                                    };
                                    throw"Mandatory Parameter od_Rate can not be null for Product name: " ;
                                } else if (d.od_Qty == null || d.od_Qty == 0) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_Qty can not be null'
                                        }

                                    };
                                    throw "Mandatory Parameter od_Qty can not be null for Product name: " ;
                                } else if (d.od_MRP == null || d.od_MRP == 0) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_MRP can not be null'
                                        }

                                    };
                                    throw "Mandatory Parameter od_MRP can not be null for Product name: " ;
                                } else {
                                    if(d.od_Calc_Price == 0) {
                                        od_Calc_Price = d.od_Rate;		// Calculated Price
                                    }
                                    else {
                                        od_Calc_Price = d.od_Calc_Price;
                                    }
                                }

                                //if(d.od_Calc_Price == d.od_Rate) {
                                // od_Calc_Price = d.od_Rate;		// Calculated Price
                                //help(drools, "calc price " + od_Calc_Price);
                                //}
                                od_Prod_QtyXPrice_Amt = od_Calc_Price * d.od_Qty;		//Price * quantity
                                od_Tax_Rate = d.od_Tax_Rate;
                                od_Bfr_Tax_Price = (od_Calc_Price * 100) / (od_Tax_Rate + 100);
                                od_Prod_QtyXPrice_Amt = od_Bfr_Tax_Price * d.od_Qty;

                                //System.out.println("rate : " + d.od_Rate + "  before : " + od_Bfr_Tax_Price + " tax : " + od_Tax_Rate + " b4value : " + od_Prod_QtyXPrice_Amt);
                            }
                            // End : Inclusive_Tax_Calculation
                            else {
                                // Begin : Exclusive_Tax_Calculation
                                //@author: Tanveer
                                if (d.od_Calc_Price == null) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_Calc_Price can not be null'
                                        }

                                    };
                                    throw "Mandatory Parameter od_Calc_Price can not be null for Product name: ";
                                } else if (d.od_Rate == null) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_Rate can not be null'
                                        }

                                    };
                                    throw "Mandatory Parameter od_Rate can not be null for Product name: ";
                                } else if (d.od_Qty == null || d.od_Qty == 0) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_Qty can not be null'
                                        }

                                    };
                                    throw "Mandatory Parameter od_Qty can not be null for Product name: ";
                                } else if (d.od_MRP == null || d.od_MRP == 0) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_MRP can not be null'
                                        }

                                    };
                                    throw "Mandatory Parameter od_MRP can not be null for Product name: ";
                                } else {
                                    if(d.od_Calc_Price == 0) {
                                        od_Calc_Price = d.od_Rate;		// Calculated Price
                                    }
                                    else {
                                        od_Calc_Price = d.od_Calc_Price;
                                    }
                                }

                                od_Prod_QtyXPrice_Amt = od_Calc_Price * d.od_Qty;		//Price * quantity
                                //@author: Tanveer
                                if (d.od_Tax_Rate == null) {
                                    objRes={
                                        message:{
                                            'code':1001,
                                            'message' : 'Mandatory Parameter od_Tax_Rate can not be null'
                                        }

                                    };
                                    throw "Mandatory Parameter od_Tax_Rate can not be null for Product name: " ;
                                }

                                od_Tax_Rate = d.od_Tax_Rate;
                                od_Bfr_Tax_Price = (od_Calc_Price);
                                od_Prod_QtyXPrice_Amt = od_Bfr_Tax_Price * d.od_Qty;

                                //System.out.println("rate : " + d.od_Rate + "  before : " + od_Bfr_Tax_Price + " tax : " + od_Tax_Rate + " b4value : " + od_Prod_QtyXPrice_Amt);
                            }
                        }
                        // End : Exclusive_Tax_Calculation
                        // Begin : Special_Discount_Calculation

                        if (d.od_Prod_Sp_Disc_Per != 0 && d.od_Prod_Sp_Disc_Amt != 0) {
                            od_Prod_Sp_Disc_Amt = (d.od_Prod_Sp_Disc_Per / 100) * od_Prod_QtyXPrice_Amt;
                        }
                        else if (d.od_Prod_Sp_Disc_Amt != 0) {
                            od_Prod_Sp_Disc_Amt = d.od_Prod_Promo_Amt;
                        }
                        else if (d.od_Prod_Sp_Disc_Per != 0) {
                            od_Prod_Sp_Disc_Amt = (d.od_Prod_Promo_Per / 100) * od_Prod_QtyXPrice_Amt;
                        }
                        else {
                            od_Prod_Sp_Disc_Amt = d.od_Prod_Sp_Disc_Amt;
                            od_Prod_Sp_Disc_Per = d.od_Prod_Sp_Disc_Per;
                        }
                        od_Prod_QtyXPrice_Amt = (od_Prod_QtyXPrice_Amt - od_Prod_Sp_Disc_Amt);		//=  Price*QTY - Special_Discount

                        // End : Special_Discount_Calculation

                        // Begin : Product_Discount_Calculation

                        if (d.od_Prod_Disc_Per != 0 && d.od_Prod_Disc_Amt != 0) {
                            od_Prod_Disc_Amt = (d.od_Prod_Disc_Per / 100) * od_Prod_QtyXPrice_Amt;
                            od_Prod_Disc_Per = d.od_Prod_Disc_Per;
                        }
                        else if (d.od_Prod_Disc_Amt != 0) {
                            od_Prod_Disc_Amt = d.od_Prod_Disc_Amt;
                            od_Prod_Disc_Per = d.od_Prod_Disc_Per;
                        }
                        else if (d.od_Prod_Disc_Per != 0) {
                            od_Prod_Disc_Amt = (d.od_Prod_Disc_Per / 100) * od_Prod_QtyXPrice_Amt;
                            od_Prod_Disc_Per = d.od_Prod_Disc_Per;
                        }
                        else {
                            od_Prod_Disc_Amt = d.od_Prod_Disc_Amt;
                            od_Prod_Disc_Per = d.od_Prod_Disc_Per;
                        }

                        od_Prod_QtyXPrice_Amt = (od_Prod_QtyXPrice_Amt - od_Prod_Disc_Amt);		//=  Price*QTY - Product_Discount

                        // End : Product_Discount_Calculation

                        // Begin : Promo_Discount_Calculation

                        if (d.od_Prod_Promo_Per != 0 && d.od_Prod_Promo_Amt != 0) {
                            od_Prod_Promo_Amt = (d.od_Prod_Promo_Per / 100) * od_Prod_QtyXPrice_Amt;
                        }
                        else if (d.od_Prod_Promo_Amt != 0) {
                            od_Prod_Promo_Amt = d.od_Prod_Promo_Amt;
                        }
                        else if (d.od_Prod_Promo_Per != 0) {
                            od_Prod_Promo_Amt = (d.od_Prod_Promo_Per / 100) * od_Prod_QtyXPrice_Amt;
                        }
                        else {
                            od_Prod_Promo_Amt = d.od_Prod_Promo_Amt;
                            od_Prod_Promo_Per = d.od_Prod_Promo_Per;
                        }
                        od_Prod_QtyXPrice_Amt = (od_Prod_QtyXPrice_Amt - od_Prod_Promo_Amt);		//=  Price*QTY - Promo_Discount

                        // End : Promo_Discount_Calculation

                        // Begin : Document_Discount_Calculation

                        if (o.oh_Doc_Disc_Per != 0) {
                            od_Inv_Disc_Amt = (o.oh_Doc_Disc_Per / 100) * od_Prod_QtyXPrice_Amt;
                            oh_Prod_Disc_Val = (oh_Prod_Disc_Val + od_Inv_Disc_Amt);
                            //@author: Tanveer
                            oh_Doc_Disc_Val = (oh_Doc_Disc_Val + od_Inv_Disc_Amt);
                        }
                        else {
                            oh_Doc_Disc_Val = o.oh_Doc_Disc_Val;
                            oh_Doc_Disc_Per = o.oh_Doc_Disc_Per;
                        }

                        od_Prod_QtyXPrice_Amt = (od_Prod_QtyXPrice_Amt - od_Inv_Disc_Amt);
                        //@author: Tanveer
                        od_Taxable_Goods_Amt = od_Prod_QtyXPrice_Amt;
                        oh_Total_Disc_Val = (oh_Total_Disc_Val + od_Prod_Disc_Amt + od_Prod_Sp_Disc_Amt + od_Prod_Promo_Amt + od_Inv_Disc_Amt);

                        // End : Document_Discount_Calculation

                        // Begin : Owt_Hdr_Calculation

                        oh_Promo_Val = (oh_Promo_Val + od_Prod_Promo_Amt);

                        oh_Coupon_Val = (oh_Coupon_Val + d.od_Coup_Amt);

                        oh_Other_Disc_Val = (oh_Other_Disc_Val + d.od_Prod_Sp_Disc_Amt);

                        oh_Doc_fright_Val = (oh_Doc_fright_Val + d.od_Fright_Amt);

                        oh_one_Val = (oh_one_Val + d.od_one_Val);

                        oh_Two_Val = (oh_Two_Val + d.od_Two_Val);

                        oh_Three_Val = (oh_Three_Val + d.od_Three_Val);

                        oh_Four_Val = (oh_Four_Val + d.od_Four_Val);

                        oh_Five_Val = (oh_Five_Val + d.od_Five_Val);

                        oh_Six_Val = (oh_Six_Val + d.od_Six_Val);

                        oh_Seven_Val = (oh_Seven_Val + d.od_Seven_Val);

                        oh_Eight_Val = (oh_Eight_Val + d.od_Eight_Val);

                        oh_Nine_Val = (oh_Nine_Val + d.od_Nine_Val);

                        oh_Ten_Val = (oh_Ten_Val + d.od_Ten_Val);

                        oh_Doc_Item_Qty = (oh_Doc_Item_Qty + d.od_Qty);

                        oh_Prod_QtyXPrice_Amt = (oh_Prod_QtyXPrice_Amt + od_Prod_QtyXPrice_Amt);

                        oh_ProdXPrice_Amt = (oh_ProdXPrice_Amt + od_Prod_QtyXPrice_Amt);

                        od_Discount = (od_Prod_Disc_Amt + od_Prod_Promo_Amt + od_Prod_Sp_Disc_Amt + od_Coup_Amt);

                        // End : Owt_Hdr_Calculation

                        // Begin : Product_Tax_Calculation

                        od_Prod_Tax1_Amt = (d.od_Tax1_Rate / 100) * od_Prod_QtyXPrice_Amt;
                        od_Prod_Tax2_Amt = (d.od_Tax2_Rate / 100) * od_Prod_QtyXPrice_Amt;
                        od_Prod_Tax3_Amt = (d.od_Tax3_Rate / 100) * od_Prod_QtyXPrice_Amt;
                        od_Prod_Tax4_Amt = (d.od_Tax4_Rate / 100) * od_Prod_QtyXPrice_Amt;
                        od_Prod_Tax5_Amt = (d.od_Tax5_Rate / 100) * od_Prod_QtyXPrice_Amt;

                        od_Prod_Tax_Amt = (od_Prod_Tax1_Amt + od_Prod_Tax2_Amt + od_Prod_Tax3_Amt+ od_Prod_Tax4_Amt+ od_Prod_Tax5_Amt);
                        od_Goods_Amt = (od_Prod_QtyXPrice_Amt + od_Prod_Tax_Amt);
                        oh_Goods_Val = (oh_Goods_Val + od_Goods_Amt);
                        od_Prod_NetAmt = (od_Prod_QtyXPrice_Amt + od_Prod_Tax_Amt);
                        oh_Taxable_Goods_Amt = (oh_Taxable_Goods_Amt + od_Taxable_Goods_Amt);
                        oh_Tax_total = (oh_Tax_total + od_Prod_Tax_Amt);
                        oh_Doc_fright_Val = (oh_Doc_fright_Val + d.od_Fright_Amt);

                        // End : Product_Tax_Calculation

                        // Begin : Owt_Dtl_Set

                        d.od_Goods_Amt = od_Goods_Amt;
                        d.od_Bfr_Tax_Price = od_Bfr_Tax_Price;
                        d.od_Prod_QtyXPrice_Amt = od_Prod_QtyXPrice_Amt;
                        d.od_Prod_Disc_Per = od_Prod_Disc_Per;
                        d.od_Prod_Disc_Amt = od_Prod_Disc_Amt;
                        d.od_Calc_Price = od_Calc_Price;
                        d.od_Inv_Disc_Per = od_Inv_Disc_Per;
                        d.od_Inv_Disc_Amt = od_Inv_Disc_Amt;
                        d.od_Prod_Promo_Amt = od_Prod_Promo_Amt;
                        d.od_Prod_Sp_Disc_Per = total_od_Prod_Sp_Disc_Per;
                        d.od_Prod_Sp_Disc_Amt = od_Prod_Sp_Disc_Amt;
                        d.od_Coup_Amt = od_Coup_Amt;

                        d.od_Prod_Tax1_Amt = od_Prod_Tax1_Amt;
                        d.od_Prod_Tax2_Amt = od_Prod_Tax2_Amt;
                        d.od_Prod_Tax3_Amt = od_Prod_Tax3_Amt;
                        d.od_Prod_Tax4_Amt = od_Prod_Tax4_Amt;
                        d.od_Prod_Tax5_Amt = od_Prod_Tax5_Amt;
                        d.od_Prod_Tax_Amt = od_Prod_Tax_Amt;

                        d.od_Prod_NetAmt = od_Prod_NetAmt;
                        d.od_Taxable_Goods_Amt = od_Taxable_Goods_Amt;

                        // End : Owt_Dtl_Set
                        // set.add(d);
                    }

                });
            }
            oh_Doc_Item_Count = count;


            //Tax summary addition
            // Begin : Owt_Tax_Calculation
            var hashSet = [];
            /*angular.forEach(o.owt_Dtl_Code, function (valueObject, keyObject) {
             if(hashSet.indexOf(valueObject['od_Tax_Rate']) == -1)
             hashSet.push(valueObject['od_Tax_Rate']);
             });*/
            for(var i=0;i<o.owt_Dtl_Code.length;i++){
                var valueObject = o.owt_Dtl_Code[i];
                if(hashSet.indexOf(valueObject['od_Tax_Rate']) == -1)
                    hashSet.push(valueObject['od_Tax_Rate']);
            }

            for(var i=0;i<hashSet.length;i++){
                var t1 = hashSet[i];
                //angular.forEach(hashSet, function (value, key){
                //	var t1 = value;
                sum = 0.0;
                od_Tax_Code_owt = 0;
                otax_1_Rate_owt = 0.0;
                otax_2_Rate_owt = 0.0;
                otax_3_Rate_owt = 0.0;
                otax_4_Rate_owt = 0.0;
                otax_5_Rate_owt = 0.0;
                otax_1_Code = 0;
                otaxble_Goods_Amt = 0.0;


                for(var i=0;i<hashSet.length;i++){
                    // angular.forEach(o.owt_Dtl_Code, function (d,k){
                    var d = hashSet[i];
                    if(d.od_Tax_Rate == t1){
                        otax_1_Amt = otax_1_Amt + d.od_Prod_Tax1_Amt;
                        otax_2_Amt = otax_2_Amt + d.od_Prod_Tax2_Amt;
                        otax_3_Amt = otax_3_Amt + d.od_Prod_Tax3_Amt;
                        otax_4_Amt = otax_4_Amt + d.od_Prod_Tax4_Amt;
                        otax_5_Amt = otax_5_Amt + d.od_Prod_Tax5_Amt;
                        otax_1_Rate_owt = d.od_Tax1_Rate;
                        otax_2_Rate_owt = d.od_Tax2_Rate;
                        otax_3_Rate_owt = d.od_Tax3_Rate;
                        otax_4_Rate_owt = d.od_Tax4_Rate;
                        otax_5_Rate_owt = d.otax_5_Rate;
                        od_Tax_Code_owt = d.od_Tax_Code;
                        otax_1_Code = d.od_Tax1_Code;
                        otaxble_Goods_Amt = (otaxble_Goods_Amt + d.od_Taxable_Goods_Amt);
                        sum = (sum + d.od_Prod_Tax_Amt);
                    }

                    //});
                }

                var tax = {};
                tax.otax_Rate=t1;
                tax.otax_Tax_total=sum;
                tax.otax_1_Amt = otax_1_Amt;
                tax.otax_2_Amt = otax_2_Amt;
                tax.otax_3_Amt = otax_3_Amt;
                tax.otax_4_Amt = otax_4_Amt;
                tax.otax_5_Amt = otax_5_Amt;
                tax.otax_1_Rate = otax_1_Rate_owt;
                tax.otax_2_Rate = otax_2_Rate_owt;
                tax.otax_3_Rate = otax_3_Rate_owt;
                tax.otax_4_Rate = otax_4_Rate_owt;
                tax.otax_5_Rate = otax_5_Rate_owt;
                var tax_Class_MsT = {};
                tax_Class_MsT.id = od_Tax_Code_owt;
                tax.otax_Code = tax_Class_MsT;
                tax.otax_1_Code = otax_1_Code;
                tax.otaxble_Goods_Amt = otaxble_Goods_Amt;



                var now = new Date();

                // 3) a java current time (now) instance
                tax.otax_TS_Added = new Date();
                tax.otax_TS_Edited = new Date();

                for(var i=0;i<o.owt_Tax_Code.length;i++){
                    var t = o.owt_Tax_Code[i];
                    //angular.forEach(o.owt_Tax_Code, function (t,kt){
                    tax.otax_user_Code_Add = t.otax_user_Code_Add;
                    tax.otax_user_Code_Updt = t.otax_user_Code_Updt;
                    //});
                }

                setTax.push(tax);

                // });
            }

            // End : Owt_Tax_Calculation

            oh_Doc_Net_Amt = (oh_Doc_Net_Amt + oh_Taxable_Goods_Amt + oh_Tax_total + oh_Doc_fright_Val + o.oh_Doc_RoundOff);
            o.oh_Coupon_Share_Per = oh_Coupon_Share_Per;
            o.oh_Goods_Val = oh_Goods_Val;
            o.oh_Total_Disc_Val = oh_Total_Disc_Val;
            o.oh_Promo_Val = oh_Promo_Val;

            o.oh_Coupon_Val = oh_Coupon_Val;
            o.oh_Doc_Disc_Val = oh_Doc_Disc_Val;
            o.oh_Other_Disc_Val = oh_Other_Disc_Val;
            o.oh_Tax_total = oh_Tax_total;
            o.oh_Doc_fright_Val = oh_Doc_fright_Val;
            o.oh_one_Val = oh_one_Val;
            o.oh_Two_Val = oh_Two_Val;
            o.oh_Three_Val = oh_Three_Val;
            o.oh_Four_Val = oh_Four_Val;
            o.oh_Five_Val = oh_Five_Val;
            o.oh_Six_Val = oh_Six_Val;
            o.oh_Seven_Val = oh_Seven_Val;
            o.oh_Eight_Val = oh_Eight_Val;
            o.oh_Nine_Val = oh_Nine_Val;
            o.oh_Ten_Val = oh_Ten_Val;
            o.oh_Doc_Net_Amt = oh_Doc_Net_Amt;
            o.oh_Doc_Item_Qty = oh_Doc_Item_Qty;
            o.oh_Taxable_Goods_Amt = oh_Taxable_Goods_Amt;
            o.oh_Doc_Item_Count = oh_Doc_Item_Count;
            o.oh_ProdXPrice_Amt = oh_ProdXPrice_Amt;

            o.owt_Tax_Code = setTax;

        });
        //@author: Tanveer.
        //Addition in json structure because of our controller/service logic.

        var oRef = {};
        /*   angular.forEach(response[0], function(value, key){
         if(value instanceof Array){
         if((value.length > 0)){
         oRef[key] = value;
         }
         }
         else if(value != null){
         oRef[key] = value;
         }

         });*/

        for(var j=0;j< response.length;j++){
            var value = response[j];
            if(value instanceof Array){
                if((value.length > 0)){
                    oRef[j] = value;
                }
            }
            else if(value != null){
                oRef[j] = value;
            }

        }
        response[0] = oRef[0];


        var data=[];
        data[0] = response[0];
        objRes={
            data:{
                'OWT_Hdr' : data
            },
            message:{
                'code':0
            }

        };

    }
    catch(err) {
        console.log(err);
    }
    return objRes;
}
function test()
{
    console.log("drl");
}