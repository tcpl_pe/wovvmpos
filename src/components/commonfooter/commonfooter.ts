import { Component } from '@angular/core';
import { SyncProvider } from '../../providers/sync/sync';

/**
 * Generated class for the CommonfooterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'commonfooter',
  templateUrl: 'commonfooter.html'
})
export class CommonfooterComponent {

  constructor(public sync: SyncProvider) { }

}
