import { NgModule } from '@angular/core';
import { CommonheaderComponent } from './commonheader/commonheader';
import { IonicModule } from 'ionic-angular';
import { CommonfooterComponent } from './commonfooter/commonfooter';
@NgModule({
	declarations: [CommonheaderComponent,
    CommonfooterComponent],
	imports: [IonicModule],
	exports: [CommonheaderComponent,
    CommonfooterComponent]
})
export class ComponentsModule {}
