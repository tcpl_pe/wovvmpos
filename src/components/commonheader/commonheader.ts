import { Component } from '@angular/core';
import * as moment from 'moment';
import { ModalController, NavController, PopoverController } from 'ionic-angular';
import { ProfilePage } from '../../pages/profile/profile';
import { NotificationPage } from '../../pages/notification/notification';
import { SyncProvider } from '../../providers/sync/sync';
/**
 * Generated class for the CommonheaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'commonheader',
  templateUrl: 'commonheader.html'
})
export class CommonheaderComponent {

  Time: any;
  Date: any;
  Month: any;
  Year: any;
  selectedDashboard: any;
  register: any = "";
  branch: any = "";
  isOnline: boolean = false;
  constructor(public modalCtrl: ModalController, public navCtrl: NavController,
    public popoverCtrl: PopoverController, public sync: SyncProvider) {
    setInterval(() => {
      this.Time = moment().format('h:mm a');
      this.Date = moment().format('DD');
      this.Month = moment().format('MMM');
      this.Year = moment().format('YYYY');
    }, 1000);
    var currentUser = JSON.parse(localStorage.getItem("currentUser"));
    this.register = currentUser.reg_Prefix;
    this.branch = currentUser.branch_Prefix;
    this.selectedDashboard = "DynamicmenuPage";
  }
  // viewProfile() {
  // this.navCtrl.push("ProfilePage");
  // let profileModal = this.modalCtrl.create('ProfilePage');
  // profileModal.present();
  // let popover = this.popoverCtrl.create('ProfilePage');
  // popover.present();

  // }
  ionViewDidLoad() {
    this.sync.GetLocalData();
  }
  ChangeRoot() {
    this.navCtrl.setRoot(this.sync.selectedDashboard);
  }

  // bhikhu 
  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create("ProfilePage");
    popover.present({
      ev: myEvent
    });
  }
  notificationPopover(notificationEvent) {
    let popover = this.popoverCtrl.create("NotificationPage");
    popover.present({
      ev: notificationEvent
    });
  }
  openCustomer() {
    let CustomerModal = this.modalCtrl.create('ProfilePage');
    CustomerModal.present();
  }

}
