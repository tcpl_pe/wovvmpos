import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { DbProvider } from "../db/db";
import { Observable } from "rxjs";
import { SyncProvider } from "../../providers/sync/sync";

@Injectable()
export class OrderdetailProvider {
  public url: any;
  public tid: any;
  public mode: string;
  public userid: any;
  public accesstoken: any;
  public FinYear_code: any;
  public wovvData: any;
  drl_obj = [];
  owt_dtl_code: any = [];
  party_obj: any = [];
  date: any = new Date();
  constructor(public http: HttpClient, public sync: SyncProvider, public db: DbProvider) {
  }
  getJobcardDetail(jobcard_number) {
    //this.url= window.this.sync.url;
    //this.tid=window.localStorage.getItem('_tid');
    //this.userid=window.localStorage.getItem('userId');
    //this.accesstoken=window.localStorage.getItem('access_token');
    //this.wovvData=JSON.parse(window.localStorage.getItem('wovvsystemView'));
    this.url = this.sync.url;
    this.tid = this.sync._tenantId;
    //this.userid = this.userid;
    this.accesstoken = localStorage.getItem('onlineToken');
    this.userid = localStorage.getItem('UserID');
    this.wovvData = this.sync.wovvSystem;
    ////////debugger
    this.FinYear_code = this.wovvData[0].org_Finyear_Id;
    //console.log(this.wovvData[0].org_Finyear_Id);
    //debugger
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'userId': this.userid,
        'access_Token': this.accesstoken
      })
    };
    let params = {
      "pageUrl": "SD",
      "entityName": "OWT_Hdr",
      "action": "view",
      "event": "multiFilter",
      "multiFilters": [
        {
          "mainEntityType": "",
          "mainEntity": "OWT_Hdr",
          "chiledEntityType": "",
          "childEntity": "OWT_Party op",
          "alias": "OWT_Hdr",
          "fk": "op.owt_Hdr_Id"
        },
        {
          "mainEntityType": "",
          "mainEntity": "OWT_Hdr",
          "chiledEntityType": "",
          "childEntity": "OWT_Dtl od",
          "alias": "OWT_Hdr",
          "fk": "od.owt_Hdr_Id"
        },
        {
          "mainEntityType": "",
          "mainEntity": "OWT_Dtl",
          "chiledEntityType": "",
          "childEntity": "Stock_Book sb",
          "alias": "sb",
          "fk": "od.od_Ref_Entity_Code"
        }
      ],
      "whereClause": [
        {
          "filterKey": "owt_hdr.org_FinYear_Code",
          "filterValue": this.FinYear_code,
          "relation": "=",
          "condition": "AND",
          "fieldValueType": "NA"
        },
        {
          "filterKey": "od_Prod_Ref_Seq",
          "filterValue": "IS NULL",
          "relation": "",
          "condition": "AND",
          "fieldValueType": "NA"
        },
        {
          "filterKey": "(owt_hdr.oh_DocNo",
          "filterValue": jobcard_number,
          "relation": "=",
          "condition": "OR"
        },
        {
          "filterKey": "od.od_Barcode",
          "filterValue": "null)",
          "relation": "=",
          "condition": "AND",
          "fieldValueType": "NA"
        },
        {
          "filterKey": "owt_hdr.oh_Doc_Series_Prefix_Alias",
          "filterValue": "JC",
          "relation": "=",
          "condition": "",
        }
      ],
      "groupBY": [
        " group by od.Id "
      ],
      "selectFields": [
        {
          "fieldName": "owt_hdr.id owt_hdr_id"
        },
        {
          "fieldName": "owt_hdr.financial_year owt_hdr_financial_year"
        },
        {
          "fieldName": "owt_hdr.oh_docdate owt_hdr_oh_docdate"
        },
        {
          "fieldName": "owt_hdr.oh_docno owt_hdr_oh_docno"
        },
        {
          "fieldName": "owt_hdr.oh_doc_aprov_status owt_hdr_oh_doc_aprov_status"
        },
        {
          "fieldName": "owt_hdr.oh_doc_item_qty owt_hdr_oh_doc_item_qty"
        },
        {
          "fieldName": "owt_hdr.oh_doc_net_amt owt_hdr_oh_doc_net_amt"
        },
        {
          "fieldName": "owt_hdr.oh_doc_remark owt_hdr_oh_doc_remark"
        },
        {
          "fieldName": "owt_hdr.oh_doc_series_prefix_alias owt_hdr_oh_doc_series_prefix_alias"
        },
        {
          "fieldName": "owt_hdr.oh_doc_series_prefix_val owt_hdr_oh_doc_series_prefix_val"
        },
        {
          "fieldName": "owt_hdr.oh_doc_status owt_hdr_oh_doc_status"
        },
        {
          "fieldName": "owt_hdr.oh_paid_amt owt_hdr_oh_paid_amt"
        },
        {
          "fieldName": "owt_hdr.oh_pay_duedt owt_hdr_oh_pay_duedt"
        },
        {
          "fieldName": "owt_hdr.oh_promo_val owt_hdr_oh_promo_val"
        },
        {
          "fieldName": "owt_hdr.oh_tax_total owt_hdr_oh_tax_total"
        },
        {
          "fieldName": "owt_hdr.oh_taxable_goods_amt owt_hdr_oh_taxable_goods_amt"
        },
        {
          "fieldName": "owt_hdr.org_finyear_code owt_hdr_org_finyear_code"
        },
        {
          "fieldName": "owt_hdr.branch_code_id owt_hdr_branch_code_id"
        },
        {
          "fieldName": "owt_hdr.deliverytype_head_code_id owt_hdr_deliverytype_head_code_id"
        },
        {
          "fieldName": "owt_hdr.div_code_id owt_hdr_div_code_id"
        },
        {
          "fieldName": "owt_hdr.oh_doc_series_prefix_code_id owt_hdr_oh_doc_series_prefix_code_id"
        },
        {
          "fieldName": "owt_hdr.org_code_id owt_hdr_org_code_id"
        },
        {
          "fieldName": "owt_hdr.register_code_id owt_hdr_register_code_id"
        },
        {
          "fieldName": "owt_hdr.stock_loc_code_id owt_hdr_stock_loc_code_id"
        },
        {
          "fieldName": "owt_hdr.branch_name owt_hdr_branch_name"
        },
        {
          "fieldName": "owt_hdr.stock_loc_name owt_hdr_stock_loc_name"
        },
        {
          "fieldName": "owt_hdr.doc_tax_type_name owt_hdr_doc_tax_type_name"
        },
        {
          "fieldName": "owt_hdr.oh_doc_class_code owt_hdr_oh_doc_class_code"
        },
        {
          "fieldName": "owt_hdr.oh_doc_class_name owt_hdr_oh_doc_class_name"
        },
        {
          "fieldName": "owt_hdr.oh_delivery_amt owt_hdr_oh_delivery_amt"
        },
        {
          "fieldName": "owt_hdr.oh_doc_ref_remark owt_hdr_oh_doc_ref_remark"
        },
        {
          "fieldName": "owt_hdr.oh_paid_status owt_hdr_oh_paid_status"
        },
        {
          "fieldName": "owt_hdr.oh_modifier_amt owt_hdr_oh_modifier_amt"
        },
        {
          "fieldName": "od.id od_id"
        },
        {
          "fieldName": "od.deliverytype_head_code od_deliverytype_head_code"
        },
        {
          "fieldName": "od.is_qty_use od_is_qty_use"
        },
        {
          "fieldName": "od.od_adjusted_qty od_od_adjusted_qty"
        },
        {
          "fieldName": "od.od_branch_name od_od_branch_name"
        },
        {
          "fieldName": "od.od_calc_price od_od_calc_price"
        },
        {
          "fieldName": "od.od_doc_date od_od_doc_date"
        },
        {
          "fieldName": "od.od_doc_no od_od_doc_no"
        },
        {
          "fieldName": "od.od_doc_series_prefix_alias od_od_doc_series_prefix_alias"
        },
        {
          "fieldName": "od.od_doc_series_prefix_code od_od_doc_series_prefix_code"
        },
        {
          "fieldName": "od.od_doc_series_prefix_val od_od_doc_series_prefix_val"
        },
        {
          "fieldName": "od.od_exchange_status od_od_exchange_status"
        },
        {
          "fieldName": "od.od_goods_amt od_od_goods_amt"
        },
        {
          "fieldName": "od.od_mrp od_od_mrp"
        },
        {
          "fieldName": "od.od_proddepartment_code od_od_proddepartment_code"
        },
        {
          "fieldName": "od.od_prodgrp_code od_od_prodgrp_code"
        },
        {
          "fieldName": "od.od_prodmfr_brand_code od_od_prodmfr_brand_code"
        },
        {
          "fieldName": "od.od_prodsgrp_code od_od_prodsgrp_code"
        },
        {
          "fieldName": "od.od_prod_alias od_od_prod_alias"
        },
        {
          "fieldName": "od.od_prod_name od_od_prod_name"
        },
        {
          "fieldName": "od.od_prod_netamt od_od_prod_netamt"
        },
        {
          "fieldName": "od.od_prod_promo_amt od_od_prod_promo_amt"
        },
        {
          "fieldName": "od.od_prod_remark od_od_prod_remark"
        },
        {
          "fieldName": "od.od_prod_status od_od_prod_status"
        },
        {
          "fieldName": "od.od_prod_typclass_code od_od_prod_typclass_code"
        },
        {
          "fieldName": "od.od_prod_typclass_name od_od_prod_typclass_name"
        },
        {
          "fieldName": "od.od_qty od_od_qty"
        },
        {
          "fieldName": "od.od_qty_approve od_od_qty_approve"
        },
        {
          "fieldName": "od.od_rate od_od_rate"
        },
        {
          "fieldName": "od.od_ref_docdate od_od_ref_docdate"
        },
        {
          "fieldName": "od.od_ref_docno od_od_ref_docno"
        },
        {
          "fieldName": "od.od_ref_series_prefix_alias od_od_ref_series_prefix_alias"
        },
        {
          "fieldName": "od.od_ref_series_prefix_val od_od_ref_series_prefix_val"
        },
        {
          "fieldName": "od.od_register_code od_od_register_code"
        },
        {
          "fieldName": "od.od_taxable_goods_amt od_od_taxable_goods_amt"
        },
        {
          "fieldName": "od.branch_code_id od_branch_code_id"
        },
        {
          "fieldName": "od.od_prod_code_id od_od_prod_code_id"
        },
        {
          "fieldName": "od.stock_loc_code_id od_stock_loc_code_id"
        },
        {
          "fieldName": "od.deliverytype_name od_deliverytype_name"
        },
        {
          "fieldName": "od.prod_subgroup_name od_prod_subgroup_name"
        },
        {
          "fieldName": "od.prod_group_name od_prod_group_name"
        },
        {
          "fieldName": "od.proddepartment_name od_proddepartment_name"
        },
        {
          "fieldName": "od.prodmfr_brand_name od_prodmfr_brand_name"
        },
        {
          "fieldName": "od.mfr_head_name od_mfr_head_name"
        },
        {
          "fieldName": "od.mfr_head_code od_mfr_head_code"
        },
        {
          "fieldName": "od.reg_name od_reg_name"
        },
        {
          "fieldName": "od.reg_prefix od_reg_prefix"
        },
        {
          "fieldName": "od.od_prod_due_dt od_od_prod_due_dt"
        },
        {
          "fieldName": "od.od_prod_ref_remark od_od_prod_ref_remark"
        },
        {
          "fieldName": "od.od_delivery_per od_od_delivery_per"
        },
        {
          "fieldName": "od.od_delivery_amt od_od_delivery_amt"
        },
        {
          "fieldName": "od.od_delivery_status_code od_od_delivery_status_code"
        },
        {
          "fieldName": "od.od_delivery_status_name od_od_delivery_status_name"
        },
        {
          "fieldName": "od.od_job_class_code od_od_job_class_code"
        },
        {
          "fieldName": "od.od_job_class_name od_od_job_class_name"
        },
        {
          "fieldName": "od.deliverytype_head_name od_deliverytype_head_name"
        },
        {
          "fieldName": "od.is_home_delivery od_is_home_delivery"
        },
        {
          "fieldName": "od.od_barcode od_od_barcode"
        },
        {
          "fieldName": "od.od_prod_modifier_amt od_od_prod_modifier_amt"
        },
        {
          "fieldName": "od.od_prod_promo_code od_od_prod_promo_code"
        },
        {
          "fieldName": "od.od_ref_entity_code od_od_ref_entity_code"
        },
        {
          "fieldName": "op.id op_id"
        },
        {
          "fieldName": "op.is_tax_registered op_is_tax_registered"
        },
        {
          "fieldName": "op.op_party_add_area op_op_party_add_area"
        },
        {
          "fieldName": "op.op_party_add_door op_op_party_add_door"
        },
        {
          "fieldName": "op.op_party_add_landmark op_op_party_add_landmark"
        },
        {
          "fieldName": "op.op_party_add_street op_op_party_add_street"
        },
        {
          "fieldName": "op.op_party_mobno op_op_party_mobno"
        },
        {
          "fieldName": "op.op_party_name op_op_party_name"
        },
        {
          "fieldName": "op.op_party_phno op_op_party_phno"
        },
        {
          "fieldName": "op.op_party_pincode op_op_party_pincode"
        },
        {
          "fieldName": "op.op_party_gst_no op_op_party_gst_no"
        },
        {
          "fieldName": "op.city_code op_city_code"
        },
        {
          "fieldName": "op.city_name op_city_name"
        },
        {
          "fieldName": "op.state_code op_state_code"
        },
        {
          "fieldName": "op.state_name op_state_name"
        },
        {
          "fieldName": "op.place_of_supply op_place_of_supply"
        },
        {
          "fieldName": "op.country_code op_country_code"
        },
        {
          "fieldName": "op.country_name op_country_name"
        },
        {
          "fieldName": "op.area_name op_area_name"
        },
        {
          "fieldName": "op.op_invoice_to op_op_invoice_to"
        },
        {
          "fieldName": "op.op_party_email op_op_party_email"
        },
        {
          "fieldName": "sb.id sb_id"
        },
        {
          "fieldName": "sb.is_added sb_is_added"
        },
        {
          "fieldName": "sb.is_qty_used sb_is_qty_used"
        },
        {
          "fieldName": "sb.psd_av_caption sb_psd_av_caption"
        },
        {
          "fieldName": "sb.psd_attribute_val sb_psd_attribute_val"
        },
        {
          "fieldName": "sb.psd_available_qty sb_psd_available_qty"
        },
        {
          "fieldName": "sb.psd_qty sb_psd_qty"
        },
        {
          "fieldName": "sb.psd_bfr_tax_price sb_psd_bfr_tax_price"
        },
        {
          "fieldName": "sb.psd_branch_name sb_psd_branch_name"
        },
        {
          "fieldName": "sb.psd_ref_trn_row_code sb_psd_ref_trn_row_code"
        },
        {
          "fieldName": "od.od_LandingCost od_od_LandingCost"
        },
        {
          "fieldName": "owt_hdr.oh_Doc_Series_Nxtno_Code owt_hdr_oh_Doc_Series_Nxtno_Code"
        },
        {
          "fieldName": "od.prod_Parent_Code od_prod_Parent_Code"
        },
        {
          "fieldName": "od.od_PurPrice od_od_PurPrice"
        },
        {
          "fieldName": "(SELECT GROUP_CONCAT(md.od_Attribute_Val) FROM owt_dtl md WHERE  md.od_Prod_Ref_Seq = od.od_Prod_Seq) prod_Modifier_Detail"
        }
      ]
    }
    return this.http.post(this.url + '?tenantId=' + this.tid, params, options)
      .map(response => {
        {
          return response;
        };
      })
      .catch(error => Observable.throw(
        { status: error.status, Errors: error }
      ));
  }
  dispatch(data) {
    this.url = this.sync.url;
    this.tid = this.sync._tenantId;
    var ID = '';
    var od_od_ref_entity_code = '';
    for (let key in data) {
      let value = data[key]['od_id'];
      let entity_code = data[key]['od_od_ref_entity_code'];
      ID += value + ',';
      od_od_ref_entity_code += entity_code + ',';
    }
    ID = ID.substring(0, ID.length - 1);
    od_od_ref_entity_code = od_od_ref_entity_code.substring(0, od_od_ref_entity_code.length - 1);
    ////////debugger
    //this.userid = this.userid;
    this.accesstoken = localStorage.getItem('onlineToken');
    this.userid = localStorage.getItem('UserID');
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'userId': this.userid,
        'access_Token': this.accesstoken
      })
    };
    let params = {
      "pageUrl": "JC",
      "entityName": "OWT_Dtl",
      "action": "update",
      "event": "dispatch",
      "whereClause": [
        {
          "filterKey": "ID",
          "filterValue": "" + ID + "",
          "relation": "IN",
          "condition": "AND"
        },
        {
          "filterKey": "od_Ref_Entity_Code",
          "filterValue": "" + od_od_ref_entity_code + "",
          "relation": "IN",
          "condition": ""
        }
      ],
      "setFields": [
        {
          "filterKey": "od_JOB_Class_Name",
          "filterValue": "Dispatch",
          "relation": "="
        }
      ]
    }
    return this.http.post(this.url + '?tenantId=' + this.tid, params, options)
      .map(response => {
        {
          return response;
        };
      })
      .catch(error => Observable.throw(
        { status: error.status, Errors: error }
      ));
  }
  docNext() {
    this.url = this.sync.url;
    this.tid = this.sync._tenantId;
    //this.userid = this.userid;
    this.accesstoken = localStorage.getItem('onlineToken');
    this.userid = localStorage.getItem('UserID');
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'userId': this.userid,
        'access_Token': this.accesstoken
      })
    };
    //////debugger
    var wovvsystemData = this.sync.wovvSystem;
    var org = this.sync.orgCode;
    var finYear = wovvsystemData[0].org_Finyear_Id;
    var branch = this.sync.branchCode;

    let params = {
      "entityName": "DocNumber",
      "pageUrl": "SalesInvoice",
      "action": "view",
      "event": "selectFilter",
      "whereClause": [
        {
          "filterKey": "entity_Rowid",
          "filterValue": "" + branch + "",
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "entity_Name",
          "filterValue": "Branch_MsT",
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "org_Code_Id",
          "filterValue": "" + org + "",
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "org_FinYear_Code_Id",
          "filterValue": finYear,
          "relation": "=",
          "condition": ""
        }
      ],
      "selectFields": [
        {
          "fieldName": "dsnn_Id"
        },
        {
          "fieldName": "active_Status"
        },
        {
          "fieldName": "approve_Status"
        },
        {
          "fieldName": "doc_Next_No"
        },
        {
          "fieldName": "doc_NxtNo_Party_Code"
        },
        {
          "fieldName": "doc_Prefix_Short"
        },
        {
          "fieldName": "entity_Rowid"
        },
        {
          "fieldName": "entity_Name"
        },
        {
          "fieldName": "is_System_Default"
        },
        {
          "fieldName": "doc_Series_Gen_Code_Id"
        },
        {
          "fieldName": "doc_Series_Prefix_Code_Id"
        },
        {
          "fieldName": "org_Code_Id"
        },
        {
          "fieldName": "org_FinYear_Code_Id"
        },
        {
          "fieldName": "doc_Series_Gen_Name"
        },
        {
          "fieldName": "doc_Series_Prefix_Alias"
        },
        {
          "fieldName": "doc_Series_Prefix_Name"
        },
        {
          "fieldName": "tid"
        }
      ]
    }
    return this.http.post(this.url + '?tenantId=' + this.tid, params, options)
      .map(response => {
        {
          return response;
          //console.log(response.data);
        };
      })
      .catch(error => Observable.throw(
        { status: error.status, Errors: error }
      ));


  }
  getJC_amount(JC_owt_hdr_id) {
    this.url = this.sync.url;
    this.tid = this.sync._tenantId;
    //this.userid = this.userid;
    this.accesstoken = localStorage.getItem('onlineToken');
    this.userid = localStorage.getItem('UserID');
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'userId': this.userid,
        'access_Token': this.accesstoken
      })
    };
    let params = {
      "pageUrl": "SD",
      "entityName": "OWT_Hdr",
      "action": "view",
      "event": "multiFilter",
      "multiFilters": [
        {
          "mainEntityType": "",
          "mainEntity": "OWT_Hdr",
          "chiledEntityType": "",
          "childEntity": "OWT_Dtl rd",
          "alias": "owt_hdr",
          "fk": "rd.owt_Hdr_Id"
        },
        {
          "mainEntityType": "",
          "mainEntity": "OWT_Dtl",
          "chiledEntityType": "",
          "childEntity": "OWT_Dtl d",
          "alias": "d",
          "fk": "rd.od_Ref_RowNo"
        },
        {
          "mainEntityType": "",
          "mainEntity": "OWT_Dtl",
          "chiledEntityType": "",
          "childEntity": "OWT_Hdr h",
          "alias": "h",
          "fk": "d.owt_Hdr_Id"
        }
      ],
      "whereClause": [
        {
          "filterKey": "h.Id",
          "filterValue": JC_owt_hdr_id,
          "relation": "=",
          "condition": ""
        }
      ],
      "selectFields": [
        {
          "fieldName": "h.Id jc_Id"
        },
        {
          "fieldName": "h.oh_DocDate jc_DocDate"
        },
        {
          "fieldName": "h.oh_DocNo jc_DocNo"
        },
        {
          "fieldName": "h.oh_Doc_Item_Qty jc_Qty"
        },
        {
          "fieldName": "h.oh_Doc_Net_Amt jc_Amount"
        },
        {
          "fieldName": "h.oh_Paid_Amt jc_PaidAmt"
        },
        {
          "fieldName": "h.oh_Taxable_Goods_Amt jc_Total"
        },
        {
          "fieldName": "h.oh_Tax_total jc_TotalTax"
        },
        {
          "fieldName": "h.oh_Total_Disc_Val jc_DiscAmt"
        },
        {
          "fieldName": "h.oh_Doc_Net_Amt jc_NetAmt"
        },
        {
          "fieldName": "count(distinct (owt_hdr.Id)) jr_Count"
        },
        {
          "fieldName": "sum(rd.od_Qty) jr_Qty"
        },
        {
          "fieldName": "sum(distinct (owt_hdr.oh_Doc_Net_Amt)) jr_Amount"
        },
        {
          "fieldName": "sum(distinct (owt_hdr.oh_Paid_Amt)) jr_PaidAmt"
        },
        {
          "fieldName": "sum(distinct (owt_hdr.oh_Doc_Net_Amt - owt_hdr.oh_Paid_Amt)) jr_PendAmt"
        },
        {
          "fieldName": "h.oh_Doc_Net_Amt - sum(distinct (owt_hdr.oh_Doc_Net_Amt - owt_hdr.oh_Paid_Amt)) - h.oh_Paid_Amt jc_PendAmt"
        }
      ]
    }
    return this.http.post(this.url + '?tenantId=' + this.tid, params, options)
      .map(response => {
        {
          return response;
          //console.log(response.data);
        };
      })
      .catch(error => Observable.throw(
        { status: error.status, Errors: error }
      ));
  }
  StockBook() {
    this.url = this.sync.url;
    this.tid = this.sync._tenantId;
    //this.userid = this.userid;
    this.accesstoken = localStorage.getItem('onlineToken');
    this.userid = localStorage.getItem('UserID');
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'userId': this.userid,
        'access_Token': this.accesstoken
      })
    };
    var org = this.sync.orgCode;
    var branch = this.sync.branchCode;
    var div = this.sync.divCode;
    //var tenantId = this.sync.tenantId;
    var stockID = this.sync.stockLocCode;

    let params = {
      "entityName": "Stock_Book",
      "pageUrl": "JobCardReturn",
      "action": "view",
      "event": "selectFilter",
      "whereClause": [
        {
          "filterKey": "psd_Branch_Code_Id",
          "filterValue": "" + branch + "",
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "psd_Div_Code_Id",
          "filterValue": "" + div + "",
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "psd_Org_Code_Id",
          "filterValue": "" + org + "",
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "approve_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "is_Qty_Used",
          "filterValue": 0,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "psd_Reg_Type_Code",
          "filterValue": 1,
          "relation": "!=",
          "condition": "AND"
        },
        {
          "filterKey": "is_Added",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "psd_Stock_Loc_Code_Id",
          "filterValue": "" + stockID + "",
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "psd_Stock_Class_Name",
          "filterValue": "Job Work",
          "relation": "=",
          "condition": ""
        }
      ],
      "selectFields": [
        {
          "fieldName": "Id"
        },
        {
          "fieldName": "psd_Prod_Name"
        },
        {
          "fieldName": "psd_Prod_Code_Id"
        },
        {
          "fieldName": "psd_Stock_Loc_Code_Id"
        },
        {
          "fieldName": "psd_Available_Qty"
        },
        {
          "fieldName": "psd_Stock_Class_Name"
        }
      ]
    }
    return this.http.post(this.url + '?tenantId=' + this.tid, params, options)
      .map(response => {
        {
          return response;
          //console.log(response.data);
        };
      })
      .catch(error => Observable.throw(
        { status: error.status, Errors: error }
      ));
  }
  PartyRpt(selectedJC) {
    this.url = this.sync.url;
    this.tid = this.sync._tenantId;
    //this.userid = this.userid;
    this.accesstoken = localStorage.getItem('onlineToken');
    this.userid = localStorage.getItem('UserID');
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'userId': this.userid,
        'access_Token': this.accesstoken
      })
    };
    //////debugger
    var org = this.sync.orgCode;
    var branch = this.sync.branchCode;
    var tenantId = this.sync.tenantId;
    let params = {
      "pageUrl": "Partyrpt",
      "entityName": "Partyrpt",
      "action": "view",
      "event": "selectFilter",
      "whereClause": [
        {
          "filterKey": "tid",
          "filterValue": tenantId,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "branch_Code_Id",
          "filterValue": branch,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "org_Code_Id",
          "filterValue": org,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "party_Name",
          "filterValue": selectedJC[0].op_op_party_name,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "payTyp_Approve_Status",
          "filterValue": "1",
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "pcpt_Status",
          "filterValue": "1",
          "relation": "=",
          "condition": ""
        }
      ],
      "selectFields": [
        {
          "fieldName": "party_Child_Id"
        },
        {
          "fieldName": "party_SaleDisc_Per"
        },
        {
          "fieldName": "approve_Status"
        },
        {
          "fieldName": "branch_Code_Id"
        },
        {
          "fieldName": "party_Code_Id"
        },
        {
          "fieldName": "party_Tax_Type_Code_Id"
        },
        {
          "fieldName": "tax_Code_Pur_Id"
        },
        {
          "fieldName": "tax_Code_Sale_Id"
        },
        {
          "fieldName": "party_Head_Id"
        },
        {
          "fieldName": "is_System_Default"
        },
        {
          "fieldName": "party_Add_Door"
        },
        {
          "fieldName": "party_Add_Landmark"
        },
        {
          "fieldName": "party_Add_Street"
        },
        {
          "fieldName": "party_Alias"
        },
        {
          "fieldName": "party_Area_Pincode"
        },
        {
          "fieldName": "party_GST_No"
        },
        {
          "fieldName": "party_MobNo"
        },
        {
          "fieldName": "party_email"
        },
        {
          "fieldName": "party_Name"
        },
        {
          "fieldName": "party_Other_Reg_No"
        },
        {
          "fieldName": "party_PhNo"
        },
        {
          "fieldName": "party_Relation_Name"
        },
        {
          "fieldName": "party_Status"
        },
        {
          "fieldName": "org_Code_Id"
        },
        {
          "fieldName": "party_Delivery_Type_Id"
        },
        {
          "fieldName": "party_Relation_Code_Id"
        },
        {
          "fieldName": "party_SalMan_Code_Id"
        },
        {
          "fieldName": "area_Name"
        },
        {
          "fieldName": "state_Name"
        },
        {
          "fieldName": "party_Contact_Add_door"
        },
        {
          "fieldName": "party_Contact_Add_Street"
        },
        {
          "fieldName": "party_Contact_Area_Name"
        },
        {
          "fieldName": "party_Contact_City_Name"
        },
        {
          "fieldName": "party_Contact_state_Name"
        },
        {
          "fieldName": "party_Contact_Country_Name"
        },
        {
          "fieldName": "pay_Typ_Id"
        },
        {
          "fieldName": "pay_Typ_Name"
        },
        {
          "fieldName": "pay_Typ_Arrange_Order"
        },
        {
          "fieldName": "party_Tax_Type_Name"
        },
        {
          "fieldName": "tid"
        },
        {
          "fieldName": "is_Tax_Registered"
        },
        {
          "fieldName": "empPosition_Code_Id"
        },
        {
          "fieldName": "Id"
        },
        {
          "fieldName": "brnach_Name"
        },
        {
          "fieldName": "party_Salesman_Name"
        },
        {
          "fieldName": "deliveryType_Name"
        },
        {
          "fieldName": "city_Code_Id"
        },
        {
          "fieldName": "city_Name"
        },
        {
          "fieldName": "country_Code_Id"
        },
        {
          "fieldName": "country_Name"
        },
        {
          "fieldName": "place_Of_Supply"
        },
        {
          "fieldName": "state_Code_Id"
        },
        {
          "fieldName": "pay_Mode_Code"
        },
        {
          "fieldName": "pay_Mode_Name"
        },
        {
          "fieldName": "ptt_acc_Ledger_Code_Id"
        },
        {
          "fieldName": "ptt_acc_Ledger_Name"
        },
        {
          "fieldName": "bank_Name"
        },
        {
          "fieldName": "pBR_Bank_Code_Id"
        }
      ],
      "paginationFields": {
        "firstResult": 0,
        "maxResults": 2500
      }
    }
    return this.http.post(this.url + '?tenantId=' + this.tid, params, options)
      .map(response => {
        {
          return response;
          //console.log(response.data);
        };
      })
      .catch(error => Observable.throw(
        { status: error.status, Errors: error }
      ));
  }
  CatalogRpt(partyRptData) {
    this.url = this.sync.url;
    this.tid = this.sync._tenantId;
    //this.userid = this.userid;
    this.accesstoken = localStorage.getItem('onlineToken');
    this.userid = localStorage.getItem('UserID');
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'userId': this.userid,
        'access_Token': this.accesstoken
      })
    };
    //////debugger
    var org = this.sync.orgCode;
    var branch = this.sync.branchCode;
    var div = this.sync.divCode;
    var tenantId = this.sync.tenantId;

    let params = {
      "pageUrl": "Catlogrpt",
      "entityName": "Catlogrpt",
      "action": "view",
      "event": "selectFilter",
      "whereClause": [
        {
          "filterKey": "tid",
          "filterValue": tenantId,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "div_Code_Id",
          "filterValue": div,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "prod_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "prodC_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "branch_Code_Id",
          "filterValue": branch,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "prod_Outward_Allow",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "org_Code_Id",
          "filterValue": org,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "prodC_Default",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "prod_TypCls_Prefix",
          "filterValue": 1031,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "sale_Subclass_Tax_Typ_Code_Id",
          "filterValue": partyRptData[0].party_Tax_Type_Code_Id,
          "relation": "=",
          "condition": ""
        }
      ],
      "selectFields": [
        {
          "fieldName": "prod_Child_Id"
        },
        {
          "fieldName": "prodC_Status"
        },
        {
          "fieldName": "branch_Code_Id"
        },
        {
          "fieldName": "div_Code_Id"
        },
        {
          "fieldName": "tax_Code_Pur_Id"
        },
        {
          "fieldName": "tax_Code_Sale_Id"
        },
        {
          "fieldName": "prod_Code_Id"
        },
        {
          "fieldName": "prod_Hdr_Code_Id"
        },
        {
          "fieldName": "prod_Name_Desc"
        },
        {
          "fieldName": "prod_Image_Path"
        },
        {
          "fieldName": "prod_MRP"
        },
        {
          "fieldName": "prod_Price"
        },
        {
          "fieldName": "prod_PurPrice"
        },
        {
          "fieldName": "prod_LandingCost"
        },
        {
          "fieldName": "prod_Status"
        },
        {
          "fieldName": "prod_Is_ParentItem"
        },
        {
          "fieldName": "prod_Is_ingrediant"
        },
        {
          "fieldName": "prod_Inventory_Allow"
        },
        {
          "fieldName": "prod_Outward_Allow"
        },
        {
          "fieldName": "prod_NegativeStock_Bill_allow"
        },
        {
          "fieldName": "prod_TypClass_Code_Id"
        },
        {
          "fieldName": "mfr_Brand_Code_Id"
        },
        {
          "fieldName": "prod_SGrup_Code_Id"
        },
        {
          "fieldName": "uom_Group_Code_Id"
        },
        {
          "fieldName": "uom_Dtl_Outward_Id"
        },
        {
          "fieldName": "org_Code_Id"
        },
        {
          "fieldName": "prod_TypCls_Name"
        },
        {
          "fieldName": "mfr_Brand_Name"
        },
        {
          "fieldName": "prod_SubGroup_Name"
        },
        {
          "fieldName": "prod_Grup_Code_Id"
        },
        {
          "fieldName": "prod_Group_Name"
        },
        {
          "fieldName": "prod_Dept_Code_Id"
        },
        {
          "fieldName": "prod_Dept_Name"
        },
        {
          "fieldName": "tax_Cls_Name"
        },
        {
          "fieldName": "tax_1_Cal_Rate"
        },
        {
          "fieldName": "tax_2_Cal_Rate"
        },
        {
          "fieldName": "tax_3_Cal_Rate"
        },
        {
          "fieldName": "tax_4_Cal_Rate"
        },
        {
          "fieldName": "tax_5_Cal_Rate"
        },
        {
          "fieldName": "tax_Inclusive"
        },
        {
          "fieldName": "tax_Total"
        },
        {
          "fieldName": "tax_1_Ledger_Code_Id"
        },
        {
          "fieldName": "tax_2_Ledger_Code_Id"
        },
        {
          "fieldName": "tax_3_Ledger_Code_Id"
        },
        {
          "fieldName": "tax_4_Ledger_Code_Id"
        },
        {
          "fieldName": "tax_5_Ledger_Code_Id"
        },
        {
          "fieldName": "sale_Subclass_Tax_Typ_Code_Id"
        },
        {
          "fieldName": "tax_Type_Name"
        },
        {
          "fieldName": "pur_Taxclass_Name"
        },
        {
          "fieldName": "pur_Subclass_Tax_1_Cal_Rate"
        },
        {
          "fieldName": "pur_Subclass_Tax_2_Cal_Rate"
        },
        {
          "fieldName": "pur_Subclass_Tax_3_Cal_Rate"
        },
        {
          "fieldName": "pur_Subclass_Tax_4_Cal_Rate"
        },
        {
          "fieldName": "pur_Subclass_Tax_5_Cal_Rate"
        },
        {
          "fieldName": "pur_Subclass_Tax_Inclusive"
        },
        {
          "fieldName": "pur_Subclass_Tax_Prn_Symbol"
        },
        {
          "fieldName": "pur_Subclass_Tax_Total"
        },
        {
          "fieldName": "pur_Subclass_Tax_1_Ledger_Code_Id"
        },
        {
          "fieldName": "pur_Subclass_Tax_2_Ledger_Code_Id"
        },
        {
          "fieldName": "pur_Subclass_Tax_3_Ledger_Code_Id"
        },
        {
          "fieldName": "pur_Subclass_Tax_4_Ledger_Code_Id"
        },
        {
          "fieldName": "pur_Subclass_Tax_5_Ledger_Code_Id"
        },
        {
          "fieldName": "branch_name"
        },
        {
          "fieldName": "prod_Allow_Disc_YN"
        },
        {
          "fieldName": "prod_PriceChange_Allow"
        },
        {
          "fieldName": "prod_Sale_Default_QTY"
        },
        {
          "fieldName": "prod_Sale_Disc_Limit"
        },
        {
          "fieldName": "prod_Sale_Disc_Per"
        },
        {
          "fieldName": "prod_Sale_Free_YN"
        },
        {
          "fieldName": "prod_Sale_Max_QTY"
        },
        {
          "fieldName": "prod_Stock_Qty"
        },
        {
          "fieldName": "tax_1_Prn_Symbol"
        },
        {
          "fieldName": "tax_2_Prn_Symbol"
        },
        {
          "fieldName": "tax_3_Prn_Symbol"
        },
        {
          "fieldName": "tax_4_Prn_Symbol"
        },
        {
          "fieldName": "tax_5_Prn_Symbol"
        },
        {
          "fieldName": "prod_UOM_YN"
        },
        {
          "fieldName": "prod_Alias"
        },
        {
          "fieldName": "prod_TypCls_Prefix"
        },
        {
          "fieldName": "prod_AV_Caption"
        },
        {
          "fieldName": "prod_ADT_Caption"
        },
        {
          "fieldName": "prod_MDT_Caption"
        },
        {
          "fieldName": "prod_Parent_Code"
        },
        {
          "fieldName": "vrnt_Group_Code_Id"
        },
        {
          "fieldName": "vrnt_Type1_Code"
        },
        {
          "fieldName": "vrnt_Type2_Code"
        },
        {
          "fieldName": "vrnt_Type3_Code"
        },
        {
          "fieldName": "tid"
        },
        {
          "fieldName": "pur_Subclass_Code_Id"
        },
        {
          "fieldName": "sale_Subclass_Code_Id"
        },
        {
          "fieldName": "prodC_Default"
        },
        {
          "fieldName": "prod_EAN_Bcode"
        },
        {
          "fieldName": "prod_DecimalQty"
        }
      ],
      "paginationFields": {
        "firstResult": 0,
        "maxResults": 5000
      }
    }
    return this.http.post(this.url + '?tenantId=' + this.tid, params, options)
      .map(response => {
        {
          return response;
          //console.log(response.data);
        };
      })
      .catch(error => Observable.throw(
        { status: error.status, Errors: error }
      ));
  }
  returnGarment(selectedJC, docNumber, CatalogRptData, PartyRpt) {
    debugger
    this.owt_dtl_code = [];
    this.drl_obj = [];
    this.party_obj = [];
    this.url = this.sync.url;
    this.tid = this.sync._tenantId;
    //this.userid = this.userid;
    this.accesstoken = localStorage.getItem('onlineToken');
    this.userid = localStorage.getItem('UserID');
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'userId': this.userid,
        'access_Token': this.accesstoken
      })
    };
    //var detailArray = [];
    var timestamp = new Date().getTime();
    var todayDate = new Date();
    var currDate = todayDate.getDate() + '/' + (todayDate.getMonth() + 1) + '/' + todayDate.getFullYear();

    //////debugger
    for (let index = 0; index < selectedJC.length; index++) {
      var stckClass = [];
      if (selectedJC[index].od_od_job_class_name == 'Received') {
        stckClass = this.sync.stockClassData.filter(function (value) {
          return value['stock_Class_Type_Name'] == "Prod Job-Work Class", value['stock_Class_Name'] == "Return";
        });
      }
      else{
         stckClass = this.sync.stockClassData.filter(function (value) {
          return value['stock_Class_Type_Name'] == "Prod Job-Work Class", value['stock_Class_Name'] == "Damage Return";
        });
      }
      var ow = {
        "od_Doc_Series_Prefix_Code": docNumber.doc_Series_Prefix_Code_Id,
        "od_Ref_Series_Gen_Val": 0,
        "od_Doc_No": docNumber.doc_Next_No,
        "od_Doc_Series_Prefix_Alias": "" + docNumber.doc_Series_Prefix_Alias + "",
        "od_Doc_Series_Prefix_Val": "" + docNumber.doc_Series_Prefix_Name + "",
        "od_Doc_Date": todayDate,
        "od_Calc_Price": 0,
        "od_Bfr_Tax_Price": 0,
        "od_Qty": selectedJC[index].od_od_qty, //multifilter
        "od_Free_Tag": 0,
        "od_Prod_QtyXPrice_Amt": 0,
        "od_Prod_Disc_Per": 0,  //percent
        "od_Prod_Disc_Amt": 0, //amt
        "od_Prod_Sp_Disc_Per": 0,
        "od_Prod_Sp_Disc_Amt": 0,
        "od_Prod_Promo_Per": 0,
        "od_Prod_Promo_Amt": 0,
        "od_Inv_Disc_Per": 0,
        "od_Inv_Disc_Amt": 0,
        "od_Coup_Per": 0,
        "od_Coup_Amt": 0,
        "od_Fright_Rate": 0,
        "od_Fright_Amt": 0,
        "od_Prod_Tax_Amt": 0,
        "od_Prod_Tax1_Amt": 0,
        "od_Prod_Tax2_Amt": 0,
        "od_Prod_Tax3_Amt": 0,
        "od_Prod_Tax4_Amt": 0,
        "od_Prod_Tax5_Amt": 0,
        "od_one_Per": 0,
        "od_one_Val": 0,
        "od_Two_Per": 0,
        "od_Two_Val": 0,
        "od_Three_Per": 0,
        "od_Three_Val": 0,
        "od_Four_Per": 0,
        "od_Four_Val": 0,
        "od_Five_Per": 0,
        "od_Five_Val": 0,
        "od_Six_Per": 0,
        "od_Six_Val": 0,
        "od_Seven_Per": 0,
        "od_Seven_Val": 0,
        "od_Eight_Per": 0,
        "od_Eight_Val": 0,
        "od_Nine_Per": 0,
        "od_Nine_Val": 0,
        "od_Ten_Per": 0,
        "od_Ten_Val": 0,
        "od_SalesMan_Instv_Per": 0,
        "od_SalesMan_Instv_Amt": 0,
        "od_UOM_Calculate_Val": 0,
        "od_Taxable_Goods_Amt": 0,
        "od_Goods_Amt": 0,
        "od_Prod_NetAmt": 0,
        "od_eanupc_Code": null,
        "od_TS_Added": new Date(),
        "od_TS_Edited": new Date(),
        "oWTDtl_user_Code_Add": this.userid,//session
        "oWTDtl_user_Code_Updt": this.userid,//session
        "od_Ref_Series_Prefix_Code": selectedJC[index].od_od_doc_series_prefix_code,//multifilter od_od_doc_series_prefix_code
        "od_Ref_DocDate": selectedJC[index].od_od_doc_date,//multifilter od_od_doc_date
        "od_Ref_DocNo": selectedJC[index].od_od_doc_no,//MF od_od_doc_no
        "od_Prod_Remark": null,
        "sequenceNo": 0,  //figure out how
        "od_StockBook_Ref_Code": selectedJC[index].sb_id,  //pass from stockbook
        "od_Register_Code": this.sync.registerCode,//session
        "od_Reg_Type_Code": 1,
        "od_Ref_Series_Prefix_Alias": selectedJC[index].od_od_doc_series_prefix_alias,//MF od_od_doc_series_prefix_alias
        "od_Stock_Class_Name": "Job Work",// stock class call
        "od_Adjusted_QTY": 0,
        "od_ADT_Caption": "NA",
        "od_Attribute_DT": ("0" + this.date.getDate()).slice(-2) + '/' + ("0" + (this.date.getMonth() + 1)).slice(-2) + '/' + this.date.getFullYear(),
        "od_Attribute_Val": "NA",
        "od_AV_Caption": "NA",
        "od_Branch_Name": this.sync.wovvSystem[0].branch_Name,
        "od_MDT_Caption": "NA",
        "od_MFR_DT": ("0" + this.date.getDate()).slice(-2) + '/' + ("0" + (this.date.getMonth() + 1)).slice(-2) + '/' + this.date.getFullYear(),
        "od_Prod_UOM_YN": CatalogRptData[0].prod_UOM_YN,
        "od_Ref_Doc_Series_Nxtno_Code": selectedJC[index].owt_hdr_oh_Doc_Series_Nxtno_Code,//MF oh.Doc_Series_Nxtno_Code
        "od_Reg_Type_Name": 1,
        "od_Stock_Loc_Name": this.sync.wovvSystem[0].stock_Loc_Name,
        "od_VGroup_Code": CatalogRptData[0].vrnt_Group_Code_Id,
        "od_VType1_Code": CatalogRptData[0].vrnt_Type1_Code,
        "od_VType2_Code": CatalogRptData[0].vrnt_Type2_Code,
        "od_VType3_Code": CatalogRptData[0].vrnt_Type3_Code,
        "prod_Parent_Code": selectedJC[index].od_prod_Parent_Code,//MF od.prod_Parent_Code
        "stock_Loc_Code": this.sync.stockLocCode,
        "od_Stock_Class_Code": stckClass[0].stock_Class_Id,
        "od_Prod_TypClass_Code": CatalogRptData[0].prod_TypClass_Code_Id,
        "od_Prod_Name": selectedJC[index].od_od_prod_name,
        "od_MRP": selectedJC[index].od_od_mrp,
        "od_Rate": selectedJC[index].od_od_rate,
        "od_Tax_Code": CatalogRptData[0].tax_Code_Sale_Id,
        "od_Tax_Rate": CatalogRptData[0].tax_Total,  // not present
        "od_Tax1_Code": CatalogRptData[0].tax_1_Ledger_Code_Id,
        "od_Tax1_Rate": CatalogRptData[0].tax_1_Cal_Rate,
        "od_Tax2_Code": CatalogRptData[0].tax_2_Ledger_Code_Id,
        "od_Tax2_Rate": CatalogRptData[0].tax_2_Cal_Rate,
        "od_Tax3_Code": CatalogRptData[0].tax_3_Ledger_Code_Id,
        "od_Tax3_Rate": CatalogRptData[0].tax_3_Cal_Rate,
        "od_Tax4_Code": CatalogRptData[0].tax_4_Ledger_Code_Id,
        "od_Tax4_Rate": CatalogRptData[0].tax_4_Cal_Rate,
        "od_Tax5_Code": CatalogRptData[0].tax_5_Ledger_Code_Id,
        "od_Tax5_Rate": CatalogRptData[0].tax_5_Cal_Rate,
        "od_Tax_Inclusive": CatalogRptData[0].tax_Inclusive,
        "od_ProdSGrp_Code": selectedJC[index].od_od_prodsgrp_code,
        "od_ProdGrp_Code": selectedJC[index].od_od_prodgrp_code,
        "od_ProdDepartment_Code": CatalogRptData[0].prod_Dept_Code_Id,
        "od_ProdMFR_Brand_Code": CatalogRptData[0].mfr_Brand_Code_Id,
        "od_Tax_Prn_Symbol": CatalogRptData[0].tax_Cls_Name,
        //"od_Prod_Image_Path": item.prod_Image_Path,
        "od_Prod_TypClass_Prefix": CatalogRptData[0].prod_TypCls_Prefix,
        "od_Prod_Status": CatalogRptData[0].prod_Status,
        "od_Prod_TypClass_Name": CatalogRptData[0].prod_TypCls_Name,
        "od_Tax_SubClass_Code": CatalogRptData[0].sale_Subclass_Code_Id,
        "od_LandingCost": selectedJC[index].od_od_LandingCost,
        "od_PurPrice": selectedJC[index].od_od_PurPrice,
        "deliveryType_Name": "",
        "od_Delivery_Per": 0,
        "od_Delivery_Amt": 0,
        "od_Delivery_Rate": 0,
        "od_Delivery_Status_Code": 0,
        "od_Delivery_Status_Name": "",
        "deliveryType_Head_Name": "",
        "is_Home_Delivery": "",
        "od_Prod_Code": selectedJC[index].od_od_prod_code_id
      };
      this.owt_dtl_code.push(ow);


    }
    this.party_obj = {
      "op_DocDate": new Date(),
      "op_user_Code_Updt": this.userid,
      "op_user_Code_Add": this.userid,
      "op_TS_Added": new Date(),
      "op_TS_Edited": new Date(),
      "op_Layout_Code": 0,
      "op_Layout_Desc": null,
      "op_Table_Layout_Code": 0,
      "op_Layout_Table_Desc": null,
      "op_Table_Prefix": null,
      "op_Number_Of_Guest": 0,
      "is_Tax_Registered": selectedJC[0].is_Tax_Registered,
      "op_Party_Code": selectedJC[0].op_id,
      "op_Party_Name": selectedJC[0].party_Name,
      "op_Party_Add_Door": selectedJC[0].party_Add_Door,
      "op_Party_MobNo": selectedJC[0].party_MobNo,
      "op_Party_Tax_Type_Code": PartyRpt[0].party_Tax_Type_Code_Id,
      "op_Party_Relation_Code": PartyRpt[0].party_Relation_Code_Id,
      "op_Party_Relation_Name": PartyRpt[0].party_Relation_Name,
      "op_Party_Tax_Type_Name": PartyRpt[0].party_Tax_Type_Name,
      "op_DocNo": docNumber.doc_Next_No,
    }
    this.drl_obj = [
      {
        "owt_Dtl_Code": this.owt_dtl_code,
        "owt_Tax_Code": [
          {
            "otax_user_Code_Add": this.userid,
            "otax_user_Code_Updt": this.userid
          }
        ],
        "owt_Party_Code": this.party_obj,
        "financial_Year": this.sync.wovvSystem[0].financial_Year,
        "org_FinYear_Code": this.sync.wovvSystem[0].org_Finyear_Id,
        "oh_DocDate": ("0" + this.date.getDate()).slice(-2) + '/' + ("0" + (this.date.getMonth() + 1)).slice(-2) + '/' + this.date.getFullYear(),
        "oh_Doc_Aprov_Status": 1,
        "oh_TrF_Rule_Code": 0,
        "oh_Doc_Status": 1,
        "oh_Ref_Series_Gen_Val": 0,
        "oh_Seq_No": 0,
        "oh_System_Date": new Date(),
        "oh_Pay_DueDt": Date.now(),
        "oh_Coupon_Share_Per": 0,
        "oh_Coupon_Val": 0,
        "oh_Other_Disc_Per": 0,
        "oh_Other_Disc_Val": 0,
        "oh_Doc_Item_Count": 0,
        "oh_Doc_Item_Qty": 0,
        "oh_one_Per": 0,
        "oh_one_Val": 0,
        "oh_Two_Per": 0,
        "oh_Two_Val": 0,
        "oh_Three_Per": 0,
        "oh_Three_Val": 0,
        "oh_Four_Per": 0,
        "oh_Four_Val": 0,
        "oh_Five_Per": 0,
        "oh_Five_Val": 0,
        "oh_Six_Per": 0,
        "oh_Six_Val": 0,
        "oh_Seven_Per": 0,
        "oh_Seven_Val": 0,
        "oh_Eight_Per": 0,
        "oh_Eight_Val": 0,
        "oh_Nine_Per": 0,
        "oh_Nine_Val": 0,
        "oh_Ten_Per": 0,
        "oh_Ten_Val": 0,
        "oh_Doc_fright_Val": 0,
        "oh_Goods_Val": 0,
        "oh_Tax_total": 0,
        "oh_Doc_RoundOff": 0,
        "oh_Doc_Net_Amt": 0,
        "oh_Employ_Time_Slot": 0,
        "oh_Prod_Disc_Val": 0,
        "oh_user_Code_Add": this.userid,
        "oh_TS_Added": new Date(),
        "oh_user_Code_Updt": this.userid,
        "oh_TS_Edited": new Date(),
        "oh_Taxable_Goods_Amt": 0, //update sometime
        "oh_ProdXPrice_Amt": 0,
        "oh_Total_Disc_Val": 0,
        "oh_Change_Given_Amt": 0,
        "oh_Doc_Disc_Per": 0,  //percent
        "oh_Doc_Disc_Val": 0, //amt
        "oh_Expiry_Date": ("0" + this.date.getDate()).slice(-2) + '/' + ("0" + (this.date.getMonth() + 1)).slice(-2) + '/' + this.date.getFullYear(),
        "oh_Promo_Val": 0,
        "oh_Ref_Oh_RowNo": null,
        "oh_counter_Session_Code": 0,
        "oh_Table_Layout_Code": 0,
        "oh_Paid_Amt": 0,
        "oh_Number_Of_Guest": 0,
        "is_Tax_Registered": selectedJC[0].op_is_tax_registered,
        "oh_Party_Tax_Type_Code": PartyRpt[0].party_Tax_Type_Code_Id,
        "stock_Loc_Code": this.sync.wovvSystem[0].stock_Loc_Code_Id,
        "register_Code": this.sync.wovvSystem[0].register_Code_Id,
        "branch_Code": this.sync.wovvSystem[0].branch_Code_Id,
        "div_Code": this.sync.wovvSystem[0].div_Code_Id,
        "org_Code": {
          "id": this.sync.wovvSystem[0].org_Code_Id
        },
        "deliveryType_Head_Code": PartyRpt[0].party_Delivery_Type_Id,
        "oh_Doc_Series_Gen_Code": docNumber.doc_Series_Gen_Code_Id,
        "oh_Doc_Series_Prefix_Code": docNumber.doc_Series_Prefix_Code_Id,
        "oh_DocNo": docNumber.doc_Next_No,
        "oh_Doc_Series_Nxtno_Code": docNumber.dsnn_Id,
        "oh_Doc_Series_Prefix_Val": docNumber.doc_Series_Prefix_Name,
        "oh_Doc_Series_Gen_Val": docNumber.doc_Series_Gen_Name,
        "oh_Doc_Series_Prefix_Alias": docNumber.doc_Series_Prefix_Alias,
        "counter_Session_ID": null,
        "oh_Doc_Remark": null
      }
    ];
    debugger
    var drlOutput = this.db.checkDrltoAngular(this.drl_obj);
    console.log(drlOutput);
    debugger
    let params = {
      "pageUrl": "JR",
      "entityName": "OWT_Hdr",
      "action": "create",
      "event": "saveSB",
      "formData": {
        "oh_counter_Session_Code": 0,
        "oh_DocNo": "" + docNumber.doc_Next_No + "",
        "oh_DocDate": currDate,
        "oh_TrF_Rule_Code": 0,
        "oh_Doc_Status": 1,
        "oh_Doc_Aprov_Status": 1,
        "oh_Employ_Time_Slot": null,
        "oh_System_Date": timestamp,
        "oh_Party_Tax_Type_Code": PartyRpt[0].party_Tax_Type_Code_Id,
        "oh_Goods_Val": drlOutput.data.OWT_Hdr[0].oh_Goods_Val,
        "oh_Total_Disc_Val": 0,
        "oh_Promo_Val": 0,
        "oh_Coupon_Share_Per": 0,
        "oh_Coupon_Val": 0,
        "oh_Doc_Disc_Per": 0,
        "oh_Doc_Disc_Val": 0,
        "oh_Other_Disc_Per": 0,
        "oh_Other_Disc_Val": 0,
        "oh_Tax_total": drlOutput.data.OWT_Hdr[0].oh_Tax_total,
        "oh_Doc_fright_Val": 0,
        "oh_one_Per": 0,
        "oh_one_Val": 0,
        "oh_Two_Per": 0,
        "oh_Two_Val": 0,
        "oh_Three_Per": 0,
        "oh_Three_Val": 0,
        "oh_Four_Per": 0,
        "oh_Four_Val": 0,
        "oh_Five_Per": 0,
        "oh_Five_Val": 0,
        "oh_Six_Per": 0,
        "oh_Six_Val": 0,
        "oh_Seven_Per": 0,
        "oh_Seven_Val": 0,
        "oh_Eight_Per": 0,
        "oh_Eight_Val": 0,
        "oh_Nine_Per": 0,
        "oh_Nine_Val": 0,
        "oh_Ten_Per": 0,
        "oh_Ten_Val": 0,
        "oh_Doc_RoundOff": 0,
        "oh_Doc_Net_Amt": drlOutput.data.OWT_Hdr[0].oh_Doc_Net_Amt,
        "oh_Doc_Item_Count": 1,
        "oh_Doc_Item_Qty": 1,
        "oh_Taxable_Goods_Amt": drlOutput.data.OWT_Hdr[0].oh_Taxable_Goods_Amt,
        "oh_ProdXPrice_Amt": drlOutput.data.OWT_Hdr[0].oh_ProdXPrice_Amt,
        "oh_Seq_No": "0",
        "oh_Ref_Series_Gen_Val": null,
        "oh_Expiry_Date": "" + currDate + "",
        "oh_TS_Added": timestamp,
        "oh_TS_Edited": timestamp,
        "oh_user_Code_Add": this.userid,//session
        "oh_user_Code_Updt": this.userid,//session
        "oh_Prod_Disc_Val": 0,
        "org_FinYear_Code": docNumber.org_FinYear_Code_Id,
        "financial_Year": drlOutput.data.OWT_Hdr[0].financial_Year,
        "oh_Doc_Series_Nxtno_Code": docNumber.dsnn_Id,
        "oh_Ref_Oh_RowNo": null,
        "oh_Doc_Series_Prefix_Alias": "" + docNumber.doc_Series_Prefix_Alias + "",
        "oh_Doc_Series_Prefix_Val": "" + docNumber.doc_Series_Prefix_Name + "",
        "oh_Doc_Series_Gen_Val": "" + docNumber.doc_Series_Gen_Name + "",
        "oh_Change_Given_Amt": 0,
        "oh_Number_Of_Guest": 0,
        "oh_Paid_Amt": 0,
        "oh_Table_Layout_Code": 0,
        "is_Tax_Registered": 0,
        "oh_Doc_Class_Code": null,
        "oh_Doc_Class_Name": stckClass[0].stock_Class_Name,
        "oh_Delivery_Amt": 0,
        "oh_Doc_Ref_Remark": "remark for the order goes here",// MF owt_hdr_oh_doc_remark
        "oh_Paid_Status": 0,
        "oh_Modifier_Amt": 0,
        "oh_Doc_Series_Prefix_Code": {
          "id": docNumber.doc_Series_Prefix_Code_Id
        },

        "owt_Dtl_Code": [
          //detailArray
        ],
        "owt_Party_Code": [
          {

            "op_DocNo": "" + docNumber.doc_Next_No + "",
            "op_DocDate": timestamp,
            "op_Party_Relation_Code": drlOutput.data.OWT_Hdr[0].owt_Party_Code.op_Party_Relation_Code,
            "op_Party_Tax_Type_Code": drlOutput.data.OWT_Hdr[0].owt_Party_Code.op_Party_Tax_Type_Code,
            "op_party_GST_No": "",
            "op_Party_Name": "" + selectedJC[0].op_op_party_name + "",
            "op_Party_Add_Door": "" + selectedJC[0].op_op_party_add_door + "",
            "op_Party_Pincode": selectedJC[0].op_op_party_pincode,
            "op_Party_MobNo": "" + selectedJC[0].op_op_party_mobno + "",
            "op_TS_Added": timestamp,
            "op_TS_Edited": timestamp,
            "op_user_Code_Add": this.userid,
            "op_user_Code_Updt": this.userid,
            "op_Party_Relation_Name": drlOutput.data.OWT_Hdr[0].owt_Party_Code.op_Party_Relation_Name,
            "op_Party_Tax_Type_Name": drlOutput.data.OWT_Hdr[0].owt_Party_Code.op_Party_Tax_Type_Name,
            "op_Layout_Code": 0,
            "op_Table_Layout_Code": 0,
            "op_Layout_Desc": drlOutput.data.OWT_Hdr[0].owt_Party_Code.op_Layout_Desc,
            "op_Layout_Table_Desc": drlOutput.data.OWT_Hdr[0].owt_Party_Code.op_Layout_Table_Desc,
            "op_Number_Of_Guest": drlOutput.data.OWT_Hdr[0].owt_Party_Code.op_Number_Of_Guest,
            "is_Tax_Registered": selectedJC[0].op_is_tax_registered,
            "op_Party_Code": {
              "id": PartyRpt[0].party_Code_Id
            }
          }
        ],
        // "deliveryType_Head_Code": {
        //   "id": selectedJC[0].od_deliverytype_head_code
        // },
        "owt_Tax_Code": [
          {

            "otax_Doc_Date": timestamp,
            "otax_doc_Series_Prefix_Code": drlOutput.data.OWT_Hdr[0].oh_Doc_Series_Prefix_Code,
            "otax_SubClass_Code": 41816,
            "otax_Code": {
              "id": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_Code
            },
            "tax_Sub_Class_Name": "18% GST (E) OUTPUT",
            "otax_Tax_total": drlOutput.data.OWT_Hdr[0].oh_Tax_total,
            "otaxble_Goods_Amt": drlOutput.data.OWT_Hdr[0].oh_Taxable_Goods_Amt,
            "otax_user_Code_Add": this.userid,//session
            "otax_user_Code_Updt": this.userid,//session
            "otax_TS_Edited": timestamp,
            "otax_Rate": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_Rate,
            "otax_1_Rate": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_1_Rate,
            "otax_2_Rate": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_2_Rate,
            "otax_2_Amt": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_2_Amt,
            "otax_1_Amt": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_1_Amt,
            "otax_3_Rate": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_3_Rate,
            "otax_3_Amt": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_3_Amt,
            "otax_4_Rate": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_4_Rate,
            "otax_4_Amt": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_4_Amt,
            "otax_5_Rate": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_5_Rate,
            "otax_4_Code": 0,
            "otax_3_Code": 0,
            "otax_5_Amt": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_5_Amt,
            "otax_5_Code": 0,
            "otax_2_Code": 0,
            "otax_1_Code": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_1_Code,
            "otax_TS_Added": timestamp
          }
        ],
        "register_Code": {
          "id": selectedJC[0].od_od_register_code
        },
        "owt_Pay_Hdr_Code": [
          {

            "opay_OH_Doc_No": "" + docNumber.doc_Next_No + "",
            "opay_OH_Doc_Date": todayDate,
            "opay_counter_Session_Code": 0,
            "opay_Principal": drlOutput.data.OWT_Hdr[0].oh_Doc_Net_Amt,
            "opay_TS_Added": timestamp,
            "opay_TS_Edited": timestamp,
            "op_user_Code_Add": this.userid,//session
            "op_user_Code_Updt": this.userid,//session
            "op_TotChangeGivan_Val": 0,
            "opay_Paid_VAl": drlOutput.data.OWT_Hdr[0].oh_Doc_Net_Amt,
            "pay_Type_Name": "Credit",
            "opd_Trns_UID": [
              {

                "opd_OH_Doc_No": "" + docNumber.doc_Next_No + "",
                "opd_OH_Doc_Date": todayDate,
                "pay_Type_Code": 41814182,
                "counter_Session_Code": 0,
                "opd_Principal": drlOutput.data.OWT_Hdr[0].oh_Doc_Net_Amt,
                "opd_Deno_Count": 0,
                "opd_NET_Amt": drlOutput.data.OWT_Hdr[0].oh_Doc_Net_Amt,
                "opd_TS_Added": timestamp,
                "opd_TS_Edited": timestamp,
                "od_user_Code_Add": this.userid,
                "od_user_Code_Updt": this.userid,
                "opd_ChangeGivan_Val": 0,
                "pay_Type_Name": "Credit"
              }
            ],
            "pay_Type_Code": {
              "id": 41814181
            }
          }
        ],
        "branch_Code": {
          "id": selectedJC[0].od_branch_code_id
        },
        "stock_Loc_Code": {
          "id": selectedJC[0].od_stock_loc_code_id
        },
        "org_Code": {
          "id": docNumber.org_Code_Id
        },
        "div_Code": {
          "id": this.sync.divCode
        },
        "oh_Doc_Series_Gen_Code": {
          "id": docNumber.doc_Series_Gen_Code_Id
        }
      }
    }
    for (let value = 0; value < selectedJC.length; value++) {
      var stckClass = [];
      if (selectedJC[value].od_od_job_class_name == 'Received') {
        stckClass = this.sync.stockClassData.filter(function (value) {
          return value['stock_Class_Type_Name'] == "Prod Job-Work Class", value['stock_Class_Name'] == "Return";
        });
      }
      else{
         stckClass = this.sync.stockClassData.filter(function (value) {
          return value['stock_Class_Type_Name'] == "Prod Job-Work Class", value['stock_Class_Name'] == "Damage Return";
        });
      }
      let detail = {
        "od_Doc_No": "" + docNumber.doc_Next_No + "",
        "od_Doc_Date": todayDate,
        "od_Ref_Series_Prefix_Alias": "" + selectedJC[value].od_od_doc_series_prefix_alias + "",
        "od_Ref_RowNo": selectedJC[value].od_id,
        "od_Ref_DocNo": selectedJC[value].od_od_doc_no,
        "od_Prod_Name": "" + selectedJC[value].od_od_prod_name + "",
        "od_UOM_Calculate_Val": 0,
        "od_MRP": selectedJC[value].od_od_mrp,
        "od_Rate": selectedJC[value].od_od_rate,
        "od_Calc_Price": selectedJC[value].od_od_calc_price,
        "od_Qty": selectedJC[value].od_od_qty,
        "od_Prod_QtyXPrice_Amt": drlOutput.data.OWT_Hdr[0].owt_Dtl_Code[value].od_Prod_QtyXPrice_Amt,
        "od_Prod_Disc_Per": 0,
        "od_Prod_Disc_Amt": 0,
        "od_Prod_Sp_Disc_Per": 0,
        "od_Prod_Sp_Disc_Amt": 0,
        "od_Prod_Promo_Per": 0,
        "od_Prod_Promo_Amt": selectedJC[value].od_od_prod_promo_amt,
        "od_Goods_Amt": selectedJC[value].od_od_goods_amt,
        "od_Coup_Per": 0,
        "od_Coup_Amt": 0,
        "od_Inv_Disc_Per": 0,
        "od_Inv_Disc_Amt": 0,
        "od_Taxable_Goods_Amt": selectedJC[value].od_od_taxable_goods_amt,
        "od_Tax_Inclusive": 0,
        "od_Tax_Rate": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_Rate,
        "od_Prod_Tax_Amt": drlOutput.data.OWT_Hdr[0].oh_Tax_total,
        "od_Tax1_Rate": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_1_Rate,
        "od_Prod_Tax1_Amt": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_1_Amt,
        "od_Tax2_Rate": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_2_Rate,
        "od_Prod_Tax2_Amt": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_2_Amt,
        "od_Tax3_Rate": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_3_Rate,
        "od_Prod_Tax3_Amt": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_3_Amt,
        "od_Tax4_Rate": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_4_Rate,
        "od_Prod_Tax4_Amt": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_4_Amt,
        "od_Tax5_Rate": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_4_Rate,
        "od_Prod_Tax5_Amt": drlOutput.data.OWT_Hdr[0].owt_Tax_Code[0].otax_5_Amt,
        "od_Fright_Rate": 0,
        "od_Fright_Amt": 0,
        "od_Prod_NetAmt": selectedJC[value].od_od_prod_netamt,
        "od_SalesMan_Instv_Per": 0,
        "od_SalesMan_Instv_Amt": 0,
        "od_one_Per": 0,
        "od_one_Val": 0,
        "od_Two_Per": 0,
        "od_Two_Val": 0,
        "od_Three_Per": 0,
        "od_Three_Val": 0,
        "od_Four_Per": 0,
        "od_Four_Val": 0,
        "od_Five_Per": 0,
        "od_Five_Val": 0,
        "od_Six_Per": 0,
        "od_Six_Val": 0,
        "od_Seven_Per": 0,
        "od_Seven_Val": 0,
        "od_Eight_Per": 0,
        "od_Eight_Val": 0,
        "od_Nine_Per": 0,
        "od_Nine_Val": 0,
        "od_Ten_Per": 0,
        "od_Ten_Val": 0,
        "od_Free_Tag": 0,
        "od_TS_Added": timestamp,
        "od_TS_Edited": timestamp,
        "od_Bfr_Tax_Price": 79,
        "od_Prod_TypClass_Code": selectedJC[value].od_od_prod_typclass_code,
        "od_ProdMFR_Brand_Code": selectedJC[value].od_od_prodmfr_brand_code,
        "od_ProdSGrp_Code": selectedJC[value].od_od_prodsgrp_code,
        "od_ProdGrp_Code": selectedJC[value].od_od_prodgrp_code,
        "od_ProdDepartment_Code": selectedJC[value].od_od_proddepartment_code,
        "od_Tax_Code": CatalogRptData[0].tax_Code_Sale_Id,
        "od_Tax1_Code": CatalogRptData[0].tax_1_Ledger_Code_Id,
        "od_Tax2_Code": CatalogRptData[0].tax_2_Ledger_Code_Id,
        "od_Ref_Series_Prefix_Code": 729757670992760,//MF
        "od_Ref_Series_Gen_Val": 0,
        "od_Ref_Series_prefix_Val": "" + selectedJC[value].od_od_doc_series_prefix_val + "",
        "od_Ref_DocDate": selectedJC[value].od_od_doc_date,
        "od_Doc_Series_Prefix_Code": selectedJC[value].od_od_doc_series_prefix_code,
        "od_Ref_Doc_Series_Nxtno_Code": 449253961862301,
        "od_Prod_TypClass_Name": "" + selectedJC[value].od_od_prod_typclass_name + "",
        "od_Register_Code": selectedJC[value].od_od_register_code,
        "od_Reg_Type_Code": 2,
        "od_Reg_Type_Name": "0",
        "od_Prod_TypClass_Prefix": 1031,
        "od_Doc_Series_Prefix_Alias": "" + drlOutput.data.OWT_Hdr[0].oh_Doc_Series_Prefix_Alias + "",
        "od_Branch_Name": "" + selectedJC[value].od_od_branch_name + "",
        "od_Stock_Loc_Name": this.sync.wovvSystem[0].stock_Loc_Name,
        "od_Adjusted_QTY": selectedJC[value].od_od_adjusted_qty,
        "od_Prod_Status": selectedJC[value].od_od_prod_status,
        "od_Prod_UOM_YN": 0,
        "od_Prod_Alias": "" + selectedJC[value].od_od_prod_alias + "",
        "od_Stock_Class_Name": "Job Work",
        "prod_Parent_Code": 0,
        "od_Attribute_DT": "20/10/2018",
        "od_Attribute_Val": "NA",
        "od_MFR_DT": "20/10/2018",
        "od_VGroup_Code": 0,
        "od_VType1_Code": 0,
        "od_VType2_Code": 0,
        "od_VType3_Code": 0,
        "sequenceNo": 3,
        "od_Tax_Prn_Symbol": "18% GST OUTPUT (E)",
        "od_Tax_SubClass_Code": 41816,
        "od_Doc_Series_Prefix_Val": "" + selectedJC[value].od_od_doc_series_prefix_val + "",
        "od_LandingCost": selectedJC[value].od_od_LandingCost,
        "od_PurPrice": selectedJC[value].od_od_PurPrice,
        "od_Barcode": "" + selectedJC[value].od_od_barcode + "",
        "od_StockBook_Ref_Code": selectedJC[value].sb_id,
        "od_Prod_Code": {
          "id": selectedJC[value].od_od_prod_code_id
        },
        "stock_Loc_Code": {
          "id": selectedJC[value].od_stock_loc_code_id
        },
        "od_Stock_Class_Code": {
          "id": stckClass[0].stock_Class_Id
        },
        "branch_Code": {
          "id": selectedJC[value].od_branch_code_id
        },
        "deliveryType_Name": "" + selectedJC[value].od_deliverytype_name + "",
        "mfr_Head_Code": selectedJC[value].od_mfr_head_code,
        "mfr_Head_Name": "" + selectedJC[value].od_mfr_head_name + "",
        "prodDepartment_Name": "" + selectedJC[value].od_proddepartment_name + "",
        "prodMFR_Brand_Name": "" + selectedJC[value].od_prodmfr_brand_name + "",
        "prod_Group_Name": "" + selectedJC[value].od_prod_group_name + "",
        "prod_SubGroup_Name": "" + selectedJC[value].od_prod_subgroup_name + "",
        "reg_Name": "" + selectedJC[value].od_reg_name + "",
        "reg_Prefix": "" + selectedJC[value].od_reg_prefix + "",
        "tax_Cls_Name": "18% GST OUTPUT (E)",
        "tax_Sub_Class_Name": "18% GST (E) OUTPUT",
        "od_Prod_Seq": value + 1,
        "od_Prod_Ref_Seq": 10,
        "od_Prod_Due_DT": selectedJC[value].od_od_prod_due_dt,
        "od_Delivery_Per": selectedJC[value].od_od_delivery_per,
        "od_Delivery_Amt": selectedJC[value].od_od_delivery_amt,
        "od_Delivery_Status_Code": selectedJC[value].od_od_delivery_status_code,
        "od_Delivery_Status_Name": "" + selectedJC[value].od_od_delivery_status_name + "",
        "od_JOB_Class_Code": stckClass[0].stock_Class_Id,
        "od_JOB_Class_Name": stckClass[0].stock_Class_Name,
        "deliveryType_Head_Name": "" + selectedJC[value].od_deliverytype_head_name + "",
        "is_Home_Delivery": selectedJC[value].od_is_home_delivery,
        "od_BarcodeGen_Code": null,
        "od_BarcodeGen_Type": null,
        "od_BarcodePRN_YN": 0,
        "od_Prod_Modifier_Per": 0,
        "od_Prod_Modifier_Amt": selectedJC[value].od_od_prod_modifier_amt,
        "od_Delivery_Rate": 2994,
        "od_Prod_Modifier_Rate": 0,
        "owtdtl_user_Code_Add": this.userid,
        "owtdtl_user_Code_Updt": this.userid
      }
      params.formData.owt_Dtl_Code.push(detail);
    }
    //////debugger

    //params.formData.owt_Dtl_Code.push(detailArray);
    console.log(params);
    ////debugger
    return this.http.post(this.url + '?tenantId=' + this.tid, params, options)
      .map(response => {
        {
          return response;
          //console.log(response.data);
        };
      })
      .catch(error => Observable.throw(
        { status: error.status, Errors: error }
      ));
  }
  AccountBook(selectedJC) {
    this.url = this.sync.url;
    this.tid = this.sync._tenantId;
    this.userid = localStorage.getItem('UserID');
    this.accesstoken = localStorage.getItem('onlineToken');
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'userId': this.userid,
        'access_Token': this.accesstoken
      })
    };
    // var org = this.sync.org;
    // var branch = this.sync.branch;
    // var tenantId = this.sync.tenantId;
    let params = {
      "pageUrl": "PickupDetail",
      "entityName": "Acc_Book",
      "action": "view",
      "event": "multiFilter",
      "multiFilters": [
        {
          "mainEntityType": "",
          "mainEntity": "Acc_Book",
          "chiledEntityType": "",
          "childEntity": "OWT_Hdr oh",
          "alias": "oh",
          "fk": "acc_book.ab_Ref_Hdr_RowID"
        }
      ],
      "whereClause": [
        {
          "filterKey": "ab_Credit",
          "filterValue": 2,         // Static value
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "oh.Id",
          "filterValue": selectedJC[0].owt_hdr_id,             // order number from fetch call (pickup_hdr)
          "relation": "=",
          "condition": ""
        }
      ],
      "selectFields": [
        {
          "fieldName": "acc_book.id as vod_Get_AB_RowID"
        },
        {
          "fieldName": "ab_Ref_Ser_Gen_Code"
        },
        {
          "fieldName": "ab_Ref_Ser_Gen_Val"
        },
        {
          "fieldName": "ab_Ref_Ser_Prefix_Code"
        },
        {
          "fieldName": "ab_Ref_Ser_Prefix_Val"
        },
        {
          "fieldName": "ab_Ref_Ser_Nxtno_Code"
        },
        {
          "fieldName": "ab_Ref_DocNo"
        },
        {
          "fieldName": "ab_Ref_DocDate"
        },
        {
          "fieldName": "ab_Ref_Hdr_RowID"
        },
        {
          "fieldName": "ab_Ref_Doc_Amt"
        },
        {
          "fieldName": "oh.Id as ohId"
        },
        {
          "fieldName": "oh.oh_DocDate as oh_DocDate"
        },
        {
          "fieldName": "ab_Ref_Doc_Amt as total_amt"
        },
        {
          "fieldName": "ab_Ref_Doc_Amt - sum(ab_Available_Amt) as paid_Amt"
        },
        {
          "fieldName": "ab_Ref_Doc_Amt - (ab_Ref_Doc_Amt - sum(ab_Available_Amt)) as pend_amt"
        },
        {
          "fieldName": "case when sum(ab_Available_Amt) = ab_Ref_Doc_Amt then '2' when sum(ab_Available_Amt) < ab_Ref_Doc_Amt then '1' when sum(ab_Available_Amt) = 0 then '0' end paid_Status"
        }
      ],
      "groupBY": ["group by ab_Ref_Hdr_RowID , ab_Ref_Ser_Prefix_Alias , ab_Ref_Ser_Nxtno_Code , ab_Ref_DocNo , ab_Ref_Doc_Amt"]
    }
    return this.http.post(this.url + '?tenantId=' + this.tid, params, options)
      .map(response => {
        {
          return response;
        };
      })
      .catch(error => Observable.throw(
        { status: error.status, Errors: error }
      ));
  }
  SavePayment(amount, partyrpt, docNextNumber, owt_Pay_Hdr_Code, AccountBook) {
    console.log(docNextNumber);
    //debugger
    this.url = this.sync.url;
    this.tid = this.sync._tenantId;

    this.userid = localStorage.getItem('UserID');
    this.accesstoken = localStorage.getItem('onlineToken');
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'userId': this.userid,
        'access_Token': this.accesstoken
      })
    };
    var wovvsystemData = this.sync.wovvSystem;
    //var org = this.sync.org;
    var finYear = wovvsystemData[0].org_Finyear_Id;
    var branch = this.sync.branchCode;
    //var date_today = new Date().toISOString().slice(0, 19).replace('T', ' ');

    var todayDate = new Date();
    var currDate = todayDate.getDate() + '/' + (todayDate.getMonth() + 1) + '/' + todayDate.getFullYear();

    let params =
    {
      "pageUrl": "IR",
      "entityName": "Voucher_Hdr",
      "action": "saveAllWithResp",
      "event": "saveAB",
      "formList": [

      ]


    }
    for (var split = 0; split < owt_Pay_Hdr_Code.length; split++) {
      let formdetails = {
        "voh_TS_Added": new Date(),
        "voh_TS_Edited": new Date(),
        "voh_User_Code_Updt": this.userid,
        "voh_User_Code_Add": this.userid,
        "voh_Doc_Remark": null,
        "voh_Doc_Series_Gen_Val": docNextNumber.doc_Series_Gen_Name,
        "voh_Doc_Series_Prefix_Alias": docNextNumber.doc_Series_Prefix_Alias,
        "voh_Doc_Series_Prefix_Val": docNextNumber.doc_Series_Prefix_Name,
        "voh_DocNo": docNextNumber.doc_Next_No,
        "voh_TRType_Name": null,
        "voh_TRDOC_No": null,
        "voh_TRDOC_Remark": null,
        "financial_Year": wovvsystemData[0].financial_Year,
        "voh_Bank_Name": partyrpt[0].bank_Name,
        "voh_Ledger_Name": owt_Pay_Hdr_Code[split]['ledgerName'],
        "voh_Party_Name": partyrpt[0].party_Name,
        "voh_Party_TaxType_Name": partyrpt[0].party_Tax_Type_Name,
        "voh_Party_Relation_Name": partyrpt[0].party_Relation_Name,
        "voh_Party_GST_No": partyrpt[0].party_GST_No,
        "voh_Party_Other_Reg_No": partyrpt[0].party_Other_Reg_No,
        "voh_Counter_Session_Code": 0,
        "voh_Doc_Series_Nxtno_Code": docNextNumber.dsnn_Id,
        "voh_TRType_Code": null,
        "div_Code": this.sync.divCode,//session //469176782732065
        "register_Code": this.userid,//session  //null
        "org_FinYear_Code": finYear,
        "voh_Bank_Code": partyrpt[0].pBR_Bank_Code_Id,
        "voh_Ledger_Code": null,//ptt_acc_ledger_code_id
        "voh_Party_Code": partyrpt[0].party_Code_Id,//party_code_id
        "voh_Party_TaxType_Code": partyrpt[0].party_Tax_Type_Code_Id,//party_tax_type_code_id
        "voh_Party_Relation_Code": partyrpt[0].party_Relation_Code_Id,//party_relation_code_id
        "voh_CR_Amt": null,
        "voh_DR_Amt": null,
        "voh_Pend_Amt": null,
        "voh_DocDate": currDate,
        "voh_TRDOC_Date": null,
        "voh_Employ_Time_Slot": null,
        "voh_System_Date": new Date(),
        "is_Tax_Registered": partyrpt[0].is_Tax_Registered,//partyrpt
        "voh_Doc_Aprov_Status": 1,
        "is_System_Default": 0,
        "voh_Doc_Status": 1,
        "voh_Doc_Series_Gen_Code": {
          "id": docNextNumber.doc_Series_Gen_Code_Id
        },
        "voh_Doc_Series_Prefix_Code": {
          "id": docNextNumber.doc_Series_Prefix_Code_Id
        },
        "branch_Code": {
          "id": branch
        },
        "org_Code": {
          "id": this.sync.orgCode
        },
        "voucher_Dtl_Code": [
          {
            "vod_TS_Added": new Date(),
            "vod_TS_Edited": new Date(),
            "vod_User_Code_Updt": this.userid,
            "vod_User_Code_Add": this.userid,
            "vod_Doc_Series_Prefix_Alias": docNextNumber.doc_Series_Prefix_Alias,
            "vod_Doc_Series_Prefix_Val": docNextNumber.doc_Series_Prefix_Name,
            "vod_DocNo": docNextNumber.doc_Next_No,
            "vod_Ledger_Name": owt_Pay_Hdr_Code[split]['ledgerName'],
            "vod_Party_Name": partyrpt[0].party_Name,
            "vod_Ref_Series_Gen_Val": AccountBook[0]['ab_Ref_Ser_Gen_Val'],
            "vod_Ref_Series_Prefix_Val": AccountBook[0]['ab_Ref_Ser_Prefix_Val'],
            "vod_Ref_DocNo": AccountBook[0]['ab_Ref_DocNo'],
            "vod_Pay_Type_Name": null,
            "financial_Year": wovvsystemData[0].financial_Year,
            "vod_Doc_Series_Prefix_Code": docNextNumber.doc_Series_Prefix_Code_Id,
            "vod_Party_Code": partyrpt[0].party_Code_Id,
            "vod_Ref_Series_Gen_Code": AccountBook[0]['ab_Ref_Ser_Gen_Code'],
            "vod_Ref_Series_Prefix_Code": AccountBook[0]['ab_Ref_Ser_Prefix_Code'],
            "vod_Ref_Series_Nxtno_Code": AccountBook[0]['ab_Ref_Ser_Nxtno_Code'],
            "vod_Ref_RowID": AccountBook[0]['ab_Ref_Hdr_RowID'],
            "vod_Get_AB_RowID": AccountBook[0]['vod_Get_AB_RowID'],
            "vod_Post_AB_RowID": null,
            "vod_Pay_Type_Code": partyrpt[0].pay_Typ_Id,
            "register_Code": null,  //169880230227470
            "vod_Amount": owt_Pay_Hdr_Code[split]['opay_Paid_VAl'],
            "vod_Ref_Doc_Total_Amt": AccountBook[0]['ab_Ref_Doc_Amt'],
            "vod_Ref_Doc_Pend_Amt": AccountBook[0]['pend_amt'],
            "vod_Employ_Time_Slot": null,
            "vod_Ref_Doc_Date": AccountBook[0]['ab_Ref_DocDate'],
            "vod_System_Date": new Date(),
            "vod_DocDate": currDate,
            "vod_Credit": owt_Pay_Hdr_Code[split]['modeCode'],
            "vod_Class_Code": null,
            "vod_Class_Name": null,
            "vod_Doc_Status": 1,
            "vod_Ledger_Code": {
              "id": partyrpt[0].ptt_acc_Ledger_Code_Id//wrong
            },
            "branch_Code": {
              "id": branch
            },
            "pay_Mode_Code": owt_Pay_Hdr_Code[split]['modeCode'],
            "pay_Mode_Name": owt_Pay_Hdr_Code[split]['modeName']
          }
        ]
      }
      params.formList.push(formdetails);
    }
    return this.http.post(this.url + '?tenantId=' + this.tid, params, options)
      .map(response => {
        {
          return response;
        };
      })
      .catch(error => Observable.throw(
        { status: error.status, Errors: error }
      ));
  }
  SendMail(invoiceNumber, partyCode, isPermanant, customerEmail) {
    this.url = this.sync.url;
    this.tid = this.sync._tenantId;

    this.userid = localStorage.getItem('UserID');
    this.accesstoken = localStorage.getItem('onlineToken');
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'userId': this.userid,
        'access_Token': this.accesstoken
      })
    };
    let params =
    {
      "pageUrl": "JC",
      "entityName": "OWT_Hdr",
      "action": "payloadRuleWithTid",
      "event": "PT_81",
      "formList": [
        {
          "invoice_Id": invoiceNumber,
          "partyEmail": customerEmail,
          "permanently": isPermanant,
          "party_Code": partyCode,
          "footer_Value": [
            {
              "authority": "SUBJECT TO JURISDICTION ONLY",
              "policy": "NO REFUND, NO EXCHANGE",
              "greetings": "THANK YOU, VISIT AGAIN OK"
            }
          ]
        }

      ]
    }
    return this.http.post(this.url + '?tenantId=' + this.tid, params, options)
      .map(response => {
        {
          return response;
        };
      })
      .catch(error => Observable.throw(
        { status: error.status, Errors: error }
      ));
  }
}
