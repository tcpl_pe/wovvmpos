import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { Observable } from "rxjs";
import { SyncProvider } from "../../providers/sync/sync";
/*
  Generated class for the GlobalsearchProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalsearchProvider {
  public url: any;
  public tid: any;
  public mode: string;
  public userid: any;
  public accesstoken: any;
  public FinYear_code: any;
  public wovvData: any;
  constructor(public http: HttpClient, public sync: SyncProvider) {
  }
  getPartyRelation() {
    this.url = this.sync.url;
    this.tid = this.sync._tenantId;
    //this.userid = this.sync.uid;
    this.accesstoken = localStorage.getItem('onlineToken');
    this.userid = localStorage.getItem('UserID');
    this.wovvData = this.sync.wovvSystem;
    this.FinYear_code = this.wovvData[0].org_Finyear_Id;
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'userId': this.userid,
        'access_Token': this.accesstoken
      })
    };
    let params = {
      "pageUrl": "SingleCustomer",
      "entityName": "Party_Relations_MsT",
      "action": "view",
      "event": "selectFilter",
      "filters": [
        {
          "filterKey": "party_Relation_Name",
          "filterValue": "Customer",
          "relation": "="
        }
      ],
      "selectFields": [
        {
          "fieldName": "id"
        },
        {
          "fieldName": "party_Relation_Name"
        }
      ]
    }
    return this.http.post(this.url + '?tenantId=' + this.tid, params, options)
      .map(response => {
        {
          return response;
        };
      })
      .catch(error => Observable.throw(
        { status: error.status, Errors: error }
      ));
  }
  addCustomer(formData, party_relation) {
    this.url = this.sync.url;
    this.tid = this.sync._tenantId;
    //this.userid = this.sync.uid;
    this.accesstoken = localStorage.getItem('onlineToken');
    this.userid = localStorage.getItem('UserID');
    this.wovvData = this.sync.wovvSystem;
    this.FinYear_code = this.wovvData[0].org_Finyear_Id;
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'userId': this.userid,
        'access_Token': this.accesstoken
      })
    };
    let params = {
      "pageUrl": "SingleCustomer",
      "entityName": "Party_Head_MsT",
      "action": "create",
      "event": "createNew",
      "formData": {
        "id": null,
        "is_Tax_Registered": 0,
        "party_Remark": formData.notify_by_email_sms,
        "party_Status": 1,
        "party_Relation_Code": {
          "id": party_relation.id
        },
        "party_Name": formData.name,
        "party_MobNo": formData.mobile,
        "party_email": formData.email,
        "party_Add_Door": formData.flat_no,
        "party_Add_Street": formData.street,
        "party_Add_Landmark": formData.area,
        "party_TS_Added": new Date(),
        "party_TS_Edited": new Date(),
        "party_User_Code_Add": this.userid,
        "party_User_Code_Updt": this.userid,
        "org_Code": {
          "id": this.sync.orgCode
        },
        "approve_Status": 1,
        "is_System_Default": 0,
        "party_Relation_Name": party_relation.party_Relation_Name
      }
    }
    return this.http.post(this.url + '?tenantId=' + this.tid, params, options)
      .map(response => {
        {
          return response;
        };
      })
      .catch(error => Observable.throw(
        { status: error.status, Errors: error }
      ));
  }
}
