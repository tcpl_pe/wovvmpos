import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { DbProvider } from "../db/db";
import { Storage } from '@ionic/storage';
import { Device } from '@ionic-native/device';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { LoadingController, Platform, ToastController } from "ionic-angular";
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import * as _ from 'underscore';
import { LoadingProvider } from '../loading/loading';

@Injectable()
export class SyncProvider {

  dev_id: any;
  user: any = {};
  utoken: any;
  inc_cat = [];
  inc_par = [];
  inc_stk = [];
  inc_delivery = [];
  inc_modifier = [];
  inc_stkClass = [];
  inc_RecipeMst = [];
  platformName: any;
  imgUrl: any;
  uniqueBranchIds: any;
  imagep: any;
  entityName: any = "Prod_Hdr_MsT"
  fileTransfer: FileTransferObject = this.transfer.create();
  foundTS: any;

  //Sync Progressbar 
  maxResult: any = 1000;
  currentCalls: any = "General Data";
  totalCalls: any = 10;
  completedCalls: any = 0;
  completedPer: any = 0;
  IsCompleted: any;
  IsError: any;

  version: any = "1.0.1";
  siteUrl: string = "";
  url: string;
  endpoint: any = "/ipos/rest/bpm/process";
  demoURL: any = "";
  demoVertical: any = "";
  demoDomain: any = "";
  demoRegMacId: any = "";
  URL: string;
  Domain: string;
  OrderId: string;
  selectedDashboard: string = "DynamicmenuPage";
  token: any;
  tenantId: any;
  _tenantId: any;
  regMacId: any = "zzzz";
  registerCode: string;
  branchCode: string;
  stockLocCode: string;
  divCode: string;
  orgCode: string;

  wovvSystem: any = [];
  wovvUser: any = [];
  wovvMenu: any = [];
  uniqueWovvUsers: any = [];
  docNumberData: any = {};
  catlogData: any = [];
  partyData: any = [];
  stockData: any = [];
  stockClassData = [];
  deliveryTypeData: any = {};
  modifireData: any = []
  recipeData: any = [];

  pendingLogMaxLimit = 5;
  numberOfDaysPreviousLogData = 0;
  isOnline: boolean = false;//check either user online or offline
  isCustomer: boolean = true;//check either customer selection is required or not
  isPrice: any = "prod_Price"; //check which price should take in invoice: prod_Price, prod_LandingCost, prod_PurPrice,prod_MRP as od_Calc_Price  
  LogData: any;
  transactionInvoices: any;

  IsCatlog: boolean = false;
  IsParty: boolean = false;
  IsDelivery: boolean = false;
  IsModifier: boolean = false;
  IsStockBook: boolean = false;
  IsStockClass: boolean = false;
  IsRecipe: boolean = false;
  owtHrdSyncData: any[];
  pendingLogData: any;

  constructor(private transfer: FileTransfer, public toastCtrl: ToastController, private file: File,
    public plt: Platform, public loadingCtrl: LoadingController, private iab: InAppBrowser,
    public storage: Storage, public db: DbProvider, public http: Http, private device: Device,
    public loading: LoadingProvider) { }

  GetLocalData() {
    this.device.uuid = this.regMacId;
    this.dev_id = this.device.uuid;
    this.storage.get("siteUrl").then((data) => {
      if (data != null)
        this.siteUrl = data;
    });
    this.storage.get("url").then((data) => {
      if (data != null)
        this.url = data;
    });
    this.storage.get("token").then((data) => {
      if (data != null)
        this.token = data;
    });
    this.storage.get("tenantId").then((data) => {
      if (data != null)
        this.tenantId = data;
    });
    this.storage.get("_tenantId").then((data) => {
      if (data != null)
        this._tenantId = data;
    });
    this.storage.get("SIDocNumber").then((data) => {
      if (data != null)
        this.docNumberData = data;
    });
    this.storage.get("registerCode").then((data) => {
      if (data != null)
        this.registerCode = data;
    });
    this.storage.get("orgCode").then((data) => {
      if (data != null)
        this.orgCode = data;
    });
    this.storage.get("divCode").then((data) => {
      if (data != null)
        this.divCode = data;
    });
    this.storage.get("branchCode").then((data) => {
      if (data != null)
        this.branchCode = data;
    });
    this.storage.get("stockLocCode").then((data) => {
      if (data != null)
        this.stockLocCode = data;
    });
    this.storage.get('WovvSystem').then((data) => {
      if (data != null)
        this.wovvSystem = data;
    });
    this.storage.get('WovvUser').then((data) => {
      if (data != null)
        this.wovvUser = data;
    });
    this.storage.get('WovvMenu').then((data) => {
      if (data != null)
        this.wovvMenu = data;
    });
    this.storage.get("Catalog").then((data) => {
      if (data != null)
        this.catlogData = data;
    });
    this.storage.get("Party").then((data) => {
      if (data != null)
        this.partyData = data;
    });
    this.storage.get("StockClass").then((data) => {
      if (data != null)
        this.stockClassData = data;
    });
    this.storage.get("DeliveryType").then((data) => {
      if (data != null)
        this.deliveryTypeData = data;
    });
    this.storage.get("Modifiers").then((data) => {
      if (data != null)
        this.modifireData = data;
    });
    this.storage.get("Recipe").then((data) => {
      if (data != null)
        this.recipeData = data;
    });
    this.storage.get("LogInvoices").then((data) => {
      if (data != null)
        this.LogData = data;
    });
    this.storage.get("invoices").then((data) => {
      if (data != null)
        this.transactionInvoices = data;
    });
    this.storage.get('isLoggedIn').then((data) => {
      if (data == 'yes') {
        this.isOnline = true;
      }
      else {
        this.isOnline = false;
      }
    });

  }

  GetServerData() {
    this.WovvSystem().then(() => {
      this.WovvUser().then(() => {
        this.WovvMenu().then(() => {
          console.log("Catalog Start");
          this.Catalog(0);
          // console.log(data);
          // this.Party(0).then(() => {
          //   console.log("Party Comepleted");
          // this.DeliveryType(0).then(() => {
          //   console.log("Delivery Comepleted");
          //   this.Modifires(0).then(() => {
          //     this.Recipe(0).then(() => {
          //       this.StockClass(0).then(() => {
          //         this.StockbookGt0(0).then(() => {
          //           this.StockbookLs0(0).then(() => {
          //           });
          //         });
          //       });
          //     });
          //   });
          // });
          // });
          // });
        });
      });
    });
  }

  WovvSystem() {
    return new Promise((resolve) => {

      this.currentCalls = "System Data";

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('access_Token', this.token);
      headers.append('Register_Id', this.registerCode);
      headers.append('Reg_Mac_Id', this.regMacId);

      var post = {
        "pageUrl": "Wovvsystem",
        "entityName": "Wovvsystem",
        "action": "payload",
        "event": "completeSyncToMysql",
        "whereClause": [
          {
            "filterKey": "tid",
            "filterValue": this.tenantId,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "branch_Code_Id",
            "filterValue": this.branchCode,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "finYear_End_Status",
            "filterValue": 0,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "reg_Type",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "register_Code_Id",
            "filterValue": this.registerCode,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "reg_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "user_Approve_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "regCH_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": ""
          }
        ],
        "selectFields": [
          { "fieldName": "user_Id" },
          { "fieldName": "user_Login_ID" },
          { "fieldName": "user_Password" },
          { "fieldName": "org_Code_Id" },
          { "fieldName": "user_Role_Id" },
          { "fieldName": "party_Name" },
          { "fieldName": "register_Code_Id" },
          { "fieldName": "stock_Loc_Code_Id" },
          { "fieldName": "div_Code_Id" },
          { "fieldName": "reg_Name" },
          { "fieldName": "reg_Prefix" },
          { "fieldName": "reg_Type" },
          { "fieldName": "branch_Code_Id" },
          { "fieldName": "branch_Name" },
          { "fieldName": "branch_Prefix" },
          { "fieldName": "branch_Tax_Type_Code_Id" },
          { "fieldName": "tax_Type_Name" },
          { "fieldName": "org_Finyear_Id" },
          { "fieldName": "financial_Year" },
          { "fieldName": "org_Type" },
          { "fieldName": "is_System_Default" },
          { "fieldName": "user_Code_Add" },
          { "fieldName": "user_Code_Updt" },
          { "fieldName": "party_Code_Id" },
          { "fieldName": "org_Code" },
          { "fieldName": "bg_Name" },
          { "fieldName": "bg_Prefix" },
          { "fieldName": "branch_Group_Code_Id" },
          { "fieldName": "register_Child_id" },
          { "fieldName": "regCH_Default" },
          { "fieldName": "year_End_Date" },
          { "fieldName": "year_Began_Date" },
          { "fieldName": "is_Global_YN" },
          { "fieldName": "branch_Status" },
          { "fieldName": "finYear_End_Status" },
          { "fieldName": "reg_Status" },
          { "fieldName": "stock_Loc_Name" },
          { "fieldName": "party_Relation_Name" },
          { "fieldName": "party_Relation_Code_Id" },
          { "fieldName": "party_MobNo" },
          { "fieldName": "branch_MobNo" },
          { "fieldName": "branch_Area_Code_Id" },
          { "fieldName": "user_party_Code_Id" },
          { "fieldName": "branch_Role_Code" },
          { "fieldName": "branch_Currency_Symbol" }
        ],
        "paginationFields": {
          "firstResult": 0,
          "maxResults": 100
        }
      };

      this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
        .subscribe(
          responce => {
            if (responce.message.code == 0) {
              this.wovvSystem = responce.data.Wovvsystem;
              localStorage.setItem('WovvSystem', JSON.stringify(responce.data.Wovvsystem));
              this.storage.set("WovvSystem", responce.data.Wovvsystem);
              this.completedCalls = this.completedCalls + 1;
              this.CalculateCompletedPer();
              resolve();
            }
            else if (responce.message.code == 4006) {
              resolve();
            }
            else {
              this.loading.presentToast("System Data Sync Failed.");
              resolve();
            }
          },
          err => {
            resolve();
            console.log(err);
          }
        );
    });
  }

  WovvUser() {
    return new Promise((resolve) => {

      this.currentCalls = "User Data";

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('access_Token', this.token);
      headers.append('Register_Id', this.registerCode);
      headers.append('Reg_Mac_Id', this.regMacId);
      var unique = {};
      var that = this;
      this.wovvSystem.forEach(function (x) {
        if (!unique[x['user_Id']]) {
          that.uniqueWovvUsers.push(x);
          unique[x['user_Id']] = true;
        }
      });
      let whereClause = '';
      for (var userIndex = 0; userIndex < this.uniqueWovvUsers.length; userIndex++) {
        let value = this.uniqueWovvUsers[userIndex]['user_Id'];
        whereClause += value + ',';
      }
      whereClause = whereClause.substring(0, whereClause.length - 1);
      var post = {
        "entityName": "WovV_User_MsT",
        "action": "view",
        "event": "selectFilter",
        "pageUrl": "User",
        "whereClause": [{
          "filterKey": "Id",
          "filterValue": whereClause,
          "relation": "IN",
          "condition": ""
        }],
        "selectFields": [
          { "fieldName": "user_Login_ID" },
          { "fieldName": "e1_Password" }
        ]
      };
      this.http.post(this.url + "?tenantId=_" + this.tenantId, post, { headers: headers }).map(res => res.json())
        .subscribe(
          responce => {
            if (responce.message.code == 0) {
              this.wovvUser = responce.data.WovV_User_MsT;
              this.storage.set("WovvUser", responce.data.WovV_User_MsT);
              this.completedCalls = this.completedCalls + 1;
              this.CalculateCompletedPer();
              resolve();
            }
            else if (responce.message.code == 4006) {
              resolve();
            }
            else {
              this.loading.presentToast("System Data Sync Failed.");
              resolve();
            }
          },
          err => {
            resolve();
            console.log(err);
          }
        );
    });
  }

  WovvMenu() {
    return new Promise((resolve) => {

      this.currentCalls = "Menu Data";

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('access_Token', this.token);
      headers.append('Register_Id', this.registerCode);
      headers.append('Reg_Mac_Id', this.regMacId);

      var post = {
        "pageUrl": "menu",
        "entityName": "Url_Role_Type",
        "action": "payloadRule",
        "event": "71",
        "formList": [
          {
            "branch_role": this.wovvSystem[0]['branch_Role_Code']
          }
        ]
      }

      this.http.post(this.url + '?tenantId=' + this._tenantId, post, { headers: headers }).map(res => res.json())
        .subscribe(
          responce => {
            if (responce.message.code == 0) {
              this.wovvMenu = responce.data.Wovvmenu;
              this.storage.set("WovvMenu", responce.data.Wovvmenu);
              this.completedCalls = this.completedCalls + 1;
              this.CalculateCompletedPer();
              resolve();
            }
            else if (responce.message.code == 4006) {
              resolve();
            }
            else {
              this.loading.presentToast("Menu Data Sync Failed.");
              resolve();
            }
          },
          err => {
            resolve();
            console.log(err);
          }
        );
    });
  }

  Catalog(i: number) {

    this.currentCalls = "Catalog Data";

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('access_Token', this.token);
    headers.append('Register_Id', this.registerCode);
    headers.append('Reg_Mac_Id', this.regMacId);

    let post = {
      "pageUrl": "Catlogrpt",
      "entityName": "Catlogrpt",
      "action": "payload",
      "event": "completeSynctoMongo",
      "whereClause": [
        {
          "filterKey": "tid",
          "filterValue": this.tenantId,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "div_Code_Id",
          "filterValue": this.divCode,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "prod_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "prodC_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "prodC_Approve_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "prod_Approve_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "prod_Outward_Allow",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "branch_Code_Id",
          "filterValue": this.branchCode,
          "relation": "=",
          "condition": ""
        }
      ],
      "selectFields": [
        { "fieldName": "prod_Child_Id" },
        { "fieldName": "prodC_Status" },
        { "fieldName": "branch_Code_Id" },
        { "fieldName": "div_Code_Id" },
        { "fieldName": "tax_Code_Pur_Id" },
        { "fieldName": "tax_Code_Sale_Id" },
        { "fieldName": "prod_Code_Id" },
        { "fieldName": "prod_Hdr_Code_Id" },
        { "fieldName": "prod_Name_Desc" },
        { "fieldName": "prod_Image_Path" },
        { "fieldName": "prod_MRP" },
        { "fieldName": "prod_Price" },
        { "fieldName": "prod_PurPrice" },
        { "fieldName": "prod_LandingCost" },
        { "fieldName": "prod_Status" },
        { "fieldName": "prod_Is_ParentItem" },
        { "fieldName": "prod_Is_ingrediant" },
        { "fieldName": "prod_Inventory_Allow" },
        { "fieldName": "prod_Outward_Allow" },
        { "fieldName": "prod_NegativeStock_Bill_allow" },
        { "fieldName": "prod_TypClass_Code_Id" },
        { "fieldName": "mfr_Brand_Code_Id" },
        { "fieldName": "prod_SGrup_Code_Id" },
        { "fieldName": "uom_Group_Code_Id" },
        { "fieldName": "uom_Dtl_Outward_Id" },
        { "fieldName": "org_Code_Id" },
        { "fieldName": "prod_TypCls_Name" },
        { "fieldName": "mfr_Brand_Name" },
        { "fieldName": "prod_SubGroup_Name" },
        { "fieldName": "prod_Grup_Code_Id" },
        { "fieldName": "prod_Group_Name" },
        { "fieldName": "prod_Dept_Code_Id" },
        { "fieldName": "prod_Dept_Name" },
        { "fieldName": "tax_Cls_Name" },
        { "fieldName": "tax_1_Cal_Rate" },
        { "fieldName": "tax_2_Cal_Rate" },
        { "fieldName": "tax_3_Cal_Rate" },
        { "fieldName": "tax_4_Cal_Rate" },
        { "fieldName": "tax_5_Cal_Rate" },
        { "fieldName": "tax_Inclusive" },
        { "fieldName": "tax_Total" },
        { "fieldName": "tax_1_Ledger_Code_Id" },
        { "fieldName": "tax_2_Ledger_Code_Id" },
        { "fieldName": "tax_3_Ledger_Code_Id" },
        { "fieldName": "tax_4_Ledger_Code_Id" },
        { "fieldName": "tax_5_Ledger_Code_Id" },
        { "fieldName": "sale_Subclass_Tax_Typ_Code_Id" },
        { "fieldName": "tax_Type_Name" },
        { "fieldName": "pur_Taxclass_Name" },
        { "fieldName": "prodC_Default" },
        { "fieldName": "pur_Subclass_Tax_1_Cal_Rate" },
        { "fieldName": "pur_Subclass_Tax_2_Cal_Rate" },
        { "fieldName": "pur_Subclass_Tax_3_Cal_Rate" },
        { "fieldName": "pur_Subclass_Tax_4_Cal_Rate" },
        { "fieldName": "pur_Subclass_Tax_5_Cal_Rate" },
        { "fieldName": "pur_Subclass_Tax_Inclusive" },
        { "fieldName": "pur_Subclass_Tax_Prn_Symbol" },
        { "fieldName": "pur_Subclass_Tax_Total" },
        { "fieldName": "pur_Subclass_Tax_1_Ledger_Code_Id" },
        { "fieldName": "pur_Subclass_Tax_2_Ledger_Code_Id" },
        { "fieldName": "pur_Subclass_Tax_3_Ledger_Code_Id" },
        { "fieldName": "pur_Subclass_Tax_4_Ledger_Code_Id" },
        { "fieldName": "pur_Subclass_Tax_5_Ledger_Code_Id" },
        { "fieldName": "prod_EAN_Bcode" },
        { "fieldName": "branch_name" },
        { "fieldName": "prod_Allow_Disc_YN" },
        { "fieldName": "prod_PriceChange_Allow" },
        { "fieldName": "prod_Sale_Default_QTY" },
        { "fieldName": "prod_Sale_Disc_Limit" },
        { "fieldName": "prod_Sale_Disc_Per" },
        { "fieldName": "prod_Sale_Free_YN" },
        { "fieldName": "prod_Sale_Max_QTY" },
        { "fieldName": "prod_Stock_Qty" },
        { "fieldName": "tax_1_Prn_Symbol" },
        { "fieldName": "tax_2_Prn_Symbol" },
        { "fieldName": "tax_3_Prn_Symbol" },
        { "fieldName": "tax_4_Prn_Symbol" },
        { "fieldName": "tax_5_Prn_Symbol" },
        { "fieldName": "prod_UOM_YN" },
        { "fieldName": "prod_Alias" },
        { "fieldName": "prod_TypCls_Prefix" },
        { "fieldName": "prod_AV_Caption" },
        { "fieldName": "prod_ADT_Caption" },
        { "fieldName": "prod_MDT_Caption" },
        { "fieldName": "prod_Parent_Code" },
        { "fieldName": "vrnt_Group_Code_Id" },
        { "fieldName": "vrnt_Type1_Code" },
        { "fieldName": "vrnt_Type2_Code" },
        { "fieldName": "vrnt_Type3_Code" },
        { "fieldName": "tid" },
        { "fieldName": "pur_Subclass_Code_Id" },
        { "fieldName": "sale_Subclass_Code_Id" },
        { "fieldName": "prod_ModifireCustomizer_Allow" },
        { "fieldName": "prod_BarcodeGen_Code" },
        { "fieldName": "prod_BarcodeGen_Type" },
        { "fieldName": "brand_Head_Name" },
        { "fieldName": "tax_SubCls_Name" },
        { "fieldName": "mfr_Brand_Head_Code_Id" },
        { "fieldName": "uom_Grup_Name" }
      ],
      "groupBY": [
        "group by branch_Code_Id,prod_Code_Id,sale_Subclass_Code_Id"
      ],
      "paginationFields": {
        "firstResult": i * this.maxResult,
        "maxResults": this.maxResult
      }
    };
    this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
      .subscribe(data => {
        if (data.message.code == 0) {
          this.catlogData = this.catlogData.concat(data.data.Catlogrpt);
          if (data.data.Catlogrpt.length >= this.maxResult) {
            this.Catalog(i + 1);
          }
          else {
            var uniqueProducts = [];
            var ModifierOnProducts: any = [];
            var uniqueProductsIds: any = [];
            this.catlogData.forEach((x) => {
              if (x['prod_ModifireCustomizer_Allow'] == 1) {
                ModifierOnProducts.push(x['prod_Code_Id']);
              }
            });
            ModifierOnProducts.forEach((x) => {
              if (!uniqueProducts[x]) {
                uniqueProductsIds.push(x);
                uniqueProducts[x] = true;
              }
            });
            this.uniqueBranchIds = uniqueProductsIds.join();
            localStorage.setItem('uniqueProductsId', uniqueProductsIds.join());
            this.foundTS = data.message.syncTime;
            this.updateTS(this.foundTS, "Catlogrpt");
            this.storage.set("Catalog", this.catlogData);
            this.completedCalls = this.completedCalls + 1;
            this.CalculateCompletedPer();
            this.Party(0);
          }
        }
        else if (data.message.code == 4006) {
          this.completedCalls = this.completedCalls + 1;
          this.CalculateCompletedPer();
          this.Party(0);
        }
        else {
          this.loading.presentToast("Server Error. Please contact administrator.");
          this.IsError = true;
        }
      }, err => {
        this.catlogData = [];
        this.IsError = true;
      });

  }

  Party(i: any) {
    // return new Promise((resolve) => {

    this.currentCalls = "Parties Data";

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('access_Token', this.token);
    headers.append('Register_Id', this.registerCode);
    headers.append('Reg_Mac_Id', this.regMacId);

    let post = {
      "pageUrl": "Partyrpt",
      "entityName": "Partyrpt",
      "action": "payload",
      "event": "completeSynctoMongo",
      "whereClause": [
        {
          "filterKey": "tid",
          "filterValue": this.tenantId,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "party_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "approve_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "branch_Code_Id",
          "filterValue": this.branchCode,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "org_Code_Id",
          "filterValue": this.orgCode,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "party_Relation_Name",
          "filterValue": "Employee",
          "relation": "=",
          "condition": "OR"
        },
        {
          "filterKey": "party_Relation_Name",
          "filterValue": "Customer",
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "pcpt_Status",
          "filterValue": "1",
          "relation": "=",
          "condition": ""
        },
      ],
      "selectFields": [
        { "fieldName": "party_Child_Id" },
        { "fieldName": "party_Email" },
        { "fieldName": "party_DOB" },
        { "fieldName": "approve_Status" },
        { "fieldName": "branch_Code_Id" },
        { "fieldName": "party_Code_Id" },
        { "fieldName": "party_Tax_Type_Code_Id" },
        { "fieldName": "tax_Code_Pur_Id" },
        { "fieldName": "tax_Code_Sale_Id" },
        { "fieldName": "party_Head_Id" },
        { "fieldName": "is_System_Default" },
        { "fieldName": "party_Add_Street" },
        { "fieldName": "party_Alias" },
        { "fieldName": "party_Area_Pincode" },
        { "fieldName": "party_GST_No" },
        { "fieldName": "party_MobNo" },
        { "fieldName": "party_Name" },
        { "fieldName": "party_Other_Reg_No" },
        { "fieldName": "party_PhNo" },
        { "fieldName": "party_Relation_Name" },
        { "fieldName": "party_Status" },
        { "fieldName": "org_Code_Id" },
        { "fieldName": "party_Delivery_Type_Id" },
        { "fieldName": "party_Relation_Code_Id" },
        { "fieldName": "party_SalMan_Code_Id" },
        { "fieldName": "area_Name" },
        { "fieldName": "state_Name" },
        { "fieldName": "party_Contact_Add_door" },
        { "fieldName": "party_Contact_Add_Street" },
        { "fieldName": "party_Contact_Area_Name" },
        { "fieldName": "party_Contact_City_Name" },
        { "fieldName": "party_Contact_state_Name" },
        { "fieldName": "party_Contact_Country_Name" },
        { "fieldName": "pay_Typ_Id" },
        { "fieldName": "pay_Typ_Name" },
        { "fieldName": "pay_Typ_Arrange_Order" },
        { "fieldName": "party_Tax_Type_Name" },
        { "fieldName": "tid" },
        { "fieldName": "is_Tax_Registered" },
        { "fieldName": "empPosition_Code_Id" },
        { "fieldName": "pay_Mode_Code" },
        { "fieldName": "pay_Mode_Name" },
        { "fieldName": "id" },
        { "fieldName": "pcpt_Status" }
      ],
      "paginationFields": {
        "firstResult": i * this.maxResult,
        "maxResults": this.maxResult
      }
    };
    this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
      .subscribe(data => {
        if (data.message.code == 0) {
          this.partyData = this.partyData.concat(data.data.Partyrpt);
          if (data.data.Partyrpt.length >= this.maxResult) {
            // resolve();
            this.Party(i + 1);
          }
          else {
            this.foundTS = data.message.syncTime;
            this.updateTS(this.foundTS, "Partyrpt");
            this.storage.set("Party", this.partyData);
            this.completedCalls = this.completedCalls + 1;
            this.CalculateCompletedPer();
            this.DeliveryType(0);
            // resolve();
          }
        }
        else if (data.message.code == 4006) {
          this.completedCalls = this.completedCalls + 1;
          this.CalculateCompletedPer();
          this.DeliveryType(0);
          // resolve();
        }
        else {
          this.loading.presentToast("Server Error. Please contact administrator.");
          this.IsError = true;
        }
      }, err => {
        this.partyData = [];
        this.IsError = true;
        // resolve();
      });
    // });
  }

  DeliveryType(i) {
    // return new Promise((resolve) => {

    this.currentCalls = "Delivery Data";

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('access_Token', this.token);
    headers.append('Register_Id', this.registerCode);
    headers.append('Reg_Mac_Id', this.regMacId);

    let post = {
      "pageUrl": "DeliveryTypeHeadMsT",
      "entityName": "DeliveryType_Head_MsT",
      "action": "payload",
      "event": "completeSynctoMongo",
      "whereClause": [
        {
          "filterKey": "approve_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "org_Code_Id",
          "filterValue": this.orgCode,
          "relation": "=",
          "condition": ""
        }
      ],
      "selectFields": [
        { "fieldName": "Id" },
        { "fieldName": "deliveryType_Name" },
        { "fieldName": "deliveryType_Status" },
        { "fieldName": "approve_Status" },
        { "fieldName": "is_System_Default" },
        { "fieldName": "deliveryCharge_Value" },
        { "fieldName": "is_DeliveryCharge_Per" },
        { "fieldName": "is_Home_Delivery" },
        { "fieldName": "deliveryDue_Cal_Value" },
        { "fieldName": "deliveryType_Default" },
        { "fieldName": "deliveryDue_Cal_Code" },
        { "fieldName": "deliveryDue_Cal_Name" }
      ],
      "paginationFields": {
        "firstResult": i * this.maxResult,
        "maxResults": this.maxResult
      }
    };
    this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers })
      .map(res => res.json())
      .subscribe(data => {
        if (data.message.code == 0) {
          if (data.data.DeliveryType_Head_MsT.length >= this.maxResult) {
            // resolve();
            this.DeliveryType(i + 1);
          }
          else {
            this.foundTS = data.message.syncTime;
            this.updateTS(this.foundTS, "DeliveryType_Head_MsT");
            this.storage.set("DeliveryType", data.data.DeliveryType_Head_MsT);
            this.completedCalls = this.completedCalls + 1;
            this.CalculateCompletedPer();
            this.Modifires(0);
            // resolve();
          }
        }
        else if (data.message.code == 4006) {
          this.completedCalls = this.completedCalls + 1;
          this.CalculateCompletedPer();
          this.Modifires(0);
          // resolve();
        }
        else {
          this.loading.presentToast("Server Error. Please contact administrator.");
          this.IsError = true;
        }
      }, err => {
        this.IsError = true;
        console.log(err);
        // resolve();
      });
    // });
  }

  Modifires(i) {
    // return new Promise((resolve) => {

    this.currentCalls = "Modifires Data";

    var uniqueIds = localStorage.getItem('uniqueProductsId');
    if (uniqueIds == null || uniqueIds == "") {
      this.Recipe(0);
      return;
    }

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('access_Token', this.token);
    headers.append('Register_Id', this.registerCode);
    headers.append('Reg_Mac_Id', this.regMacId);

    let post = {
      "pageUrl": "ProdModifierOpT",
      "entityName": "Prod_Modifier_OpT",
      "action": "payload",
      "event": "completeSynctoMongo",
      "whereClause": [
        {
          "filterKey": "approve_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "org_Code_Id",
          "filterValue": this.orgCode,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "pmo_Prod_Code",
          "filterValue": uniqueIds,
          "relation": "IN",
          "condition": ""
        }
      ],
      "selectFields": [
        { "fieldName": "Id" },
        { "fieldName": "approve_Status" },
        { "fieldName": "is_System_Default" },
        { "fieldName": "pmo_Status" },
        { "fieldName": "pmo_User_Code_Add" },
        { "fieldName": "pmo_User_Code_Updt" },
        { "fieldName": "pmo_TS_Added" },
        { "fieldName": "pmo_TS_Edited" },
        { "fieldName": "pmo_Prod_Code" },
        { "fieldName": "pmo_Prod_Name" },
        { "fieldName": "pmo_MCLS_Name" },
        { "fieldName": "pmo_MCLS_Optional" },
        { "fieldName": "pmo_MCLS_Multiple" },
        { "fieldName": "pmo_MCLS_Virtual" },
        { "fieldName": "pmo_MOPT_Code" },
        { "fieldName": "pmo_MOPT_Name" },
        { "fieldName": "pmo_MOPT_Qty" },
        { "fieldName": "pmo_MOPT_Rate" },
        { "fieldName": "pmo_MOPT_Cost" },
        { "fieldName": "pmo_MOPT_Order" },
        { "fieldName": "pmo_MOPT_UOM_YN" },
        { "fieldName": "pmo_MOPT_UOMG_Code" },
        { "fieldName": "pmo_MOPT_UOMG_Name" },
        { "fieldName": "pmo_MOPT_UOMD_Code" },
        { "fieldName": "pmo_MOPT_UOMD_Name" },
        { "fieldName": "pmo_MOPT_Default" },
        { "fieldName": "pmo_AddRate_Calc_Per" },
        { "fieldName": "pmo_Ing_Prod_YN" },
        { "fieldName": "pmo_Ing_Prod_Code" },
        { "fieldName": "pmo_Ing_Prod_Name" },
        { "fieldName": "pmo_Ing_Prod_Qty" },
        { "fieldName": "pmo_Ing_UOMG_Code" },
        { "fieldName": "pmo_Ing_UOMG_Name" },
        { "fieldName": "pmo_Ing_UOMD_Code" },
        { "fieldName": "pmo_Ing_UOMD_Name" },
        { "fieldName": "pmo_Name" },
        { "fieldName": "pmo_Prefix" },
        { "fieldName": "pmo_Order" },
        { "fieldName": "pmo_Remark" },
        { "fieldName": "branch_Code" },
        { "fieldName": "pmo_MCLS_Code_Id" },
        { "fieldName": "org_Code_Id" }
      ],
      "paginationFields": {
        "firstResult": i * this.maxResult,
        "maxResults": this.maxResult
      }
    };
    this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
      .subscribe(data => {
        if (data.message.code == 0) {
          if (data.data.Prod_Modifier_OpT.length >= this.maxResult) {
            // resolve();
            this.Modifires(i + 1);
          }
          else {
            this.foundTS = data.message.syncTime;
            this.updateTS(this.foundTS, "Prod_Modifier_OpT");
            this.storage.set("Modifiers", data.data.Prod_Modifier_OpT);
            this.completedCalls = this.completedCalls + 1;
            this.CalculateCompletedPer();
            this.Recipe(0);
            // resolve();
          }
        }
        else if (data.message.code == 4006) {
          this.completedCalls = this.completedCalls + 1;
          this.CalculateCompletedPer();
          this.Recipe(0);
          // resolve();
        }
        else {
          this.loading.presentToast("Server Error. Please contact administrator.");
          this.IsError = true;
        }
      }, err => {
        this.IsError = true;
        // resolve();
      });
    // });
  }

  Recipe(i) {
    // return new Promise((resolve) => {
    this.currentCalls = "Recipe Data";
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('access_Token', this.token);
    headers.append('Register_Id', this.registerCode);
    headers.append('Reg_Mac_Id', this.regMacId);

    let post = {
      "pageUrl": "RecipeMsT",
      "entityName": "Recipe_MsT",
      "action": "payload",
      "event": "completeSynctoMongo",
      "multiFilters": [
        {
          "mainEntityType": "",
          "mainEntity": "Recipe_MsT",
          "chiledEntityType": "",
          "childEntity": "Recipe_Dtl rd",
          "alias": "recipe_mst",
          "fk": "rd.recipe_MsT_Id"
        }
      ],
      "whereClause": [
        {
          "filterKey": "rm_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "recipe_mst.approve_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "org_Code_Id",
          "filterValue": this.orgCode,
          "relation": "=",
          "condition": ""
        }
      ],
      "selectFields": [
        { "fieldName": "recipe_mst.Id as rm_Id" },
        { "fieldName": "recipe_mst.approve_Status as rm_approve_Status" },
        { "fieldName": "recipe_mst.is_System_Default as rm_is_System_Default" },
        { "fieldName": "recipe_mst.rm_Status as rm_Status" },
        { "fieldName": "recipe_mst.rm_Prod_Name as rm_Prod_Name" },
        { "fieldName": "recipe_mst.rm_Recipe_Desc as rm_Recipe_Desc" },
        { "fieldName": "recipe_mst.rm_Recipe_Alias as rm_Recipe_Alias" },
        { "fieldName": "recipe_mst.rm_Recipe_Remark as rm_Recipe_Remark" },
        { "fieldName": "recipe_mst.rm_Recipe_Qty as rm_Recipe_Qty" },
        { "fieldName": "recipe_mst.rm_Recipe_UOM_Code as rm_Recipe_UOM_Code" },
        { "fieldName": "recipe_mst.rm_Recipe_UOM_Name as rm_Recipe_UOM_Name" },
        { "fieldName": "recipe_mst.rm_Recipe_Type_Code as rm_Recipe_Type_Code" },
        { "fieldName": "recipe_mst.rm_Recipe_Type_Name as rm_Recipe_Type_Name" },
        { "fieldName": "recipe_mst.rm_type_Class_Code as rm_type_Class_Code" },
        { "fieldName": "recipe_mst.rm_type_Class_Name as rm_type_Class_Name" },
        { "fieldName": "recipe_mst.rm_type_Class_Prefix as rm_type_Class_Prefix" },
        { "fieldName": "recipe_mst.is_Branch as is_Branch" },
        { "fieldName": "recipe_mst.branch_Code as branch_Code" },
        { "fieldName": "recipe_mst.rm_Default_Recipe as rm_Default_Recipe" },
        { "fieldName": "recipe_mst.is_Multi_Stapes as is_Multi_Stapes" },
        { "fieldName": "recipe_mst.calc_AddOn_Cost as calc_AddOn_Cost" },
        { "fieldName": "recipe_mst.rm_Prod_Code_Id as rm_Prod_Code_Id" },
        { "fieldName": "recipe_mst.org_Code_Id as org_Code_Id" },
        { "fieldName": "rd.id as rd_Id" },
        { "fieldName": "rd.approve_Status as rd_approve_Status" },
        { "fieldName": "rd.is_System_Default as rd_is_System_Default" },
        { "fieldName": "rd.rd_Ing_Status as rd_Ing_Status" },
        { "fieldName": "rd.rd_Ing_Prod_Name as rd_Ing_Prod_Name" },
        { "fieldName": "rd.rd_Ing_Prod_Qty as rd_Ing_Prod_Qty" },
        { "fieldName": "rd.rd_Ing_UOM_Group_Code as rd_Ing_UOM_Group_Code" },
        { "fieldName": "rd.rd_Ing_UOM_Group_Name as rd_Ing_UOM_Group_Name" },
        { "fieldName": "rd.rd_Ing_UOM_Dtl_Code as rd_Ing_UOM_Dtl_Code" },
        { "fieldName": "rd.rd_Ing_UOM_Dtl_AltName as rd_Ing_UOM_Dtl_AltName" },
        { "fieldName": "rd.rd_Ing_Remark as rd_Ing_Remark" },
        { "fieldName": "rd.rd_Allow_AlterNet as rd_Allow_AlterNet" },
        { "fieldName": "rd.rd_Allow_Cost_Change as rd_Allow_Cost_Change" },
        { "fieldName": "rd.rd_Allow_AddOn_Prod as rd_Allow_AddOn_Prod" },
        { "fieldName": "rd.rd_Resource_YN as rd_Resource_YN" },
        { "fieldName": "rd.rd_type_Class_Code as rd_type_Class_Code" },
        { "fieldName": "rd.rd_type_Class_Name as rd_type_Class_Name" },
        { "fieldName": "rd.rd_type_Class_Prefix as rd_type_Class_Prefix" },
        { "fieldName": "rd.rd_Is_ByProd as rd_Is_ByProd" },
        { "fieldName": "rd.rd_ByProd_Type_code as rd_ByProd_Type_code" },
        { "fieldName": "rd.rd_ByProd_Type_Name as rd_ByProd_Type_Name" },
        { "fieldName": "rd.rd_Allow_Excess_Qty as rd_Allow_Excess_Qty" },
        { "fieldName": "rd.rd_Prod_Code_Id as rd_Prod_Code_Id" },
        { "fieldName": "rd.recipe_MsT_Id as recipe_MsT_Id" }
      ],
      "paginationFields": {
        "firstResult": i * this.maxResult,
        "maxResults": this.maxResult
      }
    };
    this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
      .subscribe(data => {
        if (data.message.code == 0) {
          if (data.data.Recipe_MsT.length >= this.maxResult) {
            // resolve();
            this.Recipe(i + 1);
          }
          else {
            this.foundTS = data.message.syncTime;
            this.updateTS(this.foundTS, "Recipe_Mst");
            this.storage.set("Recipe", data.data.Recipe_MsT);
            this.completedCalls = this.completedCalls + 1;
            this.CalculateCompletedPer();
            this.StockClass(0);
            // resolve();
          }
        }
        else if (data.message.code == 4006) {
          this.completedCalls = this.completedCalls + 1;
          this.CalculateCompletedPer();
          this.StockClass(0);
          // resolve();
        }
        else {
          this.loading.presentToast("Server Error. Please contact administrator.");
          this.IsError = true;
        }
      }, err => {
        this.IsError = true;
        // resolve();
      });
    // });
  }

  StockClass(i) {
    // return new Promise((resolve) => {

    this.currentCalls = "Stock Data";

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('access_Token', this.token);
    headers.append('Register_Id', this.registerCode);
    headers.append('Reg_Mac_Id', this.regMacId);

    let post = {
      "pageUrl": "StockClass",
      "entityName": "Stock_Class",
      "action": "payload",
      "event": "completeSynctoMongo",
      "whereClause": [
        {
          "filterKey": "active_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "approve_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "org_Code_Id",
          "filterValue": this.orgCode,
          "relation": "=",
          "condition": ""
        }
      ],
      "selectFields": [
        { "fieldName": "active_Status as active_Status" },
        { "fieldName": "approve_Status as approve_Status" },
        { "fieldName": "stock_Class_Name as stock_Class_Name" },
        { "fieldName": "stock_Class_TS_Added as stock_Class_TS_Added" },
        { "fieldName": "stock_Class_TS_Edited as stock_Class_TS_Edited" },
        { "fieldName": "stock_Class_User_Code_Add as stock_Class_User_Code_Add" },
        { "fieldName": "stock_Class_User_Code_Updt as stock_Class_User_Code_Updt" },
        { "fieldName": "org_Code_Id as org_Code_Id" },
        { "fieldName": "stock_Class_Type_Code as stock_Class_Type_Code" },
        { "fieldName": "stock_Class_Type_Name as stock_Class_Type_Name" },
        { "fieldName": "Id as stock_Class_Id" }
      ],
      "paginationFields": {
        "firstResult": i * this.maxResult,
        "maxResults": this.maxResult
      }
    };

    this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
      .subscribe(data => {
        if (data.message.code == 0) {
          this.foundTS = data.message.syncTime;
          this.updateTS(this.foundTS, "Stock_Class");
          this.storage.set("StockClass", data.data.Stock_Class);
          this.completedCalls = this.completedCalls + 1;
          this.CalculateCompletedPer();
          this.StockbookGt0(0);
          // resolve();
        }
        else if (data.message.code == 4006) {
          this.completedCalls = this.completedCalls + 1;
          this.CalculateCompletedPer();
          this.StockbookGt0(0);

          // resolve();
        }
        else {
          this.loading.presentToast("Server Error. Please contact administrator.");
          this.IsError = true;
        }
      }, err => {
        this.IsError = true;
        // resolve();
      });
    // });
  }

  StockbookGt0(i: any) {
    // return new Promise((resolve) => {
    this.currentCalls = "Stock Book Data";

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('access_Token', this.token);
    headers.append('Register_Id', this.registerCode);
    headers.append('Reg_Mac_Id', this.regMacId);

    let post = {
      "pageUrl": "Stockbook",
      "entityName": "Stock_Book",
      "action": "payload",
      "event": "completeSynctoMongo",
      "whereClause": [
        {
          "filterKey": "psd_Branch_Code_Id",
          "filterValue": this.branchCode,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "approve_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "is_Added",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "psd_Available_Qty",
          "filterValue": 0,
          "relation": ">",
          "condition": "AND"
        },
        {
          "filterKey": "psd_Stock_Loc_Code_Id",
          "filterValue": this.stockLocCode,
          "relation": "=",
          "condition": ""
        }
      ],
      "selectFields": [
        { "fieldName": "Id" },
        { "fieldName": "approve_Status" },
        { "fieldName": "psd_Status" },
        { "fieldName": "psd_ADT_Caption" },
        { "fieldName": "psd_AV_Caption" },
        { "fieldName": "psd_Attribute_DT" },
        { "fieldName": "psd_Attribute_Val" },
        { "fieldName": "psd_MDT_Caption" },
        { "fieldName": "psd_MFR_DT" },
        { "fieldName": "is_Added" },
        { "fieldName": "psd_Available_Qty" },
        { "fieldName": "psd_Free_Tag" },
        { "fieldName": "psd_LandingCost" },
        { "fieldName": "psd_MRP" },
        { "fieldName": "psd_PurPrice" },
        { "fieldName": "psd_Rate" },
        { "fieldName": "psd_Prod_Code_Id" },
        { "fieldName": "psd_Prod_Name" },
        { "fieldName": "psd_Prod_Remark" },
        { "fieldName": "psd_eanupc_Code" },
        { "fieldName": "psd_Prod_Barcode" },
        { "fieldName": "psd_Stock_Loc_Code_Id" },
        { "fieldName": "psd_Stock_Loc_Name" },
        { "fieldName": "psd_Branch_Code_Id" },
        { "fieldName": "psd_Div_Code_Id" },
        { "fieldName": "psd_Org_Code_Id" },
        { "fieldName": "psd_Stock_Class_Code_Id" },
        { "fieldName": "psd_Stock_Class_Name" },
        { "fieldName": "psd_JOB_Class_Code" },
        { "fieldName": "is_Qty_Used" },
        { "fieldName": "psd_JOB_Class_Name" }
      ],
      "paginationFields": {
        "firstResult": i * this.maxResult,
        "maxResults": this.maxResult
      }
    };
    this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
      .subscribe(data => {
        if (data.message.code == 0) {
          this.stockData = this.stockData.concat(data.data.Stock_Book);
          if (data.data.Stock_Book.length >= this.maxResult) {
            // resolve();
            this.StockbookGt0(i + 1);
          }
          else {
            this.storage.set("Stockbook", this.stockData);
            this.StockbookLs0(0);
            // resolve();
          }
        }
        else if (data.message.code == 4006) {
          this.StockbookLs0(0);
          // resolve();
        }
        else {
          this.loading.presentToast("Server Error. Please contact administrator.");
          this.IsError = true;
        }
      }, err => {
        this.stockData = [];
        this.IsError = true;
        // resolve();
      });
    // });
  }

  StockbookLs0(i: any) {
    // return new Promise((resolve) => {

    this.currentCalls = "Stock Book Data";

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('access_Token', this.token);
    headers.append('Register_Id', this.registerCode);
    headers.append('Reg_Mac_Id', this.regMacId);

    let post = {
      "pageUrl": "Stockbook",
      "entityName": "Stock_Book",
      "action": "payload",
      "event": "completeSynctoMongo",
      "whereClause": [
        {
          "filterKey": "psd_Branch_Code_Id",
          "filterValue": this.branchCode,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "approve_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "psd_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "is_Added",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "psd_Available_Qty",
          "filterValue": 0,
          "relation": "<",
          "condition": "AND"
        },
        {
          "filterKey": "psd_Stock_Loc_Code_Id",
          "filterValue": this.stockLocCode,
          "relation": "=",
          "condition": ""
        }
      ],
      "selectFields": [
        { "fieldName": "Id" },
        { "fieldName": "approve_Status" },
        { "fieldName": "psd_Status" },
        { "fieldName": "psd_ADT_Caption" },
        { "fieldName": "psd_AV_Caption" },
        { "fieldName": "psd_Attribute_DT" },
        { "fieldName": "psd_Attribute_Val" },
        { "fieldName": "psd_MDT_Caption" },
        { "fieldName": "psd_MFR_DT" },
        { "fieldName": "is_Added" },
        { "fieldName": "sum(psd_Available_Qty) as psd_Available_Qty" },
        { "fieldName": "psd_Free_Tag" },
        { "fieldName": "psd_LandingCost" },
        { "fieldName": "psd_MRP" },
        { "fieldName": "psd_PurPrice" },
        { "fieldName": "psd_Rate" },
        { "fieldName": "psd_Prod_Code_Id" },
        { "fieldName": "psd_Prod_Name" },
        { "fieldName": "psd_Prod_Remark" },
        { "fieldName": "psd_eanupc_Code" },
        { "fieldName": "psd_Prod_Barcode" },
        { "fieldName": "psd_Stock_Loc_Code_Id" },
        { "fieldName": "psd_Stock_Loc_Name" },
        { "fieldName": "psd_Branch_Code_Id" },
        { "fieldName": "psd_Div_Code_Id" },
        { "fieldName": "psd_Org_Code_Id" },
        { "fieldName": "psd_Stock_Class_Code_Id" },
        { "fieldName": "psd_Stock_Class_Name" },
        { "fieldName": "psd_JOB_Class_Code" },
        { "fieldName": "is_Qty_Used" },
        { "fieldName": "psd_JOB_Class_Name" }
      ],
      "groupBY": ["group by psd_branch_code_id , psd_stock_loc_code_id , psd_Prod_Code_Id"],
      "paginationFields": {
        "firstResult": i * this.maxResult,
        "maxResults": this.maxResult
      }
    };
    this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
      .subscribe(data => {
        if (data.message.code == 0) {
          this.stockData = this.stockData.concat(data.data.Stock_Book);
          if (data.data.Stock_Book.length >= this.maxResult) {
            // resolve();
            this.StockbookLs0(i + 1);
          }
          else {
            this.foundTS = data.message.syncTime;
            this.updateTS(this.foundTS, "Stock_Book");
            this.storage.set("Stockbook", this.stockData);
            this.completedCalls = this.completedCalls + 1;
            this.CalculateCompletedPer();
            // resolve();
          }
        }
        else if (data.message.code == 4006) {
          this.completedCalls = this.completedCalls + 1;
          this.CalculateCompletedPer();
          // resolve();
        }
        else {
          this.IsError = true;
          this.loading.presentToast("Server Error. Please contact administrator.");
          this.completedCalls = this.completedCalls + 1;
          this.CalculateCompletedPer();
          // resolve();
        }
      }, err => {
        this.stockData = [];
        this.IsError = true;
        // resolve();
      });
    // });
  }

  IncrementalCall() {
    return new Promise((resolve) => {
      this.WovvSystem().then(() => {
        this.WovvUser().then(() => {
          this.WovvMenu().then(() => {
            this.IncrementCatalog(0).then(() => {
              this.IncrementParty(0).then(() => {
                this.IncrementDelivery(0).then(() => {
                  this.IncrementModifier(0).then(() => {
                    this.IncrementRecipe(0).then(() => {
                      this.IncrementStockClass(0).then(() => {
                        this.IncrementStockBook(0).then(() => {
                          this.IncrementStockBookUpdateRow(0).then(() => {
                            resolve();
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  }

  IncrementCatalog(i) {
    return new Promise((resolve) => {

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('access_Token', this.token);
      headers.append('Register_Id', this.registerCode);
      headers.append('Reg_Mac_Id', this.regMacId);

      let post = {
        "pageUrl": "Catlogrpt",
        "entityName": "Catlogrpt",
        "action": "payload",
        "event": "completeSynctoMongo",
        "whereClause": [
          {
            "filterKey": "tid",
            "filterValue": this.tenantId,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "div_Code_Id",
            "filterValue": this.divCode,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "prod_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "prodC_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "prodC_Approve_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "prod_Approve_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "prod_Outward_Allow",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "branch_Code_Id",
            "filterValue": this.branchCode,
            "relation": "=",
            "condition": ""
          }
        ],
        "selectFields": [
          { "fieldName": "prod_Child_Id" },
          { "fieldName": "prodC_Status" },
          { "fieldName": "branch_Code_Id" },
          { "fieldName": "div_Code_Id" },
          { "fieldName": "tax_Code_Pur_Id" },
          { "fieldName": "tax_Code_Sale_Id" },
          { "fieldName": "prod_Code_Id" },
          { "fieldName": "prod_Hdr_Code_Id" },
          { "fieldName": "prod_Name_Desc" },
          { "fieldName": "prod_Image_Path" },
          { "fieldName": "prod_MRP" },
          { "fieldName": "prod_Price" },
          { "fieldName": "prod_PurPrice" },
          { "fieldName": "prod_LandingCost" },
          { "fieldName": "prod_Status" },
          { "fieldName": "prod_Is_ParentItem" },
          { "fieldName": "prod_Is_ingrediant" },
          { "fieldName": "prod_Inventory_Allow" },
          { "fieldName": "prod_Outward_Allow" },
          { "fieldName": "prod_NegativeStock_Bill_allow" },
          { "fieldName": "prod_TypClass_Code_Id" },
          { "fieldName": "mfr_Brand_Code_Id" },
          { "fieldName": "prod_SGrup_Code_Id" },
          { "fieldName": "uom_Group_Code_Id" },
          { "fieldName": "uom_Dtl_Outward_Id" },
          { "fieldName": "org_Code_Id" },
          { "fieldName": "prod_TypCls_Name" },
          { "fieldName": "mfr_Brand_Name" },
          { "fieldName": "prod_SubGroup_Name" },
          { "fieldName": "prod_Grup_Code_Id" },
          { "fieldName": "prod_Group_Name" },
          { "fieldName": "prod_Dept_Code_Id" },
          { "fieldName": "prod_Dept_Name" },
          { "fieldName": "tax_Cls_Name" },
          { "fieldName": "tax_1_Cal_Rate" },
          { "fieldName": "tax_2_Cal_Rate" },
          { "fieldName": "tax_3_Cal_Rate" },
          { "fieldName": "tax_4_Cal_Rate" },
          { "fieldName": "tax_5_Cal_Rate" },
          { "fieldName": "tax_Inclusive" },
          { "fieldName": "tax_Total" },
          { "fieldName": "tax_1_Ledger_Code_Id" },
          { "fieldName": "tax_2_Ledger_Code_Id" },
          { "fieldName": "tax_3_Ledger_Code_Id" },
          { "fieldName": "tax_4_Ledger_Code_Id" },
          { "fieldName": "tax_5_Ledger_Code_Id" },
          { "fieldName": "sale_Subclass_Tax_Typ_Code_Id" },
          { "fieldName": "tax_Type_Name" },
          { "fieldName": "pur_Taxclass_Name" },
          { "fieldName": "prodC_Default" },
          { "fieldName": "pur_Subclass_Tax_1_Cal_Rate" },
          { "fieldName": "pur_Subclass_Tax_2_Cal_Rate" },
          { "fieldName": "pur_Subclass_Tax_3_Cal_Rate" },
          { "fieldName": "pur_Subclass_Tax_4_Cal_Rate" },
          { "fieldName": "pur_Subclass_Tax_5_Cal_Rate" },
          { "fieldName": "pur_Subclass_Tax_Inclusive" },
          { "fieldName": "pur_Subclass_Tax_Prn_Symbol" },
          { "fieldName": "pur_Subclass_Tax_Total" },
          { "fieldName": "pur_Subclass_Tax_1_Ledger_Code_Id" },
          { "fieldName": "pur_Subclass_Tax_2_Ledger_Code_Id" },
          { "fieldName": "pur_Subclass_Tax_3_Ledger_Code_Id" },
          { "fieldName": "pur_Subclass_Tax_4_Ledger_Code_Id" },
          { "fieldName": "pur_Subclass_Tax_5_Ledger_Code_Id" },
          { "fieldName": "prod_EAN_Bcode" },
          { "fieldName": "branch_name" },
          { "fieldName": "prod_Allow_Disc_YN" },
          { "fieldName": "prod_PriceChange_Allow" },
          { "fieldName": "prod_Sale_Default_QTY" },
          { "fieldName": "prod_Sale_Disc_Limit" },
          { "fieldName": "prod_Sale_Disc_Per" },
          { "fieldName": "prod_Sale_Free_YN" },
          { "fieldName": "prod_Sale_Max_QTY" },
          { "fieldName": "prod_Stock_Qty" },
          { "fieldName": "tax_1_Prn_Symbol" },
          { "fieldName": "tax_2_Prn_Symbol" },
          { "fieldName": "tax_3_Prn_Symbol" },
          { "fieldName": "tax_4_Prn_Symbol" },
          { "fieldName": "tax_5_Prn_Symbol" },
          { "fieldName": "prod_UOM_YN" },
          { "fieldName": "prod_Alias" },
          { "fieldName": "prod_TypCls_Prefix" },
          { "fieldName": "prod_AV_Caption" },
          { "fieldName": "prod_ADT_Caption" },
          { "fieldName": "prod_MDT_Caption" },
          { "fieldName": "prod_Parent_Code" },
          { "fieldName": "vrnt_Group_Code_Id" },
          { "fieldName": "vrnt_Type1_Code" },
          { "fieldName": "vrnt_Type2_Code" },
          { "fieldName": "vrnt_Type3_Code" },
          { "fieldName": "tid" },
          { "fieldName": "pur_Subclass_Code_Id" },
          { "fieldName": "sale_Subclass_Code_Id" },
          { "fieldName": "prod_ModifireCustomizer_Allow" },
          { "fieldName": "prod_BarcodeGen_Code" },
          { "fieldName": "prod_BarcodeGen_Type" },
          { "fieldName": "brand_Head_Name" },
          { "fieldName": "tax_SubCls_Name" },
          { "fieldName": "mfr_Brand_Head_Code_Id" },
          { "fieldName": "uom_Grup_Name" }
        ],
        "groupBY": [
          "group by branch_Code_Id ,prod_Code_Id,sale_Subclass_Code_Id"
        ],
        "paginationFields": {
          "firstResult": i * this.maxResult,
          "maxResults": this.maxResult
        }
      };
      this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
        .subscribe(responce => {
          if (responce.message.code == 0) {
            this.inc_cat = this.inc_cat.concat(responce.data.Catlogrpt);
            if (responce.data.Catlogrpt.length >= this.maxResult) {
              // resolve();
              this.IncrementCatalog(i + 1);
            }
            else {
              this.foundTS = responce.message.syncTime;
              this.updateTS(this.foundTS, "Catlogrpt");
              this.storage.get('Catalog').then((data) => {
                for (let num = 0; num < this.inc_cat.length; num++) {
                  data = _.without(data, _.findWhere(data, { prod_Child_Id: this.inc_cat[num].prod_Child_Id }));
                }
                data = data.concat(this.inc_cat);
                this.storage.set("Catalog", data);
              });
              resolve();
            }
          }
          else if (responce.message.code == 4006) {
            resolve();
          }
          else {
            this.loading.presentToast("Catalog Data Sync Failed.");
            resolve();
          }
        }, err => {
          resolve();
          this.inc_cat = [];
        });
    });
  }

  IncrementParty(i) {
    return new Promise((resolve) => {

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('access_Token', this.token);
      headers.append('Register_Id', this.registerCode);
      headers.append('Reg_Mac_Id', this.regMacId);

      let post = {
        "pageUrl": "Partyrpt",
        "entityName": "Partyrpt",
        "action": "payload",
        "event": "completeSynctoMongo",
        "whereClause": [
          {
            "filterKey": "tid",
            "filterValue": this.tenantId,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "party_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "approve_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "branch_Code_Id",
            "filterValue": this.branchCode,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "org_Code_Id",
            "filterValue": this.orgCode,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "party_Relation_Name",
            "filterValue": "Employee",
            "relation": "=",
            "condition": "OR"
          },
          {
            "filterKey": "party_Relation_Name",
            "filterValue": "Customer",
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "pcpt_Status",
            "filterValue": "1",
            "relation": "=",
            "condition": ""
          },
        ],
        "selectFields": [
          { "fieldName": "party_Child_Id" },
          { "fieldName": "party_Email" },
          { "fieldName": "party_DOB" },
          { "fieldName": "approve_Status" },
          { "fieldName": "branch_Code_Id" },
          { "fieldName": "party_Code_Id" },
          { "fieldName": "party_Tax_Type_Code_Id" },
          { "fieldName": "tax_Code_Pur_Id" },
          { "fieldName": "tax_Code_Sale_Id" },
          { "fieldName": "party_Head_Id" },
          { "fieldName": "is_System_Default" },
          { "fieldName": "party_Add_Street" },
          { "fieldName": "party_Alias" },
          { "fieldName": "party_Area_Pincode" },
          { "fieldName": "party_GST_No" },
          { "fieldName": "party_MobNo" },
          { "fieldName": "party_Name" },
          { "fieldName": "party_Other_Reg_No" },
          { "fieldName": "party_PhNo" },
          { "fieldName": "party_Relation_Name" },
          { "fieldName": "party_Status" },
          { "fieldName": "org_Code_Id" },
          { "fieldName": "party_Delivery_Type_Id" },
          { "fieldName": "party_Relation_Code_Id" },
          { "fieldName": "party_SalMan_Code_Id" },
          { "fieldName": "area_Name" },
          { "fieldName": "state_Name" },
          { "fieldName": "party_Contact_Add_door" },
          { "fieldName": "party_Contact_Add_Street" },
          { "fieldName": "party_Contact_Area_Name" },
          { "fieldName": "party_Contact_City_Name" },
          { "fieldName": "party_Contact_state_Name" },
          { "fieldName": "party_Contact_Country_Name" },
          { "fieldName": "pay_Typ_Id" },
          { "fieldName": "pay_Typ_Name" },
          { "fieldName": "pay_Typ_Arrange_Order" },
          { "fieldName": "party_Tax_Type_Name" },
          { "fieldName": "tid" },
          { "fieldName": "is_Tax_Registered" },
          { "fieldName": "empPosition_Code_Id" },
          { "fieldName": "pay_Mode_Code" },
          { "fieldName": "pay_Mode_Name" },
          { "fieldName": "id" },
          { "fieldName": "pcpt_Status" }
        ],
        "paginationFields": {
          "firstResult": i * this.maxResult,
          "maxResults": this.maxResult
        }
      };
      this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
        .subscribe(responce => {
          if (responce.message.code == 0) {
            this.inc_par = this.inc_par.concat(responce.data.Partyrpt);
            if (responce.data.Partyrpt.length >= this.maxResult) {
              // resolve();
              this.IncrementParty(i + 1);
            }
            else {
              this.foundTS = responce.message.syncTime;
              this.updateTS(this.foundTS, "Partyrpt");
              this.storage.get('Party').then((data) => {
                for (let num = 0; num < this.inc_par.length; num++) {
                  data = _.without(data, _.findWhere(data, { party_Child_Id: this.inc_par[num].party_Child_Id }));
                }
                data = data.concat(this.inc_par);
                this.storage.set("Party", data);
              });
              resolve();
            }
          }
          else if (responce.message.code == 4006) {
            resolve();
          }
          else {
            this.loading.presentToast("Party Data Sync Failed.");
            resolve();
          }
        }, err => {
          resolve();
          this.inc_par = [];
        });
    });
  }

  IncrementDelivery(i) {
    return new Promise((resolve) => {

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('access_Token', this.token);
      headers.append('Register_Id', this.registerCode);
      headers.append('Reg_Mac_Id', this.regMacId);

      let post = {
        "pageUrl": "DeliveryTypeHeadMsT",
        "entityName": "DeliveryType_Head_MsT",
        "action": "payload",
        "event": "completeSynctoMongo",
        "whereClause": [
          {
            "filterKey": "approve_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "org_Code_Id",
            "filterValue": this.orgCode,
            "relation": "=",
            "condition": ""
          }
        ],
        "selectFields": [
          { "fieldName": "Id" },
          { "fieldName": "deliveryType_Name" },
          { "fieldName": "deliveryType_Status" },
          { "fieldName": "approve_Status" },
          { "fieldName": "is_System_Default" },
          { "fieldName": "deliveryCharge_Value" },
          { "fieldName": "is_DeliveryCharge_Per" },
          { "fieldName": "is_Home_Delivery" },
          { "fieldName": "deliveryDue_Cal_Value" },
          { "fieldName": "deliveryType_Default" },
          { "fieldName": "deliveryDue_Cal_Code" },
          { "fieldName": "deliveryDue_Cal_Name" }
        ],
        "paginationFields": {
          "firstResult": i * this.maxResult,
          "maxResults": this.maxResult
        }
      };
      this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
        .subscribe(responce => {
          if (responce.message.code == 0) {
            this.inc_delivery = this.inc_delivery.concat(responce.data.DeliveryType_Head_MsT);
            if (responce.data.DeliveryType_Head_MsT.length >= this.maxResult) {
              // resolve();
              this.IncrementDelivery(i + 1);
            }
            else {
              this.foundTS = responce.message.syncTime;
              this.updateTS(this.foundTS, "DeliveryType_Head_MsT");
              this.storage.get('DeliveryType').then((data) => {
                for (let num = 0; num < this.inc_delivery.length; num++) {
                  data = _.without(data, _.findWhere(data, { Id: this.inc_delivery[num].Id }));
                }
                data = data.concat(this.inc_delivery);
                this.storage.set("DeliveryType", data);
              });
              resolve();
            }
          }
          else if (responce.message.code == 4006) {
            resolve();
          }
          else {
            this.loading.presentToast("Delivery Data Sync Failed.");
            resolve();
          }

        }, err => {
          resolve();
          this.inc_delivery = [];
        });
    });
  }

  IncrementModifier(i) {
    return new Promise((resolve) => {

      var uniqueIds = localStorage.getItem('uniqueProductsId');
      if (uniqueIds == null) {
        resolve();
      }

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('access_Token', this.token);
      headers.append('Register_Id', this.registerCode);
      headers.append('Reg_Mac_Id', this.regMacId);

      let post = {
        "pageUrl": "ProdModifierOpT",
        "entityName": "Prod_Modifier_OpT",
        "action": "payload",
        "event": "completeSynctoMongo",
        "whereClause": [
          {
            "filterKey": "approve_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "org_Code_Id",
            "filterValue": this.orgCode,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "pmo_Prod_Code",
            "filterValue": uniqueIds,
            "relation": "IN",
            "condition": ""
          }
        ],
        "selectFields": [
          { "fieldName": "Id" },
          { "fieldName": "approve_Status" },
          { "fieldName": "is_System_Default" },
          { "fieldName": "pmo_Status" },
          { "fieldName": "pmo_User_Code_Add" },
          { "fieldName": "pmo_User_Code_Updt" },
          { "fieldName": "pmo_TS_Added" },
          { "fieldName": "pmo_TS_Edited" },
          { "fieldName": "pmo_Prod_Code" },
          { "fieldName": "pmo_Prod_Name" },
          { "fieldName": "pmo_MCLS_Name" },
          { "fieldName": "pmo_MCLS_Optional" },
          { "fieldName": "pmo_MCLS_Multiple" },
          { "fieldName": "pmo_MCLS_Virtual" },
          { "fieldName": "pmo_MOPT_Code" },
          { "fieldName": "pmo_MOPT_Name" },
          { "fieldName": "pmo_MOPT_Qty" },
          { "fieldName": "pmo_MOPT_Rate" },
          { "fieldName": "pmo_MOPT_Cost" },
          { "fieldName": "pmo_MOPT_Order" },
          { "fieldName": "pmo_MOPT_UOM_YN" },
          { "fieldName": "pmo_MOPT_UOMG_Code" },
          { "fieldName": "pmo_MOPT_UOMG_Name" },
          { "fieldName": "pmo_MOPT_UOMD_Code" },
          { "fieldName": "pmo_MOPT_UOMD_Name" },
          { "fieldName": "pmo_MOPT_Default" },
          { "fieldName": "pmo_AddRate_Calc_Per" },
          { "fieldName": "pmo_Ing_Prod_YN" },
          { "fieldName": "pmo_Ing_Prod_Code" },
          { "fieldName": "pmo_Ing_Prod_Name" },
          { "fieldName": "pmo_Ing_Prod_Qty" },
          { "fieldName": "pmo_Ing_UOMG_Code" },
          { "fieldName": "pmo_Ing_UOMG_Name" },
          { "fieldName": "pmo_Ing_UOMD_Code" },
          { "fieldName": "pmo_Ing_UOMD_Name" },
          { "fieldName": "pmo_Name" },
          { "fieldName": "pmo_Prefix" },
          { "fieldName": "pmo_Order" },
          { "fieldName": "pmo_Remark" },
          { "fieldName": "branch_Code" },
          { "fieldName": "pmo_MCLS_Code_Id" },
          { "fieldName": "org_Code_Id" }
        ],
        "paginationFields": {
          "firstResult": i * this.maxResult,
          "maxResults": this.maxResult
        }
      };
      this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
        .subscribe(responce => {
          if (responce.message.code == 0) {
            this.inc_modifier = this.inc_delivery.concat(responce.data.Prod_Modifier_OpT);
            if (responce.data.Prod_Modifier_OpT.length == this.maxResult) {
              // resolve();
              this.IncrementModifier(i + 1);
            }
            else {
              this.foundTS = responce.message.syncTime;
              this.updateTS(this.foundTS, "Prod_Modifier_OpT");
              this.storage.get('Modifiers').then((data) => {
                for (let num = 0; num < this.inc_modifier.length; num++) {
                  data = _.without(data, _.findWhere(data, { Id: this.inc_modifier[num].Id }));
                }
                data = data.concat(this.inc_modifier);
                this.storage.set("Modifiers", data);
              });
              resolve();
            }
          }
          else if (responce.message.code == 4006) {
            resolve();
          }
          else {
            this.loading.presentToast("Modifier Data Sync Failed.");
            resolve();
          }
        }, err => {
          resolve();
          this.inc_modifier = [];
        });
    });
  }

  IncrementRecipe(i) {
    return new Promise((resolve) => {

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('access_Token', this.token);
      headers.append('Register_Id', this.registerCode);
      headers.append('Reg_Mac_Id', this.regMacId);

      let post = {
        "pageUrl": "RecipeMsT",
        "entityName": "Recipe_MsT",
        "action": "payload",
        "event": "completeSynctoMongo",
        "multiFilters": [
          {
            "mainEntityType": "",
            "mainEntity": "Recipe_MsT",
            "chiledEntityType": "",
            "childEntity": "Recipe_Dtl rd",
            "alias": "recipe_mst",
            "fk": "rd.recipe_MsT_Id"
          }
        ],
        "whereClause": [
          {
            "filterKey": "rm_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "recipe_mst.approve_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "org_Code_Id",
            "filterValue": this.orgCode,
            "relation": "=",
            "condition": ""
          }
        ],
        "selectFields": [
          { "fieldName": "recipe_mst.Id as rm_Id" },
          { "fieldName": "recipe_mst.approve_Status as rm_approve_Status" },
          { "fieldName": "recipe_mst.is_System_Default as rm_is_System_Default" },
          { "fieldName": "recipe_mst.rm_Status as rm_Status" },
          { "fieldName": "recipe_mst.rm_Prod_Name as rm_Prod_Name" },
          { "fieldName": "recipe_mst.rm_Recipe_Desc as rm_Recipe_Desc" },
          { "fieldName": "recipe_mst.rm_Recipe_Alias as rm_Recipe_Alias" },
          { "fieldName": "recipe_mst.rm_Recipe_Remark as rm_Recipe_Remark" },
          { "fieldName": "recipe_mst.rm_Recipe_Qty as rm_Recipe_Qty" },
          { "fieldName": "recipe_mst.rm_Recipe_UOM_Code as rm_Recipe_UOM_Code" },
          { "fieldName": "recipe_mst.rm_Recipe_UOM_Name as rm_Recipe_UOM_Name" },
          { "fieldName": "recipe_mst.rm_Recipe_Type_Code as rm_Recipe_Type_Code" },
          { "fieldName": "recipe_mst.rm_Recipe_Type_Name as rm_Recipe_Type_Name" },
          { "fieldName": "recipe_mst.rm_type_Class_Code as rm_type_Class_Code" },
          { "fieldName": "recipe_mst.rm_type_Class_Name as rm_type_Class_Name" },
          { "fieldName": "recipe_mst.rm_type_Class_Prefix as rm_type_Class_Prefix" },
          { "fieldName": "recipe_mst.is_Branch as is_Branch" },
          { "fieldName": "recipe_mst.branch_Code as branch_Code" },
          { "fieldName": "recipe_mst.rm_Default_Recipe as rm_Default_Recipe" },
          { "fieldName": "recipe_mst.is_Multi_Stapes as is_Multi_Stapes" },
          { "fieldName": "recipe_mst.calc_AddOn_Cost as calc_AddOn_Cost" },
          { "fieldName": "recipe_mst.rm_Prod_Code_Id as rm_Prod_Code_Id" },
          { "fieldName": "recipe_mst.org_Code_Id as org_Code_Id" },
          { "fieldName": "rd.id as rd_Id" },
          { "fieldName": "rd.approve_Status as rd_approve_Status" },
          { "fieldName": "rd.is_System_Default as rd_is_System_Default" },
          { "fieldName": "rd.rd_Ing_Status as rd_Ing_Status" },
          { "fieldName": "rd.rd_Ing_Prod_Name as rd_Ing_Prod_Name" },
          { "fieldName": "rd.rd_Ing_Prod_Qty as rd_Ing_Prod_Qty" },
          { "fieldName": "rd.rd_Ing_UOM_Group_Code as rd_Ing_UOM_Group_Code" },
          { "fieldName": "rd.rd_Ing_UOM_Group_Name as rd_Ing_UOM_Group_Name" },
          { "fieldName": "rd.rd_Ing_UOM_Dtl_Code as rd_Ing_UOM_Dtl_Code" },
          { "fieldName": "rd.rd_Ing_UOM_Dtl_AltName as rd_Ing_UOM_Dtl_AltName" },
          { "fieldName": "rd.rd_Ing_Remark as rd_Ing_Remark" },
          { "fieldName": "rd.rd_Allow_AlterNet as rd_Allow_AlterNet" },
          { "fieldName": "rd.rd_Allow_Cost_Change as rd_Allow_Cost_Change" },
          { "fieldName": "rd.rd_Allow_AddOn_Prod as rd_Allow_AddOn_Prod" },
          { "fieldName": "rd.rd_Resource_YN as rd_Resource_YN" },
          { "fieldName": "rd.rd_type_Class_Code as rd_type_Class_Code" },
          { "fieldName": "rd.rd_type_Class_Name as rd_type_Class_Name" },
          { "fieldName": "rd.rd_type_Class_Prefix as rd_type_Class_Prefix" },
          { "fieldName": "rd.rd_Is_ByProd as rd_Is_ByProd" },
          { "fieldName": "rd.rd_ByProd_Type_code as rd_ByProd_Type_code" },
          { "fieldName": "rd.rd_ByProd_Type_Name as rd_ByProd_Type_Name" },
          { "fieldName": "rd.rd_Allow_Excess_Qty as rd_Allow_Excess_Qty" },
          { "fieldName": "rd.rd_Prod_Code_Id as rd_Prod_Code_Id" },
          { "fieldName": "rd.recipe_MsT_Id as recipe_MsT_Id" }
        ],
        "paginationFields": {
          "firstResult": i * this.maxResult,
          "maxResults": this.maxResult
        }
      };
      this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
        .subscribe(responce => {
          if (responce.message.code == 0) {
            this.inc_RecipeMst = this.inc_RecipeMst.concat(responce.data.Recipe_MsT);
            if (responce.data.Recipe_MsT.length >= this.maxResult) {
              // resolve();
              this.IncrementRecipe(i + 1);
            }
            else {
              this.foundTS = responce.message.syncTime;
              this.updateTS(this.foundTS, "Recipe_Mst");
              this.storage.get('Recipe').then((data) => {
                for (let num = 0; num < this.inc_RecipeMst.length; num++) {
                  data = _.without(data, _.findWhere(data, { recipe_MsT_Id: this.inc_RecipeMst[num].recipe_MsT_Id }));
                }
                data = data.concat(this.inc_RecipeMst);
                this.storage.set("Recipe", data);
              });
              resolve();
            }
          }
          else if (responce.message.code == 4006) {
            resolve();
          }
          else {
            this.loading.presentToast("Recipe Data Sync Failed.");
            resolve();
          }
        }, err => {
          resolve();
          this.inc_RecipeMst = [];
        });
    });
  }

  IncrementStockClass(i) {
    return new Promise((resolve) => {

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('access_Token', this.token);
      headers.append('Register_Id', this.registerCode);
      headers.append('Reg_Mac_Id', this.regMacId);

      let post = {
        "pageUrl": "StockClass",
        "entityName": "Stock_Class",
        "action": "payload",
        "event": "completeSynctoMongo",
        "whereClause": [
          {
            "filterKey": "active_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "approve_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "org_Code_Id",
            "filterValue": this.orgCode,
            "relation": "=",
            "condition": ""
          }
        ],
        "selectFields": [
          { "fieldName": "active_Status as active_Status" },
          { "fieldName": "approve_Status as approve_Status" },
          { "fieldName": "stock_Class_Name as stock_Class_Name" },
          { "fieldName": "stock_Class_TS_Added as stock_Class_TS_Added" },
          { "fieldName": "stock_Class_TS_Edited as stock_Class_TS_Edited" },
          { "fieldName": "stock_Class_User_Code_Add as stock_Class_User_Code_Add" },
          { "fieldName": "stock_Class_User_Code_Updt as stock_Class_User_Code_Updt" },
          { "fieldName": "org_Code_Id as org_Code_Id" },
          { "fieldName": "stock_Class_Type_Code as stock_Class_Type_Code" },
          { "fieldName": "stock_Class_Type_Name as stock_Class_Type_Name" },
          { "fieldName": "Id as stock_Class_Id" }
        ],
        "paginationFields": {
          "firstResult": i * this.maxResult,
          "maxResults": this.maxResult
        }
      };
      this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
        .subscribe(responce => {
          if (responce.message.code == 0) {
            this.inc_stkClass = this.inc_stkClass.concat(responce.data.Stock_Class);
            if (responce.data.Stock_Class.length >= this.maxResult) {
              // resolve();
              this.IncrementStockClass(i + 1);
            }
            else {
              this.foundTS = responce.message.syncTime;
              this.updateTS(this.foundTS, "Stock_Class");
              this.storage.get('StockClass').then((data) => {
                for (let num = 0; num < this.inc_stkClass.length; num++) {
                  data = _.without(data, _.findWhere(data, { stock_Class_Id: this.inc_stkClass[num].stock_Class_Id }));
                }
                data = data.concat(this.inc_stkClass);
                this.storage.set("StockClass", data);
              });
              resolve();
            }
          }
          else if (responce.message.code == 4006) {
            resolve();
          }
          else {
            this.loading.presentToast("Stock Class Data Sync Failed.");
            resolve();
          }
        }, err => {
          resolve();
          this.inc_stkClass = [];
        });
    });
  }

  IncrementStockBook(i) {
    return new Promise((resolve) => {

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('access_Token', this.token);
      headers.append('Register_Id', this.registerCode);
      headers.append('Reg_Mac_Id', this.regMacId);

      let post = {
        "pageUrl": "Stockbook",
        "entityName": "Stock_Book",
        "action": "payload",
        "event": "incrementalSyncToMongo",
        "whereClause": [
          {
            "filterKey": "psd_Branch_Code_Id",
            "filterValue": this.branchCode,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "approve_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "psd_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "is_Added",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "psd_Available_Qty",
            "filterValue": 0,
            "relation": ">",
            "condition": "AND"
          },
          {
            "filterKey": "psd_Stock_Loc_Code_Id",
            "filterValue": this.stockLocCode,
            "relation": "=",
            "condition": ""
          }
        ],
        "selectFields": [
          { "fieldName": "Id" },
          { "fieldName": "approve_Status" },
          { "fieldName": "psd_Status" },
          { "fieldName": "psd_ADT_Caption" },
          { "fieldName": "psd_AV_Caption" },
          { "fieldName": "psd_Attribute_DT" },
          { "fieldName": "psd_Attribute_Val" },
          { "fieldName": "psd_MDT_Caption" },
          { "fieldName": "psd_MFR_DT" },
          { "fieldName": "is_Added" },
          { "fieldName": "psd_Available_Qty" },
          { "fieldName": "psd_Free_Tag" },
          { "fieldName": "psd_LandingCost" },
          { "fieldName": "psd_MRP" },
          { "fieldName": "psd_PurPrice" },
          { "fieldName": "psd_Rate" },
          { "fieldName": "psd_Prod_Code_Id" },
          { "fieldName": "psd_Prod_Name" },
          { "fieldName": "psd_Prod_Remark" },
          { "fieldName": "psd_eanupc_Code" },
          { "fieldName": "psd_Prod_Barcode" },
          { "fieldName": "psd_Stock_Loc_Code_Id" },
          { "fieldName": "psd_Stock_Loc_Name" },
          { "fieldName": "psd_Branch_Code_Id" },
          { "fieldName": "psd_Div_Code_Id" },
          { "fieldName": "psd_Org_Code_Id" },
          { "fieldName": "psd_Stock_Class_Code_Id" },
          { "fieldName": "psd_Stock_Class_Name" },
          { "fieldName": "psd_JOB_Class_Code" },
          { "fieldName": "is_Qty_Used" },
          { "fieldName": "psd_JOB_Class_Name" }
        ],
        "paginationFields": {
          "firstResult": i * this.maxResult,
          "maxResults": this.maxResult
        }
      };
      this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
        .subscribe(responce => {
          if (responce.message.code == 0) {
            this.inc_stk = this.inc_stk.concat(responce.data.Stock_Book);
            if (responce.data.Stock_Book.length == this.maxResult) {
              // resolve();
              this.IncrementStockBook(i + 1);
            }
            else {
              this.foundTS = responce.message.syncTime;
              this.updateTS(this.foundTS, "Stock_Book");
              this.storage.get('Stockbook').then((data) => {
                for (let num = 0; num < this.inc_stk.length; num++) {
                  data = _.without(data, _.findWhere(data, { psd_Id: this.inc_stk[num].psd_Id }));
                }
                data = data.concat(this.inc_stk);
                this.storage.set("Stockbook", data);
              });
              resolve();
            }
          }
          else if (responce.message.code == 4006) {
            resolve();
          }
          else {
            this.loading.presentToast("Stock Data Sync Failed.");
            resolve();
          }
        }, err => {
          resolve();
          this.inc_stk = [];
        });
    });
  }

  IncrementStockBookUpdateRow(i) {
    return new Promise((resolve) => {

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('access_Token', this.token);
      headers.append('Register_Id', this.registerCode);
      headers.append('Reg_Mac_Id', this.regMacId);

      let post = {
        "pageUrl": "StockbookUpdtRow",
        "entityName": "Stock_Book",
        "action": "payload",
        "event": "incrementalSyncToMongo",
        "multiFilters": [
          {
            "mainEntityType": "",
            "mainEntity": "Stock_Book",
            "chiledEntityType": "",
            "childEntity": "Stock_Book bs",
            "alias": "stock_book",
            "fk": "bs.psd_Ref_INVT_Book_Code"
          }
        ],
        "whereClause": [
          {
            "filterKey": "stock_book.psd_branch_code_id",
            "filterValue": this.branchCode,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "stock_book.psd_branch_code_id",
            "filterValue": "bs.psd_branch_code_id",
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "stock_book.approve_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "stock_book.psd_Status",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "stock_book.is_added",
            "filterValue": 1,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "stock_book.psd_available_qty",
            "filterValue": 0,
            "relation": ">=",
            "condition": "AND"
          },
          {
            "filterKey": "stock_book.psd_Stock_Loc_Code_Id",
            "filterValue": this.stockLocCode,
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "stock_book.psd_stock_loc_code_id",
            "filterValue": "bs.psd_stock_loc_code_id",
            "relation": "=",
            "condition": "AND"
          },
          {
            "filterKey": "bs.psd_Ref_Series_Prefix_Alias",
            "filterValue": "'JO', 'TO', 'WT', 'PR', 'JR', 'SR', 'LT', 'DR', 'SU', 'RC', 'RR', 'MI', 'LO'",
            "relation": "IN",
            "condition": "AND"
          },
          {
            "filterKey": "bs.psd_Ref_INVT_Book_Code",
            "filterValue": "IS NOT NULL",
            "relation": "",
            "condition": "AND",
            "fieldValueType": "NA"
          }
        ],
        "selectFields": [
          { "fieldName": "stock_book.Id as Id" },
          { "fieldName": "stock_book.approve_Status as approve_Status" },
          { "fieldName": "stock_book.psd_Status as psd_Status" },
          { "fieldName": "stock_book.psd_ADT_Caption as psd_ADT_Caption" },
          { "fieldName": "stock_book.psd_AV_Caption as psd_AV_Caption" },
          { "fieldName": "stock_book.psd_Attribute_DT as psd_Attribute_DT" },
          { "fieldName": "stock_book.psd_Attribute_Val as psd_Attribute_Val" },
          { "fieldName": "stock_book.psd_MDT_Caption as psd_MDT_Caption" },
          { "fieldName": "stock_book.psd_MFR_DT as psd_MFR_DT" },
          { "fieldName": "stock_book.is_Added as is_Added" },
          { "fieldName": "stock_book.psd_Available_Qty as psd_Available_Qty" },
          { "fieldName": "stock_book.psd_Free_Tag as psd_Free_Tag" },
          { "fieldName": "stock_book.psd_LandingCost as psd_LandingCost" },
          { "fieldName": "stock_book.psd_MRP as psd_MRP" },
          { "fieldName": "stock_book.psd_PurPrice as psd_PurPrice" },
          { "fieldName": "stock_book.psd_Rate as psd_Rate" },
          { "fieldName": "stock_book.psd_Prod_Code_Id as psd_Prod_Code_Id" },
          { "fieldName": "stock_book.psd_Prod_Name as psd_Prod_Name" },
          { "fieldName": "stock_book.psd_Prod_Remark as psd_Prod_Remark" },
          { "fieldName": "stock_book.psd_eanupc_Code as psd_eanupc_Code" },
          { "fieldName": "stock_book.psd_Prod_Barcode as psd_Prod_Barcode" },
          { "fieldName": "stock_book.psd_Stock_Loc_Code_Id as psd_Stock_Loc_Code_Id" },
          { "fieldName": "stock_book.psd_Stock_Loc_Name as psd_Stock_Loc_Name" },
          { "fieldName": "stock_book.psd_Branch_Code_Id as psd_Branch_Code_Id" },
          { "fieldName": "stock_book.psd_Div_Code_Id as psd_Div_Code_Id" },
          { "fieldName": "stock_book.psd_Org_Code_Id as psd_Org_Code_Id" },
          { "fieldName": "stock_book.psd_Stock_Class_Code_Id as psd_Stock_Class_Code_Id" },
          { "fieldName": "stock_book.psd_Stock_Class_Name as psd_Stock_Class_Name" },
          { "fieldName": "stock_book.psd_JOB_Class_Code as psd_JOB_Class_Code" },
          { "fieldName": "stock_book.psd_JOB_Class_Name as psd_JOB_Class_Name" },
          { "fieldName": "stock_book.psd_Ref_Series_Prefix_Alias as psd_Ref_Series_Prefix_Alias" },
          { "fieldName": "stock_book.is_Qty_Used as is_Qty_Used" },
          { "fieldName": "stock_book.psd_Ref_INVT_Book_Code as psd_Ref_INVT_Book_Code" }
        ],
        "paginationFields": {
          "firstResult": i * this.maxResult,
          "maxResults": this.maxResult
        }
      };
      this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
        .subscribe(responce => {
          if (responce.message.code == 0) {
            this.inc_stk = this.inc_stk.concat(responce.data.Stock_Book);
            if (responce.data.Stock_Book.length >= this.maxResult) {
              // resolve();
              this.IncrementStockBookUpdateRow(i + 1);
            }
            else {
              this.foundTS = responce.message.syncTime;
              this.updateTS(this.foundTS, "Stock_Book");
              this.storage.get('Stockbook').then((data) => {
                for (let num = 0; num < this.inc_stk.length; num++) {
                  data = _.without(data, _.findWhere(data, { psd_Id: this.inc_stk[num].psd_Id }));
                }
                data = data.concat(this.inc_stk);
                this.storage.set("Stockbook", data);
              });
              resolve();
            }
          }
          else if (responce.message.code == 4006) {
            resolve();
          }
          else {
            this.loading.presentToast("Stock Data Sync Failed.");
            resolve();
          }
        }, err => {
          resolve();
          this.inc_stk = [];
        });
    });
  }

  DocNumber() {
    return new Promise((resolve) => {

      this.currentCalls = "Document Data";

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('access_Token', this.token);
      headers.append('Register_Id', this.registerCode);
      headers.append('Reg_Mac_Id', this.regMacId);

      let post = {
        "pageUrl": "DocNumber",
        "entityName": "DocNumber",
        "action": "payload",
        "event": "completeSyncToMysql",
        "selectFields": [
          { "fieldName": "dsnn_Id" },
          { "fieldName": "active_Status" },
          { "fieldName": "approve_Status" },
          { "fieldName": "doc_Next_No" },
          { "fieldName": "doc_NxtNo_Party_Code" },
          { "fieldName": "doc_Prefix_Short" },
          { "fieldName": "entity_Rowid" },
          { "fieldName": "entity_Name" },
          { "fieldName": "is_System_Default" },
          { "fieldName": "doc_Series_Gen_Code_Id" },
          { "fieldName": "doc_Series_Prefix_Code_Id" },
          { "fieldName": "org_Code_Id" },
          { "fieldName": "org_FinYear_Code_Id" },
          { "fieldName": "doc_Series_Gen_Name" },
          { "fieldName": "doc_Series_Prefix_Alias" },
          { "fieldName": "doc_Series_Prefix_Name" },
          { "fieldName": "tid" }
        ]
      };
      this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          if (data.message.code == 0) {
            let da = _.findWhere(data.data.DocNumber, { doc_Series_Prefix_Alias: "SD" });
            this.storage.set("SIDocNumber", da);
            let jc = _.findWhere(data.data.DocNumber, { doc_Series_Prefix_Alias: "JC" });
            this.storage.set("JCDocNumber", jc);
            this.completedCalls = this.completedCalls + 1;
            this.CalculateCompletedPer();
            resolve();
          }
          else if (data.message.code == 4006) {
            this.completedCalls = this.completedCalls + 1;
            this.CalculateCompletedPer();
            resolve();
          }
          else {
            this.loading.presentToast("Server Error. Please contact administrator.");
            this.IsError = true;
          }
        }, err => {
          this.IsError = true;
          resolve();
        });
    });
  }

  FetrOption() {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('access_Token', this.token);
    headers.append('Register_Id', this.registerCode);
    headers.append('Reg_Mac_Id', this.regMacId);

    let post = {
      "pageUrl": "Setting",
      "entityName": "Fetr_Option_Dtl",
      "action": "view",
      "event": "multiFilter",
      "multiFilters": [
        {
          "mainEntityType": "",
          "mainEntity": "Fetr_Option_Dtl",
          "chiledEntityType": "",
          "childEntity": "Feature_MsT fe",
          "alias": "fe",
          "fk": "fetr_option_dtl.fetr_Code_Id"
        },
        {
          "mainEntityType": "",
          "mainEntity": "Fetr_Option_Dtl",
          "chiledEntityType": "",
          "childEntity": "Org_Head_MsT org",
          "alias": "org",
          "fk": "fetr_option_dtl.org_Code_Id"
        }
      ],
      "whereClause": [
        {
          "filterKey": "fetr_option_dtl.fod_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "fetr_option_dtl.approve_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "tid",
          "filterValue": "" + this.tenantId + "",
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "branch_Code_Id",
          "filterValue": "" + this.branchCode + "",
          "relation": "=",
          "condition": ""
        }
      ],
      "selectFields": [
        { "fieldName": "tid" },
        { "fieldName": "fetr_option_dtl.org_Code_Id as org_Code_Id" },
        { "fieldName": "branch_Code_Id" },
        { "fieldName": "fetr_Code_Id" },
        { "fieldName": "fetr_option_dtl.fetr_Desc as fetr_Desc" },
        { "fieldName": "fetr_option_dtl.fetr_Prefix as fetr_Prefix" },
        { "fieldName": "option_Code" },
        { "fieldName": "fod_Option_Val" },
        { "fieldName": "url_Code" },
        { "fieldName": "url_Desc" },
        { "fieldName": "option_Type_Code" },
        { "fieldName": "option_Type_Name" },
        { "fieldName": "fetr_option_dtl.is_System_Default as is_System_Default" },
        { "fieldName": "fetr_option_dtl.approve_Status as approve_Status" },
        { "fieldName": "fetr_option_dtl.fod_Status as fod_Status" }
      ]
    }

    this.http.post(this.url + '?tenantId=' + this._tenantId, post, { headers: headers }).map(res => res.json())
      .subscribe(data => {
        this.storage.set("FetrOptionData", data.data.Fetr_Option_Dtl);
      });
  }

  getUserToken(event, id, pass) {
    let lgn = {
      "entityName": "WovV_User_MsT",
      "action": "payload",
      "pageUrl": "User",
      "event": event,
      "formData": {
        "user_Login_ID": id,
        "user_Password": pass
      }
    };

    this.http.post(this.url + "?tenantId=" + this._tenantId, lgn).map(res => res.json())
      .subscribe(data => {
        if (data.message.code == 6002) {
          if (event == "authenticate") {
            this.getUserToken("clearUserToken", id, pass);
          }
          else {
            this.getUserToken("authenticate", id, pass);
          }
        }
        else {
          this.utoken = data.data.WovV_User_Token[0].access_Token;
        }
      }, err => {
        console.log(err);
      });
  }

  globalSearch(inputData) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('access_Token', this.token);
    headers.append('Register_Id', this.registerCode);
    headers.append('Reg_Mac_Id', this.regMacId);

    let post = {
      "pageUrl": "CustomerDashboard",
      "entityName": "Party_Head_MsT",
      "action": "view",
      "event": "selectFilter",
      "whereClause": [
        {
          "filterKey": "org_Code_Id",
          "filterValue": this.orgCode,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "party_Status",
          "filterValue": "1",
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "party_Relation_Name",
          "filterValue": "Customer",
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "party_Name",
          "filterValue": "%" + inputData + "%",
          "relation": "LIKE",
          "condition": ""
        }
      ],
      "selectFields": [
        { "fieldName": "party_Name" },
        { "fieldName": "Id" }
      ],
      "paginationFields": {
        "firstResult": 0,
        "maxResults": 2500
      }
    }
    return this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
  }

  customerDashboard(party_Name) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('access_Token', this.token);
    headers.append('Register_Id', this.registerCode);
    headers.append('Reg_Mac_Id', this.regMacId);

    let post = {
      "pageUrl": "CustomerDashboard",
      "entityName": "Party_Summary",
      "action": "payload",
      "event": "notification",
      "formList": [
        {
          "templateName": "PartyDashboard",
          "branchCode": this.branchCode,
          "partyName": party_Name,
          "partyRelationName": "Customer",
        }
      ]
    }
    return this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
  }

  jobcardDashboard(party_Name) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('access_Token', this.token);
    headers.append('Register_Id', this.registerCode);
    headers.append('Reg_Mac_Id', this.regMacId);

    let post = {
      "pageUrl": "CustomerDashboard",
      "entityName": "Party_Summary",
      "action": "payload",
      "event": "notification",
      "formList": [
        {
          "templateName": "PartyDashboard",
          "branchCode": this.branchCode,
          "partyName": party_Name,
          "partyRelationName": "Customer",
        }
      ]
    }
    return this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers }).map(res => res.json())
  }

  updateTS(foundTS, name) {
    return new Promise((resolve) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('access_Token', this.token);
      headers.append('Register_Id', this.registerCode);
      headers.append('Reg_Mac_Id', this.regMacId);

      let post = {
        "entityName": "Reg_Sync_Device",
        "action": "payload",
        "event": "lastSyncTS",
        "pageUrl": "RegSyncDevice",
        "formData":
        {
          "rsd_LastSync_TS": foundTS,
          "rsd_TS_Added": new Date(),
          "rsd_TS_Edited": new Date(),
          "entity_Name": name
        }
      }
      this.http.post(this.url + "?tenantId=" + this._tenantId, post, { headers: headers })
        .map(res => res.json()).subscribe(
          data => {
            resolve();
          }, err => {
            console.log(err);
          }
        );
    });
  }

  CalculateCompletedPer() {
    var per = Math.round((this.completedCalls * 100) / this.totalCalls);
    if (per >= 100) {
      this.completedPer = 100;
      this.IsCompleted = true;
    }
    else {
      this.completedPer = per;
    }
  }

  try_sync() {
    this.owtHrdSyncData = [];
    this.storage.get("LogInvoices").then((data) => {
      if (data) {
        this.LogData = data;
        this.storage.get("invoices").then((invData) => {
          this.transactionInvoices = invData;
          this.pendingLogData = this.LogData.filter(function (log) {
            return log['Status'] == "Pending";
          });
          if (this.pendingLogData.length > 0) {
            for (let objIndex = 0; objIndex < this.pendingLogData.length; objIndex++) {
              for (let invoiceIndex = 0; invoiceIndex < this.transactionInvoices.length; invoiceIndex++) {
                if (this.transactionInvoices[invoiceIndex] != null) {
                  if (this.transactionInvoices[invoiceIndex]['oh_DocNo'] == this.pendingLogData[objIndex]['DocNumber']
                    && this.transactionInvoices[invoiceIndex]['oh_Doc_Series_Prefix_Val'] == this.pendingLogData[objIndex]['InvoiceType']) {
                    this.owtHrdSyncData.push(this.transactionInvoices[invoiceIndex]);
                  }
                }
              }
            }
            if (this.owtHrdSyncData.length > 0) {
              this.send(this.owtHrdSyncData);
            }
          }
          else {
            this.loading.presentToast("No Pending Transactions found.");
          }
        });
      }
      else {
        this.loading.presentToast("No Pending Transactions found.");
      }
    });
  }

  send(objs) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('access_Token', this.token);
    headers.append('Register_Id', this.registerCode);
    headers.append('Reg_Mac_Id', this.regMacId);

    let out = {
      "pageUrl": "OutwardSync",
      "entityName": "OWT_Hdr_Sync",
      "action": "payload",
      "event": "outwardSync",
      "formList": objs
    };
    let loading = this.loadingCtrl.create({

    });

    loading.present();

    this.http.post(this.url + "?tenantId=" + this._tenantId, out, { headers: headers })
      .map(res => res.json())
      .subscribe(data => {
        let length = data.data['Reg_Sync_Entity_Log'][1][0].length;

        for (let objIndex = 0; objIndex < length; objIndex++) {
          if (data.data['Reg_Sync_Entity_Log'][1][0][objIndex]['id'] != null) {
            for (let logIndex = 0; logIndex < this.LogData.length; logIndex++) {
              if (this.LogData[logIndex]['DocNumber'] == data.data['Reg_Sync_Entity_Log'][1][0][objIndex]['oh_DocNo']
                && this.LogData[logIndex]['finYear'] == data.data['Reg_Sync_Entity_Log'][1][0][objIndex]['financial_Year']
                && this.LogData[logIndex]['InvoiceType'] == data.data['Reg_Sync_Entity_Log'][1][0][objIndex]['oh_Doc_Series_Prefix_Val']) {
                this.LogData[logIndex]['Status'] = "Uploaded";
              }
            }
          }
        }
        this.storage.set('LogInvoices', this.LogData);
        let toast = this.toastCtrl.create({
          message: 'Transaction invoices uploaded Successfully',
          duration: 3000,
          position: 'top'
        });
        loading.dismiss();
        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
        //console.log(this.invoices);
        this.try_sync();
      }, err => {

        console.log(err);
      });
  }

  download() {
    // this.imagep = '/ImagePath/_118121879/' + this.entityName;
    this.imagep = "/ImagePath/" + this._tenantId + '/' + this.entityName;
    console.log(this.imagep);
    this.imgUrl = this.siteUrl + this.imagep;
    // this.imgUrl = this.getDynamicURL + this.imagep;
    for (let cnt = 0; cnt < this.catlogData.length; cnt++) {
      // console.log(this.catrpt);
      console.log('Im in for download');
      const url = this.imgUrl + '/' + this.catlogData[cnt].prod_Image_Path;
      // const url = this.imgUrl + '/' + '75921720849433_Straw_Yogurt.jpeg';

      console.log("asset", url);
      let loading = this.loadingCtrl.create({
        content: 'Downloading Assets...'
      });

      if (this.plt.is('android')) {
        this.platformName = this.file.dataDirectory;
      } else if (this.plt.is('ios')) {
        this.platformName = this.file.documentsDirectory;
      } else if (this.plt.is('windows')) {
        this.platformName = this.file.applicationStorageDirectory;
      }
      console.log('platform is android');

      // this.fileTransfer.download(url, this.platformName + '75921720849433_Straw_Yogurt.jpeg').then((entry) => {
      this.fileTransfer.download(url, this.platformName + this.catlogData[cnt].prod_Image_Path)
        .then((entry) => {
          console.log('download complete: ' + entry.toURL());
          loading.dismiss();
        }, (error) => {
          console.log(error);
        });
    }
  }

  brow() {
    this.GetLocalData();
    if (this.plt.is('android') || this.plt.is('ios') || this.plt.is('windows')) {
      var browser = this.iab.create(this.siteUrl + "/wovvipos/#/menu", "_blank", {
        zoom: 'no', hideurlbar: 'yes', fullscreen: 'yes',
        hidenavigationbuttons: 'yes', clearsessioncache: 'yes', clearcache: 'yes'
      });
      browser.on('loadstop').subscribe(event => {
        browser.executeScript({
          code: "sessionStorage.setItem('Register_Id', '" + this.registerCode + "');" +
            "sessionStorage.setItem('Wovvsystem', '" + localStorage.getItem('currentUser') + "'); " +
            "sessionStorage.setItem('access_Token', '" + localStorage.getItem('onlineToken') + "'); " +
            "sessionStorage.setItem('branch_Code_Id', '" + this.branchCode + "'); " +
            "sessionStorage.setItem('multibranch', '" + JSON.stringify(this.wovvSystem) + "'); " +
            "sessionStorage.setItem('branch_Prefix', '\"" + this.wovvSystem[0].branch_Prefix + "\"'); " +
            "sessionStorage.setItem('div_Code_Id', '" + this.divCode + "'); " +
            "sessionStorage.setItem('domain_Name', '" + this.wovvSystem[0].reg_Prefix + "'); " +
            "sessionStorage.setItem('financial_Year', '\"" + this.wovvSystem[0].financial_Year + "\"'); " +
            "sessionStorage.setItem('global_branch_Code_Id', '" + this.wovvSystem[0].branch_Code_Id + "'); " +
            "sessionStorage.setItem('is_Global_Flag', '" + this.wovvSystem[0].is_Global_YN + "'); " +
            "sessionStorage.setItem('org_Code_Id', '" + this.orgCode + "'); " +
            "sessionStorage.setItem('reg_Prefix', '\"" + this.wovvSystem[0].reg_Prefix + "\"'); " +
            "sessionStorage.setItem('stock_Loc_Code_Id', '" + this.stockLocCode + "'); " +
            "sessionStorage.setItem('stock_class', '" + JSON.stringify(this.stockClassData) + "'); " +
            "sessionStorage.setItem('tenantId', '" + this.tenantId + "'); " +
            "sessionStorage.setItem('user_Id', '" + localStorage.getItem('UserID') + "'); " +
            "sessionStorage.setItem('year_Began_Date', '" + this.wovvSystem[0].year_Began_Date + "'); " +
            "sessionStorage.setItem('year_End_Date', '" + this.wovvSystem[0].year_End_Date + "'); " +
            "sessionStorage.setItem('branch_Currency_Symbol', '" + this.wovvSystem[0].branch_Currency_Symbol + "'); " +
            "sessionStorage.setItem('year_flag', '1'); " +
            "sessionStorage.setItem('isLoggedIn', true); " +
            "sessionStorage.setItem('WovvsystemView', '" + JSON.stringify(this.wovvSystem) + "');"
        });
      });
    }
    else {
      this.loading.presentToast("Coming Soon !!!");
    }
  }

}
