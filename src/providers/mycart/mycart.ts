
import { Injectable } from '@angular/core';
import { DbProvider } from "../db/db";
import { Storage } from '@ionic/storage';
import * as _ from 'underscore';
import { Http, Headers } from '@angular/http';
import { Device } from '@ionic-native/device';
import { AutoCompleteService } from "ionic2-auto-complete";
import { Printer } from '@ionic-native/printer';
import { File } from '@ionic-native/file';
import { ToastController, LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { SyncProvider } from '../sync/sync';
import * as moment from 'moment';
//declare let BTPrinter: any;
/*
  Generated class for the MycartProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MycartProvider implements AutoCompleteService {
  invoices = [];
  catalog: any = [];
  cats: any = [];
  party: any = {};
  partyrpt: any;
  sum = {};
  items = {};
  disc = {};
  rte = {};
  stockbook: any;
  owt_dtl_code: any = [];
  party_obj: any = [];
  drl_obj = [];
  owt_Pay_Hdr_Code = [];
  money: any = {};
  cash: any = {};
  cc: any = {};
  sc: any = {};
  date: any = new Date();
  curr_obj: any;
  content = "";
  labelAttribute = "party_Name";
  formValueAttribute = "party_Code_Id";
  devices: any;
  doc: any;
  cus_obj: any = null;
  group: any = [];
  subgroup: any = [];
  dataPayType: any;
  payTypeName: any;
  filteredCustomerData: any;
  selected_party_delveryType: number;
  groupHide: boolean = false;
  subGroupName: any = "";
  uid: any;
  defDeliveryType: any = [];
  summaryViewData: any = [];
  docNumber: any = {};
  logData: any = [];
  logID: number;
  owtHrdSyncData: any = [];
  pendingLogData: any;
  promotionDrlData: any;
  PromoFlag: number;
  priceLevelStockbook: boolean = true;

  constructor(private toastCtrl: ToastController, private alertCtrl: AlertController, private file: File,
    private printer: Printer, public db: DbProvider, public sync: SyncProvider, public http: Http,
    public storage: Storage) { }

  all_prods(num) {
    this.GetCatalogData(num);
    this.subgroup = [];
    this.groupHide = false;
    this.subGroupName = "Subgroup"
  }
  get_subgroups(id: any, prod_Group_Name: any) {
    this.subgroup = _.uniq(this.catalog, function (person) { return person.prod_SubGroup_Name; });
    this.subgroup = _.where(this.subgroup, { prod_Grup_Code_Id: id });
    this.cats = _.where(this.subgroup, { prod_Grup_Code_Id: id });
    // this.cats=this.subgroup
    this.subGroupName = prod_Group_Name;
    this.groupHide = true;
    console.log(this.subgroup);
  }

  get_prods(id: any) {

    this.cats = _.where(this.catalog, { prod_SGrup_Code_Id: id });
    // this.groupHide = true;
  }
  fun() {
    console.log(this.file);
  }
  getResults(keyword: any) {
    // console.log(this.partyrpt);

    let recs = _.filter(this.sync.partyData, function (person) { return person.party_Name.match(new RegExp(keyword)); });
    return _.chain(recs).map(function (item) { return item.party_Name }).uniq().value();
  }

  add_amt(type: number, num: any) {
    console.log(type);
    if (type == 1) {
      this.quick_cash(this.drl_obj[0].oh_Doc_Net_Amt, this.cash.pay_Typ_Id, null, null, 1, "Cash", "Cash");
      this.complete(num);
    }
    else if (type == 2) {
      this.quick_cash(this.drl_obj[0].oh_Doc_Net_Amt, this.cc.pay_Typ_Id, null, null, 3, "Settel", "Credit card");
      this.complete(num);
    }
    else if (type == 3) {
      this.quick_cash(this.drl_obj[0].oh_Doc_Net_Amt, this.sc.pay_Typ_Id, null, null, 2, "Credit", "Store Credit");
      this.complete(num);
    }
    else {
      this.quick_cash(this.money.deno * this.money.cnt, this.money.pay_Typ_Id, this.money.deno, this.money.cnt, null, null, null);
    }
  }
  CreatePartyObject(item) {
    this.sync.GetLocalData();
    this.party_obj = [
      {
        "op_DocDate": new Date(),
        "op_user_Code_Updt": this.sync.wovvSystem[0].user_Id,
        "op_user_Code_Add": this.sync.wovvSystem[0].user_Id,
        "op_TS_Added": new Date(),
        "op_TS_Edited": new Date(),
        "op_Layout_Code": 0,
        "op_Layout_Desc": "NA",
        "op_Table_Layout_Code": 0,
        "op_Layout_Table_Desc": "NA",
        "op_Table_Prefix": null,
        "op_Number_Of_Guest": 0,
        "is_Tax_Registered": item.is_Tax_Registered,
        "op_Party_Code": item.party_Code_Id,
        "op_Party_Name": item.party_Name,
        "op_Party_Add_Door": item.party_Add_Door,
        "op_Party_MobNo": item.party_MobNo,
        "op_Party_Tax_Type_Code": item.party_Tax_Type_Code_Id,
        "op_Party_Relation_Code": item.party_Relation_Code_Id,
        "op_Party_Relation_Name": item.party_Relation_Name,
        "op_Party_Tax_Type_Name": item.party_Tax_Type_Name,
        "op_DocNo": this.docNumber.doc_Next_No,
        "city_Code": item.city_Code_Id,//mannu changes
        "city_Name": item.city_Name,
        "state_Code": item.state_Code_Id,
        "state_Name": item.state_Name,
        "place_Of_Supply": item.place_Of_Supply,
        "country_Code": item.country_Code_Id,
        "country_Name": item.country_Name,
        "area_Name": item.area_Name,
        "op_Invoice_To": 1,// check then done
        "op_Party_Email": item.party_email
      }
    ];
  }

  doc_disc(value: any) {
    var local = value.indexOf("%")
    if (local != -1) {
      var fields = value.split('%');
      var value = fields[0];
      value = parseFloat(value)
      if (value <= 100 && value >= 0) {
        this.drl_obj[0].oh_Doc_Disc_Val = 0;
        this.drl_obj[0].oh_Doc_Disc_Per = value;
        this.drl();
      }
      else {
        let toast = this.toastCtrl.create({
          message: 'Cannot add that discount.',
          duration: 3000,
          position: 'top'
        });
        toast.present();
      }
    }
    else {
      console.log(this.drl_obj[0].oh_Doc_Net_Amt);
      if (value >= 0 && value <= this.drl_obj[0].oh_Taxable_Goods_Amt) {
        this.drl_obj[0].oh_Doc_Disc_Per = 0;
        this.drl_obj[0].oh_Doc_Disc_Val = value;
        this.drl();
      }
      else {
        let toast = this.toastCtrl.create({
          message: 'Cannot add that discount.',
          duration: 3000,
          position: 'top'
        });
        toast.present();
      }
    }
    // this.drl();
  }
  comment(value: any) {
    this.drl_obj[0].oh_Doc_Remark = value;
    this.drl();
  }

  item_disc(value: any) {
    var local = value.indexOf("%")
    if (local != -1) {
      var fields = value.split('%');
      var value = fields[0];
      value = parseFloat(value)
      if (value <= 100 && value >= 0) {
        var dtl = _.where(this.owt_dtl_code, { od_Prod_Code: this.curr_obj.prod_Code_Id });
        for (var i in dtl) {
          dtl[i].od_Prod_Disc_Per = value;
          dtl[i].od_Prod_Disc_Amt = 0;
        }
        this.drl();
        return true;
      }
      else {
        let toast = this.toastCtrl.create({
          message: 'Cannot add that discount.',
          duration: 3000,
          position: 'top'
        });
        toast.present();
        return false;
      }
    } else {
      let toast = this.toastCtrl.create({
        message: 'Please provide discount in %.',
        duration: 3000,
        position: 'top'
      });
      toast.present();
      return false;
    }
  }
  init_obj() {
    let code;
    let name;
    if (this.sync.stockClassData) {
      this.sync.stockClassData.forEach(function (el) {
        if (el.stock_Class_Name == "recieved" && el.stock_Class_Type_Name == "Doc Job-Work Class") {
          code = el.id;
          name = el.stock_Class_Name
        }
      });
    }

    this.drl_obj = [
      {
        "owt_Dtl_Code": this.owt_dtl_code,
        "owt_Tax_Code": [
          {
            "otax_user_Code_Add": this.sync.wovvSystem[0].user_Id,
            "otax_user_Code_Updt": this.sync.wovvSystem[0].user_Id,
            //  "tax_Sub_Class_Name":
          }
        ],
        "owt_Party_Code": this.party_obj,
        "financial_Year": this.sync.wovvSystem[0].financial_Year,
        "org_FinYear_Code": this.sync.wovvSystem[0].org_Finyear_Id,
        "oh_DocDate": ("0" + this.date.getDate()).slice(-2) + '/' + ("0" + (this.date.getMonth() + 1)).slice(-2) + '/' + this.date.getFullYear(),
        "oh_Doc_Aprov_Status": 1,
        "oh_TrF_Rule_Code": 0,
        "oh_Doc_Status": 1,
        "oh_Ref_Series_Gen_Val": 0,
        "oh_Seq_No": 0,
        "oh_System_Date": new Date(),
        "oh_Pay_DueDt": Date.now(),
        "oh_Coupon_Share_Per": 0,
        "oh_Coupon_Val": 0,
        "oh_Other_Disc_Per": 0,
        "oh_Other_Disc_Val": 0,
        "oh_Doc_Item_Count": 0,
        "oh_Doc_Item_Qty": 0,
        "oh_one_Per": 0,
        "oh_one_Val": 0,
        "oh_Two_Per": 0,
        "oh_Two_Val": 0,
        "oh_Three_Per": 0,
        "oh_Three_Val": 0,
        "oh_Four_Per": 0,
        "oh_Four_Val": 0,
        "oh_Five_Per": 0,
        "oh_Five_Val": 0,
        "oh_Six_Per": 0,
        "oh_Six_Val": 0,
        "oh_Seven_Per": 0,
        "oh_Seven_Val": 0,
        "oh_Eight_Per": 0,
        "oh_Eight_Val": 0,
        "oh_Nine_Per": 0,
        "oh_Nine_Val": 0,
        "oh_Ten_Per": 0,
        "oh_Ten_Val": 0,
        "oh_Doc_fright_Val": 0,
        "oh_Goods_Val": 0,
        "oh_Tax_total": 0,
        "oh_Doc_RoundOff": 0,
        "oh_Doc_Net_Amt": 0,
        "oh_Employ_Time_Slot": 0,
        "oh_Prod_Disc_Val": 0,
        "oh_user_Code_Add": this.sync.wovvSystem[0].user_Id,
        "oh_TS_Added": new Date(),
        "oh_user_Code_Updt": this.sync.wovvSystem[0].user_Id,
        "oh_TS_Edited": new Date(),
        "oh_Taxable_Goods_Amt": 0, //update sometime
        "oh_ProdXPrice_Amt": 0,
        "oh_Total_Disc_Val": 0,
        "oh_Change_Given_Amt": 0,
        "oh_Doc_Disc_Per": 0,  //percent
        "oh_Doc_Disc_Val": 0, //amt
        "oh_Expiry_Date": ("0" + this.date.getDate()).slice(-2) + '/' + ("0" + (this.date.getMonth() + 1)).slice(-2) + '/' + this.date.getFullYear(),
        "oh_Promo_Val": 0,
        "oh_Ref_Oh_RowNo": "NA",
        "oh_counter_Session_Code": 0,
        "oh_Table_Layout_Code": 0,
        "oh_Paid_Amt": 0,
        "oh_Number_Of_Guest": 0,
        "is_Tax_Registered": this.party.is_Tax_Registered,
        "oh_Party_Tax_Type_Code": this.party.party_Tax_Type_Code_Id,
        "stock_Loc_Code": this.sync.wovvSystem[0].stock_Loc_Code_Id,
        "register_Code": this.sync.wovvSystem[0].register_Code_Id,
        "branch_Code": this.sync.wovvSystem[0].branch_Code_Id,
        "div_Code": this.sync.wovvSystem[0].div_Code_Id,
        "org_Code": {
          "id": this.sync.wovvSystem[0].org_Code_Id
        },
        "deliveryType_Head_Code": this.party.party_Delivery_Type_Id,
        "oh_Doc_Series_Gen_Code": this.docNumber.doc_Series_Gen_Code_Id,
        "oh_Doc_Series_Prefix_Code": this.docNumber.doc_Series_Prefix_Code_Id,
        "oh_DocNo": this.docNumber.doc_Next_No,
        "oh_Doc_Series_Nxtno_Code": this.docNumber.dsnn_Id,
        "oh_Doc_Series_Prefix_Val": this.docNumber.doc_Series_Prefix_Name,
        "oh_Doc_Series_Gen_Val": this.docNumber.doc_Series_Gen_Name,
        "oh_Doc_Series_Prefix_Alias": this.docNumber.doc_Series_Prefix_Alias,
        // "counter_Session_ID": null,
        "oh_Doc_Remark": "BY SR :" + new Date(),
        "oh_Doc_Class_Code": code,//mannu changes
        "oh_Doc_Class_Name": name,
        "oh_Delivery_Amt": 0,
        "oh_Doc_Ref_Remark": "remark",
        "oh_Paid_Status": 0,
        "oh_Modifier_Amt": 0,
        "branch_Name": this.sync.wovvSystem[0].branch_Name,
        "stock_Loc_Name": this.sync.wovvSystem[0].stock_Loc_Name

        // "deliveryType_Head_Code":this.selected_party_delveryType
      }
    ];
    this.sum = {};
  }
  GetDocNumber(num: any) {
    let that = this;
    if (num == 1) {
      this.storage.get("SIDocNumber").then((data) => {
        that.docNumber = data;
      });
    }
    else if (num == 2) {
      this.storage.get("JCDocNumber").then((data) => {
        that.docNumber = data;
      });
    }
  }
  Enter(num: any) {
    //this.cats = [];
    this.owt_Pay_Hdr_Code = [];
    this.owt_dtl_code = [];
    this.drl_obj = [];
    this.summaryViewData = [];
    this.curr_obj = {};
    this.GetDocNumber(num);
    // this.storage.get("StockClass").then((data) => {
    //   this.sync.stockClassData = data;
    // this.storage.get("Party_rpt_sync").then((data) => {
    this.dataPayType = _.where(this.sync.partyData, { is_System_Default: 0 });
    this.filteredCustomerData = _.uniq(this.dataPayType, 'party_Code_Id');
    // this.partyrpt = _.where(data);
    this.cash = _.findWhere(this.sync.partyData, { party_Name: 'Unregistered Sales', pay_Typ_Name: 'Cash' });
    this.cc = _.findWhere(this.sync.partyData, { party_Name: 'Unregistered Sales', pay_Typ_Name: 'Credit Card' });
    this.sc = _.findWhere(this.sync.partyData, { pay_Typ_Name: 'Store Credit' });
    this.dataPayType = _.where(this.sync.partyData, { party_Name: 'Unregistered Sales' });  //get all stocks for the product

    if (!this.party) {
      this.party = _.findWhere(this.sync.partyData, { party_Name: 'Unregistered Sales' });  //get all stocks for the product
    }
    this.storage.get("Stockbook").then((data) => {
      this.init_obj();
      if (this.cus_obj) {
        this.UpdateParty(this.cus_obj, num);
      }
      else {
        if (this.party_obj.length == 0) {
          this.CreatePartyObject(this.party);          //update party details
        }
      }
      if (data) {
        this.stockbook = data;
      }
    });
    this.storage.get("LogInvoices").then((data) => {
      if (data) {
        this.logData = data;
      }
    });
  }

  rate(value: number) {
    if (value > 0) {
      var dtl = _.where(this.owt_dtl_code, { od_Prod_Code: this.curr_obj.prod_Code_Id });
      for (var i in dtl) {
        //dtl[i].od_Rate=value;
        dtl[i].od_Calc_Price = value;
      }

      var cat = _.where(this.catalog, { prod_Code_Id: this.curr_obj.prod_Code_Id });
      for (var i in cat) {
        cat[i].prod_Price = value;
      }
      this.drl();
    }
  }

  UpdateParty(item: any, num: any) {
    this.party = _.findWhere(this.sync.partyData, { party_Code_Id: item.party_Code_Id });  //get all stocks for the product
    this.GetCatalogData(num);
    this.CreatePartyObject(this.party);
    this.cus_obj = null;
  }

  GetCatalogData(num: any) {
    this.catalog = [];
    this.cats = [];
    if (num == 1) {
      let standard = this.sync.catlogData.filter((cat) => {
        return cat['prod_TypCls_Prefix'] != 1031 && cat['prodC_Default'] == 1 && cat['sale_Subclass_Tax_Typ_Code_Id'] == this.party.party_Tax_Type_Code_Id;
      });
      this.catalog = _.uniq(standard, function (x) { return x.prod_Code_Id; });
      this.cats = this.catalog;
      this.group = _.uniq(this.cats, function (person) { return person.prod_Group_Name; });
    }
    else if (num == 2) {
      let standard = this.sync.catlogData.filter((cat) => {
        return cat['prod_TypCls_Prefix'] == 1031 && cat['prodC_Default'] == 1 && cat['sale_Subclass_Tax_Typ_Code_Id'] == this.party.party_Tax_Type_Code_Id;
      });
      this.catalog = _.uniq(standard, function (x) { return x.prod_Code_Id; });
      this.cats = this.catalog;
      this.group = _.uniq(this.cats, function (person) { return person.prod_Group_Name; });
    }
  }
  p_click(item: any, num: any) {
    var qty = 1;
    if (item['Quantity']) {
      qty = parseInt(item['Quantity']);
    }
    this.uid = null;
    let code_dtl;
    let name_dtl;
    if (this.sync.stockClassData) {
      this.sync.stockClassData.forEach(function (el) {
        if (el.stock_Class_Name == "Received" && el.stock_Class_Type_Name == "Prod Job-Work Class") {
          code_dtl = el.id;
          name_dtl = el.stock_Class_Name
        }
      });
    }
    if (num == 1) {
      code_dtl = null;
      name_dtl = null;
    }
    try {
      var stk = _.findWhere(this.stockbook, {
        psd_Prod_Code_Id: item.prod_Code_Id,
        psd_Stock_Class_Name: "Saleable",
        is_Qty_Used: 0
      });// get single stock object to be used
    }
    catch (err) {
      console.log(err);
    }
    if (stk) //if object is returned
    {
      this.uid = stk.Id;
      console.log(moment(stk.psd_Attribute_DT).format('DD/MM/YYYY'));
      console.log(moment(stk.psd_Attribute_DT));
      console.log(stk.psd_Attribute_DT);
      try {
        stk.psd_Available_Qty--;
        if (stk.psd_Available_Qty == 0) {
          stk.is_Qty_Used = 1;
        }
        var cat = _.findWhere(this.owt_dtl_code, { od_Prod_Code: stk.psd_Prod_Code_Id, od_StockBook_Ref_Code: this.uid });
        if (cat) {
          cat.od_Qty++;
        }
        else {
          var ow = {
            "od_Doc_Series_Prefix_Code": this.docNumber.doc_Series_Prefix_Code_Id,
            "od_Ref_Series_Gen_Val": null,
            "od_Doc_No": this.docNumber.doc_Next_No,
            "od_Doc_Series_Prefix_Alias": this.docNumber.doc_Series_Prefix_Alias,
            "od_Doc_Series_Prefix_Val": this.docNumber.doc_Series_Prefix_Name,
            "od_Doc_Date": new Date(),
            "od_Calc_Price": item.prod_Price,
            "od_Bfr_Tax_Price": 0,
            "od_Qty": qty,
            "od_Free_Tag": 0,
            "od_Prod_QtyXPrice_Amt": 0,
            "od_Prod_Disc_Per": 0,  //percent
            "od_Prod_Disc_Amt": 0, //amt
            "od_Prod_Sp_Disc_Per": 0,
            "od_Prod_Sp_Disc_Amt": 0,
            "od_Prod_Promo_Per": 0,
            "od_Prod_Promo_Amt": 0,
            "od_Inv_Disc_Per": 0,
            "od_Inv_Disc_Amt": 0,
            "od_Coup_Per": 0,
            "od_Coup_Amt": 0,
            "od_Fright_Rate": 0,
            "od_Fright_Amt": 0,
            "od_Prod_Tax_Amt": 0,
            "od_Prod_Tax1_Amt": 0,
            "od_Prod_Tax2_Amt": 0,
            "od_Prod_Tax3_Amt": 0,
            "od_Prod_Tax4_Amt": 0,
            "od_Prod_Tax5_Amt": 0,
            "od_one_Per": 0,
            "od_one_Val": 0,
            "od_Two_Per": 0,
            "od_Two_Val": 0,
            "od_Three_Per": 0,
            "od_Three_Val": 0,
            "od_Four_Per": 0,
            "od_Four_Val": 0,
            "od_Five_Per": 0,
            "od_Five_Val": 0,
            "od_Six_Per": 0,
            "od_Six_Val": 0,
            "od_Seven_Per": 0,
            "od_Seven_Val": 0,
            "od_Eight_Per": 0,
            "od_Eight_Val": 0,
            "od_Nine_Per": 0,
            "od_Nine_Val": 0,
            "od_Ten_Per": 0,
            "od_Ten_Val": 0,
            "od_SalesMan_Instv_Per": 0,
            "od_SalesMan_Instv_Amt": 0,
            "od_UOM_Calculate_Val": 0,
            "od_Taxable_Goods_Amt": 0,
            "od_Goods_Amt": 0,
            "od_Prod_NetAmt": 0,
            "od_eanupc_Code": (this.priceLevelStockbook) ? stk.psd_eanupc_Code : null,
            "od_TS_Added": new Date(),
            "od_TS_Edited": new Date(),
            "oWTDtl_user_Code_Add": this.sync.wovvSystem[0].user_Id,
            "oWTDtl_user_Code_Updt": this.sync.wovvSystem[0].user_Id,
            "od_Ref_Series_Prefix_Code": null,
            "od_Ref_DocDate": null,
            "od_Ref_DocNo": null,
            "od_Prod_Remark": "By SR :" + new Date(),
            "sequenceNo": 1,  //figure out how
            "od_StockBook_Ref_Code": this.uid,  //pass from stockbook
            "od_Register_Code": this.sync.wovvSystem[0].register_Code_Id,
            "od_Reg_Type_Code": 1,
            "od_Ref_Series_Prefix_Alias": null,
            "od_Stock_Class_Name": item.prod_TypCls_Name,
            "od_Adjusted_QTY": 0,
            "od_ADT_Caption": (this.priceLevelStockbook) ? stk.psd_ADT_Caption : null,
            "od_Attribute_DT": (this.priceLevelStockbook) ? stk.psd_Attribute_DT : null,
            "od_Attribute_Val": (this.priceLevelStockbook) ? stk.psd_Attribute_Val : null,
            "od_AV_Caption": (this.priceLevelStockbook) ? stk.psd_AV_Caption : null,
            "od_Branch_Name": this.sync.wovvSystem[0].branch_Name,
            "od_MDT_Caption": (this.priceLevelStockbook) ? stk.psd_MDT_Caption : null,
            "od_MFR_DT": (this.priceLevelStockbook) ? stk.psd_MFR_DT : null,
            "od_Prod_UOM_YN": item.prod_UOM_YN,
            "od_Ref_Doc_Series_Nxtno_Code": 0,
            "od_Reg_Type_Name": null,
            "od_Stock_Loc_Name": this.sync.wovvSystem[0].stock_Loc_Name,
            "od_VGroup_Code": item.vrnt_Group_Code_Id,
            "od_VType1_Code": item.vrnt_Type1_Code,
            "od_VType2_Code": item.vrnt_Type2_Code,
            "od_VType3_Code": item.vrnt_Type3_Code,
            "prod_Parent_Code": 0,
            "stock_Loc_Code": this.sync.wovvSystem[0].stock_Loc_Code_Id,
            "od_Stock_Class_Code": this.sync.stockClassData[0].id,
            "od_Prod_TypClass_Code": item.prod_TypClass_Code_Id,
            "od_Prod_Name": item.prod_Name_Desc,
            "od_MRP": (this.priceLevelStockbook) ? stk.psd_MRP : item.prod_MRP,
            "od_Rate": (this.priceLevelStockbook) ? stk.psd_Rate : item.prod_Price,
            "od_Tax_Code": item.tax_Code_Sale_Id,
            "od_Tax_Rate": item.tax_Total,  // not present
            "od_Tax1_Code": item.tax_1_Ledger_Code_Id,
            "od_Tax1_Prn_Symbol": item.tax_1_Prn_Symbol,
            "od_Tax1_Rate": item.tax_1_Cal_Rate,
            "od_Tax2_Code": item.tax_2_Ledger_Code_Id,
            "od_Tax2_Prn_Symbol": item.tax_2_Prn_Symbol,
            "od_Tax2_Rate": item.tax_2_Cal_Rate,
            "od_Tax3_Code": item.tax_3_Ledger_Code_Id,
            "od_Tax3_Prn_Symbol": item.tax_3_Prn_Symbol,
            "od_Tax3_Rate": item.tax_3_Cal_Rate,
            "od_Tax4_Code": item.tax_4_Ledger_Code_Id,
            "od_Tax4_Prn_Symbol": item.tax_4_Prn_Symbol,
            "od_Tax4_Rate": item.tax_4_Cal_Rate,
            "od_Tax5_Code": item.tax_5_Ledger_Code_Id,
            "od_Tax5_Prn_Symbol": item.tax_5_Prn_Symbol,
            "od_Tax5_Rate": item.tax_5_Cal_Rate,
            "od_Tax_Inclusive": item.tax_Inclusive,
            "od_ProdSGrp_Code": item.prod_SGrup_Code_Id,
            "od_ProdGrp_Code": item.prod_Grup_Code_Id,
            "od_ProdDepartment_Code": item.prod_Dept_Code_Id,
            "od_ProdMFR_Brand_Code": item.mfr_Brand_Code_Id,
            "od_Tax_Prn_Symbol": item.tax_Cls_Name,
            "od_Prod_Image_Path": item.prod_Image_Path,
            "od_Prod_TypClass_Prefix": item.prod_TypCls_Prefix,
            "od_Prod_Status": item.prod_Status,
            "od_Prod_TypClass_Name": item.prod_TypCls_Name,
            "od_Tax_SubClass_Code": item.sale_Subclass_Code_Id,
            "od_LandingCost": (this.priceLevelStockbook) ? stk.psd_LandingCost : item.prod_LandingCost,
            "od_PurPrice": (this.priceLevelStockbook) ? stk.psd_PurPrice : item.prod_PurPrice,
            "od_Prod_Code": item.prod_Code_Id,
            "od_BarcodeGen_Code": item.prod_BarcodeGen_Code,// mannu chnages...
            "od_BarcodeGen_Type": item.prod_BarcodeGen_Type,
            "od_BarcodePRN_YN": 1,
            "deliveryType_Name": "",
            "od_Delivery_Per": 0,
            "od_Delivery_Amt": 0,
            "od_Delivery_Status_Code": 0,
            "od_Delivery_Status_Name": "Pending",
            "deliveryType_Head_Name": "",
            "is_Home_Delivery": "",
            "prod_SubGroup_Name": item.prod_SubGroup_Name,
            "prod_Group_name": item.prod_Group_Name,
            "prodDepartment_Name": item.prod_Dept_Name,
            "prodMFR_Brand_Name": item.mfr_Brand_Name,//prob
            "mfr_Head_Name": item.brand_Head_Name,
            "tax_Cls_Name": item.tax_Cls_Name,
            "tax_Sub_Class_Name": item.tax_SubCls_Name,//prob
            "mfr_Head_Code": item.mfr_Brand_Head_Code_Id,//prob
            "reg_Name": this.sync.wovvSystem[0].reg_Name,
            "prod_UOM_Grp_Name": item.uom_Grup_Name,//prob
            "prod_Salesman_name": null,
            "reg_Prefix": this.sync.wovvSystem[0].reg_Prefix,
            "prod_VGroup_Name": null,
            "prod_VType1_Name": null,
            "prod_VType2_Name": null,
            "prod_VType3_Name": null,
            "od_Prod_Seq": 0,
            "od_Prod_Ref_Seq": null,
            "od_Prod_Due_DT": null,
            "od_Prod_Ref_Remark": "",
            "od_JOB_Class_Code": code_dtl,
            "od_JOB_Class_Name": name_dtl,
            "od_Prod_PMO_Code": 0,
            "od_Prod_Modifier_Per": 0,
            "od_Prod_Modifier_Amt": 0,
            "od_Prod_Promo_Code": null,
            "od_Delivery_Rate": null,
            "od_Prod_Modifier_Rate": null,
            "od_Barcode": (this.priceLevelStockbook) ? stk.psd_Prod_Barcode : null,
            "branch_Code": this.sync.wovvSystem[0].branch_Code_Id,
            "od_Prod_Alias": item.prod_Alias
          };
          this.owt_dtl_code.push(ow);
        }
        this.sum[item.prod_Code_Id] = ++this.sum[item.prod_Code_Id] || 1;
        console.log(this.owt_dtl_code);
        this.set_Delivery_Type(this.defDeliveryType[0], this.owt_dtl_code.length - 1);
        console.log(this.owt_dtl_code);
      }
      catch (err) {
        console.log(err)
      }
    }
    else {
      try {
        if (item.prod_NegativeStock_Bill_allow == 1) {
          var obj = _.findWhere(this.owt_dtl_code, { od_Prod_Code: item.prod_Code_Id, od_StockBook_Ref_Code: null });
          if (obj && item.prod_TypCls_Prefix !== 1031 && item.prod_TypCls_Prefix !== 1000) {
            obj.od_Qty++;
          }
          else {
            let ow1: any = {
              "od_Doc_Series_Prefix_Code": this.docNumber.doc_Series_Prefix_Code_Id,
              "od_Ref_Series_Gen_Val": null,
              "od_Doc_No": this.docNumber.doc_Next_No,
              "od_Doc_Series_Prefix_Alias": this.docNumber.doc_Series_Prefix_Alias,
              "od_Doc_Series_Prefix_Val": this.docNumber.doc_Series_Prefix_Name,
              "od_Doc_Date": new Date(),
              "od_Calc_Price": item.prod_Price,
              "od_Bfr_Tax_Price": 0,
              "od_Qty": qty,
              "od_Free_Tag": 0,
              "od_Prod_QtyXPrice_Amt": 0,
              "od_Prod_Disc_Per": 0,
              "od_Prod_Disc_Amt": 0,
              "od_Prod_Sp_Disc_Per": 0,
              "od_Prod_Sp_Disc_Amt": 0,
              "od_Prod_Promo_Per": 0,
              "od_Prod_Promo_Amt": 0,
              "od_Inv_Disc_Per": 0,
              "od_Inv_Disc_Amt": 0,
              "od_Coup_Per": 0,
              "od_Coup_Amt": 0,
              "od_Fright_Rate": 0,
              "od_Fright_Amt": 0,
              "od_Prod_Tax_Amt": 0,
              "od_Prod_Tax1_Amt": 0,
              "od_Prod_Tax2_Amt": 0,
              "od_Prod_Tax3_Amt": 0,
              "od_Prod_Tax4_Amt": 0,
              "od_Prod_Tax5_Amt": 0,
              "od_one_Per": 0,
              "od_one_Val": 0,
              "od_Two_Per": 0,
              "od_Two_Val": 0,
              "od_Three_Per": 0,
              "od_Three_Val": 0,
              "od_Four_Per": 0,
              "od_Four_Val": 0,
              "od_Five_Per": 0,
              "od_Five_Val": 0,
              "od_Six_Per": 0,
              "od_Six_Val": 0,
              "od_Seven_Per": 0,
              "od_Seven_Val": 0,
              "od_Eight_Per": 0,
              "od_Eight_Val": 0,
              "od_Nine_Per": 0,
              "od_Nine_Val": 0,
              "od_Ten_Per": 0,
              "od_Ten_Val": 0,
              "od_SalesMan_Instv_Per": 0,
              "od_SalesMan_Instv_Amt": 0,
              "od_UOM_Calculate_Val": 0,
              "od_Taxable_Goods_Amt": 0,
              "od_Goods_Amt": 0,
              "od_Prod_NetAmt": 0,
              "od_eanupc_Code": null,
              "od_TS_Added": new Date(),
              "od_TS_Edited": new Date(),
              "oWTDtl_user_Code_Add": this.sync.wovvSystem[0].user_Id,
              "oWTDtl_user_Code_Updt": this.sync.wovvSystem[0].user_Id,
              "od_Ref_Series_Prefix_Code": null,
              "od_Ref_DocDate": null,
              "od_Ref_DocNo": null,
              "od_Prod_Remark": "By SR :" + new Date(),
              "sequenceNo": 1,  //figure out how
              "od_StockBook_Ref_Code": this.uid,  //pass from stockbook
              "od_Register_Code": this.sync.wovvSystem[0].register_Code_Id,
              "od_Reg_Type_Code": 1,
              "od_Ref_Series_Prefix_Alias": null,
              "od_Stock_Class_Name": item.prod_TypCls_Name,
              "od_Adjusted_QTY": 0,
              "od_ADT_Caption": null,
              "od_Attribute_DT": null,/////needs to be change
              "od_Attribute_Val": null,
              "od_AV_Caption": null,
              "od_Branch_Name": this.sync.wovvSystem[0].branch_Name,
              "od_MDT_Caption": null,
              "od_MFR_DT": null,/////needs to be change
              "od_Prod_UOM_YN": item.prod_UOM_YN,
              "od_Ref_Doc_Series_Nxtno_Code": 0,
              "od_Reg_Type_Name": null,
              "od_Stock_Loc_Name": this.sync.wovvSystem[0].stock_Loc_Name,
              "od_VGroup_Code": item.vrnt_Group_Code_Id,
              "od_VType1_Code": item.vrnt_Type1_Code,
              "od_VType2_Code": item.vrnt_Type2_Code,
              "od_VType3_Code": item.vrnt_Type3_Code,
              "prod_Parent_Code": 0,
              "stock_Loc_Code": this.sync.wovvSystem[0].stock_Loc_Code_Id,
              "od_Stock_Class_Code": this.sync.stockClassData[0].id,
              "od_Prod_TypClass_Code": item.prod_TypClass_Code_Id,
              "od_Prod_Name": item.prod_Name_Desc,
              "od_MRP": item.prod_MRP,///nees to be change
              "od_Rate": item.prod_Price,////needs to be change
              "od_Tax_Code": item.tax_Code_Sale_Id,
              "od_Tax_Rate": item.tax_Total,  // not present
              "od_Tax1_Code": item.tax_1_Ledger_Code_Id,
              "od_Tax1_Prn_Symbol": item.tax_1_Prn_Symbol,
              "od_Tax1_Rate": item.tax_1_Cal_Rate,
              "od_Tax2_Code": item.tax_2_Ledger_Code_Id,
              "od_Tax2_Prn_Symbol": item.tax_2_Prn_Symbol,
              "od_Tax2_Rate": item.tax_2_Cal_Rate,
              "od_Tax3_Code": item.tax_3_Ledger_Code_Id,
              "od_Tax3_Prn_Symbol": item.tax_3_Prn_Symbol,
              "od_Tax3_Rate": item.tax_3_Cal_Rate,
              "od_Tax4_Code": item.tax_4_Ledger_Code_Id,
              "od_Tax4_Prn_Symbol": item.tax_4_Prn_Symbol,
              "od_Tax4_Rate": item.tax_4_Cal_Rate,
              "od_Tax5_Code": item.tax_5_Ledger_Code_Id,
              "od_Tax5_Prn_Symbol": item.tax_5_Prn_Symbol,
              "od_Tax5_Rate": item.tax_5_Cal_Rate,
              "od_Tax_Inclusive": item.tax_Inclusive,
              "od_ProdSGrp_Code": item.prod_SGrup_Code_Id,
              "od_ProdGrp_Code": item.prod_Grup_Code_Id,
              "od_ProdDepartment_Code": item.prod_Dept_Code_Id,
              "od_ProdMFR_Brand_Code": item.mfr_Brand_Code_Id,
              "od_Tax_Prn_Symbol": item.tax_Cls_Name,
              "od_Prod_Image_Path": item.prod_Image_Path,
              "od_Prod_TypClass_Prefix": item.prod_TypCls_Prefix,
              "od_Prod_Status": item.prod_Status,
              "od_Prod_TypClass_Name": item.prod_TypCls_Name,
              "od_Tax_SubClass_Code": item.sale_Subclass_Code_Id,
              "od_LandingCost": item.prod_LandingCost,////needs to be change
              "od_PurPrice": item.prod_PurPrice,////needs to be change
              "od_Prod_Code": item.prod_Code_Id,
              "od_BarcodeGen_Code": item.prod_BarcodeGen_Code,// mannu chnages...
              "od_BarcodeGen_Type": item.prod_BarcodeGen_Type,
              "od_BarcodePRN_YN": 1,
              "deliveryType_Name": "",
              "od_Delivery_Per": 0,
              "od_Delivery_Amt": 0,
              "od_Delivery_Status_Code": 0,
              "od_Delivery_Status_Name": "Pending",
              "deliveryType_Head_Name": "",
              "is_Home_Delivery": "",
              "prod_SubGroup_Name": item.prod_SubGroup_Name,
              "prod_Group_name": item.prod_Group_Name,
              "prodDepartment_Name": item.prod_Dept_Name,
              "prodMFR_Brand_Name": item.mfr_Brand_Name,//prob
              "mfr_Head_Name": item.brand_Head_Name,
              "tax_Cls_Name": item.tax_Cls_Name,
              "tax_Sub_Class_Name": item.tax_SubCls_Name,//prob
              "mfr_Head_Code": item.mfr_Brand_Head_Code_Id,//prob
              "reg_Name": this.sync.wovvSystem[0].reg_Name,
              "prod_UOM_Grp_Name": item.uom_Grup_Name,//prob
              "prod_Salesman_name": null,
              "reg_Prefix": this.sync.wovvSystem[0].reg_Prefix,
              "prod_VGroup_Name": null,
              "prod_VType1_Name": null,
              "prod_VType2_Name": null,
              "prod_VType3_Name": null,
              "od_Prod_Seq": 0,
              "od_Prod_Ref_Seq": null,
              "od_Prod_Due_DT": null,
              "od_Prod_Ref_Remark": "",
              "od_JOB_Class_Code": code_dtl,
              "od_JOB_Class_Name": name_dtl,
              "od_Prod_PMO_Code": 0,
              "od_Prod_Modifier_Per": 0,
              "od_Prod_Modifier_Amt": 0,
              "od_Prod_Promo_Code": null,
              "od_Delivery_Rate": null,
              "od_Prod_Modifier_Rate": null,
              "od_Barcode": null,/////needs to be change
              "branch_Code": this.sync.wovvSystem[0].branch_Code_Id,
              "od_Prod_Alias": item.prod_Alias
            };
            if (item.prod_TypCls_Prefix == 1031) {
              ow1.od_BarcodeGen_Code = item.prod_BarcodeGen_Code;
              ow1.od_BarcodeGen_Type = item.prod_BarcodeGen_Type;
              ow1.od_BarcodePRN_YN = 1;
              ow1.prod_SubGroup_Name = item.prod_SubGroup_Name;
              ow1.prod_Group_name = item.prod_Group_Name;
              ow1.prodDepartment_Name = item.prod_Dept_Name;
              ow1.prodMFR_Brand_Name = item.mfr_Brand_Name;
              ow1.mfr_Head_Name = item.brand_Head_Name;
              ow1.tax_Sub_Class_Name = item.tax_SubCls_Name;
              ow1.tax_Cls_Name = item.tax_Cls_Name;
              ow1.mfr_Head_Code = item.mfr_Brand_Head_Code_Id;
              ow1.reg_Name = item.tax_Cls_Name;
              ow1.prod_UOM_Grp_Name = item.uom_Grup_Name;
              ow1.prod_Salesman_name = null
            }
            this.owt_dtl_code.push(ow1);
          }
          this.sum[item.prod_Code_Id] = ++this.sum[item.prod_Code_Id] || 1;
          this.set_Delivery_Type(this.defDeliveryType[0], this.owt_dtl_code.length - 1);
        }
        else {
          let toast = this.toastCtrl.create({
            message: 'Oops, Negative stock is not allowed for product : ' + item.prod_Name_Desc + ' !',
            duration: 3000,
            position: 'top'
          });
          toast.present();
        }
      }
      catch (err) {
        console.log(err);
      }
    }
    this.drl();
  }
  changeDRLpayload(drlData, relationTrue) {
    return new Promise((resolve) => {
      this.promotionDrlData = [];
      this.promotionDrlData = JSON.parse(JSON.stringify(drlData));

      if (relationTrue) {
        this.promotionDrlData.branch_Code = {
          "id": this.promotionDrlData.branch_Code
        }
        this.promotionDrlData.deliveryType_Head_Code = {
          "id": this.promotionDrlData.deliveryType_Head_Code
        }
        this.promotionDrlData.div_Code = {
          "id": this.promotionDrlData.div_Code
        }
        this.promotionDrlData.oh_Doc_Series_Gen_Code = {
          "id": this.promotionDrlData.oh_Doc_Series_Gen_Code
        }
        this.promotionDrlData.oh_Doc_Series_Prefix_Code = {
          "id": this.promotionDrlData.oh_Doc_Series_Prefix_Code
        }
        this.promotionDrlData.register_Code = {
          "id": this.promotionDrlData.register_Code
        }
        this.promotionDrlData.stock_Loc_Code = {
          "id": this.promotionDrlData.stock_Loc_Code
        }
        for (let owtIndex = 0; owtIndex < this.promotionDrlData.owt_Dtl_Code.length; owtIndex++) {
          this.promotionDrlData.owt_Dtl_Code[owtIndex]['branch_Code'] = {
            "id": this.promotionDrlData.owt_Dtl_Code[owtIndex]['branch_Code']
          }
          this.promotionDrlData.owt_Dtl_Code[owtIndex]['od_Prod_Code'] = {
            "id": this.promotionDrlData.owt_Dtl_Code[owtIndex]['od_Prod_Code']
          }
          this.promotionDrlData.owt_Dtl_Code[owtIndex]['od_Stock_Class_Code'] = {
            "id": this.promotionDrlData.owt_Dtl_Code[owtIndex]['od_Stock_Class_Code']
          }
          this.promotionDrlData.owt_Dtl_Code[owtIndex]['stock_Loc_Code'] = {
            "id": this.promotionDrlData.owt_Dtl_Code[owtIndex]['stock_Loc_Code']
          }

        }
        for (let payIndex = 0; payIndex < this.promotionDrlData.owt_Party_Code.length; payIndex++) {
          this.promotionDrlData.owt_Party_Code[payIndex]['op_Party_Code'] = {
            "id": this.promotionDrlData.owt_Party_Code[payIndex]['op_Party_Code']
          }
        }
        for (let taxIndex = 0; taxIndex < this.promotionDrlData.owt_Tax_Code.length; taxIndex++) {
          this.promotionDrlData.owt_Tax_Code[taxIndex]['otax_Code'] = {
            "id": this.promotionDrlData.owt_Tax_Code[taxIndex]['otax_Code']
          }
        }
        resolve();
      }
      else {
        this.promotionDrlData.branch_Code = this.promotionDrlData.branch_Code.id;

        this.promotionDrlData.deliveryType_Head_Code = this.promotionDrlData.deliveryType_Head_Code.id;

        this.promotionDrlData.div_Code = this.promotionDrlData.div_Code.id;

        this.promotionDrlData.oh_Doc_Series_Gen_Code = this.promotionDrlData.oh_Doc_Series_Gen_Code.id;

        this.promotionDrlData.oh_Doc_Series_Prefix_Code = this.promotionDrlData.oh_Doc_Series_Prefix_Code.id;

        this.promotionDrlData.register_Code = this.promotionDrlData.register_Code.id;

        this.promotionDrlData.stock_Loc_Code = this.promotionDrlData.stock_Loc_Code.id;

        for (let owtIndex = 0; owtIndex < this.promotionDrlData.owt_Dtl_Code.length; owtIndex++) {
          this.promotionDrlData.owt_Dtl_Code[owtIndex]['branch_Code'] = this.promotionDrlData.owt_Dtl_Code[owtIndex]['branch_Code'].id;

          this.promotionDrlData.owt_Dtl_Code[owtIndex]['od_Prod_Code'] = this.promotionDrlData.owt_Dtl_Code[owtIndex]['od_Prod_Code'].id;

          this.promotionDrlData.owt_Dtl_Code[owtIndex]['od_Stock_Class_Code'] = this.promotionDrlData.owt_Dtl_Code[owtIndex]['od_Stock_Class_Code'].id;

          this.promotionDrlData.owt_Dtl_Code[owtIndex]['stock_Loc_Code'] = this.promotionDrlData.owt_Dtl_Code[owtIndex]['stock_Loc_Code'].id;


        }
        for (let payIndex = 0; payIndex < this.promotionDrlData.owt_Party_Code.length; payIndex++) {
          this.promotionDrlData.owt_Party_Code[payIndex]['op_Party_Code'] = this.promotionDrlData.owt_Party_Code[payIndex]['op_Party_Code'].id;

        }
        for (let taxIndex = 0; taxIndex < this.promotionDrlData.owt_Tax_Code.length; taxIndex++) {
          this.promotionDrlData.owt_Tax_Code[taxIndex]['otax_Code'] = this.promotionDrlData.owt_Tax_Code[taxIndex]['otax_Code'].id;

        }
        this.drl_obj = [];
        this.drl_obj[0] = this.promotionDrlData;
        this.owt_dtl_code = this.promotionDrlData.owt_Dtl_Code;
        resolve();
      }
    });
  }
  checkPromo() {
    return new Promise((resolve) => {
      this.PromoFlag = null;
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('access_Token', this.sync.token);
      headers.append('Register_Id', this.sync.registerCode);
      headers.append('Reg_Mac_Id', this.sync.regMacId);
      this.changeDRLpayload(this.drl_obj[0], true).then(() => {

        let out = {
          "pageUrl": this.promotionDrlData['oh_Doc_Series_Prefix_Alias'],
          "entityName": "OWT_Hdr",
          "action": "payloadRuleWithTid",
          "event": "48item",
          "formList": [
            this.promotionDrlData
          ]
        };
        this.http.post(this.sync.url + "?tenantId=" + this.sync._tenantId, out, { headers: headers })
          .map(res => res.json())
          .subscribe(data => {
            console.log(data);
            if (data.message.code == 0) {
              if (data.data.Offer_Data['objectList']) {
                let offerName = data.data.Offer_Data['objectList'][1]
              }
              else {
                this.drl_obj = [];
                this.drl_obj = data.data.Offer_Data;
                this.changeDRLpayload(this.drl_obj[0], false);
                this.PromoFlag = 0;
              }
            }
            resolve(this.PromoFlag);
          });
      });
    });
  }
  d_click(item: any) {
    if (this.sum[item.prod_Code_Id] == 1) {
      let toast = this.toastCtrl.create({
        message: 'Minimum 1 quantity is required!',
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
    if (this.sum[item.prod_Code_Id] > 1) {
      //  alert("decrement");
      this.sum[item.prod_Code_Id] = --this.sum[item.prod_Code_Id] || 0;
      var obj = _.findWhere(this.owt_dtl_code.reverse(), { od_Prod_Code: item.prod_Code_Id });
      this.owt_dtl_code.reverse();
      console.log(obj);
      obj.od_Qty--;
      console.log("item removed");

      if (obj.od_StockBook_Ref_Code)  //update stockbook
      {
        var stk = _.findWhere(this.stockbook, { Id: obj.od_StockBook_Ref_Code });
        stk.psd_Available_Qty++;
        if (stk.is_Qty_Used == 1) {
          stk.is_Qty_Used = 0;
        }

      }

      if (obj.od_Qty == 0) {
        this.owt_dtl_code = _.without(this.owt_dtl_code, obj)
      }
      this.drl_obj[0].owt_Dtl_Code = this.owt_dtl_code;
      console.log(obj);
      this.drl();
    }
  }

  bulk(value: number, num: any) {
    if (value >= 0) {
      console.log(this.curr_obj);
      console.log(value);
      var diff = this.sum[this.curr_obj.prod_Code_Id] - value;
      console.log(diff);
      if (!diff) {
        for (var i = 0; i < value; i++) {
          this.p_click(this.curr_obj, num);
        }
      }
      else {
        if (diff > 0) {
          for (var i = 0; i < diff; i++) {
            this.d_click(this.curr_obj);
          }
        }
        else if (diff == 0) {
          console.log("same");
        }
        else {
          for (var i = 0; i > diff; i--) {
            this.p_click(this.curr_obj, num);
          }
        }
      }
    }
  }

  deleteall() {
    //var obj=_.where(this.owt,{prod_id:item.prod_id});
    //this.owt= _.without(this.owt,obj);
    console.log(this.sum[this.curr_obj.prod_Code_Id]);
    var count = this.sum[this.curr_obj.prod_Code_Id];
    for (var i = 0; i < count; i++) {

      this.d_click(this.curr_obj);
    }
    // this.drl();
  }

  drl() {
    this.db.checkDrltoAngular(this.drl_obj);
    this.drl_obj[0].oh_Doc_Net_Amt = this.drl_obj[0].oh_Doc_Net_Amt.toFixed(2);
    if (this.owt_dtl_code.length > 0) {
      this.summaryViewData = [];
      var unique = {};
      this.owt_dtl_code.forEach((x) => {
        if (!(unique[x.od_Prod_Name] && unique[x.od_Prod_Code] && unique[x.od_Calc_Price])) {
          this.summaryViewData.push(x);
          unique[x.od_Prod_Name] = true;
          unique[x.od_Prod_Code] = true;
          unique[x.od_Calc_Price] = true;
        }
      });
    }
  }

  /*payment(amt:any)
   {
  
   }*/

  getType(id: any) {
    var record = _.findWhere(this.sync.partyData, { pay_Typ_Id: id });
    return record.pay_Typ_Name;
  }
  quick_cash(amt: number, method: any, deno: any, count: any, pay_Mode_Code: any, pay_Mode_Name: any, pay_Typ_Name: any) {

    var isPresent = _.findWhere(this.owt_Pay_Hdr_Code, { pay_Type_Code: method });

    if (isPresent) {
      var obj = _.findWhere(isPresent.opd_Trns_UID, { opd_Deno_Val: deno });
      if (obj) {
        obj.opd_Deno_Count += count;
      }
      else {
        isPresent.opd_Trns_UID.push({
          "opd_OH_Doc_Date": new Date(),
          "pay_Type_Code": method,
          "counter_Session_Code": 0,
          "opd_Principal": this.drl_obj[0].oh_Doc_Net_Amt,
          "opd_Deno_Val": deno,
          "opd_Deno_Count": count,
          "opd_NET_Amt": this.drl_obj[0].oh_Doc_Net_Amt,
          "opd_TS_Added": new Date(),
          "opd_TS_Edited": new Date(),
          "od_user_Code_Add": this.sync.wovvSystem[0].user_Id,
          "od_user_Code_Updt": this.sync.wovvSystem[0].user_Id,
          "opd_ChangeGivan_Val": 0,
          "opd_OH_Doc_No": this.docNumber.doc_Next_No,
          "pay_Type_Name": pay_Typ_Name,
          "pay_Mode_Code": pay_Mode_Code,// new change by mannu
          "pay_Mode_Name": pay_Mode_Name,
        });
      }
      isPresent.opay_Paid_VAl = (+isPresent.opay_Paid_VAl) + (+amt);
    }
    else {
      this.owt_Pay_Hdr_Code.push(   //1000 -> 100-3, 50-4, 500-cc
        {
          "opay_OH_Doc_Date": new Date(),
          "opay_counter_Session_Code": 0,
          "opay_Principal": this.drl_obj[0].oh_Doc_Net_Amt, //1000
          "opay_TS_Added": new Date(),
          "opay_TS_Edited": new Date(),
          "op_user_Code_Add": this.sync.wovvSystem[0].user_Id,
          "op_user_Code_Updt": this.sync.wovvSystem[0].user_Id,
          "op_TotChangeGivan_Val": 0,
          "opay_Paid_VAl": amt,  //500
          "pay_Type_Code": method,
          "pay_Mode_Code": pay_Mode_Code,// new change by mannu
          "pay_Mode_Name": pay_Mode_Name,

          "opd_Trns_UID": [
            {
              "opd_OH_Doc_Date": new Date(),
              "pay_Type_Code": method,
              "counter_Session_Code": 0,
              "opd_Principal": this.drl_obj[0].oh_Doc_Net_Amt, //1000
              "opd_Deno_Val": deno,
              "opd_Deno_Count": count,
              "opd_NET_Amt": this.drl_obj[0].oh_Doc_Net_Amt, //500
              "opd_TS_Added": new Date(),
              "opd_TS_Edited": new Date(),
              "od_user_Code_Add": this.sync.wovvSystem[0].user_Id,
              "od_user_Code_Updt": this.sync.wovvSystem[0].user_Id,
              "opd_ChangeGivan_Val": 0,
              "opd_OH_Doc_No": this.docNumber.doc_Next_No,
              "pay_Type_Name": pay_Typ_Name
            }
          ]
        }
      );
    }
    this.drl_obj[0].owt_Pay_Hdr_Code = this.owt_Pay_Hdr_Code;

    console.log(this.drl_obj);

  }

  delete_payment(obj: any) {

    this.owt_Pay_Hdr_Code = _.without(this.owt_Pay_Hdr_Code, obj);
  }
  complete(num: any) {
    if (this.drl_obj[0].owt_Dtl_Code.length > 0) {
      for (let owtIndex = 0; owtIndex < this.drl_obj[0].owt_Dtl_Code.length; owtIndex++) {
        if (this.drl_obj[0].owt_Dtl_Code[owtIndex]['od_StockBook_Ref_Code'] == null) {
          console.log("found null");
          var that = this;
          let checkNegativeStockBlock = this.stockbook.filter(function (block) {
            return block['psd_Prod_Code_Id'] == that.drl_obj[0].owt_Dtl_Code[owtIndex]['od_Prod_Code']
              && block['psd_Available_Qty'] < 0 && block['Id'] == null;
          });

          if (checkNegativeStockBlock.length != 0) {
            this.stockbook = _.without(this.stockbook, _.findWhere(this.stockbook, {
              psd_Prod_Code_Id: that.drl_obj[0].owt_Dtl_Code[owtIndex]['od_Prod_Code'],
              Id: null
            }));
            let negativeStock = {
              "psd_Available_Qty": (checkNegativeStockBlock[0]['psd_Available_Qty']) + (this.drl_obj[0].owt_Dtl_Code[owtIndex]['od_Qty'] * -1),
              "psd_Prod_Name": checkNegativeStockBlock[0]['psd_Prod_Name'],
              "psd_Stock_Class_Name": checkNegativeStockBlock[0]['psd_Stock_Class_Name'],
              "psd_Stock_Loc_Code_Id": checkNegativeStockBlock[0]['psd_Stock_Loc_Code_Id'],
              "is_Qty_Used": 1,
              "Id": null,
              "psd_Prod_Code_Id": checkNegativeStockBlock[0]['psd_Prod_Code_Id']
            }
            this.stockbook.push(negativeStock);
          }
          else {

            let negativeStock = {
              "psd_Available_Qty": this.drl_obj[0].owt_Dtl_Code[owtIndex]['od_Qty'] * -1,
              "psd_Prod_Name": this.drl_obj[0].owt_Dtl_Code[owtIndex]['od_Prod_Name'],
              "psd_Stock_Class_Name": this.drl_obj[0].owt_Dtl_Code[owtIndex]['od_Stock_Class_Name'],
              "psd_Stock_Loc_Code_Id": this.sync.stockLocCode,
              "is_Qty_Used": 1,
              "Id": null,
              "psd_Prod_Code_Id": this.drl_obj[0].owt_Dtl_Code[owtIndex]['od_Prod_Code']
            }
            this.stockbook.push(negativeStock);
          }
        }

      }
      this.logID = this.logData.length + 1;
      let logEntry = {
        "ID": this.logID,
        "Date": this.drl_obj[0]['oh_DocDate'],
        "DocNumber": this.drl_obj[0]['oh_DocNo'],
        "finYear": this.drl_obj[0]['financial_Year'],
        "InvoiceType": this.drl_obj[0]['oh_Doc_Series_Prefix_Val'],
        "Status": "Pending"
      }
      this.logData.push(logEntry);
      this.storage.set("Stockbook", this.stockbook);
      this.docNumber.doc_Next_No++;
      if (num == 1) {
        this.storage.set("SIDocNumber", this.docNumber);
      }
      else if (num == 2) {
        this.storage.set("JCDocNumber", this.docNumber);
      }
      // this.storage.set("StockClass", this.sync.stockClassData);
      // this.storage.set("Party_rpt_sync", this.partyrpt);
      //this.storage.set("Catalog_rpt_sync", this.catalog);
      this.storage.get("invoices").then((data) => {
        if (!data || data.length < 1) {
          this.invoices = [];
          console.log("blank");
        }
        else {
          this.invoices = data;
          console.log(JSON.stringify(this.invoices));
        }
        this.invoices.push(this.drl_obj[0]);
        this.storage.set("invoices", this.invoices);
        this.cus_obj = null;
        this.selected_party_delveryType = null;
        //  alert("data stored");
        this.Enter(num);
        this.storage.set('LogInvoices', this.logData).then(() => {
          this.try_sync();
        });
        // this.try_sync();
      });
    }
    else {
      let toast = this.toastCtrl.create({
        message: 'Cannot save invoice.',
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }

  }

  try_sync() {
    this.owtHrdSyncData = [];
    this.storage.get("LogInvoices").then((data) => {
      this.logData = [];
      if (data != null) {
        this.logData = data;
      }
      this.storage.get("invoices").then((invData) => {
        this.invoices = [];
        if (invData != null) {
          this.invoices = invData;
        }
        this.pendingLogData = this.logData.filter((log) => {
          return log['Status'] == "Pending";
        });
        if (this.pendingLogData.length >= this.sync.pendingLogMaxLimit && this.pendingLogData.length != 0) {
          for (let objIndex = 0; objIndex < this.pendingLogData.length; objIndex++) {
            for (let invoiceIndex = 0; invoiceIndex < this.invoices.length; invoiceIndex++) {
              if (this.invoices[invoiceIndex] != null) {
                if (this.invoices[invoiceIndex]['oh_DocNo'] == this.pendingLogData[objIndex]['DocNumber']
                  && this.invoices[invoiceIndex]['oh_Doc_Series_Prefix_Val'] == this.pendingLogData[objIndex]['InvoiceType']) {
                  this.owtHrdSyncData.push(this.invoices[invoiceIndex]);
                }
              }
            }
          }
          if (this.owtHrdSyncData.length > 0) {
            let toast = this.toastCtrl.create({
              message: 'Syncing invoices.',
              duration: 3000,
              position: 'top'
            });
            toast.present();
            this.send(this.owtHrdSyncData);
          }
        }
      });
    });
  }

  send(objs) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('access_Token', this.sync.token);
    headers.append('Register_Id', this.sync.registerCode);
    headers.append('Reg_Mac_Id', this.sync.regMacId);

    let out = {
      "pageUrl": "OutwardSync",
      "entityName": "OWT_Hdr_Sync",
      "action": "payload",
      "event": "outwardSync",
      "formList": objs
    };
    this.http.post(this.sync.url + "?tenantId=" + this.sync._tenantId, out, { headers: headers })
      .map(res => res.json())
      .subscribe(data => {
        let length = data.data['Reg_Sync_Entity_Log'][1][0].length;

        for (let objIndex = 0; objIndex < length; objIndex++) {
          if (data.data['Reg_Sync_Entity_Log'][1][0][objIndex]['id'] != null) {
            for (let logIndex = 0; logIndex < this.logData.length; logIndex++) {
              if (this.logData[logIndex]['DocNumber'] == data.data['Reg_Sync_Entity_Log'][1][0][objIndex]['oh_DocNo']
                && this.logData[logIndex]['finYear'] == data.data['Reg_Sync_Entity_Log'][1][0][objIndex]['financial_Year']
                && this.logData[logIndex]['InvoiceType'] == data.data['Reg_Sync_Entity_Log'][1][0][objIndex]['oh_Doc_Series_Prefix_Val']) {
                this.logData[logIndex]['Status'] = "Uploaded";
              }
            }
          }
        }
        this.storage.set('LogInvoices', this.logData);
        let toast = this.toastCtrl.create({
          message: 'Sync Successfull',
          duration: 3000,
          position: 'top'
        });
        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });

        toast.present();
        console.log(this.invoices);
        this.try_sync();
      }, err => {

        console.log(err);
      });
  }
  /*
  
   party_Code_Id
   oh_Doc_Net_Amt
   Total_Tax = oh_Tax_total
   Total_Quantity = oh_Doc_Item_Qty
   Round_Off = oh_Doc_RoundOff
   Total_Discount = oh_Total_Disc_Val
  
   */
  print() {

    this.content = "Bill No: " + this.docNumber.doc_Next_No + " " + ("0" + this.date.getDate()).slice(-2) + '/' + ("0" + (this.date.getMonth() + 1)).slice(-2) + '/' + this.date.getFullYear();
    this.content += " <table> <thead> <tr> <th>Name</th> <th>Rate</th> <th>Qty</th> </tr> </thead> <tbody>";

    for (var lp = 0; lp < this.owt_dtl_code.length; lp++) {
      this.content += "<tr><td>" + this.owt_dtl_code[lp].od_Prod_Name + "</td><td>" + this.owt_dtl_code[lp].od_Rate + "</td><td>" + this.owt_dtl_code[lp].od_Qty + "</td></tr>";
    }
    this.content += " <tr><td>Discount</td><td> </td><td> </td><td>" + this.drl_obj[0].oh_Total_Disc_Val + " </td> </tr>";
    this.content += " <tr><td>Taxes</td><td> </td><td> </td><td>" + this.drl_obj[0].oh_Tax_total.toFixed(2) + " </td> </tr>";
    this.content += " <tr><td>Total</td><td> </td><td>" + this.drl_obj[0].oh_Doc_Item_Qty + " </td><td>" + this.drl_obj[0].oh_Doc_Net_Amt + " </td> </tr>";
    this.content += " </tbody> </table>";
    this.content += " <br> Subject To Jurisdiction Only<br> No Refund, No Exchange<br> Thank You Visit Again";
    console.log(this.content);
    this.print_doc();
    // this.complete();

  }


  find() {
    this.printer.pick().then((data) => {

      let alert = this.alertCtrl.create({
        title: JSON.stringify(data),
        subTitle: JSON.stringify(data),
        buttons: ['Dismiss']
      });
      alert.present();
    });
    /*   BTPrinter.list((data) => {
         console.log('Printers list:', data);
         this.devices=data[0];
         this.select();
       }, (err) => {
         console.error(`Error: ${err}`);
   
       });
     */
  }
  select() {
    console.log("selected");
    /*   BTPrinter.connect((data)=>{
         console.log("Success");
         console.log(data);
       },(err)=>{
         console.log("Error");
         console.log(err)
       }, this.devices);
   */
  }
  print1() {
    console.log(this.doc);
    /*       BTPrinter.print((data)=>{
             console.log("Success");
             console.log(data)
           },(err)=>{
             console.log("Error");
             console.log(err)
           }, this.doc);
           */
  }
  print_doc() {
    this.printer.print(this.content);
  }

  checkStockBookProduct(item: any) {
    console.log(item);
    var stk = _.where(this.stockbook, { psd_Prod_Code_Id: item.prod_Code_Id, psd_Stock_Class_Name: "Saleable", is_Qty_Used: 0 });  //get all stocks for the product
    stk = _.groupBy(stk, 'psd_Stock_Loc_Code_Id');

    // 0 means not allowed
    /*console.log(stk);
     console.log(_.size(stk));
     console.log(_.keys(stk).length);
     console.log(_.keys(stk)[0]);
     */
    console.log(JSON.stringify(stk[_.keys(stk)[0]]));
    console.log(stk[_.keys(stk)[0]].filter(p => p.psd_Available_Qty >= 1));
    //console.log(stk[_.keys(stk)[0]].psd_Available_Qty.reduce((sum, item) => sum + item.Amt, 0));
    let total: number = 0;

    stk[_.keys(stk)[0]].forEach((e: any) => {
      total = total + Number(e.psd_Available_Qty);
    });
    console.log(total);

    var obj = [
      {
        "owt_Dtl_Code": [
          {
            "od_Doc_Series_Prefix_Code": this.docNumber.doc_Series_Prefix_Code_Id,
            "od_Ref_Series_Gen_Val": null,
            "od_Doc_No": this.docNumber.doc_Next_No,
            "od_Doc_Series_Prefix_Alias": this.docNumber.doc_Series_Prefix_Alias,
            "od_Doc_Series_Prefix_Val": this.docNumber.doc_Series_Prefix_Name,
            "od_Doc_Date": new Date(),
            "od_Calc_Price": 0,
            "od_Bfr_Tax_Price": 0,
            "od_Qty": 1,             //figure out how
            "od_Free_Tag": 0,
            "od_Prod_QtyXPrice_Amt": 0,
            "od_Prod_Disc_Per": 0,
            "od_Prod_Disc_Amt": 0,
            "od_Prod_Sp_Disc_Per": 0,
            "od_Prod_Sp_Disc_Amt": 0,
            "od_Prod_Promo_Per": 0,
            "od_Prod_Promo_Amt": 0,
            "od_Inv_Disc_Per": 0,
            "od_Inv_Disc_Amt": 0,
            "od_Coup_Per": 0,
            "od_Coup_Amt": 0,
            "od_Fright_Rate": 0,
            "od_Fright_Amt": 0,
            "od_Prod_Tax_Amt": 0,
            "od_Prod_Tax1_Amt": 0,
            "od_Prod_Tax2_Amt": 0,
            "od_Prod_Tax3_Amt": 0,
            "od_Prod_Tax4_Amt": 0,
            "od_Prod_Tax5_Amt": 0,
            "od_one_Per": 0,
            "od_one_Val": 0,
            "od_Two_Per": 0,
            "od_Two_Val": 0,
            "od_Three_Per": 0,
            "od_Three_Val": 0,
            "od_Four_Per": 0,
            "od_Four_Val": 0,
            "od_Five_Per": 0,
            "od_Five_Val": 0,
            "od_Six_Per": 0,
            "od_Six_Val": 0,
            "od_Seven_Per": 0,
            "od_Seven_Val": 0,
            "od_Eight_Per": 0,
            "od_Eight_Val": 0,
            "od_Nine_Per": 0,
            "od_Nine_Val": 0,
            "od_Ten_Per": 0,
            "od_Ten_Val": 0,
            "od_SalesMan_Instv_Per": 0,
            "od_SalesMan_Instv_Amt": 0,
            "od_UOM_Calculate_Val": 0,
            "od_Taxable_Goods_Amt": 0,
            "od_Goods_Amt": 0,
            "od_Prod_NetAmt": 0,
            "od_eanupc_Code": (this.priceLevelStockbook) ? stk.psd_eanupc_Code : null,
            "od_TS_Added": new Date(),
            "od_TS_Edited": new Date(),
            "oWTDtl_user_Code_Add": this.sync.wovvSystem[0].user_Id,
            "oWTDtl_user_Code_Updt": this.sync.wovvSystem[0].user_Id,
            "od_Ref_Series_Prefix_Code": null,
            "od_Ref_DocDate": null,
            "od_Ref_DocNo": null,
            "od_Prod_Remark": "By SR :" + new Date(),
            "sequenceNo": 1,  //figure out how
            "od_StockBook_Ref_Code": this.uid,  //pass from stockbook
            "od_Register_Code": this.sync.wovvSystem['Wovvsystem'][0].register_Code_Id,
            "od_Reg_Type_Code": 1,
            "od_Ref_Series_Prefix_Alias": null,
            "od_Stock_Class_Name": item.prod_TypCls_Name,
            "od_Adjusted_QTY": 0,
            "od_ADT_Caption": "NA",/////needs to be change
            "od_Attribute_DT": ("0" + this.date.getDate()).slice(-2) + '/' + ("0" + (this.date.getMonth() + 1)).slice(-2) + '/' + this.date.getFullYear(),//////needs to be change
            "od_Attribute_Val": "NA",/////needs to be change
            "od_AV_Caption": "NA",///////needs to be change
            "od_Branch_Name": this.sync.wovvSystem[0].branch_Name,
            "od_MDT_Caption": "NA",/////needs to be change
            "od_MFR_DT": ("0" + this.date.getDate()).slice(-2) + '/' + ("0" + (this.date.getMonth() + 1)).slice(-2) + '/' + this.date.getFullYear(),/////needs to be change
            "od_Prod_UOM_YN": item.prod_UOM_YN,
            "od_Ref_Doc_Series_Nxtno_Code": 0,
            "od_Reg_Type_Name": null,
            "od_Stock_Loc_Name": this.sync.wovvSystem[0].stock_Loc_Name,
            "od_VGroup_Code": item.vrnt_Group_Code_Id,
            "od_VType1_Code": item.vrnt_Type1_Code,
            "od_VType2_Code": item.vrnt_Type2_Code,
            "od_VType3_Code": item.vrnt_Type3_Code,
            "prod_Parent_Code": 0,
            "stock_Loc_Code": this.sync.wovvSystem[0].stock_Loc_Code_Id,
            "od_Stock_Class_Code": this.sync.stockClassData[0].id,
            "od_Prod_TypClass_Code": item.prod_TypClass_Code_Id,
            "od_Prod_Name": item.prod_Name_Desc,
            "od_MRP": item.prod_MRP,////needs to be change
            "od_Rate": item.prod_Price,/////needs to be change
            "od_Tax_Code": item.tax_Code_Sale_Id,
            "od_Tax_Rate": 12,  // not present
            "od_Tax1_Code": item.tax_1_Ledger_Code_Id,
            "od_Tax1_Prn_Symbol": item.tax_1_Prn_Symbol,
            "od_Tax1_Rate": item.tax_1_Cal_Rate,
            "od_Tax2_Code": item.tax_2_Ledger_Code_Id,
            "od_Tax2_Prn_Symbol": item.tax_2_Prn_Symbol,
            "od_Tax2_Rate": item.tax_2_Cal_Rate,
            "od_Tax3_Code": item.tax_3_Ledger_Code_Id,
            "od_Tax3_Prn_Symbol": item.tax_3_Prn_Symbol,
            "od_Tax3_Rate": item.tax_3_Cal_Rate,
            "od_Tax4_Code": item.tax_4_Ledger_Code_Id,
            "od_Tax4_Prn_Symbol": item.tax_4_Prn_Symbol,
            "od_Tax4_Rate": item.tax_4_Cal_Rate,
            "od_Tax5_Code": item.tax_5_Ledger_Code_Id,
            "od_Tax5_Prn_Symbol": item.tax_5_Prn_Symbol,
            "od_Tax5_Rate": item.tax_5_Cal_Rate,
            "od_Tax_Inclusive": item.tax_Inclusive,
            "od_ProdSGrp_Code": item.prod_SGrup_Code_Id,
            "od_ProdGrp_Code": item.prod_Grup_Code_Id,
            "od_ProdDepartment_Code": item.prod_Dept_Code_Id,
            "od_ProdMFR_Brand_Code": item.mfr_Brand_Code_Id,
            "od_Tax_Prn_Symbol": item.tax_Cls_Name,
            "od_Prod_Image_Path": item.prod_Image_Path,
            "od_Prod_TypClass_Prefix": item.prod_TypCls_Prefix,
            "od_Prod_Status": item.prod_Status,
            "od_Prod_TypClass_Name": item.prod_TypCls_Name,
            "od_Tax_SubClass_Code": item.sale_Subclass_Code_Id,
            "od_LandingCost": item.prod_LandingCost,////needs to be change
            "od_PurPrice": item.prod_PurPrice,////needs to be change
            "od_Prod_Code": { "id": item.prod_Code_Id },
            "od_BarcodeGen_Code": item.prod_BarcodeGen_Code,// mannu chnages...
            "od_BarcodeGen_Type": item.prod_BarcodeGen_Type,
            "od_BarcodePRN_YN": 0,
            "deliveryType_Name": "",
            "od_Delivery_Per": "",
            "od_Delivery_Amt": 0,
            "od_Delivery_Status_Code": 0,
            "od_Delivery_Status_Name": "",
            "deliveryType_Head_Name": "",
            "is_Home_Delivery": "",
            "prod_SubGroup_Name": "",
            "prod_Group_name": "",
            "prodDepartment_Name": "",
            "prodMFR_Brand_Name": "",
            "mfr_Head_Name": "",
            "tax_Cls_Name": "",
            "tax_Sub_Class_Name": "",
            "mfr_Head_Code": "",
            "reg_Name": "",
            "prod_UOM_Grp_Name": "",
            "prod_Salesman_name": "",
            "reg_Prefix": "",
            "prod_VGroup_Name": null,
            "prod_VType1_Name": null,
            "prod_VType2_Name": null,
            "prod_VType3_Name": null,
            "od_Prod_Seq": 0,
            "od_Prod_Ref_Seq": 0,
            "od_Prod_Due_DT": 0,
            "od_Prod_Ref_Remark": "",
            "od_JOB_Class_Code": 0,
            "od_JOB_Class_Name": "",
            "od_Prod_PMO_Code": 0,
            "od_Prod_Modifier_Per": "",
            "od_Prod_Modifier_Amt": 0,
            "od_Prod_Promo_Code": 0,
            "od_Delivery_Rate": 0,
            "od_Prod_Modifier_Rate": 0,
            "od_Barcode": null,/////needs to be change
            "branch_Code": this.sync.wovvSystem[0].branch_Code_Id,
            "od_Prod_Alias": item.prod_Alias
          }
        ],
        "owt_Tax_Code": [
          {
            "otax_user_Code_Add": this.sync.wovvSystem[0].user_Id,
            "otax_user_Code_Updt": this.sync.wovvSystem[0].user_Id
          }
        ],
        "owt_Party_Code": [
          {
            "op_DocDate": new Date(),
            "op_user_Code_Updt": this.sync.wovvSystem[0].user_Id,
            "op_user_Code_Add": this.sync.wovvSystem[0].user_Id,
            "op_TS_Added": new Date(),
            "op_TS_Edited": new Date(),
            "op_Layout_Code": 0,
            "op_Layout_Desc": "NA",
            "op_Table_Layout_Code": 0,
            "op_Layout_Table_Desc": "NA",
            "op_Table_Prefix": null,
            "op_Number_Of_Guest": 0,
            "is_Tax_Registered": (this.party.is_Tax_Registered ? undefined : null),
            "op_Party_Code": this.party.party_Code_Id,
            "op_Party_Name": this.party.party_Name,
            "op_Party_Add_Door": this.party.party_Add_Door,
            "op_Party_MobNo": this.party.party_MobNo,
            "op_Party_Tax_Type_Code": this.party.party_Tax_Type_Code_Id,
            "op_Party_Relation_Code": this.party.party_Relation_Code_Id,
            "op_Party_Relation_Name": this.party.party_Relation_Name,
            "op_Party_Tax_Type_Name": this.party.party_Tax_Type_Name,
            "op_DocNo": this.docNumber.doc_Next_No,
          }
        ],
        "financial_Year": this.sync.wovvSystem[0].financial_Year,
        "org_FinYear_Code": this.sync.wovvSystem[0].org_Finyear_Id,
        "oh_DocDate": ("0" + this.date.getDate()).slice(-2) + '/' + ("0" + (this.date.getMonth() + 1)).slice(-2) + '/' + this.date.getFullYear(),
        "oh_Doc_Aprov_Status": 1,
        "oh_TrF_Rule_Code": 0,
        "oh_Doc_Status": 1,
        "oh_Ref_Series_Gen_Val": 0,
        "oh_Seq_No": 0,
        "oh_System_Date": new Date(),
        "oh_Pay_DueDt": Date.now(),
        "oh_Coupon_Share_Per": 0,
        "oh_Coupon_Val": 0,
        "oh_Other_Disc_Per": 0,
        "oh_Other_Disc_Val": 0,
        "oh_Doc_Item_Count": 0,
        "oh_Doc_Item_Qty": 0,
        "oh_one_Per": 0,
        "oh_one_Val": 0,
        "oh_Two_Per": 0,
        "oh_Two_Val": 0,
        "oh_Three_Per": 0,
        "oh_Three_Val": 0,
        "oh_Four_Per": 0,
        "oh_Four_Val": 0,
        "oh_Five_Per": 0,
        "oh_Five_Val": 0,
        "oh_Six_Per": 0,
        "oh_Six_Val": 0,
        "oh_Seven_Per": 0,
        "oh_Seven_Val": 0,
        "oh_Eight_Per": 0,
        "oh_Eight_Val": 0,
        "oh_Nine_Per": 0,
        "oh_Nine_Val": 0,
        "oh_Ten_Per": 0,
        "oh_Ten_Val": 0,
        "oh_Doc_fright_Val": 0,
        "oh_Goods_Val": 0,
        "oh_Tax_total": 0,
        "oh_Doc_RoundOff": 0,
        "oh_Doc_Net_Amt": 0,
        "oh_Employ_Time_Slot": 0,
        "oh_Prod_Disc_Val": 0,
        "oh_user_Code_Add": this.sync.wovvSystem[0].user_Id,
        "oh_TS_Added": new Date(),
        "oh_user_Code_Updt": this.sync.wovvSystem[0].user_Id,
        "oh_TS_Edited": new Date(),
        "oh_Taxable_Goods_Amt": 0, //update sometime
        "oh_ProdXPrice_Amt": 0,
        "oh_Total_Disc_Val": 0,
        "oh_Change_Given_Amt": 0,
        "oh_Doc_Disc_Per": 0,
        "oh_Doc_Disc_Val": 0,
        "oh_Expiry_Date": ("0" + this.date.getDate()).slice(-2) + '/' + ("0" + (this.date.getMonth() + 1)).slice(-2) + '/' + this.date.getFullYear(),
        "oh_Promo_Val": 0,
        "oh_Ref_Oh_RowNo": "NA",
        "oh_counter_Session_Code": 0,
        "oh_Table_Layout_Code": 0,
        "oh_Paid_Amt": 0,
        "oh_Number_Of_Guest": 0,
        "is_Tax_Registered": this.party.is_Tax_Registered,
        "oh_Party_Tax_Type_Code": this.party.party_Tax_Type_Code_Id,
        "stock_Loc_Code": this.sync.wovvSystem[0].stock_Loc_Code_Id,
        "register_Code": this.sync.wovvSystem[0].register_Code_Id,
        "branch_Code": this.sync.wovvSystem[0].branch_Code_Id,
        "div_Code": this.sync.wovvSystem[0].div_Code_Id,
        "org_Code": {
          "id": this.sync.wovvSystem[0].org_Code_Id
        },
        "deliveryType_Head_Code": this.party.party_Delivery_Type_Id,
        "oh_Doc_Series_Gen_Code": this.docNumber.doc_Series_Gen_Code_Id,
        "oh_Doc_Series_Prefix_Code": this.docNumber.doc_Series_Prefix_Code_Id,
        "oh_DocNo": this.docNumber.doc_Next_No,
        "oh_Doc_Series_Nxtno_Code": this.docNumber.dsnn_Id,
        "oh_Doc_Series_Prefix_Val": this.docNumber.doc_Series_Prefix_Name,
        "oh_Doc_Series_Gen_Val": this.docNumber.doc_Series_Gen_Name,
        "oh_Doc_Series_Prefix_Alias": "SD",
        // "counter_Session_ID": null,
        "oh_Doc_Remark": "By SR :" + new Date(),
        "oh_Doc_Class_Code": "stock",
        "oh_Doc_Class_Name": "",
        "oh_Delivery_Amt": 0,
        "oh_Doc_Ref_Remark": "remark",
        "oh_Paid_Status": 0,
        "oh_Modifier_Amt": 0,
        "branch_Name": "",
        "stock_Loc_Name": ""


      }
    ];
    console.log(JSON.stringify(obj));
    if (_.keys(stk).length == 1) {
      // alert("1");
    }
    else {
      //alert("2");
    }
  }
  forVoid() {
    this.sum[this.curr_obj.prod_Code_Id] = --this.sum[this.curr_obj.prod_Code_Id] || 0;
  }
  set_Delivery_Type(prm, i) {
    if (prm.deliveryDue_Cal_Name) {
      this.owt_dtl_code[i].deliveryType_Head_Code = prm.Id;
      this.owt_dtl_code[i].deliveryType_Head_Name = prm.deliveryType_Name
      this.owt_dtl_code[i].deliveryType_Name = prm.deliveryType_Name
      this.owt_dtl_code[i].od_Delivery_Per = prm.is_DeliveryCharge_Per
      this.owt_dtl_code[i].od_Delivery_Rate = prm.deliveryCharge_Value
      this.owt_dtl_code[i].is_Home_Delivery = prm.is_Home_Delivery
      if (prm.deliveryDue_Cal_Name == "Days") {
        var date: any = new Date().getTime();
        var msOfDays = prm.deliveryDue_Cal_Value * 24 * 60 * 60 * 1000
        var new_date = date + msOfDays
        date = new Date(new_date)
        this.owt_dtl_code[i].od_Prod_Due_DT = date;
        // this.owt_dtl_code[i].od_Delivery_Amt = this.owt_dtl_code[i].od_Delivery_Rate * this.owt_dtl_code[i].od_Qty;
        // this.drl();
      } else {
        var date: any = new Date().getTime();
        var msOfDays = prm.deliveryDue_Cal_Value * 60 * 60 * 1000
        var new_date = date + msOfDays
        date = new Date(new_date)
        this.owt_dtl_code[i].od_Prod_Due_DT = date;
        // this.owt_dtl_code[i].od_Delivery_Amt = this.owt_dtl_code[i].od_Delivery_Rate * this.owt_dtl_code[i].od_Qty;
        // this.drl();
        console.log(this.owt_dtl_code);
      }
    }
  }
}
