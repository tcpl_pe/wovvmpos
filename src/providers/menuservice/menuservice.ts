import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { Observable } from "rxjs";
import { SyncProvider } from '../sync/sync';

@Injectable()
export class MenuserviceProvider {

  // public url: any;
  // public tid: any;
  // public _tid: any;
  // public userId: any;
  // public accessToken: any;
  // public wovvsystemView: any;
  // public urlRoleTypeData: any;
  // public branch: any;
  public mac_id: any = "zzzz";
  // public wovvsystem: any;

  constructor(public http: Http, public sync: SyncProvider) {
    // this.url = "https://test.wovvipos.com/ipos/rest/bpm/process";
    // //  this.url = "https://test.wovvipos.com/ipos/rest/bpm/process";
    // this.tid = window.localStorage.getItem('tid');
    // this.userId = window.localStorage.getItem('uid');
    // this.accessToken = window.localStorage.getItem('token');
    // this._tid = window.localStorage.getItem('_tid');
    // this.branch = window.localStorage.getItem('branch');
    this.mac_id = "zzzz";
    // this.wovvsystemView = JSON.parse(window.localStorage.getItem('wovvsystem'));
  }
  getFetrOptionDetail() {
    this.sync.GetLocalData();
    // this.url = "https://test.wovvipos.com/ipos/rest/bpm/process";
    // this.tid = window.localStorage.getItem('tid');
    // this.userId = window.localStorage.getItem('uid');
    // this.accessToken = window.localStorage.getItem('token');
    // this._tid = window.localStorage.getItem('_tid');
    // this.branch = window.localStorage.getItem('branch');
    // this.mac_id = "zzzz";
    // this.wovvsystemView = JSON.parse(window.localStorage.getItem('wovvsystem'));

    let params = {
      "pageUrl": "Setting",
      "entityName": "Fetr_Option_Dtl",
      "action": "view",
      "event": "multiFilter",
      "multiFilters": [
        {
          "mainEntityType": "",
          "mainEntity": "Fetr_Option_Dtl",
          "chiledEntityType": "",
          "childEntity": "Feature_MsT fe",
          "alias": "fe",
          "fk": "fetr_option_dtl.fetr_Code_Id"
        },
        {
          "mainEntityType": "",
          "mainEntity": "Fetr_Option_Dtl",
          "chiledEntityType": "",
          "childEntity": "Org_Head_MsT org",
          "alias": "org",
          "fk": "fetr_option_dtl.org_Code_Id"
        }
      ],
      "whereClause": [
        {
          "filterKey": "fetr_option_dtl.fod_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "fetr_option_dtl.approve_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "tid",
          "filterValue": "" + this.sync.tenantId + "",
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "branch_Code_Id",
          "filterValue": "" + this.sync.branchCode + "",
          "relation": "=",
          "condition": ""
        }
      ],
      "selectFields": [
        { "fieldName": "tid" },
        { "fieldName": "fetr_option_dtl.org_Code_Id as org_Code_Id" },
        { "fieldName": "branch_Code_Id" },
        { "fieldName": "fetr_Code_Id" },
        { "fieldName": "fetr_option_dtl.fetr_Desc as fetr_Desc" },
        { "fieldName": "fetr_option_dtl.fetr_Prefix as fetr_Prefix" },
        { "fieldName": "option_Code" },
        { "fieldName": "fod_Option_Val" },
        { "fieldName": "url_Code" },
        { "fieldName": "url_Desc" },
        { "fieldName": "option_Type_Code" },
        { "fieldName": "option_Type_Name" },
        { "fieldName": "fetr_option_dtl.is_System_Default as is_System_Default" },
        { "fieldName": "fetr_option_dtl.approve_Status as approve_Status" },
        { "fieldName": "fetr_option_dtl.fod_Status as fod_Status" }
      ]
    }

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('access_Token', this.sync.token);
    headers.append('Register_Id', this.sync.registerCode);
    headers.append('Reg_Mac_Id', this.sync.regMacId);

    return this.http.post(this.sync.url + '?tenantId=' + this.sync._tenantId, params, { headers: headers })
      .map(response => { return response.json(); })
      // .subscribe(data => {
      //   this.storage.set("Modifiers", data.data.Prod_Modifier_OpT).then(() => {
      //     //this.called = true;
      //   });
      // });
      .catch(error => Observable.throw({ status: error.status, Errors: error }));
  }

  // urlRoleType() {
  //   this.sync.GetLocalData();
  //   var user_Role_Code = this.sync.wovvSystemData[0].user_Role_Id;
  //   let params = {
  //     "pageUrl": "Authorization",
  //     "entityName": "Url_Role_Type",
  //     "action": "payloadRuleWithTid",
  //     "event": "275",
  //     "formList": [
  //       {
  //         "user_Role_Code": user_Role_Code,
  //         "branch_Role_Code": 874273273976775,
  //         "userId": "76463692325421"
  //       }
  //     ]
  //   }

  //   var options = {
  //     headers: new HttpHeaders({
  //       'Content-Type': 'application/json',
  //       'access_Token': this.sync.token,
  //       'Reg_Mac_Id': this.mac_id,
  //       'Register_Id': this.sync.registerCode
  //     })
  //   };
  //   return this.http.post(this.sync.url + '?tenantId=' + this.sync._tenantId, params, options)
  //     .map(response => { return response; })
  //     .catch(error => Observable.throw({ status: error.status, Errors: error }));
  // }

  wovvmenu() {
    this.sync.GetLocalData();
    // var filterValue = '';
    // this.urlRoleTypeData = JSON.parse(window.localStorage.getItem('urlRoleType'));
    // this.wovvsystem = JSON.parse(window.localStorage.getItem('wovvsystem'));
    // for (let key in this.urlRoleTypeData) {
    //   let value = this.urlRoleTypeData[key];
    //   filterValue += value + ',';
    // }
    // filterValue = filterValue.substring(0, filterValue.length - 1);
    // var user_Role_Code = this.sync.wovvSystemData[0].user_Role_Id;
    console.log(this.sync.wovvSystem);

    let params = {
      "pageUrl": "menu",
      "entityName": "Url_Role_Type",
      "action": "payloadRule",
      "event": "71",
      "formList": [
        {
          "branch_role": this.sync.wovvSystem[0]['branch_Role_Code']
        }
      ]
    }
    // var options = {
    //   headers: new HttpHeaders({
    //     'Content-Type': 'application/json',
    //     'access_Token': this.sync.token,
    //     'Register_Id': this.sync.registerCode,
    //     'Reg_Mac_Id': this.sync.regMacId
    //   })
    // };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('access_Token', this.sync.token);
    headers.append('Register_Id', this.sync.registerCode);
    // headers.append('Reg_Mac_Id', this.device.uuid);
    headers.append('Reg_Mac_Id', this.sync.regMacId);

    return this.http.post(this.sync.url + '?tenantId=' + this.sync._tenantId, params, { headers: headers })
      .map(response => { return response.json(); })
      .catch(error => Observable.throw({ status: error.status, Errors: error }));
  }
  onlineLogin(username, password) {
    let params = {
      "entityName": "WovV_User_MsT",
      "action": "payload",
      "pageUrl": "User",
      "event": "authenticate",
      "formData": {
        "user_Password": password,
        "user_Login_ID": username
      }
    };
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.sync.url + '?tenantId=' + this.sync._tenantId, params, { headers: headers })
      .map(response => { return response.json() })
      .catch(error => Observable.throw({ status: error.status, Errors: error }));
  }

  userAvailable(username, password) {
    let params = {
      "entityName": "WovV_User_MsT",
      "action": "payload",
      "pageUrl": "User",
      "event": "clearUserToken",
      "formData": {
        "user_Password": password,
        "user_Login_ID": username
      }
    }
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.sync.url + '?tenantId=' + this.sync._tenantId, params, { headers: headers })
      .map(response => { return response.json(); })
      .catch(error => Observable.throw({ status: error.status, Errors: error }));
  }
}
