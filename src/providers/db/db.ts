import { Injectable } from '@angular/core';

//import  pouchdbquicksearch from 'pouchdb-quick-search';


/*
  Generated class for the DbProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DbProvider {


  constructor() { }
  /**
   * Created by Omkar on 5/25/2018.
   */
  /**
   * client send handle api { formList }
   * @param {*Response} response
   * return updated "OWT_Hdr"
   *
   * @author Bhumi
   * Rule Version 1.2.2 Date 04-01-2017
   * Rule Version 2 Date 09-01-2017
   */
  checkDrltoAngular(response) {
    var objRes: any = [];
    var setTax: any = [];

    //----------------------------------Owt_Hdr_Veriables_Start--------------------------------------
    var oh_Coupon_Share_Per = 0.0;
    var oh_Goods_Val = 0.0;
    var oh_Total_Disc_Val = 0.0;
    var oh_Prod_Disc_Val = 0.0;
    var oh_Promo_Val = 0.0;
    var oh_Coupon_Val = 0.0;
    var oh_Doc_Disc_Val = 0.0;
    var oh_Other_Disc_Val = 0.0;
    var oh_Doc_Disc_Per = 0.0;
    var oh_Tax_total = 0.0;
    var oh_Doc_fright_Val = 0.0;
    var oh_one_Val = 0.0;
    var oh_Two_Val = 0.0;
    var oh_Three_Val = 0.0;
    var oh_Four_Val = 0.0;
    var oh_Five_Val = 0.0;
    var oh_Six_Val = 0.0;
    var oh_Seven_Val = 0.0;
    var oh_Eight_Val = 0.0;
    var oh_Nine_Val = 0.0;
    var oh_Ten_Val = 0.0;

    var oh_Doc_Net_Amt = 0.0;
    var oh_Doc_Item_Qty = 0.0;
    var oh_Taxable_Goods_Amt = 0.0;
    var oh_Prod_QtyXPrice_Amt = 0.0;
    var oh_Doc_Item_Count = 0;
    var oh_ProdXPrice_Amt = 0.0;

    //----------------------------------Owt_Hdr_Veriables_End--------------------------------------
    //----------------------------------Owt_Dtl_Veriables_Start--------------------------------------
    var od_Bfr_Tax_Price = 0.0;
    var od_Calc_Price = 0.0;
    var od_Goods_Amt = 0.0;

    var od_Prod_QtyXPrice_Amt :number = 0.0;
    var total_rate = 0.0;
    var running_total = 0.0;
    //var prerate = 0.0;
    var total = 0.0;
    //var new_rate = 0.0;

    //var total_od_Prod_Disc_Per = 0.0;
    var od_Prod_Disc_Amt = 0.0;
    // var total_od_Prod_Promo_Per = 0.0;
    var od_Prod_Promo_Amt = 0.0;
    var od_Prod_Promo_Per = 0.0;
    var total_od_Prod_Sp_Disc_Per = 0.0;
    var od_Prod_Sp_Disc_Per = 0.0;
    var od_Prod_Sp_Disc_Amt = 0.0;
    //var total_od_Coup_Per = 0.0;
    var od_Coup_Amt = 0.0;
    var od_Prod_Disc_Per = 0.0;
    var od_Inv_Disc_Amt = 0.0;
    var od_Discount = 0.0;
    var od_Inv_Disc_Per = 0.0;
    //var od_Rate = 0.0;
    var od_Prod_Tax1_Amt = 0.0;
    var od_Prod_Tax2_Amt = 0.0;
    var od_Prod_Tax3_Amt = 0.0;
    var od_Prod_Tax4_Amt = 0.0;
    var od_Prod_Tax5_Amt = 0.0;
    var od_Prod_Tax_Amt = 0.0;
    var od_Tax_Rate = 0.0;
    var od_Taxable_Goods_Amt = 0.0;
    var od_Prod_NetAmt = 0.0;
    var od_Tax_Inclusive;
    var od_Free_Tag = 0;
    var prodName = "";
    var prodId;
    var count = 0;
    var od_Delivery_Per = 0.0;
    var od_Delivery_Amt = 0.0;
    var od_Delivery_Rate = 0.0;
    var oh_Delivery_Amt = 0.0;
    var oh_Modifier_Amt = 0.0;

    //----------------------------------Owt_Dtl_Veriables_End--------------------------------------
    //----------------------------------Owt_Tax_Veriables_Start--------------------------------------
    var otax_1_Amt = 0.0;
    var otax_2_Amt = 0.0;
    var otax_3_Amt = 0.0;
    var otax_4_Amt = 0.0;
    var otax_5_Amt = 0.0;
    var otaxble_Goods_Amt = 0.0;

    var otax_1_Rate_owt = 0.0;
    var otax_2_Rate_owt = 0.0;
    var otax_3_Rate_owt = 0.0;
    var otax_4_Rate_owt = 0.0;
    var otax_5_Rate_owt = 0.0;
    var od_Tax_Code_owt = 0;
    var otax_1_Code = 0;
    var sum = 0.0;
    //----------------------------------Owt_Tax_Veriables_End--------------------------------------
    var modifierObjSet: any;
    var newDetailObjSet: any;// set for detail objects which will be updated with modifier's amount
    var map = [];
    var modifierAmtForProduct;
    var modifierValueSet;

    try {
      response.forEach((o, k) => {
        // console.log(k);
        // //----------------------------------Modifiers_Calculation_Start--------------------------------------
        // o.owt_Dtl_Code.forEach((d, k) => {
        //   prodName = d.od_Prod_Name;
        //   prodId = d.od_Prod_Code;
        //   // Condition for Tax Inclusive/Exclusive		
        //   if (d.od_Tax_Inclusive == null) {
        //     //  throw new GlobalException(ErrorCodes.MANDATORY_PARAMETERS_NOT_SET, "Mandatory Parameter od_Tax_Inclusive can not be null");
        //     objRes = {
        //       message: {
        //         'code': 1001,
        //         'message': 'Mandatory Parameter od_Tax_Inclusive can not be null'
        //       }

        //     };
        //     throw "Mandatory Parameter od_Tax_Inclusive can not be null";
        //   }
        //   else {
        //     if (d.od_Calc_Price == null) {
        //       objRes = {
        //         message: {
        //           'code': 1001,
        //           'message': 'Mandatory Parameter od_Tax_Inclusive can not be null'
        //         }
        //       };
        //       throw "Mandatory Parameter od_Calc_Price can not be null for Product name: ";
        //     }
        //     else if (d.od_Rate == null) {
        //       objRes = {
        //         message: {
        //           'code': 1001,
        //           'message': 'Mandatory Parameter od_Rate can not be null'
        //         }
        //       };
        //       throw "Mandatory Parameter od_Rate can not be null for Product name";
        //     }
        //     else if (d.od_Qty == null || d.od_Qty == 0) {
        //       objRes = {
        //         message: {
        //           'code': 1001,
        //           'message': 'Mandatory Parameter od_Qty can not be null'
        //         }
        //       };
        //       throw "Mandatory Parameter od_Qty can not be null for Product name: ";
        //     }
        //     else if (d.od_MRP == null || d.od_MRP == 0) {
        //       objRes = {
        //         message: {
        //           'code': 1001,
        //           'message': 'Mandatory Parameter od_MRP can not be null'
        //         }
        //       };
        //       throw "Mandatory Parameter od_MRP can not be null for Product name: ";
        //     }
        //     else if (d.od_Tax_Rate == null) {
        //       objRes = {
        //         message: {
        //           'code': 1001,
        //           'message': 'Mandatory Parameter od_Tax_Rate can not be null'
        //         }
        //       };
        //       throw "Mandatory Parameter od_Tax_Rate can not be null for Product name"
        //     }
        //     else {
        //       if (d.od_Calc_Price == 0) {
        //         od_Calc_Price = d.od_Rate;		// Calculated Price
        //       }
        //       else {
        //         od_Calc_Price = d.od_Calc_Price;
        //       }
        //     }

        //     od_Tax_Inclusive = d.od_Tax_Inclusive;
        //     if (od_Tax_Inclusive == 1) {
        //       //  Begin : Inclusive_Tax_Calculation
        //       od_Prod_QtyXPrice_Amt = od_Calc_Price * d.od_Qty;    //Price * quantity				
        //       od_Tax_Rate = d.od_Tax_Rate;
        //       od_Bfr_Tax_Price = (od_Calc_Price * 100) / (od_Tax_Rate + 100);
        //       od_Prod_QtyXPrice_Amt = od_Bfr_Tax_Price * d.od_Qty;
        //       // End : Inclusive_Tax_Calculation				
        //     }
        //     else {
        //       // Begin : Exclusive_Tax_Calculation
        //       od_Prod_QtyXPrice_Amt = od_Calc_Price * d.od_Qty;	//Price * quantity		
        //       od_Tax_Rate = d.od_Tax_Rate;
        //       od_Bfr_Tax_Price = (od_Calc_Price);
        //       od_Prod_QtyXPrice_Amt = od_Bfr_Tax_Price * d.od_Qty;
        //     }
        //     // End : Exclusive_Tax_Calculation				
        //   }

        //   // making map of modifier objects (having reference sequence id) by unique sequence id
        //   // map - > modifiers map (key = Od_Prod_Ref_Seq , value = set of modifiers having same Od_Prod_Ref_Seq)
        //   if (d.od_Prod_Ref_Seq != null) {
        //     let obj = map.filter((data) => { return data.key === d.od_Prod_Ref_Seq })
        //     if (obj != null) {
        //       // if same Od_Prod_Ref_Seq is present as key then add object into existing set
        //       modifierObjSet.push(d);
        //       var seqobj = { key: d.od_Prod_Ref_Seq, value: modifierObjSet }
        //       map.push(seqobj);
        //     } else {
        //       // if Od_Prod_Ref_Seq is different than put new set having the object into map
        //       modifierObjSet = [];
        //       modifierObjSet.add(d);
        //       var seqobj = { key: d.od_Prod_Ref_Seq, value: modifierObjSet }
        //       map.push(seqobj);
        //     }
        //   }
        //   // set detail variables for next loop
        //   d.od_Bfr_Tax_Price = od_Bfr_Tax_Price;
        //   d.od_Prod_QtyXPrice_Amt = od_Prod_QtyXPrice_Amt;
        //   d.od_Calc_Price = od_Calc_Price;
        // });
        // // map.entrySet().forEach(entry -> {
        // //             System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
        // //         });

        // o.owt_Dtl_Code.forEach((d, k) => {
        //   map.forEach(element => {
        //     if (d.od_Prod_Seq == element.key) {
        //       var modifierAmtForProduct = 0.0;      // total modifiers amount for a product
        //       modifierValueSet = element.value;  // set of modifier objects

        //       // iterating set of Modifier's objects taken from map value
        //       modifierValueSet.forEach(setObj => {
        //         var modifierObj = setObj;
        //         var od_Prod_Modifier_Rate = 0.0;
        //         var od_Prod_Modifier_Amt = 0.0;
        //         var od_Prod_Modifier_Per = 0.0;

        //         // System.out.println("od_Prod_Modifier_Per : " + modifierObj.getOd_Prod_Modifier_Per());
        //         // System.out.println("od_Prod_Modifier_Rate : " + modifierObj.getOd_Prod_Modifier_Rate());

        //         // modifier value can be in percentage or in rate, we have to calculate amount from that and add into od_Prod_QtyXPrice_Amt
        //         if (modifierObj.od_Prod_Modifier_Per != 0) {
        //           od_Prod_Modifier_Rate = (modifierObj.od_Prod_Modifier_Per / 100) * d.od_Bfr_Tax_Price;
        //           od_Prod_Modifier_Amt = od_Prod_Modifier_Rate * d.od_Qty;
        //           od_Prod_Modifier_Per = modifierObj.od_Prod_Modifier_Per;
        //           modifierAmtForProduct = modifierAmtForProduct + od_Prod_Modifier_Amt;

        //         }
        //         else if (modifierObj.od_Prod_Modifier_Rate != 0) {
        //           od_Prod_Modifier_Rate = modifierObj.od_Prod_Modifier_Rate;
        //           od_Prod_Modifier_Amt = od_Prod_Modifier_Rate * d.od_Qty;
        //           od_Prod_Modifier_Per = modifierObj.od_Prod_Modifier_Per;
        //           modifierAmtForProduct = modifierAmtForProduct + od_Prod_Modifier_Amt;
        //         }
        //         // System.out.println("for sequence id : " + d.getOd_Prod_Seq() + " od_Prod_Modifier_Per : " + od_Prod_Modifier_Per + " od_Prod_Modifier_Amt : " + od_Prod_Modifier_Amt + " od_Prod_Modifier_Rate : " + od_Prod_Modifier_Rate);

        //         // setting modifier product's rate , amount and percentage
        //         modifierObj.od_Prod_Modifier_Per = od_Prod_Modifier_Per;
        //         modifierObj.od_Prod_Modifier_Amt = od_Prod_Modifier_Amt;
        //         modifierObj.od_Prod_Modifier_Rate = od_Prod_Modifier_Rate;
        //         newDetailObjSet.add(modifierObj);
        //       });

        //     }
        //   });
        //   // System.out.println("modifierAmtForProduct : " + modifierAmtForProduct);
        //   od_Prod_QtyXPrice_Amt = d.od_Prod_QtyXPrice_Amt + modifierAmtForProduct;        //=  Price*QTY + Modifiers_Charge
        //   // System.out.println("after addition of modifiers od_Prod_QtyXPrice_Amt : " + od_Prod_QtyXPrice_Amt);
        //   oh_Modifier_Amt = oh_Modifier_Amt + modifierAmtForProduct;  // total of all product's modifier amount

        //   d.od_Prod_Modifier_Amt = modifierAmtForProduct;
        //   d.od_Bfr_Tax_Price = od_Bfr_Tax_Price;
        //   d.od_Prod_QtyXPrice_Amt = od_Prod_QtyXPrice_Amt;
        //   d.od_Calc_Price = od_Calc_Price;

        //   newDetailObjSet.push(d);
        // });


        // // if modifier is present in given input payload then apply new detail set (newDetailObjSet) changes to header object
        // if (newDetailObjSet.length > 0)
        //   o.owt_Dtl_Code = newDetailObjSet;
        // // help(drools, " None of the product has modifiers defined !!");
        // o.oh_Modifier_Amt = oh_Modifier_Amt; //0.0 as default value
        // //----------------------------------Modifiers_Calculation_End--------------------------------------

        // condition for checking Document Discount as Value
        if (o.oh_Doc_Disc_Val != 0 && o.oh_Doc_Disc_Per == 0) {

          o.owt_Dtl_Code.forEach((d, k) => {
            console.log(k);
            // oh_Delivery_Amt = (oh_Delivery_Amt + d.od_Delivery_Amt)
            count++;
            od_Free_Tag = d.od_Free_Tag;

            if (od_Free_Tag == 1) {
              //oh_Doc_Item_Qty = oh_Doc_Item_Qty + d.getod_Qty().doubleValue();
            }
            else {
              prodName = d.od_Prod_Name;
              // prod_id=d.od_Prod_Code.id;
              //help(drools, "in for loop..." + prod_id);
              // Condition for Tax Inclusive/Exclusive
              //@author: Tanveer
              if (d.od_Tax_Inclusive == null) {
                //  throw new GlobalException(ErrorCodes.MANDATORY_PARAMETERS_NOT_SET, "Mandatory Parameter od_Tax_Inclusive can not be null");
                objRes = {
                  message: {
                    'code': 1001,
                    'message': 'Mandatory Parameter od_Tax_Inclusive can not be null'
                  }

                };
                throw "Mandatory Parameter od_Tax_Inclusive can not be null";
              }
              else {
                od_Tax_Inclusive = d.od_Tax_Inclusive;
                if (od_Tax_Inclusive == 1) {
                  // Begin : Inclusive_Tax_Calculation
                  if (d.od_Calc_Price == null) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_Tax_Inclusive can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_Calc_Price can not be null for Product name: ";
                  } else if (d.od_Rate == null) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_Rate can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_Rate can not be null for Product name";
                  } else if (d.od_Qty == null || d.od_Qty == 0) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_Qty can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_Qty can not be null for Product name: ";
                  } else if (d.od_MRP == null || d.od_MRP == 0) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_MRP can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_MRP can not be null for Product name: ";
                  } else {
                    if (d.od_Calc_Price == 0) {
                      od_Calc_Price = d.od_Rate;		// Calculated Price
                    }
                    else {
                      od_Calc_Price = d.od_Calc_Price;
                    }
                  }

                  od_Prod_QtyXPrice_Amt = od_Calc_Price * d.od_Qty;		//Price * quantity
                  if (d.od_Tax_Rate == null) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_Tax_Rate can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_Tax_Rate can not be null for Product name"
                  }

                  od_Tax_Rate = d.od_Tax_Rate;
                  od_Bfr_Tax_Price = (od_Calc_Price * 100) / (od_Tax_Rate + 100);
                  od_Prod_QtyXPrice_Amt = od_Bfr_Tax_Price * d.od_Qty;

                  //help(drools, "rate : " + d.od_Rate + "  before : " + od_Bfr_Tax_Price + " tax : " + od_Tax_Rate + " b4value : " + od_Prod_QtyXPrice_Amt);

                }
                // End : Inclusive_Tax_Calculation
                else {
                  // Begin : Exclusive_Tax_Calculation
                  //help(drools, "--------------------prod disc------------------------* qty" + d.od_Qty());
                  //if(d.od_Calc_Price == d.od_Rate) {

                  //@author: Tanveer
                  if (d.od_Calc_Price == null) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_Calc_Price can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_Calc_Price can not be null for Product name: ";
                  } else if (d.od_Rate == null) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_Rate can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_Rate can not be null for Product name: ";
                  } else if (d.od_Qty == null || d.od_Qty == 0) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_Qty can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_Qty can not be null for Product name: ";
                  } else if (d.od_MRP == null || d.od_MRP == 0) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_MRP can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_MRP can not be null for Product name: ";
                  } else {
                    if (d.od_Calc_Price == 0) {
                      od_Calc_Price = d.od_Rate;		// Calculated Price
                    }
                    else {
                      od_Calc_Price = d.od_Calc_Price;
                    }
                  }

                  od_Prod_QtyXPrice_Amt = od_Calc_Price * d.od_Qty;		//Price * quantity
                  od_Tax_Rate = d.od_Tax_Rate;
                  od_Bfr_Tax_Price = (od_Calc_Price);
                  od_Prod_QtyXPrice_Amt = od_Bfr_Tax_Price * d.od_Qty;
                }
              }

              // End : Exclusive_Tax_Calculation

              // Begin : Delivery_Charge_Calculation

              // System.out.println("Od_Delivery_Per : " + d.getOd_Delivery_Per());  // always come in percentage product wise
              // System.out.println("before addition of delivery charges od_Prod_QtyXPrice_Amt : " + od_Prod_QtyXPrice_Amt);
              if (d.od_Delivery_Per != 0) {
                // od_Delivery_Rate = (d.od_Delivery_Rate / 100) * od_Prod_QtyXPrice_Amt;
                od_Delivery_Amt = od_Prod_QtyXPrice_Amt * d.od_Delivery_Rate / 100;
                // od_Delivery_Amt = od_Delivery_Rate * d.od_Qty;
                od_Delivery_Per = d.od_Delivery_Per;
                od_Delivery_Rate = d.od_Delivery_Rate;
              }
              else {
                // od_Delivery_Amt = d.od_Delivery_Amt;
                od_Delivery_Amt = od_Delivery_Rate * d.od_Qty;
                od_Delivery_Per = d.od_Delivery_Per;
                od_Delivery_Rate = d.od_Delivery_Rate;
              }
              // // System.out.println("od_Delivery_Per : " + od_Delivery_Per + " od_Delivery_Amt : " + od_Delivery_Amt + " od_Delivery_Rate : " + od_Delivery_Rate);
              od_Prod_QtyXPrice_Amt = od_Prod_QtyXPrice_Amt + od_Delivery_Amt;		//=  Price*QTY + Delivery_Charge
              // System.out.println("after addition of delivery charges od_Prod_QtyXPrice_Amt : " + od_Prod_QtyXPrice_Amt);
              oh_Delivery_Amt = oh_Delivery_Amt + od_Delivery_Amt;

              // End : Delivery_Charge_Calculation


              // Begin : Special_Discount_Calculation

              if (d.od_Prod_Sp_Disc_Per != 0 && d.od_Prod_Sp_Disc_Amt != 0) {
                od_Prod_Sp_Disc_Amt = (d.od_Prod_Sp_Disc_Per / 100) * od_Prod_QtyXPrice_Amt;
              }
              else if (d.od_Prod_Sp_Disc_Amt != 0) {
                od_Prod_Sp_Disc_Amt = d.od_Prod_Promo_Amt;
              }
              else if (d.od_Prod_Sp_Disc_Per != 0) {
                od_Prod_Sp_Disc_Amt = (d.od_Prod_Promo_Per / 100) * od_Prod_QtyXPrice_Amt;
              }
              else {
                od_Prod_Sp_Disc_Amt = d.od_Prod_Sp_Disc_Amt;
                od_Prod_Sp_Disc_Per = d.od_Prod_Sp_Disc_Per;
              }
              od_Prod_QtyXPrice_Amt = (od_Prod_QtyXPrice_Amt - od_Prod_Sp_Disc_Amt);		//=  Price*QTY - Special_Discount

              // End : Special_Discount_Calculation

              // Begin : Product_Discount_Calculation


              if (d.od_Prod_Disc_Per != 0 && d.od_Prod_Disc_Amt != 0) {
                od_Prod_Disc_Amt = (d.od_Prod_Disc_Per / 100) * od_Prod_QtyXPrice_Amt;
                od_Prod_Disc_Per = d.od_Prod_Disc_Per;
              }
              else if (d.od_Prod_Disc_Amt != 0) {
                od_Prod_Disc_Amt = d.od_Prod_Disc_Amt;
                od_Prod_Disc_Per = d.od_Prod_Disc_Per;
              }
              else if (d.od_Prod_Disc_Per != 0) {
                od_Prod_Disc_Amt = (d.od_Prod_Disc_Per / 100) * od_Prod_QtyXPrice_Amt;
                od_Prod_Disc_Per = d.od_Prod_Disc_Per;
              }
              else {
                od_Prod_Disc_Amt = d.od_Prod_Disc_Amt;
                od_Prod_Disc_Per = d.od_Prod_Disc_Per;
              }
              od_Prod_QtyXPrice_Amt = (od_Prod_QtyXPrice_Amt - od_Prod_Disc_Amt);		//=  Price*QTY - Product_Discount

              // End : Product_Discount_Calculation

              // Begin : Promo_Discount_Calculation

              if (d.od_Prod_Promo_Per != 0 && d.od_Prod_Promo_Amt != 0) {
                od_Prod_Promo_Amt = (d.od_Prod_Promo_Per / 100) * od_Prod_QtyXPrice_Amt;
              }
              else if (d.od_Prod_Promo_Amt != 0) {
                od_Prod_Promo_Amt = d.od_Prod_Promo_Amt;
              }
              else if (d.od_Prod_Promo_Per != 0) {
                od_Prod_Promo_Amt = (d.od_Prod_Promo_Per / 100) * od_Prod_QtyXPrice_Amt;
              }
              else {
                od_Prod_Promo_Amt = d.od_Prod_Promo_Amt;
                od_Prod_Promo_Per = d.od_Prod_Promo_Per;
              }
              od_Prod_QtyXPrice_Amt = (od_Prod_QtyXPrice_Amt - od_Prod_Promo_Amt);		//=  Price*QTY - Promo_Discount

              // End : Promo_Discount_Calculation

              oh_Total_Disc_Val = (oh_Total_Disc_Val + od_Prod_Disc_Amt + od_Prod_Sp_Disc_Amt + od_Prod_Promo_Amt);
              od_Taxable_Goods_Amt = (od_Taxable_Goods_Amt + od_Prod_QtyXPrice_Amt);
              total = od_Taxable_Goods_Amt;

              d.od_Bfr_Tax_Price = od_Bfr_Tax_Price;
              d.od_Prod_QtyXPrice_Amt = od_Prod_QtyXPrice_Amt;
              d.od_Prod_Disc_Per = od_Prod_Disc_Per;
              d.od_Prod_Disc_Amt = od_Prod_Disc_Amt;
              d.od_Prod_Promo_Per = od_Prod_Promo_Per;
              d.od_Prod_Promo_Amt = od_Prod_Promo_Amt;
              d.od_Prod_Sp_Disc_Per = total_od_Prod_Sp_Disc_Per;
              d.od_Prod_Sp_Disc_Amt = od_Prod_Sp_Disc_Amt;
              d.od_Taxable_Goods_Amt = od_Taxable_Goods_Amt;
              d.od_Calc_Price = od_Calc_Price;
              d.od_Delivery_Per = od_Delivery_Per;
              d.od_Delivery_Amt = od_Delivery_Amt;
              d.od_Delivery_Rate = od_Delivery_Rate;
            }
            console.log(d);

          });

          // Begin : Document_Discount_Calculation(as Value)
          o.owt_Dtl_Code.forEach((d, k) => {
            console.log(k);
            od_Free_Tag = d.od_Free_Tag;
            if (od_Free_Tag == 1) {
              //System.out.println("free tag is applied on the product : " + d.getod_Prod_Name() + " ( id ) : " + d.getod_Prod_Code().getId().longValue());
              oh_Doc_Item_Qty = oh_Doc_Item_Qty + d.od_Qty;
              // set.add(d);
            } else {
              //System.out.println("--------------------document disc------------------------");
              //help(drools, "new rate before doc disc in doc loop:------------------" + od_Prod_QtyXPrice_Amt);
              //help(drools, "before doc disc: in doc loop------------------" + od_Taxable_Goods_Amt);
              if (o.oh_Doc_Disc_Val != 0) {
                running_total = d.od_Prod_QtyXPrice_Amt;
                oh_Doc_Disc_Val = o.oh_Doc_Disc_Val;
                total_rate = running_total;
                running_total = (running_total + (running_total / total) * oh_Doc_Disc_Val);
                od_Inv_Disc_Amt = running_total - total_rate;
              }

              od_Prod_QtyXPrice_Amt = (d.od_Prod_QtyXPrice_Amt - od_Inv_Disc_Amt);
              od_Taxable_Goods_Amt = od_Prod_QtyXPrice_Amt;
              oh_Total_Disc_Val = (oh_Total_Disc_Val + od_Inv_Disc_Amt);
              od_Discount = od_Prod_Disc_Amt + od_Prod_Promo_Amt + od_Prod_Sp_Disc_Amt + od_Coup_Amt;
              // End : Document_Discount_Calculation

              // Begin : Owt_Hdr_Calculation

              oh_Promo_Val = (oh_Promo_Val + od_Prod_Promo_Amt);

              oh_Coupon_Val = (oh_Coupon_Val + d.od_Coup_Amt);

              oh_Other_Disc_Val = (oh_Other_Disc_Val + d.od_Prod_Sp_Disc_Amt);

              oh_Doc_fright_Val = (oh_Doc_fright_Val + d.od_Fright_Amt);

              oh_one_Val = (oh_one_Val + d.od_one_Val);

              oh_Two_Val = (oh_Two_Val + d.od_Two_Val);

              oh_Three_Val = (oh_Three_Val + d.od_Three_Val);

              oh_Four_Val = (oh_Four_Val + d.od_Four_Val);

              oh_Five_Val = (oh_Five_Val + d.od_Five_Val);

              oh_Six_Val = (oh_Six_Val + d.od_Six_Val);

              oh_Seven_Val = (oh_Seven_Val + d.od_Seven_Val);

              oh_Eight_Val = (oh_Eight_Val + d.od_Eight_Val);

              oh_Nine_Val = (oh_Nine_Val + d.od_Nine_Val);

              oh_Ten_Val = (oh_Ten_Val + d.od_Ten_Val);

              oh_Doc_Item_Qty = (oh_Doc_Item_Qty + d.od_Qty);

              oh_Prod_QtyXPrice_Amt = (oh_Prod_QtyXPrice_Amt + od_Prod_QtyXPrice_Amt);

              oh_ProdXPrice_Amt = (oh_ProdXPrice_Amt + od_Prod_QtyXPrice_Amt);

              // End : Owt_Hdr_Calculation

              // Begin : Product_Tax_Calculation
              od_Prod_Tax1_Amt = (d.od_Tax1_Rate / 100) * od_Prod_QtyXPrice_Amt;
              od_Prod_Tax2_Amt = (d.od_Tax2_Rate / 100) * od_Prod_QtyXPrice_Amt;
              od_Prod_Tax3_Amt = (d.od_Tax3_Rate / 100) * od_Prod_QtyXPrice_Amt;
              od_Prod_Tax4_Amt = (d.od_Tax4_Rate / 100) * od_Prod_QtyXPrice_Amt;
              od_Prod_Tax5_Amt = (d.od_Tax5_Rate / 100) * od_Prod_QtyXPrice_Amt;

              od_Prod_Tax_Amt = (od_Prod_Tax1_Amt + od_Prod_Tax2_Amt + od_Prod_Tax3_Amt + od_Prod_Tax4_Amt + od_Prod_Tax5_Amt);
              od_Prod_NetAmt = (od_Prod_QtyXPrice_Amt + od_Prod_Tax_Amt);
              od_Goods_Amt = (od_Prod_QtyXPrice_Amt + od_Prod_Tax_Amt);
              oh_Goods_Val = (oh_Goods_Val + od_Goods_Amt);
              oh_Taxable_Goods_Amt = (oh_Taxable_Goods_Amt + od_Taxable_Goods_Amt);
              oh_Tax_total = (oh_Tax_total + od_Prod_Tax_Amt);
              oh_Doc_fright_Val = (oh_Doc_fright_Val + d.od_Fright_Amt);

              // End : Product_Tax_Calculation

              // Begin : Owt_Dtl_Set

              d.od_Goods_Amt = od_Goods_Amt;
              d.od_Coup_Amt = od_Coup_Amt;
              d.od_Prod_Tax1_Amt = od_Prod_Tax1_Amt;
              d.od_Prod_Tax2_Amt = od_Prod_Tax2_Amt;
              d.od_Prod_Tax3_Amt = od_Prod_Tax3_Amt;
              d.od_Prod_Tax4_Amt = od_Prod_Tax4_Amt;
              d.od_Prod_Tax5_Amt = od_Prod_Tax5_Amt;
              d.od_Inv_Disc_Amt = od_Inv_Disc_Amt;
              d.od_Prod_Tax_Amt = od_Prod_Tax_Amt;
              d.od_Taxable_Goods_Amt = od_Taxable_Goods_Amt;
              d.od_Prod_NetAmt = od_Prod_NetAmt;

              // End : Owt_Dtl_Set
              // set.add(d);
            }
          });

        }
        else {
          o.owt_Dtl_Code.forEach((d, k) => {
            console.log(k);
            // oh_Delivery_Amt = (oh_Delivery_Amt + d.od_Delivery_Amt)
            count++;
            od_Free_Tag = d.od_Free_Tag;
            if (od_Free_Tag == 1) {
              //System.out.println("free tag is applied on the product : " + d.getod_Prod_Name() + " ( id ) : " + d.getod_Prod_Code().getId().longValue());
              oh_Doc_Item_Qty = oh_Doc_Item_Qty + d.od_Qty;
              //set.add(d);
            } else {
              //@author: Tanveer
              prodName = d.od_Prod_Name;
              //prod_id=d.od_Prod_Code.id;
              //help(drools, "in for loop..." + prod_id);
              // Condition for Tax Inclusive/Exclusive

              if (d.od_Tax_Inclusive == null) {

                // throw new GlobalException(ErrorCodes.MANDATORY_PARAMETERS_NOT_SET, "Mandatory Parameter od_Tax_Inclusive can not be null for product id: " + prod_id);
                objRes = {
                  message: {
                    'code': 1001,
                    'message': 'Mandatory Parameter od_Tax_Inclusive can not be null'
                  }

                };
                throw "Mandatory Parameter od_Tax_Inclusive can not be null";
              }
              else {
                od_Tax_Inclusive = d.od_Tax_Inclusive;
                if (od_Tax_Inclusive == 1) {
                  //help(drools, "tax inclusive...");
                  // Begin : Inclusive_Tax_Calculation

                  //@author: Tanveer
                  if (d.od_Calc_Price == null) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_Calc_Price can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_Calc_Price can not be null for Product name";
                  } else if (d.od_Rate == null) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_Rate can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_Rate can not be null for Product name: ";
                  } else if (d.od_Qty == null || d.od_Qty == 0) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_Qty can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_Qty can not be null for Product name: ";
                  } else if (d.od_MRP == null || d.od_MRP == 0) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_MRP can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_MRP can not be null for Product name: ";
                  } else {
                    if (d.od_Calc_Price == 0) {
                      od_Calc_Price = d.od_Rate;		// Calculated Price
                    }
                    else {
                      od_Calc_Price = d.od_Calc_Price;
                    }
                  }

                  //if(d.od_Calc_Price == d.od_Rate) {
                  // od_Calc_Price = d.od_Rate;		// Calculated Price
                  //help(drools, "calc price " + od_Calc_Price);
                  //}
                  od_Prod_QtyXPrice_Amt = od_Calc_Price * d.od_Qty;		//Price * quantity
                  od_Tax_Rate = d.od_Tax_Rate;
                  od_Bfr_Tax_Price = (od_Calc_Price * 100) / (od_Tax_Rate + 100);
                  od_Prod_QtyXPrice_Amt = od_Bfr_Tax_Price * d.od_Qty;

                  //System.out.println("rate : " + d.od_Rate + "  before : " + od_Bfr_Tax_Price + " tax : " + od_Tax_Rate + " b4value : " + od_Prod_QtyXPrice_Amt);
                }
                // End : Inclusive_Tax_Calculation
                else {
                  // Begin : Exclusive_Tax_Calculation
                  //@author: Tanveer
                  if (d.od_Calc_Price == null) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_Calc_Price can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_Calc_Price can not be null for Product name: ";
                  } else if (d.od_Rate == null) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_Rate can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_Rate can not be null for Product name: ";
                  } else if (d.od_Qty == null || d.od_Qty == 0) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_Qty can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_Qty can not be null for Product name: ";
                  } else if (d.od_MRP == null || d.od_MRP == 0) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_MRP can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_MRP can not be null for Product name: ";
                  } else {
                    if (d.od_Calc_Price == 0) {
                      od_Calc_Price = d.od_Rate;		// Calculated Price
                    }
                    else {
                      od_Calc_Price = d.od_Calc_Price;
                    }
                  }

                  od_Prod_QtyXPrice_Amt = od_Calc_Price * d.od_Qty;		//Price * quantity
                  //@author: Tanveer
                  if (d.od_Tax_Rate == null) {
                    objRes = {
                      message: {
                        'code': 1001,
                        'message': 'Mandatory Parameter od_Tax_Rate can not be null'
                      }

                    };
                    throw "Mandatory Parameter od_Tax_Rate can not be null for Product name: ";
                  }

                  od_Tax_Rate = d.od_Tax_Rate;
                  od_Bfr_Tax_Price = (od_Calc_Price);
                  od_Prod_QtyXPrice_Amt = od_Bfr_Tax_Price * d.od_Qty;

                  //System.out.println("rate : " + d.od_Rate + "  before : " + od_Bfr_Tax_Price + " tax : " + od_Tax_Rate + " b4value : " + od_Prod_QtyXPrice_Amt);
                }
              }
              // End : Exclusive_Tax_Calculation

              // Begin : Delivery_Charge_Calculation

              // System.out.println("Od_Delivery_Per : " + d.getOd_Delivery_Per());  // always come in percentage product wise
              // System.out.println("before addition of delivery charges od_Prod_QtyXPrice_Amt : " + od_Prod_QtyXPrice_Amt);
              if (d.od_Delivery_Per != 0) {
                // od_Delivery_Rate = (d.od_Delivery_Rate / 100) * od_Prod_QtyXPrice_Amt;
                od_Delivery_Amt = od_Prod_QtyXPrice_Amt * d.od_Delivery_Rate / 100;
                // od_Delivery_Amt = od_Delivery_Rate * d.od_Qty;
                od_Delivery_Per = d.od_Delivery_Per;
                od_Delivery_Rate = d.od_Delivery_Rate;
              }
              else {
                // od_Delivery_Amt = d.od_Delivery_Amt;
                od_Delivery_Amt = od_Delivery_Rate * d.od_Qty;
                od_Delivery_Per = d.od_Delivery_Per;
                od_Delivery_Rate = d.od_Delivery_Rate;
              }
              // // System.out.println("od_Delivery_Per : " + od_Delivery_Per + " od_Delivery_Amt : " + od_Delivery_Amt + " od_Delivery_Rate : " + od_Delivery_Rate);
              od_Prod_QtyXPrice_Amt = od_Prod_QtyXPrice_Amt + od_Delivery_Amt;		//=  Price*QTY + Delivery_Charge
              // System.out.println("after addition of delivery charges od_Prod_QtyXPrice_Amt : " + od_Prod_QtyXPrice_Amt);
              oh_Delivery_Amt = oh_Delivery_Amt + od_Delivery_Amt;

              // End : Delivery_Charge_Calculation

              // Begin : Special_Discount_Calculation

              if (d.od_Prod_Sp_Disc_Per != 0 && d.od_Prod_Sp_Disc_Amt != 0) {
                od_Prod_Sp_Disc_Amt = (d.od_Prod_Sp_Disc_Per / 100) * od_Prod_QtyXPrice_Amt;
              }
              else if (d.od_Prod_Sp_Disc_Amt != 0) {
                od_Prod_Sp_Disc_Amt = d.od_Prod_Promo_Amt;
              }
              else if (d.od_Prod_Sp_Disc_Per != 0) {
                od_Prod_Sp_Disc_Amt = (d.od_Prod_Promo_Per / 100) * od_Prod_QtyXPrice_Amt;
              }
              else {
                od_Prod_Sp_Disc_Amt = d.od_Prod_Sp_Disc_Amt;
                od_Prod_Sp_Disc_Per = d.od_Prod_Sp_Disc_Per;
              }
              od_Prod_QtyXPrice_Amt = (od_Prod_QtyXPrice_Amt - od_Prod_Sp_Disc_Amt);		//=  Price*QTY - Special_Discount

              // End : Special_Discount_Calculation

              // Begin : Product_Discount_Calculation

              if (d.od_Prod_Disc_Per != 0 && d.od_Prod_Disc_Amt != 0) {
                od_Prod_Disc_Amt = (d.od_Prod_Disc_Per / 100) * od_Prod_QtyXPrice_Amt;
                od_Prod_Disc_Per = d.od_Prod_Disc_Per;
              }
              else if (d.od_Prod_Disc_Amt != 0) {
                od_Prod_Disc_Amt = d.od_Prod_Disc_Amt;
                od_Prod_Disc_Per = d.od_Prod_Disc_Per;
              }
              else if (d.od_Prod_Disc_Per != 0) {
                od_Prod_Disc_Amt = (d.od_Prod_Disc_Per / 100) * od_Prod_QtyXPrice_Amt;
                od_Prod_Disc_Per = d.od_Prod_Disc_Per;
              }
              else {
                od_Prod_Disc_Amt = d.od_Prod_Disc_Amt;
                od_Prod_Disc_Per = d.od_Prod_Disc_Per;
              }

              od_Prod_QtyXPrice_Amt = (od_Prod_QtyXPrice_Amt - od_Prod_Disc_Amt);		//=  Price*QTY - Product_Discount

              // End : Product_Discount_Calculation

              // Begin : Promo_Discount_Calculation

              if (d.od_Prod_Promo_Per != 0 && d.od_Prod_Promo_Amt != 0) {
                od_Prod_Promo_Amt = (d.od_Prod_Promo_Per / 100) * od_Prod_QtyXPrice_Amt;
              }
              else if (d.od_Prod_Promo_Amt != 0) {
                od_Prod_Promo_Amt = d.od_Prod_Promo_Amt;
              }
              else if (d.od_Prod_Promo_Per != 0) {
                od_Prod_Promo_Amt = (d.od_Prod_Promo_Per / 100) * od_Prod_QtyXPrice_Amt;
              }
              else {
                od_Prod_Promo_Amt = d.od_Prod_Promo_Amt;
                od_Prod_Promo_Per = d.od_Prod_Promo_Per;
              }
              od_Prod_QtyXPrice_Amt = (od_Prod_QtyXPrice_Amt - od_Prod_Promo_Amt);		//=  Price*QTY - Promo_Discount

              // End : Promo_Discount_Calculation

              // Begin : Document_Discount_Calculation

              if (o.oh_Doc_Disc_Per != 0) {
                od_Inv_Disc_Amt = (o.oh_Doc_Disc_Per / 100) * od_Prod_QtyXPrice_Amt;
                oh_Prod_Disc_Val = (oh_Prod_Disc_Val + od_Inv_Disc_Amt);
                //@author: Tanveer
                oh_Doc_Disc_Val = (oh_Doc_Disc_Val + od_Inv_Disc_Amt);
              }
              else {
                oh_Doc_Disc_Val = o.oh_Doc_Disc_Val;
                oh_Doc_Disc_Per = o.oh_Doc_Disc_Per;
              }

              od_Prod_QtyXPrice_Amt = (od_Prod_QtyXPrice_Amt - od_Inv_Disc_Amt);
              //@author: Tanveer
              od_Taxable_Goods_Amt = od_Prod_QtyXPrice_Amt;
              oh_Total_Disc_Val = (oh_Total_Disc_Val + od_Prod_Disc_Amt + od_Prod_Sp_Disc_Amt + od_Prod_Promo_Amt + od_Inv_Disc_Amt);

              // End : Document_Discount_Calculation

              // Begin : Owt_Hdr_Calculation

              oh_Promo_Val = (oh_Promo_Val + od_Prod_Promo_Amt);

              oh_Coupon_Val = (oh_Coupon_Val + d.od_Coup_Amt);

              oh_Other_Disc_Val = (oh_Other_Disc_Val + d.od_Prod_Sp_Disc_Amt);

              oh_Doc_fright_Val = (oh_Doc_fright_Val + d.od_Fright_Amt);

              oh_one_Val = (oh_one_Val + d.od_one_Val);

              oh_Two_Val = (oh_Two_Val + d.od_Two_Val);

              oh_Three_Val = (oh_Three_Val + d.od_Three_Val);

              oh_Four_Val = (oh_Four_Val + d.od_Four_Val);

              oh_Five_Val = (oh_Five_Val + d.od_Five_Val);

              oh_Six_Val = (oh_Six_Val + d.od_Six_Val);

              oh_Seven_Val = (oh_Seven_Val + d.od_Seven_Val);

              oh_Eight_Val = (oh_Eight_Val + d.od_Eight_Val);

              oh_Nine_Val = (oh_Nine_Val + d.od_Nine_Val);

              oh_Ten_Val = (oh_Ten_Val + d.od_Ten_Val);

              oh_Doc_Item_Qty = (oh_Doc_Item_Qty + d.od_Qty);

              oh_Prod_QtyXPrice_Amt = (oh_Prod_QtyXPrice_Amt + od_Prod_QtyXPrice_Amt);

              oh_ProdXPrice_Amt = (oh_ProdXPrice_Amt + od_Prod_QtyXPrice_Amt);

              od_Discount = (od_Prod_Disc_Amt + od_Prod_Promo_Amt + od_Prod_Sp_Disc_Amt + od_Coup_Amt);

              // End : Owt_Hdr_Calculation

              // Begin : Product_Tax_Calculation

              od_Prod_Tax1_Amt = (d.od_Tax1_Rate / 100) * od_Prod_QtyXPrice_Amt;
              od_Prod_Tax2_Amt = (d.od_Tax2_Rate / 100) * od_Prod_QtyXPrice_Amt;
              od_Prod_Tax3_Amt = (d.od_Tax3_Rate / 100) * od_Prod_QtyXPrice_Amt;
              od_Prod_Tax4_Amt = (d.od_Tax4_Rate / 100) * od_Prod_QtyXPrice_Amt;
              od_Prod_Tax5_Amt = (d.od_Tax5_Rate / 100) * od_Prod_QtyXPrice_Amt;

              od_Prod_Tax_Amt = (od_Prod_Tax1_Amt + od_Prod_Tax2_Amt + od_Prod_Tax3_Amt + od_Prod_Tax4_Amt + od_Prod_Tax5_Amt);
              od_Goods_Amt = (od_Prod_QtyXPrice_Amt + od_Prod_Tax_Amt);
              oh_Goods_Val = (oh_Goods_Val + od_Goods_Amt);
              od_Prod_NetAmt = (od_Prod_QtyXPrice_Amt + od_Prod_Tax_Amt);
              oh_Taxable_Goods_Amt = (oh_Taxable_Goods_Amt + od_Taxable_Goods_Amt);
              oh_Tax_total = (oh_Tax_total + od_Prod_Tax_Amt);
              oh_Doc_fright_Val = (oh_Doc_fright_Val + d.od_Fright_Amt);

              // End : Product_Tax_Calculation

              // Begin : Owt_Dtl_Set

              d.od_Goods_Amt = od_Goods_Amt;
              d.od_Bfr_Tax_Price = od_Bfr_Tax_Price;
              d.od_Prod_QtyXPrice_Amt = od_Prod_QtyXPrice_Amt;
              d.od_Prod_Disc_Per = od_Prod_Disc_Per;
              d.od_Prod_Disc_Amt = od_Prod_Disc_Amt;
              d.od_Calc_Price = od_Calc_Price;
              d.od_Inv_Disc_Per = od_Inv_Disc_Per;
              d.od_Inv_Disc_Amt = od_Inv_Disc_Amt;
              d.od_Prod_Promo_Amt = od_Prod_Promo_Amt;
              d.od_Prod_Sp_Disc_Per = total_od_Prod_Sp_Disc_Per;
              d.od_Prod_Sp_Disc_Amt = od_Prod_Sp_Disc_Amt;
              d.od_Coup_Amt = od_Coup_Amt;

              d.od_Prod_Tax1_Amt = od_Prod_Tax1_Amt;
              d.od_Prod_Tax2_Amt = od_Prod_Tax2_Amt;
              d.od_Prod_Tax3_Amt = od_Prod_Tax3_Amt;
              d.od_Prod_Tax4_Amt = od_Prod_Tax4_Amt;
              d.od_Prod_Tax5_Amt = od_Prod_Tax5_Amt;
              d.od_Prod_Tax_Amt = od_Prod_Tax_Amt;

              d.od_Prod_NetAmt = od_Prod_NetAmt;
              d.od_Taxable_Goods_Amt = od_Taxable_Goods_Amt;

              d.od_Delivery_Per = od_Delivery_Per;
              d.od_Delivery_Amt = od_Delivery_Amt;
              d.od_Delivery_Rate = od_Delivery_Rate;

              // End : Owt_Dtl_Set
              // set.add(d);
            }

          });
        }
        oh_Doc_Item_Count = count;


        //Tax summary addition
        // Begin : Owt_Tax_Calculation
        var hashSet = [];
        /*angular.forEach(o.owt_Dtl_Code, function (valueObject, keyObject) {
         if(hashSet.indexOf(valueObject['od_Tax_Rate']) == -1)
         hashSet.push(valueObject['od_Tax_Rate']);
         });*/
        for (var i = 0; i < o.owt_Dtl_Code.length; i++) {
          var valueObject = o.owt_Dtl_Code[i];
          if (hashSet.indexOf(valueObject['od_Tax_Rate']) == -1)
            hashSet.push(valueObject['od_Tax_Rate']);
        }

        for (var i = 0; i < hashSet.length; i++) {
          var t1 = hashSet[i];
          //angular.forEach(hashSet, function (value, key){
          //	var t1 = value;
          sum = 0.0;
          od_Tax_Code_owt = 0;
          otax_1_Rate_owt = 0.0;
          otax_2_Rate_owt = 0.0;
          otax_3_Rate_owt = 0.0;
          otax_4_Rate_owt = 0.0;
          otax_5_Rate_owt = 0.0;
          otax_1_Code = 0;
          otaxble_Goods_Amt = 0.0;
          //alert(o.owt_Dtl_Code.length);
          for (var j = 0; j < o.owt_Dtl_Code.length; j++) {
            // angular.forEach(o.owt_Dtl_Code, function (d,k){
            var d = o.owt_Dtl_Code[j];
            if (d.od_Tax_Rate == t1) {
              otax_1_Amt = otax_1_Amt + d.od_Prod_Tax1_Amt;
              otax_2_Amt = otax_2_Amt + d.od_Prod_Tax2_Amt;
              otax_3_Amt = otax_3_Amt + d.od_Prod_Tax3_Amt;
              otax_4_Amt = otax_4_Amt + d.od_Prod_Tax4_Amt;
              otax_5_Amt = otax_5_Amt + d.od_Prod_Tax5_Amt;
              otax_1_Rate_owt = d.od_Tax1_Rate;
              otax_2_Rate_owt = d.od_Tax2_Rate;
              otax_3_Rate_owt = d.od_Tax3_Rate;
              otax_4_Rate_owt = d.od_Tax4_Rate;
              otax_5_Rate_owt = d.otax_5_Rate;
              od_Tax_Code_owt = d.od_Tax_Code;
              //  console.log("DRL "+JSON.stringify(d));
              otax_1_Code = d.od_Tax1_Code;
              otaxble_Goods_Amt = (otaxble_Goods_Amt + d.od_Taxable_Goods_Amt);
              sum = (sum + d.od_Prod_Tax_Amt);
            }

            //});
          }

          var tax: any = {};
          tax.otax_Rate = t1;
          tax.otax_Tax_total = sum;
          tax.otax_1_Amt = otax_1_Amt;
          tax.otax_2_Amt = otax_2_Amt;
          tax.otax_3_Amt = otax_3_Amt;
          tax.otax_4_Amt = otax_4_Amt;
          tax.otax_5_Amt = otax_5_Amt;
          tax.otax_1_Rate = otax_1_Rate_owt;
          tax.otax_2_Rate = otax_2_Rate_owt;
          tax.otax_3_Rate = otax_3_Rate_owt;
          tax.otax_4_Rate = otax_4_Rate_owt;
          tax.otax_5_Rate = otax_5_Rate_owt;
          //    var tax_Class_MsT:any = {};
          //    tax_Class_MsT.id = od_Tax_Code_owt;
          //    tax.otax_Code = tax_Class_MsT;
          tax.otax_Code = od_Tax_Code_owt;   //updated
          tax.otax_1_Code = otax_1_Code;
          tax.otaxble_Goods_Amt = otaxble_Goods_Amt;



          var now = new Date();

          // 3) a java current time (now) instance
          tax.otax_TS_Added = now;
          tax.otax_TS_Edited = now;

          for (var iter = 0; iter < o.owt_Tax_Code.length; iter++) {
            var t = o.owt_Tax_Code[iter];
            //angular.forEach(o.owt_Tax_Code, function (t,kt){
            tax.otax_user_Code_Add = t.otax_user_Code_Add;
            tax.otax_user_Code_Updt = t.otax_user_Code_Updt;
            //});
          }

          setTax.push(tax);

          // });
        }

        // End : Owt_Tax_Calculation

        // oh_Doc_Net_Amt = (oh_Doc_Net_Amt + oh_Taxable_Goods_Amt + oh_Tax_total + oh_Doc_fright_Val + o.oh_Doc_RoundOff + oh_Delivery_Amt);
        oh_Doc_Net_Amt = (oh_Doc_Net_Amt + oh_Taxable_Goods_Amt + oh_Tax_total + oh_Doc_fright_Val + o.oh_Doc_RoundOff);
        o.oh_Coupon_Share_Per = oh_Coupon_Share_Per;
        o.oh_Goods_Val = oh_Goods_Val;
        o.oh_Total_Disc_Val = oh_Total_Disc_Val;
        o.oh_Promo_Val = oh_Promo_Val;
        o.oh_Coupon_Val = oh_Coupon_Val;
        o.oh_Doc_Disc_Val = oh_Doc_Disc_Val;
        o.oh_Other_Disc_Val = oh_Other_Disc_Val;
        o.oh_Tax_total = oh_Tax_total;
        o.oh_Doc_fright_Val = oh_Doc_fright_Val;
        o.oh_one_Val = oh_one_Val;
        o.oh_Two_Val = oh_Two_Val;
        o.oh_Three_Val = oh_Three_Val;
        o.oh_Four_Val = oh_Four_Val;
        o.oh_Five_Val = oh_Five_Val;
        o.oh_Six_Val = oh_Six_Val;
        o.oh_Seven_Val = oh_Seven_Val;
        o.oh_Eight_Val = oh_Eight_Val;
        o.oh_Nine_Val = oh_Nine_Val;
        o.oh_Ten_Val = oh_Ten_Val;
        o.oh_Doc_Net_Amt = oh_Doc_Net_Amt;
        o.oh_Doc_Item_Qty = oh_Doc_Item_Qty;
        o.oh_Taxable_Goods_Amt = oh_Taxable_Goods_Amt;
        o.oh_Doc_Item_Count = oh_Doc_Item_Count;
        o.oh_ProdXPrice_Amt = oh_ProdXPrice_Amt;
        o.oh_Delivery_Amt = oh_Delivery_Amt;
        o.owt_Tax_Code = setTax;

      });
      //@author: Tanveer.
      //Addition in json structure because of our controller/service logic.

      var oRef = {};
      /*   angular.forEach(response[0], function(value, key){
       if(value instanceof Array){
       if((value.length > 0)){
       oRef[key] = value;
       }
       }
       else if(value != null){
       oRef[key] = value;
       }
 
       });*/

      for (var j = 0; j < response.length; j++) {
        var value = response[j];
        if (value instanceof Array) {
          if ((value.length > 0)) {
            oRef[j] = value;
          }
        }
        else if (value != null) {
          oRef[j] = value;
        }

      }
      response[0] = oRef[0];


      var data = [];
      data[0] = response[0];
      objRes = {
        data: {
          'OWT_Hdr': data
        },
        message: {
          'code': 0
        }

      };

    }
    catch (err) {
      console.log(err);
    }
    return objRes;
  }
  test() {
    alert("called");
    console.log("drl");
  }

}