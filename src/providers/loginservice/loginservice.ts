import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { Observable } from "rxjs";
import { Storage } from '@ionic/storage';
import { SyncProvider } from '../sync/sync';
import { Header } from 'ionic-angular/components/toolbar/toolbar-header';
import { HttpHandler } from '@angular/common/http/src/backend';

/*
  Generated class for the LoginserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginserviceProvider {
  [x: string]: {};
  // public url: any;
  // public tid: any;
  // public userId: any;
  // public accessToken: any;
  // public uid: any;

  constructor(public http: HttpClient, public _storage: Storage, public sync: SyncProvider) {
    console.log('Hello LoginserviceProvider Provider');
  }
  DemoActivation() {
    let params = {
      "entityName": "Register_MsT",
      "destEntityName": "Party_SubDomain",
      "action": "payload",
      "event": "demo_Reg",
      "pageUrl": "Register",
      "formData": {
        "reg_MacID": this.sync.regMacId,
        "vertical": this.sync.demoVertical
      }
    };
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'domainName': this.sync.demoDomain
      })
    };
    return this.http.post(this.sync.url, params, options)
      .map(response => { return response; })
      .catch(error => Observable.throw({ status: error.status, Errors: error }));
  }
  DemoWovvSystem() {
    let params = {
      "pageUrl": "Wovvsystem",
      "entityName": "Wovvsystem",
      "action": "payload",
      "event": "completeSyncToMysql",
      "whereClause": [
        {
          "filterKey": "tid",
          "filterValue": this.sync.tenantId,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "branch_Code_Id",
          "filterValue": this.sync.branchCode,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "finYear_End_Status",
          "filterValue": 0,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "reg_Type",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "register_Code_Id",
          "filterValue": this.sync.registerCode,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "reg_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "user_Approve_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "regCH_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": ""
        }
      ],
      "selectFields": [
        { "fieldName": "user_Id" },
        { "fieldName": "user_Login_ID" },
        { "fieldName": "user_Password" },
        { "fieldName": "org_Code_Id" },
        { "fieldName": "user_Role_Id" },
        { "fieldName": "party_Name" },
        { "fieldName": "register_Code_Id" },
        { "fieldName": "stock_Loc_Code_Id" },
        { "fieldName": "div_Code_Id" },
        { "fieldName": "reg_Name" },
        { "fieldName": "reg_Prefix" },
        { "fieldName": "reg_Type" },
        { "fieldName": "branch_Code_Id" },
        { "fieldName": "branch_Name" },
        { "fieldName": "branch_Prefix" },
        { "fieldName": "branch_Tax_Type_Code_Id" },
        { "fieldName": "tax_Type_Name" },
        { "fieldName": "org_Finyear_Id" },
        { "fieldName": "financial_Year" },
        { "fieldName": "org_Type" },
        { "fieldName": "is_System_Default" },
        { "fieldName": "user_Code_Add" },
        { "fieldName": "user_Code_Updt" },
        { "fieldName": "party_Code_Id" },
        { "fieldName": "org_Code" },
        { "fieldName": "bg_Name" },
        { "fieldName": "bg_Prefix" },
        { "fieldName": "branch_Group_Code_Id" },
        { "fieldName": "register_Child_id" },
        { "fieldName": "regCH_Default" },
        { "fieldName": "year_End_Date" },
        { "fieldName": "year_Began_Date" },
        { "fieldName": "is_Global_YN" },
        { "fieldName": "branch_Status" },
        { "fieldName": "finYear_End_Status" },
        { "fieldName": "reg_Status" },
        { "fieldName": "stock_Loc_Name" },
        { "fieldName": "party_Relation_Name" },
        { "fieldName": "party_Relation_Code_Id" },
        { "fieldName": "party_MobNo" },
        { "fieldName": "branch_MobNo" },
        { "fieldName": "branch_Area_Code_Id" },
        { "fieldName": "user_party_Code_Id" },
        { "fieldName": "branch_Role_Code" },
        { "fieldName": "branch_Currency_Symbol" }
      ],
      "paginationFields": {
        "firstResult": 0,
        "maxResults": 100
      }
    };
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'access_Token': this.sync.token,
        'Register_Id': String(this.sync.registerCode),
        'Reg_Mac_Id': 'zzzz'
      })

    };
    return this.http.post('https://10.10.10.227:8443/ipos/rest/bpm/process' + "?tenantId=" + this.sync._tenantId, params, options)
      .map(response => { return response; })
      .catch(error => Observable.throw({ status: error.status, Errors: error }));
  }
  DemoWovvUsrMst(FilterValue) {
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'access_Token': this.sync.token,
        'Register_Id': String(this.sync.registerCode),
        'Reg_Mac_Id': 'zzzz'
      })
    };
    var params = {
      "entityName": "WovV_User_MsT",
      "action": "view",
      "event": "selectFilter",
      "pageUrl": "User",
      "whereClause": [{
        "filterKey": "Id",
        "filterValue": FilterValue,
        "relation": "IN",
        "condition": ""
      }],
      "selectFields": [
        { "fieldName": "user_Login_ID" },
        { "fieldName": "e1_Password" }
      ]
    };
    return this.http.post('https://10.10.10.227:8443/ipos/rest/bpm/process' + "?tenantId=" + this.sync._tenantId, params, options)
      .map(response => { return response; })
      .catch(error => Observable.throw({ status: error.status, Errors: error }));
  }
  onlineLogin(username, password) {
    // this.url = window.this.sync.url;
    // this.tid = window.localStorage.getItem('_tid');

    let params = {
      "entityName": "WovV_User_MsT",
      "action": "payload",
      "pageUrl": "User",
      "event": "authenticate",
      "formData": {
        "user_Password": password,
        "user_Login_ID": username
      }
    };
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(this.sync.url + '?tenantId=' + this.sync._tenantId, params, options)
      .map(response => { return response; })
      .catch(error => Observable.throw({ status: error.status, Errors: error }));
  }
  userAvailable(username, password) {
    // this.url = window.this.sync.url;
    // this.tid = window.localStorage.getItem('_tid');
    let params = {
      "entityName": "WovV_User_MsT",
      "action": "payload",
      "pageUrl": "User",
      "event": "clearUserToken",
      "formData": {
        "user_Password": password,
        "user_Login_ID": username
      }
    }

    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(this.sync.url + '?tenantId=' + this.sync._tenantId, params, options)
      .map(response => { return response; })
      .catch(error => Observable.throw({ status: error.status, Errors: error }));
  }
  checkDomain(domainName) {
    return this.http.get(this.sync.url + domainName)
      .map(response => { return response; })
      .catch(error => Observable.throw({ status: error.status, Errors: error }));
  }
  wovvsystemcall() {
    // this.url = window.this.sync.url;
    // this.tid = window.localStorage.getItem('_tid');
    // this.userId = window.localStorage.getItem('userId');
    // this.accessToken = window.localStorage.getItem('access_token');

    let params = {
      "entityName": "Wovvsystem",
      "pageUrl": "login",
      "action": "view",
      "event": "selectFilter",
      "whereClause": [
        {
          "filterKey": "user_Id",
          "filterValue": this.sync.registerCode,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "finYear_End_Status",
          "filterValue": 0,
          "relation": "=",
          "condition": "AND"
        },
        {
          "filterKey": "reg_Type",
          "filterValue": 1,
          "relation": "!=",
          "condition": "AND"
        },
        {
          "filterKey": "regCH_Status",
          "filterValue": 1,
          "relation": "=",
          "condition": ""
        }
      ],
      "selectFields": [
        {
          "fieldName": "user_Id"
        },
        {
          "fieldName": "user_Login_ID"
        },
        {
          "fieldName": "user_Password"
        },
        {
          "fieldName": "org_Code_Id"
        },
        {
          "fieldName": "user_Role_Id"
        },
        {
          "fieldName": "party_Name"
        },
        {
          "fieldName": "register_Code_Id"
        },
        {
          "fieldName": "stock_Loc_Code_Id"
        },
        {
          "fieldName": "div_Code_Id"
        },
        {
          "fieldName": "reg_Name"
        },
        {
          "fieldName": "reg_Prefix"
        },
        {
          "fieldName": "reg_Type"
        },
        {
          "fieldName": "branch_Code_Id"
        },
        {
          "fieldName": "branch_Name"
        },
        {
          "fieldName": "branch_Prefix"
        },
        {
          "fieldName": "branch_Tax_Type_Code_Id"
        },
        {
          "fieldName": "tax_Type_Name"
        },
        {
          "fieldName": "org_Finyear_Id"
        },
        {
          "fieldName": "financial_Year"
        },
        {
          "fieldName": "org_Type"
        },
        {
          "fieldName": "is_System_Default"
        },
        {
          "fieldName": "user_Code_Add"
        },
        {
          "fieldName": "user_Code_Updt"
        },
        {
          "fieldName": "party_Code_Id"
        },
        {
          "fieldName": "org_Code"
        },
        {
          "fieldName": "bg_Name"
        },
        {
          "fieldName": "bg_Prefix"
        },
        {
          "fieldName": "branch_Group_Code_Id"
        },
        {
          "fieldName": "register_Child_id"
        },
        {
          "fieldName": "regCH_Default"
        },
        {
          "fieldName": "year_End_Date"
        },
        {
          "fieldName": "year_Began_Date"
        },
        {
          "fieldName": "is_Global_YN"
        },
        {
          "fieldName": "branch_Status"
        },
        {
          "fieldName": "finYear_End_Status"
        },
        {
          "fieldName": "reg_Status"
        },
        {
          "fieldName": "stock_Loc_Name"
        },
        {
          "fieldName": "party_Relation_Name"
        },
        {
          "fieldName": "party_Relation_Code_Id"
        },
        {
          "fieldName": "party_MobNo"
        },
        {
          "fieldName": "branch_MobNo"
        },
        {
          "fieldName": "branch_Area_Code_Id"
        },
        {
          "fieldName": "user_party_Code_Id"
        },
        {
          "fieldName": "branch_Role_Code"
        },
        {
          "fieldName": "org_Name"
        },
        {
          "fieldName": "branch_Currency_Symbol"
        }
      ]
    }
    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'access_Token': this.sync.token,
        'userId': this.sync.registerCode
      })
    };
    return this.http.post(this.sync.url + '?tenantId=' + this.sync._tenantId, params, options)
      .map(response => { return response; })
      .catch(error => Observable.throw({ status: error.status, Errors: error }));
  }
  Logout() {
    // this.url = window.this.sync.url;
    // this.tid = window.localStorage.getItem('_tid');
    // this.userId = window.localStorage.getItem('UserID');
    // this.uid = window.localStorage.getItem('uid');
    // this.accessToken = window.localStorage.getItem('onlineToken');
    this.sync.GetLocalData();
    let params = {
      "entityName": "WovV_User_MsT",
      "action": "authenticate",
      "event": "logout",
      "pageUrl": "Logout",
      "formData": {
        "wovv_User_Code_Id": this.sync.registerCode,
        "org_code_id": this.sync.orgCode
      }
    }

    var options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'access_Token': this.sync.token,
        'Register_Id': this.sync.registerCode
      })
    };

    return this.http.post(this.sync.url + '?tenantId=' + this.sync._tenantId + '&tkUserId=' + this.sync.registerCode, params, options)
      .map(response => { return response; })
      .catch(error => Observable.throw({ status: error.status, Errors: error }));
  }


}
