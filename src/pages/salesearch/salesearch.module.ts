import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalesearchPage } from './salesearch';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SalesearchPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(SalesearchPage),
  ],
})
export class SalesearchPageModule { }
