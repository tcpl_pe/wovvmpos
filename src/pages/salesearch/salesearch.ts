import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, ToastController, MenuController } from 'ionic-angular';
import { GlobalsearchProvider } from '../../providers/globalsearch/globalsearch';
import { SyncProvider } from '../../providers/sync/sync';
import { OrderdetailProvider } from '../../providers/orderdetail/orderdetail';
import { OneSignal } from '@ionic-native/onesignal';

/**
 * Generated class for the SalesearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-salesearch',
  templateUrl: 'salesearch.html',
})
export class SalesearchPage {

  searchInput: any = '';
  searchItem = [];
  global_search: any = "customer_search";
  customer_info: any = [];

  public select_mode: any = '';
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public _globalsearchservice: GlobalsearchProvider,
    public sync: SyncProvider,
    public menu: MenuController,
    public _orderlistservice: OrderdetailProvider,
    private oneSignal: OneSignal
  ) {
  }

  ionViewDidLoad() {
    this.menu.enable(false);
  }
  ionViewWillEnter() {
    this.sync.GetLocalData();
  }
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: '<h1>Confirm Purchase</h1>',
      message: 'Enter Name',
      inputs: [
        {
          name: 'username',
          placeholder: 'Username'
        },
        {
          name: 'password',
          placeholder: 'Password',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Buy',
          handler: () => {
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }

  CreateSalesInvoice() {
    this.navCtrl.setRoot('mSales');
  }

  AddNewCustomer() {
    let contactModal = this.modalCtrl.create('AddcustomerPage');
    contactModal.present();
  }

  initializeItems(inputData) {

    this.sync.globalSearch(inputData).subscribe(data => {
      this.searchItem = data.data.Party_Head_MsT
    })
  }
  onInput(ev: any) {
    if (this.global_search == 'customer_search') {
      let search = ev.target.value;
      if (search.length >= 3) {
        //this.initializeItems(search);
        this.sync.globalSearch(search).subscribe(data => {
          this.searchItem = data.data.Party_Head_MsT

          if (search && search.trim() != '' && this.searchItem) {
            this.searchItem = this.searchItem.filter((item) => {
              return (item.party_Name.toLowerCase().indexOf(search.toLowerCase()) > -1);
            })
          }
          else {
            let toast = this.toastCtrl.create({
              message: 'No customer found!',
              duration: 3000,
              position: 'top'
            });
            toast.present();
          }
        })
      }
    }
    else {
      this.searchItem = [];
    }
  }


  gotoJobcart(ev: any) {
    //  //debugger
    if (this.global_search == 'invoice_search') {
      this._orderlistservice.getJobcardDetail(ev.target.value).subscribe(
        result => {
          if (result.message.type == 'SUCCESS') {
            this.navCtrl.setRoot('OrderdetailsPage', { data: ev.target.value });
          }
          else {
            let toast = this.toastCtrl.create({
              message: 'No jobcard found!',
              duration: 3000,
              position: 'top'
            });
            toast.present();
          }
        });
    }
  }
  openTransactionDashboard() {
    this.navCtrl.setRoot('TransactionDashboardPage', {
      data: 'Sales'
    });
  }

  moveToDashboard(party_Name) {
    this.navCtrl.setRoot('mDashboardDetails', { data: party_Name });
  }

  CreateJobCard() {
    this.navCtrl.setRoot('mJobCard', { customer: null });
  }

}
