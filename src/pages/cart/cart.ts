import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  constructor(public navCtrl: NavController,public viewCtrl: ViewController, public navParams: NavParams) {
  }
  closeCart(){
    this.viewCtrl.dismiss();
  }
  arrayOne(n: number): any[] {
    return Array(n);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
  }

}
