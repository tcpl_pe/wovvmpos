import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ModalController, LoadingController } from 'ionic-angular';
import { SyncProvider } from '../../providers/sync/sync';
import { LoadingProvider } from '../../providers/loading/loading';
import { MycartProvider } from '../../providers/mycart/mycart';
import { SalesPage } from '../home/home';
import { mSales } from '../salesinvoice/salesinvoice';

/**
 * Generated class for the DynamicmenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dynamicmenu',
  templateUrl: 'dynamicmenu.html',
})
export class DynamicmenuPage {

  public menuBlocks: any = [];
  public wovvmenuData: any;
  public mainMenuBLockUrls: any = [];
  public mainMenuBLockUrls1: any = [];
  public mainMenuBLockUrls2: any = [];
  public mainMenuBLockUrls3: any = [];
  public mainMenuBLockUrls4: any = [];
  public mainMenuBLockUrls5: any = [];
  public wovvsystemView: any;
  public UserRoleId: any;
  public menuwithNull: any = [];
  public mainMenuBLock: any = [];
  public mainMenuBLock1: any = [];
  public mainMenuBLock2: any = [];
  public mainMenuBLock3: any = [];
  public mainMenuBLock4: any = [];
  public mb_Codes: any = [];
  public mainMenuSubBLock1: any = [];
  public mainMenuSubBLock2: any = [];
  public mainMenuSubBLock3: any = [];
  public mainMenuSubBLock4: any = [];
  public mainMenuSubBLock5: any = [];
  public mainMenuSubBLock6: any = [];
  public mainMenuSubBLock7: any = [];
  public isLoggedIn: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loading: LoadingProvider,
    public menu: MenuController, public sync: SyncProvider, public modalCtrl: ModalController,
    public loadingCtrl: LoadingController, public mycat: MycartProvider) {

    this.menu.enable(true);
  }
  ionViewDidLoad() {
    // this.menu.enable(true);
    this.FilterMenuData();
  }

  redirectToUrl(data) {
    if (data.ourl_URL_Desc == 'mDashboardDetails') {
      data.ourl_URL_Desc = 'TransactionDashboardPage';
    }
    else if (data.ourl_URL_Desc == 'mWebPanel') {
      this.OpenWebPanel();
    }
    else if (data.ourl_URL_Desc == 'mCustomer') {
      let contactModal = this.modalCtrl.create('AddcustomerPage');
      contactModal.present();
    }
    else if (data.ourl_URL_Desc == 'mSetting') {
      this.loading.presentToast("Coming Soon !!!")
    }
    else if (data.ourl_URL_Desc == 'mIncSync') {
      this.IncrementalCall();
    }
    else if (data.ourl_URL_Desc == 'mSalesSearch') {
      this.navCtrl.setRoot('HomePage')
    }
    else if (data.ourl_URL_Desc == 'mJobCardSearch') {
      this.navCtrl.setRoot('JobsearchPage')
    }
    else if (data.ourl_URL_Desc == 'mSyncLog') {
      this.navCtrl.setRoot('SyncLogPage')
    }
    else if (data.ourl_URL_Desc == 'mUpload') {
      this.sync.try_sync();
    }
    else if (data.ourl_URL_Desc == 'mSync') {
      this.sync.GetServerData();
    }
    else if (data.ourl_URL_Desc == 'mSales') {
      this.navCtrl.setRoot(mSales);
    }
    else {
      this.navCtrl.setRoot(SalesPage);
    }
  }

  FilterMenuData() {

    this.wovvmenuData = this.sync.wovvMenu;
    for (var i = 0; i < this.wovvmenuData.length; i++) {
      if ((this.wovvmenuData[i]['mb_Parent_Code'] == null)) {

        this.menuwithNull.push(this.wovvmenuData[i]);
      }
    }

    this.wovvsystemView = this.sync.wovvSystem;
    if (this.wovvsystemView != '' && this.wovvsystemView != undefined)
    {
      this.UserRoleId = this.wovvsystemView[0]['user_Role_Id'];
    }
    //*****************************MENU BLOCK CREATION********************************//

    var that = this;

    this.mainMenuBLock = this.menuwithNull.filter(function (number) {
      return number['role_Id'] == that.UserRoleId;
    });
    this.mainMenuBLock1 = this.mainMenuBLock.filter(function (number) {
      return number['mb_Has_Sub_Mb_Page'] == 1;
    });
    this.mainMenuBLock2 = this.mainMenuBLock1.filter(function (number) {
      return number['mb_Menu_Code'] == 2;
    });
    this.mainMenuBLock3 = this.mainMenuBLock2.filter(function (number) {
      return number['ourl_Status'] == 1;
    });
    this.mainMenuBLock4 = this.mainMenuBLock3.filter(function (number) {
      return number['is_System_Default'] == 1;
    });
    var unique = {};
    this.mainMenuBLock4.forEach(function (x) {
      if (!unique[x['mb_Desc']]) {
        that.menuBlocks.push(x);
        unique[x['mb_Desc']] = true;
      }
    });

    //************************************************************************************//
    //*****************************SUB-MENU BLOCK CREATION********************************//
    for (var j = 0; j < this.menuBlocks.length; j++) {
      this.mb_Codes.push(this.menuBlocks[j].mb_Code);
    };
    this.mainMenuSubBLock1 = this.wovvmenuData.filter(function (number) {
      return number['mb_Is_Sub_Menu'] == 1;
    });
    this.mainMenuSubBLock2 = this.mainMenuSubBLock1.filter(function (number) {
      return number['role_Id'] == that.UserRoleId;
    });
    this.mainMenuSubBLock3 = this.mainMenuSubBLock2.filter(function (number) {
      return number['is_Sub_URL'] == 0;
    });
    this.mainMenuSubBLock4 = this.mainMenuSubBLock3.filter(function (number) {
      return number['ourl_WEB'] == 0;
    });
    this.mainMenuSubBLock5 = this.mainMenuSubBLock4.filter(function (number) {
      return number['ourl_Menu_Access'] == 1;
    });
    this.mainMenuSubBLock6 = this.mainMenuSubBLock5.filter(function (number) {
      return number['is_System_Default'] == 1;
    });


    var unique = {};
    this.mainMenuSubBLock6.forEach(function (x) {
      if (!unique[x['ourl_URL_Desc']]) {
        that.mainMenuSubBLock7.push(x);
        unique[x['ourl_URL_Desc']] = true;
      }
    });
    for (var i = 0; i < this.mb_Codes.length; i++) {
      var subMenuBLK = [];
      for (var j = 0; j < this.mainMenuSubBLock7.length; j++) {
        if (this.mb_Codes[i] == this.mainMenuSubBLock7[j].mb_Parent_Code) {
          subMenuBLK.push(this.mainMenuSubBLock7[j]);
        }
      };
      this.menuBlocks[i]['SubmenuBlocks'] = subMenuBLK;
    };
    //console.log(this.menuBlocks);

    //************************************************************************************//
    //**********************************MAIN MENU URL*************************************//
    this.mainMenuBLockUrls = this.wovvmenuData.filter(function (number) {
      return number['ourl_Menu_Access'] == 1;
    });
    // $scope.mainMenuBLockUrls = $filter('filter')($scope.wovmenu, {
    //   'ourl_Menu_Access': 1
    // }, true);
    this.mainMenuBLockUrls1 = this.mainMenuBLockUrls.filter(function (number) {
      return number['role_Id'] == that.UserRoleId;
    });
    // $scope.mainMenuBLockUrls1 = $filter('filter')($scope.mainMenuBLockUrls, {
    //   'role_Id': $scope.UserRoleId
    // }, true);
    this.mainMenuBLockUrls2 = this.mainMenuBLockUrls1.filter(function (number) {
      return number['is_Sub_URL'] == 0;
    });
    // $scope.mainMenuBLockUrls2 = $filter('filter')($scope.mainMenuBLockUrls1, {
    //   'is_Sub_URL': 0
    // }, true);
    this.mainMenuBLockUrls3 = this.mainMenuBLockUrls2.filter(function (number) {
      return number['ourl_WEB'] == 0;
    });
    // $scope.mainMenuBLockUrls3 = $filter('filter')($scope.mainMenuBLockUrls2, {
    //   'ourl_WEB': 1
    // }, true);
    this.mainMenuBLockUrls4 = this.mainMenuBLockUrls3.filter(function (number) {
      return number['is_System_Default'] == 1;
    });
    // $scope.mainMenuBLockUrls4 = $filter('filter')($scope.mainMenuBLockUrls3, {
    //   'is_System_Default': 1
    // }, true);
    var unique = {};
    this.mainMenuBLockUrls4.forEach(function (x) {
      if (!unique[x['ourl_URL_Desc']]) {
        that.mainMenuBLockUrls5.push(x);
        unique[x['ourl_URL_Desc']] = true;
      }
    });
    //$scope.mainMenuBLockUrls5 = $filter('unique')($scope.mainMenuBLockUrls4, 'ourl_URL_Desc');
    for (var k = 0; k < this.mb_Codes.length; k++) {
      var subMenuUrlBLK = [];
      for (var l = 0; l < this.mainMenuBLockUrls5.length; l++) {
        if (this.mb_Codes[k] == this.mainMenuBLockUrls5[l].mb_Code) {
          subMenuUrlBLK.push(this.mainMenuBLockUrls5[l]);
        }
      };
      this.menuBlocks[k]['Submenu'] = subMenuUrlBLK;
    };
    this.menuBlocks.sort(function (a, b) {
      return a.mb_Order - b.mb_Order
    })
  }

  IncrementalCall() {
    let loading = this.loadingCtrl.create({
      content: 'Syncing In Progress...'
    });
    loading.present();
    this.sync.IncrementalCall().then(() => {
      loading.dismiss();
    });
  }

  OpenWebPanel() {
    this.sync.brow();
  }
  openCustomer() {
    let CustomerModal = this.modalCtrl.create('ProfilePage');
    CustomerModal.present();
  }
}
