import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DynamicmenuPage } from './dynamicmenu';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    DynamicmenuPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(DynamicmenuPage),
  ],
})
export class DynamicmenuPageModule { }
