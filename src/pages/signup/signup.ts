import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, MenuController, ModalController, ToastController } from 'ionic-angular';
import { SyncProvider } from '../../providers/sync/sync';
import { Device } from '@ionic-native/device';
import { DbProvider } from '../../providers/db/db';
import { Storage } from '@ionic/storage';
import { Http, Headers } from '@angular/http';
import { MenuserviceProvider } from '../../providers/menuservice/menuservice';
import { LoadingProvider } from '../../providers/loading/loading';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  login: any = { url: 'https://10.10.10.227:8443', username: '', password: '' };
  post: {};
  uniqueWovvUsers: any = [];
  wovvsystem: any;

  constructor(
    public navCtrl: NavController, 
    public sync: SyncProvider, 
    public loadingCtrl: LoadingController, 
    private device: Device,
    public db: DbProvider, 
    public storage: Storage, 
    public menu: MenuController, 
    public http: Http, 
    public toastCtrl: ToastController, 
    public modalCtrl: ModalController,
    public _menuServiceProvider: MenuserviceProvider, 
    public loading: LoadingProvider) { }

  ionViewDidEnter() {
    this.login.url = this.sync.URL;
    this.login.username = this.sync.Domain;
    this.login.password = this.sync.OrderId;
    this.menu.enable(false);
    this.db.checkDrltoAngular([]);
  }

  Activate() {
    if (this.login.url == '') {
      this.loading.presentToast("Url cannot be left blank!");
      return;
    }
    else if (this.login.username == '') {
      this.loading.presentToast("Domain name cannot be left blank!");
      return;
    }
    else if (this.login.password == '') {
      this.loading.presentToast("Order ID cannot be left blank!");
      return;
    }
    else {
      this.sync.siteUrl = this.login.url;
      this.sync.Domain = this.login.username;
      this.sync.OrderId = this.login.password;
      this.storage.set("sideMenuDetails", JSON.stringify({ URL: this.sync.URL, Domain: this.sync.Domain, OrderId: this.sync.OrderId }));
      this.storage.set("siteUrl", this.sync.siteUrl);
      this.sync.url = this.sync.siteUrl + this.sync.endpoint;
      this.storage.set("url", this.sync.url);

      let headers = new Headers();
      headers.append('domainName', this.login.username);
      this.post = {
        "entityName": "Register_MsT",
        "destEntityName": "Party_SubDomain",
        "action": "payload",
        "event": "activation",
        "pageUrl": "Register",
        "formData": {
          "app_OrderID": this.login.password,
          "reg_MacID": "zzzz"
        }
      };

      this.http.post(this.sync.url, this.post, { headers: headers }).map(res => res.json())
        .subscribe(data => {
          if (data.message.code == 1001) {
            this.loading.presentToast("Please enter proper Domain Name and Order ID!");
          }
          else if (data.message.code == 4006) {
            this.loading.presentToast("Oops, Either Order ID is invalid or Device is already Activated!");
          }
          else if (data.message.code == 6002) {
            this.loading.presentToast("Oops, Order ID is already Activated!");
          }
          else {
            if (data.data.Register_MsT && data.message.regMacId) {

              this.sync.token = data.message.responseToken;
              let splitTenantId = data.message.regMacId.split('_');
              this.sync.tenantId = Number(splitTenantId[1]);
              this.sync._tenantId = data.message.regMacId;
              this.sync.registerCode = data.data.Register_MsT[0].id;
              this.sync.branchCode = data.data.Register_MsT[0].branch_Code;
              this.sync.stockLocCode = data.data.Register_MsT[0].stock_Loc_Code;
              this.sync.divCode = data.data.Register_MsT[0].div_Code;
              this.sync.orgCode = data.data.Register_MsT[0].org_Code.id;

              this.storage.set("IsActivated", true);
              this.storage.set("token", this.sync.token);
              this.storage.set("tenantId", this.sync.tenantId);
              this.storage.set("_tenantId", this.sync._tenantId);
              this.storage.set("registerCode", this.sync.registerCode);
              this.storage.set("orgCode", this.sync.orgCode);
              this.storage.set("divCode", this.sync.divCode);
              this.storage.set("branchCode", this.sync.branchCode);
              this.storage.set("stockLocCode", this.sync.stockLocCode);

              this.navCtrl.setRoot('SyncdataPage');
            }
            else {
              this.loading.presentToast("Something went wrong.Please Contact Administration.");
            }
          }
        }, err => {
          this.loading.presentToast("Please Enter Valid Domain");
        });
    }
  }
}
