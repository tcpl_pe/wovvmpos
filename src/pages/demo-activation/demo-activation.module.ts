import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DemoActivationPage } from './demo-activation';
import { ComponentsModule } from '../../components/components.module';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    DemoActivationPage,
  ],
  imports: [
    ComponentsModule,
    HttpModule,
    IonicPageModule.forChild(DemoActivationPage),
  ],
})
export class DemoActivationPageModule {}
