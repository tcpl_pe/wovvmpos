import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Component } from '@angular/core';
import 'rxjs/Rx';
import { Observable } from "rxjs";
import { NavController, NavParams, IonicPage, LoadingController } from 'ionic-angular';
import { SyncProvider } from '../../providers/sync/sync';
import { LoginserviceProvider } from '../../providers/loginservice/loginservice';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the DemoActivationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-demo-activation',
  templateUrl: 'demo-activation.html',
})
export class DemoActivationPage {

  uniqueWovvUsers: any = [];
  wovvsystem: any;

  constructor(
    public http: HttpClient, 
    public navCtrl: NavController, 
    public loadingCtrl: LoadingController,
    public storage: Storage, 
    public navParams: NavParams, 
    public sync: SyncProvider,
    public Loginservice: LoginserviceProvider) { }

  Activation() {
    this.navCtrl.setRoot('SignupPage');
  }

  DemoActivation() {
    let loading = this.loadingCtrl.create({ content: 'Please wait..' });
    loading.present();
    this.storage.set("siteUrl", this.sync.demoURL);
    this.sync.url = this.sync.demoURL + this.sync.endpoint;
    this.storage.set("url", this.sync.url);

    this.Loginservice.DemoActivation().subscribe(data => {

      loading.dismiss();

      localStorage.setItem('isDemo', 'Y');
      localStorage.setItem('isNew', 'N');

      this.sync.token = data.message.responseToken;
      let splitTenantId = data.message.regMacId.split('_');
      this.sync.tenantId = Number(splitTenantId[1]);
      this.sync._tenantId = data.message.regMacId;
      this.sync.registerCode = data.data.Register_MsT[0].Id;
      this.sync.branchCode = data.data.Register_MsT[0].branch_Code;
      this.sync.stockLocCode = data.data.Register_MsT[0].stock_Loc_Code;
      this.sync.divCode = data.data.Register_MsT[0].div_Code;
      this.sync.orgCode = data.data.Register_MsT[0].org_Code_Id;

      this.storage.set("IsActivated", true);
      this.storage.set("token", this.sync.token);
      this.storage.set("tenantId", this.sync.tenantId);
      this.storage.set("_tenantId", this.sync._tenantId);
      this.storage.set("registerCode", this.sync.registerCode);
      this.storage.set("orgCode", this.sync.orgCode);
      this.storage.set("divCode", this.sync.divCode);
      this.storage.set("branchCode", this.sync.branchCode);
      this.storage.set("stockLocCode", this.sync.stockLocCode);

      this.navCtrl.setRoot('SyncdataPage');
    });
  }

  // GetCredentials() {
  //   //this.sync.GetLocalData();
  //   let loading = this.loadingCtrl.create({
  //     content: 'Please wait..'
  //   });
  //   loading.present();
  //   var unique = {};
  //   var that = this;
  //   this.wovvsystem = JSON.parse(localStorage.getItem('WovvSystem'));
  //   this.wovvsystem.forEach(function (x) {
  //     if (!unique[x['user_Id']]) {
  //       that.uniqueWovvUsers.push(x);
  //       unique[x['user_Id']] = true;
  //     }
  //   });
  //   let whereClause = '';
  //   for (var userIndex = 0; userIndex < this.uniqueWovvUsers.length; userIndex++) {
  //     let value = this.uniqueWovvUsers[userIndex]['user_Id'];
  //     whereClause += value + ',';
  //   }
  //   whereClause = whereClause.substring(0, whereClause.length - 1);
  //   this.Loginservice.DemoWovvUsrMst(whereClause).subscribe(result => {
  //     loading.dismiss();
  //     this._storage.set("LoginData", result["data"].WovV_User_MsT).then(() => {
  //       this.navCtrl.setRoot('SyncdataPage');
  //     });
  //   });
  // }
  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad DemoActivationPage');
  // }
}