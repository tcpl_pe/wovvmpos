import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams, ModalController, MenuController } from 'ionic-angular';
import { CartPage } from '../cart/cart';
import { MycartProvider } from "../../providers/mycart/mycart";
import { Storage } from '@ionic/storage';
import { LoadingProvider } from '../../providers/loading/loading';

/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {

  currencySymbol: any;
  ttl: any = 0;
  dif: any = 0;
  value: any = "";
  count: number = 0;
  paymentTypeShow: any = [];
  get_partyrpt: any = [];
  party_details: any = {};

  constructor(public viewCtrl: ViewController,
    public menu: MenuController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public mycat: MycartProvider,
    public storage: Storage,
    public loading: LoadingProvider) {
      this.menu.enable(true);
  }
  closePayment() {
    this.viewCtrl.dismiss();
  }
  arrayOne(n: number): any[] {
    return Array(n);
  }
  openModel(value) {
    let cartModal = this.modalCtrl.create(CartPage);
    cartModal.present();
  }
  ionViewDidLoad() {
    this.mycat.GetDocNumber(1);
    var currentUserData = JSON.parse(localStorage.getItem('currentUser'));
    this.currencySymbol = currentUserData.branch_Currency_Symbol;
    this.count = this.mycat.summaryViewData.length;
    this.paymentTypeShow = this.navParams.get('paymentData');
  }
  getAmt() {
    this.ttl = 0;
    for (var i = 0; i < this.mycat.owt_Pay_Hdr_Code.length; i++) {
      this.ttl += (+this.mycat.owt_Pay_Hdr_Code[i].opay_Paid_VAl);

    }
    this.ttl = this.ttl.toFixed(2);
    return this.ttl;
  }
  diff() {
    if (this.mycat.drl_obj[0]) {
      // console.log("obj is created");
      this.dif = this.mycat.drl_obj[0].oh_Doc_Net_Amt - this.ttl;
      this.dif = this.dif.toFixed(2);
    }
    return this.dif;
  }
  exact() {
    this.value = this.diff();
  }
  addTyp(no: any, typeName: any, pay_Mode_Code: any, pay_Mode_Name: any, pay_Typ_Name: any) {
    if (this.value == "" || this.value == "0") {
      this.loading.presentToast("Please Enter Amount");
      return;
    }
    console.log('id_for_click', no, 'cash for click', typeName);
    if (this.diff() > 0) {
      console.log("check : " + this.value, no, null, null, pay_Mode_Code, pay_Mode_Name, pay_Typ_Name);

      this.mycat.quick_cash(this.value, no, null, null, pay_Mode_Code, pay_Mode_Name, pay_Typ_Name);
    }
    else {
      this.loading.presentToast("You cannot add more money if difference is < 0.");
    }
    this.value = "";
    console.log(typeName);

  }
  getVal() {
    return (this.diff() * -1);
  }
  save() {
    if (this.diff() <= 0) {
      this.mycat.drl_obj[0].oh_Change_Given_Amt = this.getVal();
      this.mycat.drl_obj[0].deliveryType_Head_Code = this.mycat.selected_party_delveryType;
      // alert("clicked");
      this.mycat.complete(1);
      // this.navCtrl.setRoot(DynamicmenuPage)
      // this.ClearData();
      this.loading.presentToast("Invoice saved sucessfully.");
      this.viewCtrl.dismiss();
      return true;

    }
    else {
      //alert("Amount still due");
      this.loading.presentToast("Amount still due.");
      return false;
    }

  }
}
