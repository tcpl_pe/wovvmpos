import { Component } from '@angular/core';
import { ModalController, NavController, AlertController, MenuController, NavParams,PopoverController } from 'ionic-angular';
import { PaymentPage } from '../payment/payment';
import { CartPage } from '../cart/cart';
import { EditPage } from '../edit/edit';
import { BarcodePage } from '../barcode/barcode';
import { CustomerPage } from '../customer/customer';
import { ProfilePage } from '../profile/profile';
import { SyncProvider } from '../../providers/sync/sync';
import { MycartProvider } from '../../providers/mycart/mycart';
import * as _ from 'underscore';
import { LoadingProvider } from '../../providers/loading/loading';
import { Storage } from '@ionic/storage';
import { FilterPage } from '../filter/filter';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class SalesPage {
  
  currencySymbol: any;
  isOnline: boolean = false;
  product_stock: any = 0;
  public party_Name: any;
  party_details: any = {};
  customerName: any = "";
  deliveryTypeName: any;
  paymentTypeShow: any = [];
  get_partyrpt: any = [];
  count: number = 0;

  constructor(public navCtrl: NavController,
    public menu: MenuController,
    private alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public sync: SyncProvider,
    public mycat: MycartProvider,
    public navParams: NavParams,
    public loading: LoadingProvider,
    public storage: Storage,
    public popoverCtrl:PopoverController) {
      
    this.menu.enable(true);
    this.sync.GetLocalData();
  }
  payement() {
    let paymentModal = this.modalCtrl.create(PaymentPage, {
      paymentData: this.paymentTypeShow
    });
    paymentModal.onDidDismiss(data => {
      this.count=this.mycat.summaryViewData.length;
    });
    paymentModal.present();
  }
  // openCart() {
  //   let cartModal = this.modalCtrl.create(CartPage);
  //   cartModal.present();
  // }
  arrayOne(n: number): any[] {
    return Array(n);
  }
  openBarcode() {
    let barcodeModal = this.modalCtrl.create(BarcodePage);
    barcodeModal.present();
  }
  openCustomer() {
    let CustomerModal = this.modalCtrl.create('ProfilePage');
    CustomerModal.present();
  }
  openEdit(item: any, i: number) {
    let cartModal = this.modalCtrl.create(EditPage,{
      item:item,
      index:i
    });
    cartModal.present();
  }
  openFilter(myEvent) {
    let popover = this.popoverCtrl.create('FilterPage');
    popover.present({
      ev: myEvent
    });
  }
  swipeLeft(event: any): any {
    let alert = this.alertCtrl.create({
      title: 'Product Name',
      subTitle: 'Remove Quantity -1',
      buttons: ['Dismiss']
    });
    alert.present();
  }
  swipeRight(event: any): any {
    let alert = this.alertCtrl.create({
      title: 'Product Name',
      subTitle: 'Add Quantity +1',
      buttons: ['Dismiss']
    });
    alert.present();
  }
  swipeUp(event: any): any {
    this.payement();
  }
  ionViewDidLoad() {
    let obj;
    var currentUserData = JSON.parse(localStorage.getItem('currentUser'));
    this.currencySymbol = currentUserData.branch_Currency_Symbol;
    var isLoggedIn = localStorage.getItem('isLoggedIn');
    if (isLoggedIn == 'yes') {
      this.isOnline = true;
    }
    this.product_stock = 0;
    this.mycat.summaryViewData = [];
    this.count=this.mycat.summaryViewData.length;
    this.party_Name = this.navParams.get('customer');
    if (this.party_Name != null) {
      obj = _.findWhere(this.sync.partyData, { party_Name: this.party_Name });
    }
    else {
      obj = _.findWhere(this.sync.partyData, { party_Name: 'Unregistered Sales' });
    }
    if (obj != null) {
      this.SelectCustomer(obj);
    }
  }
  quickPay(num) {
    //if (this.ShowSplitPayment) {
    this.mycat.add_amt(num, 1);
    // this.ShowPartyFilter = true;
    // this.ShowProduct = false;
    // this.ShowProductFilter = true;
    // this.ShowProductEdit = true;
    // this.ShowPartyDetails = true;
    //this.ShowSplitPayment = true;
    //this.ShowScanner = true;
    //this.customerName = "";
    // this.mycat.GetCatalogData(1);
    //this.mycat.init_obj();
  }

  SelectProduct(prodID: any) {
    let prodData = _.findWhere(this.mycat.catalog, { prod_Code_Id: prodID.values[0] });
    this.mycat.p_click(prodData, 1);
    // this.ShowProductFilter = true;
    // this.ShowProduct = false;
  }

  getQuantity(item) {
    let quant = 0
    let prodData = _.findWhere(this.mycat.catalog, { prod_Code_Id: item.prod_Code_Id });
    if (prodData) {
      //let qty = _.findWhere(this.mycat.owt_dtl_code, { od_Prod_Code: prodData.prod_Code_Id });
      let qty = this.mycat.owt_dtl_code.filter(function (number) {
        return number['od_Prod_Code'] == prodData.prod_Code_Id;
      });
      if (qty) {
        ////
        for (let index = 0; index < qty.length; index++) {
          quant += qty[index]['od_Qty'];

        }
        return quant;
      }
    }
  }

  SelectCustomer(item: any) {

    // this.ShowPartyFilter = true;
    // this.ShowProduct = false;
    // this.ShowProductFilter = true;
    // this.ShowProductEdit = true;
    // this.ShowPartyDetails = true;
    // this.ShowSplitPayment = true;
    // this.ShowScanner = true;

    let obj = _.findWhere(this.sync.partyData, { party_Name: item.party_Name });
    this.mycat.cus_obj = obj;
    this.party_details = obj;
    this.customerName = obj.party_Name;
    this.mycat.selected_party_delveryType = obj.party_Delivery_Type_Id;
    let defDelivery = _.findWhere(this.sync.deliveryTypeData, { Id: obj.party_Delivery_Type_Id });
    this.deliveryTypeName = defDelivery.deliveryType_Name;
    this.mycat.defDeliveryType = [];
    this.mycat.defDeliveryType.push(defDelivery);
    if (this.customerName == "Unregistered Sales") {
      this.customerName = "";
    }
    this.mycat.UpdateParty(obj, 1);
    this.mycat.Enter(1);
  }
  checkOtherItem(item) {
    if (item['prod_TypCls_Prefix'] == 1000) {
      let sendItem = JSON.parse(JSON.stringify(item));
      let alert = this.alertCtrl.create({
        title: sendItem["prod_Name_Desc"],
        inputs: [
          {
            name: 'Description',
            placeholder: 'Description',
            value: sendItem["prod_Name_Desc"]
          },
          {
            name: 'Rate',
            placeholder: 'Rate',
            value: sendItem["prod_Price"]
          },
          {
            name: 'Quantity',
            placeholder: 'Quantity',
            value: 1
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: data => {
              //console.log('Cancel clicked');
            }
          },
          {
            text: 'Add to cart',
            handler: data => {
              if (data.Description, data.Rate, data.Quantity) {
                sendItem["prod_Name_Desc"] = data.Description;
                sendItem["prod_Price"] = data.Rate;
                sendItem['Quantity'] = data.Quantity;
                this.mycat.p_click(sendItem, 1);
                this.count = this.mycat.summaryViewData.length;
              } else {
                this.loading.presentToast("Please Fill All the fields.")
                return false;
              }
            }
          }
        ]
      });
      alert.present();
    }
    else {
      this.mycat.p_click(item, 1);
      this.count = this.mycat.summaryViewData.length;
    }
  }

  b2() {
    if (this.mycat.owt_dtl_code.length > 0) {
      this.paymentTypeShow = [];
      this.storage.get("Party").then((data) => {
        this.get_partyrpt = data

        let pts = [];
        if (this.get_partyrpt) {
          this.get_partyrpt.forEach(element => {
            if (this.party_details && this.party_details.party_Code_Id === element.party_Code_Id) {
              pts.push(element)
            }
          });
          var flags = [], l = pts.length, i;
          for (i = 0; i < l; i++) {
            if (flags[pts[i].pay_Typ_Name]) continue;
            flags[pts[i].pay_Typ_Name] = true;
            this.paymentTypeShow.push(pts[i]);
          }
        }
        this.payement();
      })
    }
  }
}
