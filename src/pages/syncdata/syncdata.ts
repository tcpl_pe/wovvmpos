import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MenuserviceProvider } from '../../providers/menuservice/menuservice';
import { SyncProvider } from '../../providers/sync/sync';

@IonicPage()
@Component({
  selector: 'page-syncdata',
  templateUrl: 'syncdata.html',
})

export class SyncdataPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public sync: SyncProvider,
    public menuServiceProvider: MenuserviceProvider) { }

  ionViewWillEnter() {
    this.SyncData();
  }

  SyncData() {
    this.sync.totalCalls = 10;
    this.sync.completedCalls = 0;
    this.sync.completedPer = 0;
    this.sync.GetServerData();
    this.sync.DocNumber();
    this.sync.FetrOption();
  }

  GoToLogin() {
    this.navCtrl.setRoot('LoginPage');
  }
}
