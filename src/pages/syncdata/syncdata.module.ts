import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SyncdataPage } from './syncdata';

@NgModule({
  declarations: [
    SyncdataPage,
  ],
  imports: [
    IonicPageModule.forChild(SyncdataPage),
  ],
})
export class SyncdataPageModule {}
