import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { MycartProvider } from '../../providers/mycart/mycart';
import * as _ from 'underscore';
import { LoadingProvider } from '../../providers/loading/loading';
/**
 * Generated class for the EditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit',
  templateUrl: 'edit.html',
})
export class EditPage {

  value: any = "";
  index: any;
  item: any;
  product_stock: any = 0;
  @ViewChild('AmountInput') AmountInput;

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public mycat: MycartProvider,
    public loading: LoadingProvider,
    private alertCtrl: AlertController
  ) {

    this.item = this.navParams.get('item');
    this.index = this.navParams.get('index');
  }
  closeEdit() {
    this.viewCtrl.dismiss();
  }
  ionViewDidLoad() {
    this.product_stock = 0;
    var stk;
    let obj = _.findWhere(this.mycat.cats, { prod_Name_Desc: this.item.prod_Name_Desc });
    this.mycat.curr_obj = obj;
    if (this.mycat.stockbook != '') {
      stk = this.mycat.stockbook.filter(function (number) {
        return number['psd_Prod_Code_Id'] == obj.prod_Code_Id;
      });
    }
    if (stk != '' && stk) {
      if (this.mycat.curr_obj.prod_TypCls_Name == "Standard") {
        for (let index = 0; index < stk.length; index++) {
          this.product_stock += stk[index]['psd_Available_Qty'];
        }
      }
    }
    else {
      this.product_stock = 0;
    }
  }

  rate() {
    if (this.value == "" || this.value == 0) {
      this.loading.presentToast("Please enter price.");
      return;
    }
    else if (this.value > this.mycat.curr_obj.prod_MRP) {
      this.loading.presentToast("Cannot sell above maximum retail price.");
      return;
    }
    else {
      this.mycat.rate(this.value);
      this.value = "";
      this.viewCtrl.dismiss();
    }
  }

  removeProductFromCart() {
    let alert = this.alertCtrl.create({
      title: 'Remove Item',
      message: 'Do you want remove this item from cart?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'Yes',
          handler: () => {
            let obj = _.findWhere(this.mycat.owt_dtl_code, { od_Prod_Name: this.item.prod_Name_Desc })
            let index = this.mycat.owt_dtl_code.indexOf(obj);

            this.mycat.owt_dtl_code.splice(index, 1);
            this.mycat.summaryViewData.splice(index, 1);
            this.mycat.forVoid();
            this.mycat.drl();
            this.viewCtrl.dismiss();
          }
        }
      ]
    });
    alert.present();
  }
  changeQuantity(num) {
    this.mycat.bulk(this.value, num);
    this.value = '';
    this.viewCtrl.dismiss();
  }
  item_disc() {
    if (this.value == "") {
      this.loading.presentToast("Please Enter Valid Product Discount");
      setTimeout(() => {
        this.AmountInput.setFocus();
      }, 1000);
      return;
    }
    if (this.mycat.item_disc(this.value)) {
      this.value = "";
    }
    else {
      setTimeout(() => {
        this.AmountInput.setFocus();
      }, 1000);
    }
    this.viewCtrl.dismiss();
  }
  doc_disc() {
    if (this.value == "") {
      this.loading.presentToast("Please Enter Valid Document Discount");
      return;
    }
    this.mycat.doc_disc(this.value);
    this.value = "";
    this.viewCtrl.dismiss();
  }
}
