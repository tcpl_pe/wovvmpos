import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController, MenuController, ModalController } from 'ionic-angular';
import { MycartProvider } from '../../providers/mycart/mycart';
import { DbProvider } from '../../providers/db/db';
import { SyncProvider } from '../../providers/sync/sync';
import { UserData } from '../../providers/user-data';
import { Http, Headers } from '@angular/http';
import { NgForm } from '@angular/forms';
import { MenuserviceProvider } from '../../providers/menuservice/menuservice';
import * as _ from 'underscore';
import CryptoJS from "crypto-js";
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { SalesPage } from '../home/home';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  login: any = { username: '', password: '' };
  submitted = false;
  wovvsystem: any;
  device_reg: any;
  id: any;
  lgn: any = {};
  FetrOptionData: any;
  urlRoleTypeData: any;
  wovvmenuData: any;
  userLoginData: any;
  UserID: any;
  constructor(
    public _menuServiceProvider: MenuserviceProvider, 
    public mycat: MycartProvider, 
    public toastCtrl: ToastController,
    public db: DbProvider, 
    public alertCtrl: AlertController, 
    public storage: Storage, 
    public sync: SyncProvider,
    public loadingCtrl: LoadingController, 
    public menu: MenuController, 
    public modalCtrl: ModalController,
    public navCtrl: NavController, 
    public userData: UserData, 
    public http: Http
    ) {

    var isLoggedIn = localStorage.getItem('isLoggedIn');
    if (isLoggedIn == 'yes') {
      this.navCtrl.setRoot('DynamicmenuPage');
    }
  }

  ionViewWillEnter() {
    this.menu.enable(true);
    this.sync.GetLocalData();
  }

  ionViewWillLeave() {
    // this.menu.enable(false);
  }

  fetrOption() {
    this._menuServiceProvider.getFetrOptionDetail().subscribe(
      r => {
        this.FetrOptionData = r.data.Fetr_Option_Dtl;
        localStorage.setItem('FetrOptionData', JSON.stringify(this.FetrOptionData));
      });
  }

  wovvmenu() {
    this._menuServiceProvider.wovvmenu().subscribe(
      r => {
        this.wovvmenuData = r.data.Wovvmenu;
        localStorage.setItem('wovvmenu', JSON.stringify(this.wovvmenuData));
      }
    );
  }

  Login(form: NgForm) {
    this.submitted = true;
    if (form.valid) {
      const headers = new Headers();
      headers.append('Content-Type', 'application/json');
      var key = CryptoJS.enc.Base64.parse('2b7e151628aed2a6abf7158809cf4f3c');
      var iv = CryptoJS.enc.Base64.parse('3ad77bb40d7a3660a89ecaf32466ef97');
      var encrypted = CryptoJS.AES.encrypt(this.login.password, key, { iv: iv, });
      encrypted = encrypted.ciphertext.toString(CryptoJS.enc.Base64)

      this.sync.user = _.findWhere(this.sync.wovvUser, { user_Login_ID: this.login.username, e1_Password: encrypted });
      if (this.sync.user) {
        this._menuServiceProvider.onlineLogin(this.login.username, encrypted).subscribe(
          r => {
            var that = this;
            this.userLoginData = r.data;
            var code = r.message.code;
            var userId = r.data.WovV_User_MsT[0].user_Login_ID;
            var password = r.data.WovV_User_MsT[0].user_Password;
            if (code == 6002) {
              that._menuServiceProvider.userAvailable(userId, password).subscribe(
                result => {
                  if (result.message.type == "SUCCESS") {
                    let toast = that.toastCtrl.create({
                      message: 'Other User Logged Out!',
                      duration: 3000,
                      position: 'top'
                    });
                    toast.present();
                    that.saveToken(result);
                  }
                  else {
                    let toast = that.toastCtrl.create({
                      message: 'Something went wrong!',
                      duration: 3000,
                      position: 'top'
                    });
                    toast.present();
                  }
                });
            }
            else {
              this.saveToken(r);
            }
          }
        );

        if (this.sync.LogData) {
          if (this.sync.LogData.length > 0) {
            for (let logIndex = 0; logIndex < this.sync.LogData.length; logIndex++) {
              if (this.sync.LogData[logIndex]['Status'] == "Uploaded") {
                var checkDate = moment().subtract(this.sync.numberOfDaysPreviousLogData, 'days').format("DD/MM/YYYY");
                if (checkDate <= this.sync.LogData[logIndex]['Date']) {
                  this.sync.transactionInvoices = _.without(this.sync.transactionInvoices, _.findWhere(this.sync.transactionInvoices, { oh_DocNo: this.sync.LogData[logIndex]['DocNumber'], oh_Doc_Series_Prefix_Val: this.sync.LogData[logIndex]['InvoiceType'] }));
                  this.sync.LogData[logIndex]['Status'] = "Deleted";
                }
              }
            }
          }
          this.storage.set("LogInvoices", this.sync.LogData);
          this.storage.set("invoices", this.sync.transactionInvoices);
        }

        this.sync.isOnline = true;
        localStorage.setItem('isLoggedIn', "yes");
        this.storage.set('isLoggedIn', "yes");
        var that = this;
        // this.wovvsystem = JSON.parse(localStorage.getItem('WovvSystem'));
        this.wovvsystem = this.sync.wovvSystem.filter(function (number) {
          return number['user_Login_ID'] == that.sync.user.user_Login_ID;
        });
        localStorage.setItem("currentUser", JSON.stringify(this.wovvsystem[0]));
        this.UserID = this.wovvsystem[0]['user_Id'];
        localStorage.setItem('UserID', this.UserID);
        this.storage.set('UserID', this.UserID);

        this.navCtrl.setRoot('DynamicmenuPage');
      }
      else {
        alert("Invalid Username or password");
      }
    }
  }
  saveToken(result) {
    localStorage.setItem('onlineToken', result.data.WovV_User_MsT[0].access_token);
    this.storage.set('onlineToken', result.data.WovV_User_MsT[0].access_token);
    this.sync.GetLocalData();
  }
}
