import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoginserviceProvider } from "../../providers/loginservice/loginservice";
import { SyncProvider } from "../../providers/sync/sync";
import { LoginPage } from '../login/login';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  public username: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public _loginservice: LoginserviceProvider, public toastCtrl: ToastController, public sync: SyncProvider, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.username = currentUser.party_Name;
  }
  logout() {
    localStorage.setItem('isLoggedIn', 'no');
    localStorage.removeItem('UserID');
    localStorage.removeItem('onlineToken');
    this.viewCtrl.dismiss();
    let toast = this.toastCtrl.create({
      message: 'Logout Sucessfully!',
      duration: 3000,
      position:'top'
    });
    toast.present();
    this.navCtrl.setRoot('LoginPage');
  }
  logoutClose(){
    this.viewCtrl.dismiss();
  }
}
