import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { mSales } from './salesinvoice';
import { DevExtremeModule } from 'devextreme-angular';

@NgModule({
  declarations: [
    mSales,
  ],
  imports: [
    DevExtremeModule,
    IonicPageModule.forChild(mSales),
  ],
})
export class SalesinvoicePageModule { }
